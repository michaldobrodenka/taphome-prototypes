﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TapHome.Prototypes.Wpf.CommonUI;
using TapHome.Prototypes.Wpf.CommonUI.Elements;

namespace TapHome.Prototypes.Wpf
{
    public class CommonUITest2 : CommonUIPage2
    {
        private List<string> timeZones = new List<string>()
        {
            "Africa/Bangui",
            "Africa/Cairo",
            "Africa/Casablanca",
            "Africa/Harare",
            "Africa/Johannesburg",
            "Africa/Lagos",
            "Africa/Monrovia",
            "Africa/Nairobi",
            "Africa/Windhoek",
            "America/Anchorage",
            "America/Argentina",
            "America/Asuncion",
            "America/Bahia",
            "America/Bogota",
            "America/Buenos_Aires",
            "America/Caracas",
            "America/Cayenne",
            "America/Chicago",
            "America/Chihuahua",
            "America/Cuiaba",
            "America/Denver",
            "America/Fortaleza",
            "America/Godthab",
            "America/Guatemala",
            "America/Halifax",
            "America/Indianapolis",
            "America/Indiana",
            "America/La_Paz",
            "America/Los_Angeles",
            "America/Mexico_City",
            "America/Montevideo",
            "America/New_York",
            "America/Noronha",
            "America/Phoenix",
            "America/Regina",
            "America/Santa_Isabel",
            "America/Santiago",
            "America/Sao_Paulo",
            "America/St_Johns",
            "America/Tijuana",
            "Antarctica/McMurdo",
            "Atlantic/South_Georgia",
            "Asia/Almaty",
            "Asia/Amman",
            "Asia/Baghdad",
            "Asia/Baku",
            "Asia/Bangkok",
            "Asia/Beirut",
            "Asia/Calcutta",
            "Asia/Colombo",
            "Asia/Damascus",
            "Asia/Dhaka",
            "Asia/Dubai",
            "Asia/Irkutsk",
            "Asia/Jerusalem",
            "Asia/Kabul",
            "Asia/Kamchatka",
            "Asia/Karachi",
            "Asia/Katmandu",
            "Asia/Kolkata",
            "Asia/Krasnoyarsk",
            "Asia/Kuala_Lumpur",
            "Asia/Kuwait",
            "Asia/Magadan",
            "Asia/Muscat",
            "Asia/Novosibirsk",
            "Asia/Oral",
            "Asia/Rangoon",
            "Asia/Riyadh",
            "Asia/Seoul",
            "Asia/Shanghai",
            "Asia/Singapore",
            "Asia/Taipei",
            "Asia/Tashkent",
            "Asia/Tbilisi",
            "Asia/Tehran",
            "Asia/Tokyo",
            "Asia/Ulaanbaatar",
            "Asia/Vladivostok",
            "Asia/Yakutsk",
            "Asia/Yekaterinburg",
            "Asia/Yerevan",
            "Atlantic/Azores",
            "Atlantic/Cape_Verde",
            "Atlantic/Reykjavik",
            "Australia/Adelaide",
            "Australia/Brisbane",
            "Australia/Darwin",
            "Australia/Hobart",
            "Australia/Perth",
            "Australia/Sydney",
            "Europe/Amsterdam",
            "Europe/Athens",
            "Europe/Belgrade",
            "Europe/Berlin",
            "Europe/Brussels",
            "Europe/Budapest",
            "Europe/Dublin",
            "Europe/Helsinki",
            "Europe/Istanbul",
            "Europe/Kiev",
            "Europe/London",
            "Europe/Minsk",
            "Europe/Moscow",
            "Europe/Paris",
            "Europe/Sarajevo",
            "Europe/Warsaw",
            "Indian/Mauritius",
            "Pacific/Apia",
            "Pacific/Auckland",
            "Pacific/Fiji",
            "Pacific/Guadalcanal",
            "Pacific/Guam",
            "Pacific/Honolulu",
            "Pacific/Pago_Pago",
            "Pacific/Port_Moresby",
            "Pacific/Tongatapu"
        };

        public CommonUITest2(Dictionary<string, object> parameters) : base(String.Empty, parameters)
        {
            var section = new CommonSection2(this, "very long section name Test section");
            AddSection(section);

            var slatTypes = new List<string>()
            {
                "90", "180"
            };

            var deviceValues = new List<string>()
            {
                "Disabled", "Reed Contact", "Push Button",
            };

            var deviceRadioRootElement = new DeviceRadioRootElement2(null, "DI1", "Name1", deviceValues, 1, true);
            section.Add(deviceRadioRootElement);
            deviceValues.Add("Impulse Counter");
            var deviceRadioRootElement2 = new DeviceRadioRootElement2(null, "DI2", "Name2", deviceValues, 1, true);

            section.Add(deviceRadioRootElement2);

            var equationElement = new EquationElement2(null, "1+2+3=6");
            section.Add(equationElement);
            var rolesElement = new RolesElement2(null, "Roles", Client.Desktop.CommonUI.Dependencies.UserPermission.Admin,new List<string> { "User1","Admin1","Service1"},new bool[] {true, false, false });
            section.Add(rolesElement);
            var radioElement = new RadioRootElement2(null, "Type of slats", slatTypes, 1, true);
            section.Add(radioElement);
            var button1 = new CommonButton2(null, "Default", CommonButton2.Style.Default);
            button1.Clicked += (s,e) => { MessageBox.Show("Button Clicked"); };
            section.Add(button1);
            var button2 = new CommonButton2(null, "Delete", CommonButton2.Style.Delete);
            section.Add(button2);
            var radioElement2 = new RadioRootElement2(null, "Time zone", this.timeZones, 5, true);
            section.Add(radioElement2);
            var stringElement = new StringElement2(this, "String element", "Value", "Title", "message", true, false) { Enabled = true };
            stringElement.Clicked += (s, e) => {
            };
            section.Add(stringElement);
            var stringElementWmenu = new StringElementWithMenu2(this, "String element", "Value", "Title", "message", true, false) { Enabled = true };
            section.Add(stringElementWmenu);
            var dateElement = new DateElement2(this, "Date element ") { Enabled = true };
            dateElement.MinimumDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(60));
            dateElement.MaximumDate = DateTime.UtcNow.Add(TimeSpan.FromDays(60));
            dateElement.SelectedDate = DateTime.UtcNow;
            section.Add(dateElement);
            var sliderElement = new SliderElement2(this, "Slider  element", 0, true) { Enabled = true, MaxValue = 100, ValueSelected = 0 };
            sliderElement.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "ValueSelected")
                {
                    sliderElement.ValueText = String.Format("Slider value: {0}", (s as SliderElement2).ValueSelected.ToString());
                }
            };
            section.Add(sliderElement);
            var rangeSliderElement = new RangeSliderElement2(this, "Slider  element", "ss", 0, true) { Enabled = true, MaxValue = 100, MinValueSelected = 0, MaxValueSelected = 5, MinValue = 0 };
            rangeSliderElement.PropertyChanged += (s, e) =>
            {
                rangeSliderElement.LeftText = String.Format("Slider value: {0}", (s as RangeSliderElement2).MinValueSelected.ToString());
                rangeSliderElement.RightText = String.Format("Slider value: {0}", (s as RangeSliderElement2).MaxValueSelected.ToString());
            };
            rangeSliderElement.MaxValueSelected = 0;
            section.Add(rangeSliderElement);

            for (int i = 0; i < 10; i++)
            {
                //var baseElement = new ElementBase(this, "Base element");
                //section.Add(baseElement);

                var labelElement = new LabelElement2(this, "Label element " + i, "Value") { Enabled = i % 2 == 0 };
                section.Add(labelElement);

                var labelElementWithDescription = new LabelElement2(this, "Label element with desc " + i, "Value", "Description");
                section.Add(labelElementWithDescription);

                var booleanElement = new BooleanElement2(this, "Boolean element " + i, true) { Enabled = i % 2 == 0 };
                section.Add(booleanElement);

                //var radioRootElement = new RadioRootElement(this, "Radio root element", timeZones, 0, true);
                //section.Add(radioRootElement);
            }

        }


    }
}
