﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace TapHome.Prototypes.Wpf.Dialogs.ViewModels
{
    public class StringDialogViewModel : INotifyPropertyChanged
    {
        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                this.title = value;
                OnPropertyChanged();
            }
        }

        private string value;
        public string Value
        {
            get { return value; }
            set
            {
                this.value = value;
                OnPropertyChanged();
            }
        }

        public ICommand OkCommand
        {
            get
            {
                return new RelayCommand(o => ((StringDialog)o).Close(Value));
            }
        }
        public ICommand CancelCommand
        {
            get
            {
                return new RelayCommand(o => ((StringDialog)o).Close(null));
            }
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
