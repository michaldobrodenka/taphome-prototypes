﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace TapHome.Prototypes.Wpf.Dialogs.ViewModels
{
    // todo: GK - move
    public class RadioButtonViewModel
    {
        public object Tag { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class RadioDialogViewModel : INotifyPropertyChanged
    {
        public string Caption { get; set; }
        public string[] Values { get; set; }
        public int SelectedIndex { get; set; }

        private bool showThirdButton;
        public bool ShowThirdButton
        {
            get { return showThirdButton; }
            set
            {
                if (showThirdButton == value)
                    return;

                showThirdButton = value;
                OnPropertyChanged();
            }
        }

        public Action ThirdButtonAction { get; set; }
        public ObservableCollection<RadioButtonViewModel> RadioButtons { get; set; }

        public ICommand OkCommand
        {
            get
            {
                return new RelayCommand(o => ((RadioDialog)o).Close(SelectedIndex));
            }
        }
        public ICommand CancelCommand
        {
            get
            {
                return new RelayCommand(o => ((RadioDialog)o).Close(-1));
            }
        }
        // todo: GK
        public ICommand RenameCommand
        {
            get
            {
                return new RelayCommand(o => ((RadioDialog)o).Close(-1));
            }
        }

        public static ObservableCollection<RadioButtonViewModel> ValuesToObservableCollection(string[] values, int selectedIndex)
        {
            var observableCollection = new ObservableCollection<RadioButtonViewModel>();
            for (var i = 0; i < values.Length; i++)
            {
                var value = values[i];
                var isSelected = i == selectedIndex;
                var vm = new RadioButtonViewModel { Tag = i, Name = value, IsSelected = isSelected };

                observableCollection.Add(vm);
            }

            return observableCollection;
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
