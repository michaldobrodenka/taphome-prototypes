﻿using System;
using TapHome.Prototypes.Wpf.Dialogs.ViewModels;

namespace TapHome.Prototypes.Wpf.Dialogs
{
    public partial class RadioDialog : Dialog<Int32>
    {
        public class SelectedIndexChangedEventArgs : EventArgs
        {
            public int Selected { get; set; }
        }
        public event EventHandler<SelectedIndexChangedEventArgs> SelectedIndexChanged;

        public string Caption
        {
            get { return viewModel.Caption; }
            set { viewModel.Caption = value; }
        }

        public bool ShowThirdButton
        {
            get { return viewModel.ShowThirdButton; }
            set { viewModel.ShowThirdButton = value; }
        }

        private RadioDialogViewModel viewModel;

        public RadioDialog(string caption, string[] values, int selectedIndex, bool showThirdButton, Action thirdButtonAction)
        {
            this.viewModel = new RadioDialogViewModel
            {
                Caption = caption,
                Values = values,
                SelectedIndex = selectedIndex,
                ShowThirdButton = showThirdButton,
                ThirdButtonAction = thirdButtonAction,
                RadioButtons = RadioDialogViewModel.ValuesToObservableCollection(values, selectedIndex),
            };

            DataContext = viewModel;

            InitializeComponent();
        }

        public void RadioButtonList_SelectedValueChanged(object sender, EventArgs e)
        {
            this.SelectedIndexChanged?.Invoke(sender, new SelectedIndexChangedEventArgs { Selected = viewModel.SelectedIndex });
        }
    }
}
