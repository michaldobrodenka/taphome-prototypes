﻿using System.Windows;

namespace TapHome.Prototypes.Wpf.Dialogs
{
    public class Dialog<T> : Window
    {
        public T Result { get; set; }

        public void Close(T result)
        {
            Result = result;
            Close();
        }
    }
}
