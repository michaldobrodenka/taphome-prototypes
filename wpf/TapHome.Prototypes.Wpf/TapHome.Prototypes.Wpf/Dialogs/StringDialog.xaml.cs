﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TapHome.Prototypes.Wpf.Dialogs.ViewModels;

namespace TapHome.Prototypes.Wpf.Dialogs
{
    /// <summary>
    /// Interaction logic for StringDialog.xaml
    /// </summary>
    public partial class StringDialog : Dialog<string>
    {
        public StringDialog(string title, string initialValue = null)
        {
            DataContext = new StringDialogViewModel
            {
                Title = title,
                Value = initialValue,
            };

            InitializeComponent();
        }
    }
}
