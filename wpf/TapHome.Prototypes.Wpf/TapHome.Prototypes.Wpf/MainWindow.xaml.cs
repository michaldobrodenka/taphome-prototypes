﻿using System.Windows;
using TapHome.Commons.CommonFrames;
using TapHome.Prototypes.Wpf.CommonUI;

namespace TapHome.Prototypes.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var page2 = new CommonUITest2(null);

            //var frontEnd = new CommonUIPageWizardFrontend();
            var device = new Client.Desktop.CommonUI.Dependencies.Device() { SerialNumber = "NTC2DI2DO2-FFFF-FFFF-FFFF" };
            var moduleconfig = new TaphomeBusModuleConfigurationFrame(null, device);

            var frontEnd = new CommonUIPage2FrontEnd();
            frontEnd.mainPageItemsControl.ItemsSource = moduleconfig.Sections;
            this.Content = frontEnd;

            //var sectionElement = new SectionElements()
            //{
            //    ItemsSource = page2.Sections[0].Elements,
            //};
            //this.Content = sectionElement;


            //var host = new CommonUIHost();

            //var page = new CommonUITest(null);
            //host.Push(page);

            //Content = host;
        }
    }
}
