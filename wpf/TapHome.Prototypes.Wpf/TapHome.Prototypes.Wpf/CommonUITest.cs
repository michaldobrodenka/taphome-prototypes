﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TapHome.Prototypes.Wpf.CommonUI;
using TapHome.Prototypes.Wpf.CommonUI.Elements;

namespace TapHome.Prototypes.Wpf
{
    public class CommonUITest : CommonUIPage
    {
        private List<string> timeZones = new List<string>()
        {
            "Africa/Bangui",
            "Africa/Cairo",
            "Africa/Casablanca",
            "Africa/Harare",
            "Africa/Johannesburg",
            "Africa/Lagos",
            "Africa/Monrovia",
            "Africa/Nairobi",
            "Africa/Windhoek",
            "America/Anchorage",
            "America/Argentina",
            "America/Asuncion",
            "America/Bahia",
            "America/Bogota",
            "America/Buenos_Aires",
            "America/Caracas",
            "America/Cayenne",
            "America/Chicago",
            "America/Chihuahua",
            "America/Cuiaba",
            "America/Denver",
            "America/Fortaleza",
            "America/Godthab",
            "America/Guatemala",
            "America/Halifax",
            "America/Indianapolis",
            "America/Indiana",
            "America/La_Paz",
            "America/Los_Angeles",
            "America/Mexico_City",
            "America/Montevideo",
            "America/New_York",
            "America/Noronha",
            "America/Phoenix",
            "America/Regina",
            "America/Santa_Isabel",
            "America/Santiago",
            "America/Sao_Paulo",
            "America/St_Johns",
            "America/Tijuana",
            "Antarctica/McMurdo",
            "Atlantic/South_Georgia",
            "Asia/Almaty",
            "Asia/Amman",
            "Asia/Baghdad",
            "Asia/Baku",
            "Asia/Bangkok",
            "Asia/Beirut",
            "Asia/Calcutta",
            "Asia/Colombo",
            "Asia/Damascus",
            "Asia/Dhaka",
            "Asia/Dubai",
            "Asia/Irkutsk",
            "Asia/Jerusalem",
            "Asia/Kabul",
            "Asia/Kamchatka",
            "Asia/Karachi",
            "Asia/Katmandu",
            "Asia/Kolkata",
            "Asia/Krasnoyarsk",
            "Asia/Kuala_Lumpur",
            "Asia/Kuwait",
            "Asia/Magadan",
            "Asia/Muscat",
            "Asia/Novosibirsk",
            "Asia/Oral",
            "Asia/Rangoon",
            "Asia/Riyadh",
            "Asia/Seoul",
            "Asia/Shanghai",
            "Asia/Singapore",
            "Asia/Taipei",
            "Asia/Tashkent",
            "Asia/Tbilisi",
            "Asia/Tehran",
            "Asia/Tokyo",
            "Asia/Ulaanbaatar",
            "Asia/Vladivostok",
            "Asia/Yakutsk",
            "Asia/Yekaterinburg",
            "Asia/Yerevan",
            "Atlantic/Azores",
            "Atlantic/Cape_Verde",
            "Atlantic/Reykjavik",
            "Australia/Adelaide",
            "Australia/Brisbane",
            "Australia/Darwin",
            "Australia/Hobart",
            "Australia/Perth",
            "Australia/Sydney",
            "Europe/Amsterdam",
            "Europe/Athens",
            "Europe/Belgrade",
            "Europe/Berlin",
            "Europe/Brussels",
            "Europe/Budapest",
            "Europe/Dublin",
            "Europe/Helsinki",
            "Europe/Istanbul",
            "Europe/Kiev",
            "Europe/London",
            "Europe/Minsk",
            "Europe/Moscow",
            "Europe/Paris",
            "Europe/Sarajevo",
            "Europe/Warsaw",
            "Indian/Mauritius",
            "Pacific/Apia",
            "Pacific/Auckland",
            "Pacific/Fiji",
            "Pacific/Guadalcanal",
            "Pacific/Guam",
            "Pacific/Honolulu",
            "Pacific/Pago_Pago",
            "Pacific/Port_Moresby",
            "Pacific/Tongatapu"
        };

        public CommonUITest(Dictionary<string, object> parameters) : base(String.Empty, parameters)
        {
            var section = new CommonSection(this, "Test section");
            AddSection(section);
            //var radioElement = new RadioRootElement(this, "Time zone", this.timeZones, 5, true);
            //var radioElementShort = new RadioRootElement(this, "Small radio", new List<string>(new string[] { "first", "second" }), 0, true);

            //section.Add(radioElement);
            //section.Add(radioElementShort);

            for (int i = 0; i < 1; i++)
            {
                //var baseElement = new ElementBase(this, "Base element");
                //section.Add(baseElement);

                var labelElement = new LabelElement(this, "Label element", "Value");
                section.Add(labelElement);

                var labelElementWithDescription = new LabelElement(this, "Label element with desc", "Value", "Description");
                section.Add(labelElementWithDescription);

                var radioRootElement = new RadioRootElement(this, "Radio root element", timeZones, 0, true);
                section.Add(radioRootElement);
            }

            //var variableElement = new VariableElement(new object(), new DeviceVariable { Abbr = "Me", ID = 3, Variable = "Mega" });
            //variableElement.VariableClick += (sender, args) => radioElementShort.Enabled = !radioElementShort.Enabled;
            //this.section.Add(variableElement);

            //var equationElement = new EquationElement(new object(), "IF(St > 25.5, 13.4, 45.5)");
            //this.section.Add(equationElement);

            //var stringElementWithMenu = new StringElementWithMenu(new object(), "StringElWithMenu", "mega", "totle", "Massage", true);
            //stringElementWithMenu.MenuClicked += (sender, args) => { equationElement.Equation = equationElement.Equation.Substring(0, equationElement.Equation.Length - 1); };
            //this.section.Add(stringElementWithMenu);
            
            var booleanElementWithMenu = new BooleanElementWithMenu(new object(), "BooleanElWithMenu", false, "super duper description", false);
            //booleanElementWithMenu.MenuClicked += (sender, args) =>
            //{
            //    var actionDialog = new ActionDialog(new SwitchAction
            //    {
            //        OutputDeviceIDs = { 0 },
            //        ActionType = SwitchAction.SwitchActionType.On,
            //        OneTimeOnly = false,
            //    });
            //    var result = actionDialog.ShowModal(ParentWindow);
            //};
            section.Add(booleanElementWithMenu);

            //var actionElement = new AddActionElement(new object(), BaseAction.GetDefaultAction("Switch"));
            //actionElement.ActionClick += (sender, args) =>
            //{
            //    var dialog = new ActionDialog(args.Action);
            //    dialog.ShowModal(ParentWindow);
            //    actionElement.Action = args.Action;
            //};
            //this.section.Add(actionElement);

            //var dateElement = new DateElement(new object(), "DateElement");
            //dateElement.MaximumDate = DateTime.UtcNow;
            //dateElement.MinimumDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(60));
            //dateElement.SelectedDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(9));
            //this.section.Add(dateElement);

            //for (int i = 0; i < 3; i++)
            //{
            //    var sliderElement = new SliderElement(new Context(), $"Slider {i}", i);
            //    sliderElement.MaxValue = (i + 1) * 100;
            //    sliderElement.ValueChanged += (sender, args) =>
            //    {
            //        variableElement.Value = sliderElement.Value;
            //    };
            //    this.section.Add(sliderElement);
            //}

            for (int i = 0; i < 3; i++)
            {
                var booleanElement = new BooleanElement(null, $"Boolean {i}", i % 2 == 0, i % 2 == 1 ? "asd" : "", i % 2 == 0);
                var b = i;
                booleanElement.ValueChanged += (sender, args) =>
                {
                    //section.Elements[b + 1].BackgroundColor = booleanElement.Value ? Color.FromRgb(0xff2244) : Color.FromRgb(0x123123);
                    //((BooleanElement)section.Elements.First(e => e is BooleanElement)).Enabled = booleanElement.Value;
                };
                section.Add(booleanElement);
            }

            //for (int i = 0; i < 3; i++)
            //{
            //    var addDeviceElement = new AddDeviceElement(new Context(), $"Add device {i}", AddDeviceElement.Behaviour.PickSingleDevice, "asdf");
            //    this.section.Add(addDeviceElement);
            //}

            for (int i = 0; i < 50; i++)
            {
                //this.section.Add(new Label() { Text = "Element "+i, Size = new Size(100, 50), BackgroundColor = Colors.AliceBlue });
                section.Add(new StringElement(this, "Element " + i, "value", "totle", "Massage", true));
            }

            //this.section.Add(new PasswordElement(this, "********", false));
            //this.section.Add(new RolesElement(this, "Roles", UserPermission.User, null, new[] { true, false, false }));
            //this.section.Add(new RolesElement(this, "Roles", UserPermission.User | UserPermission.Admin, null, new[] { true, true, false }));
            //this.section.Add(new RolesElement(this, "Roles", UserPermission.User | UserPermission.Service, null, new[] { true, false, true }));
            //this.section.Add(new RolesElement(this, "Roles", UserPermission.Admin | UserPermission.Service, null, new[] { false, true, true }));
            //this.section.Add(new RolesElement(this, "Roles", UserPermission.User | UserPermission.Admin | UserPermission.Service, null, new[] { true, true, true }));

            //Task.Run(() =>
            //{
            //    Thread.Sleep(100);

            //    Application.Instance.Invoke(() =>
            //    {
            //        Dictionary<string, object> parameters2 = new Dictionary<string, object>();
            //        parameters2.Add(TapHomeBusModuleConfigurationPageWithFrames.DeviceIdExtraKey, 20);
            //        parameters2.Add(TapHomeBusModuleConfigurationPageWithFrames.DeviceSerialExtraKey, "this.ccu.CcuId");

            //        //CommonUIPage.NavigateToPageWithParameters(this, typeof(CommonUITestPage2), parameters2);
            //        //CommonUIPage.NavigateToPageWithParameters(this, typeof(TapHomeBusModuleConfigurationPageWithFrames), parameters2);
            //        CommonUIPage.NavigateToPageWithParameters(this, typeof(SettingsPage), parameters2);
            //    });
            //});
        }

        public override void OnInit()
        {
            base.OnInit();

            var mainList = new CommonMainList(null);

            this.SetMainList(mainList);
        }

        public override void CreateControls()
        {

        }

        public override void OnBack()
        {
        }
    }
}
