﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class RadioRootElement2 : ElementBase2
    {
        public class RadioRootSelectedChangedEventArgs : EventArgs
        {
            public int Selected { get; set; }
            //public string Name { get; set; }
        }

        public event EventHandler<RadioRootSelectedChangedEventArgs> SelectedChanged;

        private int selectedValueIndex;
        public int SelectedValueIndex
        {
            get { return selectedValueIndex; }
            set
            {
                if (selectedValueIndex == value)
                    return;

                selectedValueIndex = value;
                NotifyPropertyChanged();

                this.SelectedChanged?.Invoke(this, new RadioRootSelectedChangedEventArgs() { Selected = value });
            }
        }

        public string Value
        {
            get
            {
                if (SelectedValueIndex > -1 && SelectedValueIndex < Values.Count)
                    return Values[SelectedValueIndex];
                else
                    return string.Empty;
            }
        }

        private string disabledDetailText;
        public string DisabledDetailText { get => disabledDetailText; set { disabledDetailText = value; NotifyPropertyChanged(); } }
        private List<string> values;
        public List<string> Values
        {
            get => values;
            set
            {
                values = value;
                NotifyPropertyChanged();
            }
        }

        public bool hasRemoveButton;

        protected RadioRootElement2(object context) : base(context, string.Empty, string.Empty)
        {
        }

        public RadioRootElement2(object context, string caption, List<string> values, int selectedValueIndex = 0, bool enabled = false, bool removeButton = false)
            : base(context, caption, string.Empty, enabled)
        {
            //Init(context, caption, enabled);

            this.hasRemoveButton = removeButton;

            DisabledDetailText = null;
            Values = values;
            SelectedValueIndex = selectedValueIndex;
        }
    }
}
