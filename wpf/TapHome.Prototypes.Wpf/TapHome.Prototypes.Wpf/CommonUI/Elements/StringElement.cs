﻿
using System;
using TapHome.Prototypes.Wpf.Dialogs;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class StringElement : LabelElement
    {
        public class TextViewHelperEventArgs : EventArgs
        {
            public string Name { get; set; }
        }

        public event EventHandler<TextViewHelperEventArgs> NameChanged;

        private string name;
        public new string Name
        {
            get { return name; }
            set
            {
                name = value;
                Value = value;
            }
        }

        private bool isEditable;
        public bool IsEditable
        {
            get { return this.isEditable; }
            set { this.isEditable = value; }
        }

        private bool showMoreIcon;
        public bool ShowMoreIcon
        {
            get { return this.showMoreIcon; }
            set { this.showMoreIcon = value; }
        }

        public event EventHandler<EventArgs> Clicked;

        public Action SetClickedEvent
        {
            set
            {
                Clicked += (sender, e) => { Console.WriteLine(); value(); };
            }
        }

        // todo: GK?
        //private ImageView imageView;

        private string value;
        private string message;
        private string image;

        public StringElement(object context, string caption, string value, string title = "", string message = "", bool enabled = false, bool isEditable = true, string description = "", string image = "", bool showMoreIcon = true)
            : base(context, caption, value, description, enabled)
        {
            this.Name = value;
            this.message = message;
            this.isEditable = isEditable;
            this.image = image;
            this.showMoreIcon = showMoreIcon;

            this.InitializeComponent();
        }
        void FireClicked()
        {
            this.Clicked?.Invoke(this, new EventArgs { });
        }
        
        private void ShowRenameDialog()
        {
            var dialog = new StringDialog(message, this.Name);
            dialog.ShowDialog();

            if (dialog.Result != null)
            {
                this.Name = dialog.Result;
                this.FireNameChanged();
            }
        }

        void FireNameChanged()
        {
            this.NameChanged?.Invoke(this, new TextViewHelperEventArgs { Name = this.Name });
        }
        
        protected override void ElementClick()
        {
            if (!Enabled)// && !(this is StringElementWithMenu))
                return;

            if (isEditable)
            {
                ShowRenameDialog();
            }
            else
            {
                FireClicked();
            }
        }
    }
}
