﻿using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class DeviceRadioRootElement2 : RadioRootElement2
    {
        public class DeviceNameChangedEventArgs : EventArgs
        {
            public int Selected { get; set; }
            public string Name { get; set; }
        }

        public event EventHandler<DeviceNameChangedEventArgs> DeviceNameChanged;

        private string generatedName;
        private string deviceName;
        private string originalName;
        private bool colapsed, showRename;
        private ICommand clickCommand;


        public string ShownName
        {
            get => Values[SelectedValueIndex];
        }
        public bool ShowMore { get; set; }
        public string Name
        {
            get => this.deviceName;
            set
            {
                if (this.deviceName != value)
                {
                    this.deviceName = value;
                    this.RefreshName(generatedName, value);
                    this.NotifyPropertyChanged();
                    FireDeviceNameChanged();
                }
            }
        }
        public bool Colapsed
        {
            get => this.colapsed;
            set
            {
                this.colapsed = value;
                this.NotifyPropertyChanged();
            }
        }
        public bool ShowRename
        {
            get => this.showRename;
            set
            {
                this.showRename = value;
                if (value == false)
                {
                    Name = String.Empty;
                }
                this.NotifyPropertyChanged();
            }
        }

        public ICommand ClickCommand
        {
            get => clickCommand ?? (clickCommand = new CommandHandler(() => ShowMoreToggle(), true));
        }

        public DeviceRadioRootElement2(object context, string caption, string name, List<string> values, int selectedValueIndex = 0, bool enabled = false)
            : base(context, caption, values, selectedValueIndex, enabled)
        {
            this.RefreshName(caption, name);

            ShowRename = selectedValueIndex == 0 ? false : true;
            Colapsed = values?.Count > 3 ? false : true;
            this.SelectedChanged += (s, e) => 
            {
                ShowRename = e.Selected == 0 ? false : true;
                NotifyPropertyChanged("ShownName");
            };
        }

        private void RefreshName(string caption, string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                Caption = caption;
            }
            else
            {
                Caption = caption + " (" + name + ")";
                this.originalName = name;
            }

            this.generatedName = caption;
            this.deviceName = name;
        }        

        private void FireDeviceNameChanged()
        {
            this.DeviceNameChanged?.Invoke(this, new DeviceNameChangedEventArgs { Selected = this.SelectedValueIndex, Name = this.deviceName });
        }

        private void ShowMoreToggle()
        {
            ShowMore = !ShowMore;
            NotifyPropertyChanged("ShowMore");
        }
    }
}
