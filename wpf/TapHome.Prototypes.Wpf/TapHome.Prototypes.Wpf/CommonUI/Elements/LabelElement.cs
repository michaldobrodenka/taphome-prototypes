﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class LabelElement : ElementBase
    {
        public string Value
        {
            get { return this.valueLabel.Text; }
            set { this.valueLabel.Text = value; }
        }

        public string Description
        {
            get { return descriptionLabel.Text; }
            set { descriptionLabel.Text = value; }
        }

        protected TextBlock valueLabel;

        protected TextBlock descriptionLabel;

        public LabelElement(object context, string caption, string value, string description = "", bool enabled = true)
            : base(context, caption, enabled)
        {
            CompleteLayout();

            this.Value = value;
            this.Description = description;
        }

        private void CompleteLayout()
        {
            this.valueLabel = new TextBlock();
            this.descriptionLabel = new TextBlock();

            this.firstRowLayout.Children.Add(valueLabel);
            this.secondRowLayout.Children.Add(this.descriptionLabel);
        }
    }
}
