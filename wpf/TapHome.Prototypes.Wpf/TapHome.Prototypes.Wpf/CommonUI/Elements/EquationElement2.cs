﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using TapHome.Translations;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class EquationElement2 : ElementBase2
    {
        public class EquationEventArgs : EventArgs
        {
            public string Equation { get; set; }
        }

        private string equation, newEquation;
        private bool renameMode = false;
        private ICommand clickCommand, saveClicked, cancelClicked;
        public string Equation
        {
            get { return this.equation; }
            set
            {
                if (this.equation != value)
                {
                    this.equation = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string NewEquation
        {
            get { return this.newEquation; }
            set
            {
                if (this.newEquation != value)
                {
                    this.newEquation = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string Footer
        {
            get
            {
                var footer = $"{i18n.smartrule_evaluation_formula_helper}\n{i18n.smartrule_evaluation_formula_helper2}\n{i18n.smartrule_evaluation_formula_helper3}";
                // i18n escapes \n
                return footer.Replace("\\n", "\n");
            }
        }
        public bool RenameMode
        {
            get { return this.renameMode; }
            set
            {
                if (this.renameMode != value)
                {
                    this.renameMode = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ICommand ClickCommand
        {
            get => clickCommand ?? (clickCommand = new CommandHandler(() => ShowRenameDialog(), true));
        }
        public ICommand SaveClicked
        {
            get => saveClicked ?? (saveClicked = new CommandHandler(() => SaveBtnClicked(), true));
        }
        public ICommand CancelClicked
        {
            get => cancelClicked ?? (cancelClicked = new CommandHandler(() => RenameMode = false, true));
        }
        public event EventHandler<EquationEventArgs> EquationChanged;

        public EquationElement2(object context, string caption) : base(context, String.Empty)
        {
            Equation = caption;
        }

        public void ShowRenameDialog()
        {
            RenameMode = true;
            NewEquation = Equation;
        }
        public void ShowRenameDialog(string equation)
        {
            RenameMode = true;
            NewEquation = equation;
        }
        private void SaveBtnClicked()
        {
            RenameMode = false;
            Equation = NewEquation;
        }
        private void FireEquationChanged()
        {
            EquationChanged?.Invoke(this, new EquationEventArgs { Equation = this.Equation });
        }
    }
}
