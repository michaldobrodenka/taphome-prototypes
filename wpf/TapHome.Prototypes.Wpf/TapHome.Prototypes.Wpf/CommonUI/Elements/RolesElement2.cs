﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using TapHome.Client.Desktop.CommonUI.Dependencies;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class RolesElement2 : ElementBase2
    {
        public class RolesElementSelectedChangedEventArgs : System.EventArgs
        {
            public bool[] Selected { get; set; }
        }

        private List<CheckBox> items = new List<CheckBox>()
        {
            new CheckBox() { IsChecked = false, Content = "User"},
            new CheckBox() { IsChecked = false, Content = "Admin"},
            new CheckBox() { IsChecked = false, Content = "Service" }
        };

        private UserPermission permissions;
        private List<string> values;
        private bool[] selectedIndexValues;
        private string text;
        private bool showComboBox;

        public List<CheckBox> Items
        {
            get { return this.items; }
            set
            {
                if (this.items != value)
                {
                    this.items = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        private bool isDropDownOpen;
        public bool IsDropDownOpen
        {
            get { return this.isDropDownOpen; }
            set
            {
                if (this.isDropDownOpen != value)
                {
                    this.isDropDownOpen = value;
                    if (this.isDropDownOpen == false)
                        OnClosing();
                }
            }
        }

        public string Text
        {
            get { return this.text; }
            set
            {
                this.text = value;
                this.NotifyPropertyChanged();
            }
        }

        public event EventHandler<RolesElementSelectedChangedEventArgs> SelectedChanged;
        public event EventHandler<EventArgs> Clicked;


        public RolesElement2(object context, string caption, UserPermission permissions) 
            : base(context, caption)
        {
            this.permissions = permissions;

            items[0].IsChecked = permissions.HasFlag(UserPermission.User);
            items[1].IsChecked = permissions.HasFlag(UserPermission.Admin);
            items[2].IsChecked = permissions.HasFlag(UserPermission.Service);
            InitCheckbox();
            UpdateValue();
        }
        public RolesElement2(object context, string caption, UserPermission permissions, List<string> values, bool[] selectedIndexValues)
            : base(context, caption)
        {
            this.permissions = permissions;
            this.values = values;
            this.selectedIndexValues = selectedIndexValues;

            items[0].IsChecked = selectedIndexValues[0];
            items[1].IsChecked = selectedIndexValues[1];
            items[2].IsChecked = selectedIndexValues[2];

            items[0].Content = values[0];
            items[1].Content = values[1];
            items[2].Content = values[2];

            InitCheckbox();
            UpdateValue();
        }

        private void InitCheckbox()
        {
            items[0].Unchecked += CheckBoxChanged;
            items[0].Checked += CheckBoxChanged;
            items[1].Unchecked += CheckBoxChanged;
            items[1].Checked += CheckBoxChanged;
            items[2].Unchecked += CheckBoxChanged;
            items[2].Checked += CheckBoxChanged;
        }

        private void CheckBoxChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            permissions = UserPermission.None;

            if ((bool)items[0].IsChecked)
                permissions |= UserPermission.User;

            if ((bool)items[1].IsChecked)
                permissions |= UserPermission.Admin;

            if ((bool)items[2].IsChecked)
                permissions |= UserPermission.Service;
        }

        private void UpdateValue()
        {
            Text = GetRolesString();
        }
        private string GetRolesString()
        {
            string value = string.Empty;

            if (permissions.HasFlag(UserPermission.User))
            {
                value += "U ";
            }
            if (permissions.HasFlag(UserPermission.Admin))
            {
                value += "A ";
            }
            if (permissions.HasFlag(UserPermission.Service))
            {
                value += "S ";
            }

            return value;
        }
        public void OnClosing()
        {
            if(selectedIndexValues[2] != permissions.HasFlag(UserPermission.Service))
            {
                MessageBoxResult result = MessageBox.Show("bla bla bla TODO: Translations", "Warning", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    this.selectedIndexValues[0] = permissions.HasFlag(UserPermission.User);
                    this.selectedIndexValues[1] = permissions.HasFlag(UserPermission.Admin);
                    this.selectedIndexValues[2] = permissions.HasFlag(UserPermission.Service);
                }
                else
                {
                    permissions = UserPermission.None;
                    if (this.selectedIndexValues[0])
                    {
                        permissions |= UserPermission.User;
                        items[0].IsChecked = true;
                    }
                    else
                        items[0].IsChecked = false;

                    if (this.selectedIndexValues[1])
                    {
                        permissions |= UserPermission.Admin;
                        items[1].IsChecked = true;
                    }
                    else
                        items[1].IsChecked = false;

                    if (this.selectedIndexValues[2])
                    {
                        permissions |= UserPermission.Service;
                        items[2].IsChecked = true;
                    }
                    else
                        items[2].IsChecked = false;
                }
            }
            else
            {
                this.selectedIndexValues[0] = permissions.HasFlag(UserPermission.User);
                this.selectedIndexValues[1] = permissions.HasFlag(UserPermission.Admin);
                this.selectedIndexValues[2] = permissions.HasFlag(UserPermission.Service);
            }

            UpdateValue();
            SelectedChanged?.Invoke(this, new RolesElementSelectedChangedEventArgs { Selected = this.selectedIndexValues });
        }
    }
}
