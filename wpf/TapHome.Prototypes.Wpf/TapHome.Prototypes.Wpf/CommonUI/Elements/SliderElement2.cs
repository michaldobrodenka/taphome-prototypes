﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class SliderElement2 : ElementBase2
    {
        private string valueText;
        private int maxValue;
        private int valueSelected;
        public string ValueText
        {
            get { return this.valueText; }
            set
            {
                if (this.valueText != value)
                {
                    this.valueText = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public int ValueSelected
        {
            get { return this.valueSelected; }
            set
            {
                if (this.valueSelected != value)
                {
                    this.valueSelected = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public int MaxValue
        {
            get { return this.maxValue; }
            set
            {
                if (this.maxValue != value)
                {
                    this.maxValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public int id;
        public SliderElement2(object context, string caption = "", int id = 0, bool enabled = false)
            : base(context, caption, enabled)
        {
            this.id = id;
        }
    }
}
