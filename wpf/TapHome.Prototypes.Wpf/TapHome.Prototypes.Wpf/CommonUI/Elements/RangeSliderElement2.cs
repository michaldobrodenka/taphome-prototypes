﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class RangeSliderElement2 : ElementBase2
    {
        private string leftText;
        private string rightText;
        private int minValue;
        private int maxValue;
        private int minValueSelected;
        private int maxValueSelected;
        public string LeftText
        {
            get { return this.leftText; }
            set
            {
                if (this.leftText != value)
                {
                    this.leftText = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public string RightText
        {
            get { return this.rightText; }
            set
            {
                if (this.rightText != value)
                {
                    this.rightText = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public int MinValueSelected
        {
            get { return this.minValueSelected; }
            set
            {
                if (this.minValueSelected != value)
                {
                    this.minValueSelected = value;
                    this.NotifyPropertyChanged();
                    if (this.MaxValueSelected < value)
                        this.MaxValueSelected = value;
                }
            }
        }
        public int MaxValueSelected
        {
            get { return this.maxValueSelected; }
            set
            {
                if (this.maxValueSelected != value)
                {
                    this.maxValueSelected = value;
                    this.NotifyPropertyChanged();
                    if (this.MinValueSelected > value)
                        this.MinValueSelected = value;
                }
            }
        }
        public int MinValue
        {
            get { return this.minValue; }
            set
            {
                if (this.minValue != value)
                {
                    this.minValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public int MaxValue
        {
            get { return this.maxValue; }
            set
            {
                if (this.maxValue != value)
                {
                    this.maxValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public int id;
        public RangeSliderElement2(object context, string leftTitle = "", string rightTitle = "", int id = 0, bool enabled = true)
            : base(context, leftTitle, enabled)
        {
            this.id = id;
        }
    }
}
