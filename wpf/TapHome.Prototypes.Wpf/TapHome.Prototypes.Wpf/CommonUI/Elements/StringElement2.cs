﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TapHome.Prototypes.Wpf.Dialogs;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class StringElement2 : ElementBase2
    {
        public class TextViewHelperEventArgs : EventArgs
        {
            public string Name { get; set; }
        }

        private string value, message, title;
        private string image;
        private bool showMoreIcon;
        private ICommand clickCommand;

        public event EventHandler<EventArgs> Clicked;
        public event EventHandler<TextViewHelperEventArgs> NameChanged;
        public string Value
        {
            get { return this.value; }
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    this.NotifyPropertyChanged();
                    this.NameChanged?.Invoke(this, new TextViewHelperEventArgs { Name = value });
                }
            }
        }
        public string Message
        {
            get { return this.message; }
            set
            {
                if (this.message != value)
                {
                    this.message = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public bool HasMessage
        {
            get => !String.IsNullOrEmpty(this.Message);
        }
        public string Title
        {
            get { return this.title; }
            set
            {
                if (this.title != value)
                {
                    this.title = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public bool HasTitle
        {
            get => !String.IsNullOrEmpty(this.Title);
        }
        private bool isEditable;
        public bool IsEditable
        {
            get { return this.isEditable; }
            set
            {
                if (this.isEditable != value)
                {
                    this.isEditable = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public bool ShowMoreIcon
        {
            get { return this.showMoreIcon; }
            set { this.showMoreIcon = value; }
        }

        public ICommand ClickCommand
        {
            get => clickCommand ?? (clickCommand = new CommandHandler(() => this.Clicked?.Invoke(this, new EventArgs { }), true));
        }

        public StringElement2(object context, string caption, string value, string title = "", string message = "", bool enabled = false, bool isEditable = true, string description = "", string image = "", bool showMoreIcon = true)
            : base(context, caption, description, enabled)
        {
            Value = value;
            Message = message;
            Title = title;
            IsEditable = isEditable;
            this.image = image;
            this.showMoreIcon = showMoreIcon;
        }
    }
}
