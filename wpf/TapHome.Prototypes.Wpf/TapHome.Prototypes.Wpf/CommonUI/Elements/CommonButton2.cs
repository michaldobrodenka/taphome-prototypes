﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class CommonButton2 : ElementBase2
    {
        public enum Style
        {
            Default = 0,
            Delete = 1,
        }

        public event EventHandler<EventArgs> Clicked;
        public event EventHandler<EventArgs> LongClickStart;
        public event EventHandler<EventArgs> LongClickStop;

        private string value;
        private Brush background;
        private ICommand clickCommand;
        public string Value
        {
            get { return this.value; }
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public Brush Background
        {
            get { return this.background; }
            set
            {
                if (this.background != value)
                {
                    this.background = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public ICommand ClickCommand
        {
            get => clickCommand ?? (clickCommand = new CommandHandler(() => this.Clicked?.Invoke(this, new EventArgs { }), true));
        }


        public CommonButton2(object context, string caption, Style style) : base (context, String.Empty)
        {
            Value = caption;
            if (style == Style.Delete)
                Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFFF0000");
            // TODO: longClickStart/Stop ??? 
        }
    }
}
