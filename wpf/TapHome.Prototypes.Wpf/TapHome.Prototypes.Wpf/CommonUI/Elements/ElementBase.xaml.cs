﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public abstract partial class ElementBase : UserControl
    {
        private bool enabled;

        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                this.enabled = value;
                foreach(var control in LogicalTreeHelper.GetChildren(this))
                {
                    if (control as UIElement != null)
                    {
                        (control as UIElement).IsEnabled = value;
                    }
                }
            }
        }

        public string Caption
        {
            get { return this.captionLabel.Content as string; }
            set { this.captionLabel.Content = value; }
        }

        public bool LineUpVisible
        {
            set
            {
                var border = this.mainBorder.BorderThickness;
                border.Top = value ? 1 : 0;
                this.mainBorder.BorderThickness = border;
            }
        }

        public bool LineDownVisible
        {
            set
            {
                var border = this.mainBorder.BorderThickness;
                border.Bottom = value ? 1 : 0;
                this.mainBorder.BorderThickness = border;
            }
        }

        protected object context;

        public ElementBase(object context, string caption, bool enabled = true)
        {
            InitializeComponent();
            this.Caption = caption;
            this.Enabled = enabled;

            this.MouseDown += ElementBase_MouseDown;
            //this.MouseDoubleClick += ElementBase_MouseDoubleClick;
        }

        public ElementBase(object context, string caption, string description, bool enabled = true)
        {
            InitializeComponent();
            this.Caption = caption;
            this.Enabled = enabled;

            this.MouseDown += ElementBase_MouseDown;
            //this.MouseDoubleClick += ElementBase_MouseDoubleClick;
        }

        //private void ElementBase_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    if (e.Handled)
        //        return;

        //    this.ElementClick();
        //}

        private void ElementBase_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled)
                return;

            if (this.enabled)
                this.ElementClick();
        }

        protected virtual void ElementClick() { }
    }
}
