﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class DateElement2 : ElementBase2
    {
        private DateTime selectedDate, minimumDate, maximumDate;
        public DateTime SelectedDate
        {
            get { return this.selectedDate; }
            set
            {
                if (this.selectedDate != value)
                {
                    this.selectedDate = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public DateTime MinimumDate
        {
            get { return this.minimumDate; }
            set
            {
                if (this.minimumDate != value)
                {
                    this.minimumDate = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public DateTime MaximumDate
        {
            get { return this.maximumDate; }
            set
            {
                if (this.maximumDate != value)
                {
                    this.maximumDate = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
        public DateElement2(object context, string caption)
            :base (context, caption)
        {

        }
    }
}
