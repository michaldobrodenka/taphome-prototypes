﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class BooleanElement : ElementBase
    {
        public event EventHandler ValueChanged;

        public bool Value
        {
            get { return this.checkBox.IsChecked.Value; }
            set { this.checkBox.IsChecked = value; }
        }

        protected CheckBox checkBox;

        public BooleanElement(object context, string caption, bool value, string description = "", bool enabled = true, string image = "")
            : base(context, caption, description, enabled)
        { 
            //this.image = image;

            this.CompleteLayout();
        }

        private void CompleteLayout()
        {
            this.checkBox = new CheckBox();

            this.firstRowLayout.Children.Add(checkBox);
            checkBox.Checked += CheckBox_Checked;
            checkBox.Unchecked += CheckBox_Checked;
        }

        protected override void ElementClick()
        {
            this.checkBox.IsChecked = !this.checkBox.IsChecked ?? false;
        }

        private void CheckBox_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            ValueChanged?.Invoke(this, null);
        }
    }
}
