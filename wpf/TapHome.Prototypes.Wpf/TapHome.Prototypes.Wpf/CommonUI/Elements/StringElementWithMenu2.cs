﻿using System;
using System.Windows.Input;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class StringElementWithMenu2 : StringElement2
    {
        public EventHandler<EventArgs> MenuClicked;
        private ICommand menuclickCommand;
        public ICommand MenuClickCommand
        {
            get => menuclickCommand ?? (menuclickCommand = new CommandHandler(() => this.MenuClicked?.Invoke(this, new EventArgs { }), true));
        }
        public StringElementWithMenu2(object context, string caption, string value, string title = "", string message = "", bool enabled = false, bool isEditable = true, string description = "", string image = "", bool showMoreIcon = true)
            : base(context, caption, value, title, message, enabled, isEditable, description, image, showMoreIcon)
        {

        }

    }
}
