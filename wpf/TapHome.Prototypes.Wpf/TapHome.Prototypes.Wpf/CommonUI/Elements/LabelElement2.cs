﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class LabelElement2 : ElementBase2
    {
        private string value;

        public string Value
        {
            get { return this.value; }
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        public LabelElement2(object context, string caption, string value, string description = "", bool enabled = true)
            : base(context, caption, description, enabled)
        {
            this.value = value;
        }
    }
}
