﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class DoRadioElement2 : RadioRootElement2
    {
        private string relayName;
        public int SelectedRelayNumber
        {
            get
            {
                return this.outputValueIndices[this.SelectedValueIndex];
            }
            set
            {
                KeyValuePair<int, int> selectedKv = this.outputValueIndices.FirstOrDefault(kv => kv.Value == value);

                if (!selectedKv.Equals(default(KeyValuePair<int, int>)))
                {
                    this.SelectedValueIndex = selectedKv.Key;
                }
                else
                {
                    this.SelectedValueIndex = 0; // none 
                }
            }
        }

        Dictionary<int, int> outputValueIndices; // index, relay number
        public DoRadioElement2(object context, string caption, List<int> relays, List<string> values, string relayName)
            : base(context, caption, values)
        {
            this.relayName = relayName;
            this.outputValueIndices = new Dictionary<int, int>();
            this.ReplaceRelays(relays);
        }

        public void ReplaceRelays(List<int> newRelays)
        {
            this.outputValueIndices.Clear();

            var stringValues = GenerateRelayValues(newRelays);

            int newSelectedIndex = 0;

            if (this.SelectedValueIndex >= 0 && this.Values != null)
                newSelectedIndex = stringValues.IndexOf(this.Values[this.SelectedValueIndex]);

            if (newSelectedIndex == -1)
                newSelectedIndex = 0;

            this.Values = stringValues;
            this.SelectedValueIndex = newSelectedIndex;
        }

        private List<string> GenerateRelayValues(IList<int> relays)
        {
            var stringValues = new List<string>();
            stringValues.Add("None");
            this.outputValueIndices.Add(0, 0);
            for (int i = 1; i < relays.Count + 1; i++)
            {
                var relayNumber = relays[i - 1];
                stringValues.Add(String.Format("{0}{1}", relayName, relayNumber));
                this.outputValueIndices.Add(i, relayNumber);
            }

            return stringValues;
        }
    }
}
