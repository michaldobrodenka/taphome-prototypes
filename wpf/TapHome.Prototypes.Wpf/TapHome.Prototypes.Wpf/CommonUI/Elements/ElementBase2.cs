﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class ElementBase2 : INotifyPropertyChanged
    {
        private string caption;

        public string Caption
        {
            get { return this.caption; }
            set
            {
                if (this.caption != value)
                {
                    caption = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool enabled;

        public bool Enabled
        {
            get => enabled;
            set
            {
                if (enabled != value)
                {
                    this.enabled = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string description;

        public string Description
        {
            get => description;
            set
            {
                if (this.description != value)
                {
                    description = value;
                    this.NotifyPropertyChanged("HasDescription");
                    this.NotifyPropertyChanged();
                }
            }
        }

        public bool HasDescription
        {
            get
            {
                return !String.IsNullOrEmpty(this.Description);
            }
        }

        public ElementBase2(object context, string caption, bool enabled = true)
        {
            this.Enabled = enabled;
            this.Caption = caption;
        }

        public ElementBase2(object context, string caption, string description, bool enabled = true)
        {
            this.Caption = caption;
            this.Enabled = enabled;
            this.Description = description;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
