﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class BooleanElement2 : ElementBase2
    {
        private bool value;
        public event EventHandler ValueChanged;

        public bool Value
        {
            get => value;
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    this.NotifyPropertyChanged();
                    ValueChanged?.Invoke(this, null);
                }
            }
        }
        public BooleanElement2(object context, string caption, bool value, string description = "", bool enabled = true, string image = "")
            : base(context, caption, description, enabled)
        {
            this.Value = value;
        }
    }
}
