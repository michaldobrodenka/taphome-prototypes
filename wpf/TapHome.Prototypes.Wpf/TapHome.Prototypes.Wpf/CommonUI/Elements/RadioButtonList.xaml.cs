﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    /// <summary>
    /// Interaction logic for RadioButtonList.xaml
    /// </summary>
    public partial class RadioButtonList : ItemsControl, INotifyPropertyChanged
    {
        private string group1 { get; }

        private List<RadioButton> radioButtons = new List<RadioButton>();

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); /*this.RefreshIcon();*/ }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        //public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register("SelectedIndex", typeof(int), typeof(RadioButtonList), new PropertyMetadata(-1, ValueChanged));
        public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register("SelectedIndex", typeof(int), typeof(RadioButtonList), new FrameworkPropertyMetadata(-1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ValueChanged));
        

        public event PropertyChangedEventHandler PropertyChanged;

        public RadioButtonList()
        {
            this.group1 = Guid.NewGuid().ToString();

            InitializeComponent();
            //this.ItemTemplateSelector = new RadioButtonTemplateSelector();
        }

        private static void ValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var radioButtonList = sender as RadioButtonList;

            radioButtonList.SetSelectedIndex((int)e.NewValue);
            radioButtonList.NotifyPropertyChanged("SelectedIndex");
        }

        public void SetSelectedIndex(int index)
        {
            if (this.SelectedIndex != index)
            {
                this.SelectedIndex = index;
            }

            if (index >= 0)
            {
                foreach (var radioButton in this.radioButtons)
                {
                    if ((int)radioButton.Tag == index && radioButton.IsChecked == false)
                    {
                        radioButton.IsChecked = true;
                    }
                }
            }
            else
            {
                foreach (var radioButton in this.radioButtons)
                {
                    radioButton.IsChecked = false;
                }
            }
        }

        private void radioButton_Loaded(object sender, RoutedEventArgs e)
        {
            var radioButton = (RadioButton)sender;
            radioButton.GroupName = this.group1;
            radioButton.Tag = (this.ItemsSource as List<string>).IndexOf(radioButton.Content as string);
            this.radioButtons.Add(radioButton);

            if (this.SelectedIndex != -1 && (int)radioButton.Tag == this.SelectedIndex)
            {
                radioButton.IsChecked = true;
            }
        }

        private void radioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.SelectedIndex = (int)(sender as RadioButton).Tag;
        }

        public void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    //public class RadioButtonTemplateSelector : DataTemplateSelector
    //{
    //    public override DataTemplate SelectTemplate(object item, DependencyObject container)
    //    {
    //        FrameworkElement elemnt = container as FrameworkElement;

    //        var template= elemnt.FindResource("RadioElementTemplate") as DataTemplate;

    //        //var radioButton = template.FindName("radioButton", this);
    //        return template;
    //    }
    //}

}
