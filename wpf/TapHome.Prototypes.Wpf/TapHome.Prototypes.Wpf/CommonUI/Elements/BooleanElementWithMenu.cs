﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class BooleanElementWithMenu : BooleanElement
    {
        public EventHandler MenuClicked;

        private Button menuButton;

        public BooleanElementWithMenu(object context, string caption, bool value, string description = "", bool enabled = true, string image = "")
            : base(context, caption, value, description, enabled, image)
        {
            this.CompleteLayout();
        }

        private void CompleteLayout()
        {
            this.menuButton = new Button() { Content = "|||" };
            this.firstRowLayout.Children.Add(this.menuButton);

            menuButton.Click += MenuButton_Click;
            //menuButton.MouseDoubleClick += MenuButton_MouseDoubleClick;
        }

        //private void MenuButton_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    e.Handled = true;
        //}

        private void MenuButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            e.Handled = true;
            MenuClicked?.Invoke(sender, e);

        }
    }
}
