﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TapHome.Translations;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class PasswordElement2 : ElementBase2
    {
        public class ChangePasswordEventArgs : EventArgs
        {
            public string Password { get; set; }
        }

        public class GetPasswordEventArgs : EventArgs
        {
            public string OldPassword { get; set; }
            public string NewPassword { get; set; }
        }

        private bool numericInput, hiddenPassword, showChangePassword, showOldPassword;
        private string value, caption;
        private ICommand changePasswordClicked, saveClicked;


        public string Password
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }
        public string OldPasscode { get; set; }
        public bool ShowChangePassword
        {
            get => showChangePassword;
            set { showChangePassword = value; NotifyPropertyChanged(); }
        }
        public bool ShowOldPassword
        {
            get => showOldPassword;
            set
            {
                if (showOldPassword != value)
                {
                    showOldPassword = value;
                    NotifyPropertyChanged();
                }
            }
        }
        
        public ICommand ChangePasswordClicked
        {
            get => changePasswordClicked ?? (changePasswordClicked = new CommandHandler(() => ShowChangePassword = !ShowChangePassword, true));
        }
        public ICommand SaveClicked
        {
            get => saveClicked ?? (saveClicked = new CommandHandler(param => Clicked(param), true));
        }
        public event EventHandler<ChangePasswordEventArgs> PasswordChanged;
        public event EventHandler<GetPasswordEventArgs> PasswordGet;

        public PasswordElement2(object context, string password, bool numericInput = true, string caption = null, bool showHiddenPassword = false)
			: base(context, "Change password")
        {
            Password = password;
            this.numericInput = numericInput;
            this.caption = caption;
            this.hiddenPassword = showHiddenPassword;

            if (String.IsNullOrEmpty(Password))
                ShowOldPassword = false;
            else
                ShowOldPassword = true;
        }

        private void ShowChangePasswordClicked()
        {
            ShowChangePassword = !ShowChangePassword;
        }
        private void Clicked(object element)
        {
            var childrens = (element as StackPanel).Children;
            var passList = new List<string>();

            foreach (var children in childrens)
            {
                var passwordBox = children as PasswordBox;
                if (passwordBox != null)
                {
                    passList.Add(passwordBox.Password);
                    passwordBox.Password = string.Empty;
                }
            }
            OldPasscode = passList[0];

            if (numericInput)
            {
                if (String.IsNullOrEmpty(Password) || OldPasscode == Password)
                {
                    if (passList[1] != passList[2])
                    {
                        MessageBox.Show(i18n.smartrule_phasealarm_passcode_mismatch);
                        return;
                    }
                    else if (OldPasscode != passList[1])
                    {
                        Password = passList[1];
                        FirePasswordChanged();
                    }
                }
                else
                    MessageBox.Show(i18n.smartrule_phasealarm_passcode_wrong);
            }
            else
            {
                if (passList[1] != passList[2])
                {
                    MessageBox.Show(i18n.smartrule_phasealarm_passcode_mismatch);
                    return;
                }
                else if (OldPasscode != passList[1])
                {
                    Password = passList[1];
                    FireGetPassword();
                }
            }
            ShowChangePassword = false;
        }

        private void FireGetPassword()
        {
            this.PasswordGet?.Invoke(this, new GetPasswordEventArgs { NewPassword = this.Password, OldPassword = this.OldPasscode });
        }

        private void FirePasswordChanged()
        {
            this.PasswordChanged?.Invoke(this, new ChangePasswordEventArgs { Password = this.Password });
        }
    }
}
