﻿using System;
using System.Collections.Generic;
using TapHome.Prototypes.Wpf.Dialogs;

namespace TapHome.Prototypes.Wpf.CommonUI.Elements
{
    public class RadioRootElement : LabelElement
    {
        public class RadioRootSelectedChangedEventArgs : EventArgs
        {
            public int Selected { get; set; }
            //public string Name { get; set; }
        }

        public event EventHandler<RadioRootSelectedChangedEventArgs> SelectedChanged;

        public event EventHandler<EventArgs> RemoveClicked;

        public int SelectedValueIndex
        {
            get { return selectedValueIndex; }
            set
            {
                selectedValueIndex = value;
                Value = values[SelectedValueIndex];
            }
        }

        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        public string DisabledDetailText { get; set; }

        protected bool removeButton;
        protected List<string> values;
        private int selectedValueIndex;
        private bool enabled;

        protected RadioRootElement(object context) : base(context, string.Empty, string.Empty)
        {
        }

        public RadioRootElement(object context, string caption, List<string> values, int selectedValueIndex = 0, bool enabled = false, bool removeButton = false)
            : base(context, caption, string.Empty)
        {
            this.InitializeComponent();

            this.Caption = caption;
            this.Enabled = enabled;
            this.Value = values[selectedValueIndex];
            this.values = values;
            this.selectedValueIndex = selectedValueIndex;

            this.removeButton = removeButton;
        }

        public void ReplaceValues(List<string> newItems)
        {
            int newSelectedIndex = 0;

            if (this.selectedValueIndex >= 0 && this.values != null)
                newSelectedIndex = newItems.IndexOf(this.values[this.selectedValueIndex]);

            if (newSelectedIndex == -1)
                newSelectedIndex = 0;
            
            this.SelectedValueIndex = newSelectedIndex;
        }

        protected override void ElementClick()
        {
            OnClick();
        }

        public void OnClick(CommonUIPage page = null)
        {
            if (!this.enabled)
                return;

            int checkedItemIndex = this.selectedValueIndex;

            ShowRadioDialog(checkedItemIndex);
        }

        protected virtual void ShowRadioDialog(int checkedItemIndex)
        {
            Action thirdButtonAction = null;
            if (removeButton)
                thirdButtonAction = FireRemoveClicked;

            var dialog = new RadioDialog(this.Caption, this.values.ToArray(), this.selectedValueIndex, removeButton, thirdButtonAction);

            //dialog.ShowDialog();
            dialog.ShowDialog();

            if (dialog.Result >= 0)
            {
                this.SelectedValueIndex = dialog.Result;
                this.FireSelectedChanged();
            }
        }

        void FireSelectedChanged()
        {
            SelectedChanged?.Invoke(this, new RadioRootSelectedChangedEventArgs { Selected = this.SelectedValueIndex/*, Name = this.deviceName*/ });
        }

        void FireRemoveClicked()
        {
            this.RemoveClicked?.Invoke(this, new EventArgs { });
        }
    }
}
