﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TapHome.Prototypes.Wpf.CommonUI
{
    /// <summary>
    /// Interaction logic for CommonUIPage2.xaml
    /// </summary>
    public partial class CommonUIPage2FrontEnd : UserControl
    {
        public CommonUIPage2FrontEnd()
        {
            InitializeComponent();
        }
    }

    public class SectionsDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement elemnt = container as FrameworkElement;
            //if (item is LabelElement2)
            //{
            //    return elemnt.FindResource("LabelElementTemplate") as DataTemplate;
            //}
            //else if (item is BooleanElement2)
            //{
            //    return elemnt.FindResource("BooleanElementTemplate") as DataTemplate;
            //}

            return elemnt.FindResource("SectionWithHeaderOnSeparateRowTemplate") as DataTemplate;
        }
    }
}
