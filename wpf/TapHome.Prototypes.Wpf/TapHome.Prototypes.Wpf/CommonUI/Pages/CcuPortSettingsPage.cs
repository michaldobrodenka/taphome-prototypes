// todo: GK
//#if ANDROID
//using Android.App;
//using Com.TapHome.Android.Views.Elements;
//#endif
//#if IOS
//using Com.TapHome.iOS.CommonUI;
//using Com.TapHome.iOS;
//#endif
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using TapHome.Client.Desktop.CommonUI;
//using TapHome.Client.Desktop.CommonUI.Dependencies;
//using TapHome.Client.Desktop.CommonUI.Elements;
//using TapHome.Translations;

//namespace TapHome.Commons.CommonPages
//{
//#if ANDROID
//    [Activity(Label = "CcuPortSettings", Theme = "@style/TapHomeTheme.NoActionBar")]
//#endif
//    public class CcuPortSettingsPage : CommonUIPage
//	{
//		public enum PortType
//		{
//			None = 0,
//			Ccu = 1,
//			SlaveInterface = 2,
//		}

//		public class UartDescription
//		{
//			public string SystemName { get; set; }
//			public string DisplayName { get; set; }
//			public int? CcuId { get; set; }
//			public PortType PortType { get; set; }
//		}

//		public const string ExtraCcuId = "ExtraCcuId";
//        public const string ExtraSlaveInterfaceId = "ExtraSlaveInterfaceId";
//        public const string ExtraCcuPorts = "ExtraCcuPorts";

//        public static bool IsChanged;

//        public static List<UartDescription> Ports
//        {
//            get
//            {
//                return availableCcuPorts; 
//            }
//        }

//        private static List<UartDescription> availableCcuPorts;

//        private List<RadioElement> radioElements;

//        //private List<GenericCcu> ccus;

//        //private List<SlaveInterface> slaveInterfaces;

//        //private GenericCcu ccu;

//        private CommonSection section;

//        private bool multiplePorts;

//        private List<string> multiplePortsCcuModels = new List<string>() { "TapHomeBusCCU" };

//        private int id;

//        private PortType portType;

//        public ClientCommunicationManager CommunicationManager
//        {
//            get
//            {
//                return ApplicationManager.Instance.GetCommunicationManager(Storage.LastUsedLocationId.Value);
//            }
//        }

//        public CcuPortSettingsPage() : base(string.Empty, null)
//		{

//			this.title = "Terminals";

//			SetTitle();

//			SetLightTheme();

//			InitSection();
//		}

//        public CcuPortSettingsPage(Dictionary<string, object> parameters) : base(string.Empty, parameters)
//		{

//			this.title = "Terminals";

//			SetTitle();

//			SetLightTheme();

//			InitSection();
//		}

//        public override void OnInit()
//        {
//            base.OnInit();
//        }

//        private void InitSection()
//        {
//            this.RemoveAllSections();

//            radioElements = new List<RadioElement>();

//            section = new CommonSection(this, String.Empty, true);

//            //ccus = Storage.GetCcusById(Storage.LastUsedLocationId.Value).Values.ToList();

//            int ccuId = (int) GetParameter(ExtraCcuId, typeof(int));

//            int slaveInterfaceId = (int)GetParameter(ExtraSlaveInterfaceId, typeof(int));

//	        id = 1;

//            portType = PortType.Ccu;

//			//ccu = ccus.FirstOrDefault(p => p.CcuId == ccuId);

//			//availableCcuPorts =
//			//    JsonConvert.DeserializeObject<List<UartDescription>>((string) GetParameter(ExtraCcuPorts, typeof(string)));
//			availableCcuPorts = new List<UartDescription>();
//			for (var i = 0; i < 10; i++)
//			{
//				availableCcuPorts.Add(new UartDescription { DisplayName = $"asdf{i}", CcuId = 1, PortType = PortType.Ccu, SystemName = $"qwerty{i}" });
//			}
//			foreach (var port in availableCcuPorts)
//            {
//                if (port.CcuId != null && port.PortType == PortType.None)
//                {
//                    port.PortType = PortType.Ccu;
//                }
//            }

//            //multiplePorts = multiplePortsCcuModels.Contains(ccu?.Model);
//	        multiplePorts = false;

//	        foreach (var port in availableCcuPorts)
//            {
//                if (multiplePorts)
//                {
//                    AddBooleanElement(port);
//                }

//                else
//                {
//                    AddRadioElement(port);
//                }
//            }

//            if (!multiplePorts)
//            {
//                AddRadioElement(new UartDescription()
//                {
//                    SystemName = String.Empty,
//                    DisplayName = i18n.deviceType_None,
//                });
//            }

//            AddSection(section);
//        }

//        private void AddRadioElement(UartDescription port)
//        {
//            var radioElement = new RadioElement(this, port.DisplayName, false,
//                String.Empty, GetName(port), IsEnabled(port));

//            radioElement.ValueChanged += (sender, e) =>
//            {
//                if (!radioElement.Value)
//                {
//                    return;
//                }

//                IsChanged = true;

//                port.CcuId = id;
//                port.PortType = portType;

//                radioElement.Description = GetName(port);

//                UpdateRadioElements(radioElement);

//                UpdateRadioElements(port);
//            };

//            radioElements.Add(radioElement);

//            section.Add(radioElement);
//        }

//        private void AddBooleanElement(UartDescription port)
//        {
//            var booleanElement = new BooleanElement(this, port.DisplayName, id == port.CcuId,
//                GetName(port), IsEnabled(port));

//            booleanElement.ValueChanged += (sender, e) =>
//            {
//                IsChanged = true;

//                int? selectedId = null;

//                if (booleanElement.Value)
//                {
//                    selectedId = id;
//                }

//                port.CcuId = selectedId;
//                port.PortType = portType;

//                booleanElement.Description = GetName(port);
//            };

//            section.Add(booleanElement);
//        }

//        private void UpdateRadioElements(RadioElement radioElement)
//        {
//            foreach (var element in radioElements)
//            {
//                if (element != radioElement)
//                {
//                    element.Value = false;

//                    if (element.Enabled)
//                    {
//                        element.Description = String.Empty;
//                    }
//                }
//            }
//        }

//        private void UpdateRadioElements(UartDescription port)
//        {
//            foreach (var ccuPort in availableCcuPorts)
//            {
//                if (ccuPort.SystemName != port.SystemName && portType == ccuPort.PortType)
//                {
//                    ccuPort.CcuId = null;
//                    ccuPort.PortType = PortType.None;
//                }
//            }
//        }

//        private string GetName(UartDescription port)
//        {
//	        return "TopVec";
//	        //if (port != null && port.CcuId != null && !String.IsNullOrEmpty(port.SystemName))
//	        //{
//	        //    if (port.PortType == PortType.Ccu)
//	        //    {
//	        //        var portCcu = ccus.FirstOrDefault(p => p.CcuId == port.CcuId);

//	        //        if (portCcu != null)
//	        //        {
//	        //            return BuildUtils.ReplaceTapHomeInText(portCcu.Name);
//	        //        }
//	        //    }

//	        //    else if (port.PortType == PortType.SlaveInterface)
//	        //    {
//	        //        var slaveInterface = slaveInterfaces.FirstOrDefault(p => p.SlaveInterfaceId == port.CcuId);

//	        //        if (slaveInterface != null)
//	        //        {
//	        //            return ((SlaveInterfaceType)slaveInterface.SlaveInterfaceTypeId).ToString();
//	        //        }
//	        //    }
//	        //}

//	        //return String.Empty;
//        }

//        private bool IsEnabled(UartDescription port)
//        {
//            if ((port.CcuId == id && port.PortType == this.portType) || port.CcuId == null)
//            {
//                return true;
//            }

//            return false;
//        }

//        public override void OnBack()
//        {
//            GoBack();
//        }

//        public override void CreateControls()
//        {
//        }
//    }
//}