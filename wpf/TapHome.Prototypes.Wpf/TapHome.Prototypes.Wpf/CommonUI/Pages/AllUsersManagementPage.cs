// todo: GK
//#if ANDROID
//using Android.App;
//using Com.TapHome.Android.Views.Elements;
//using Android.Content;
//using Com.TapHome.Android;
//#endif
//#if IOS
//using Com.TapHome.iOS.CommonUI;
//using Com.TapHome.iOS;
//#endif
//using System;
//using System.Collections.Generic;
//using TapHome.Translations;
//using System.Linq;
//using TapHome.Client.Desktop.CommonUI;
//using TapHome.Client.Desktop.CommonUI.Dependencies;
//using TapHome.Client.Desktop.CommonUI.Elements;
//using TapHome.Commons;

//namespace TapHome.Commons.CommonPages
//{
//#if ANDROID
//    [Activity(Label = "User management", Theme = "@style/TapHomeTheme")]
//#endif
//    public class AllUsersManagementPage : CommonUIPage
//    {
//        public AllUsersManagementPage(Dictionary<string, object> parameters) : base(string.Empty, parameters)
//        {
//        }

//        public AllUsersManagementPage() : base(string.Empty, null)
//        {
//        }

//        private CommonSection userManagementSection;
//        private CommonSection addButtonSection;

//        private CommonButton addButton;

//        private int progressDialogHash;

//        public ClientCommunicationManager CommunicationManager
//        {
//            get
//            {
//                return ApplicationManager.Instance.GetCommunicationManager(Storage.LastUsedLocationId.Value);
//            }
//        }

//        public override void OnInit()
//        {
//            title = i18n.settings_userManagement;
//            SetTitle();
//            SetLightTheme();

//            userManagementSection = new CommonSection(this, i18n.allUsers_info, true);
//            addButtonSection = new CommonSection(this, String.Empty, true);
//            AddSection(userManagementSection);
//            AddSection(addButtonSection);

//            addButton = new CommonButton(this, i18n.addNewUser, CommonButton.Style.Default);
//            addButton.Clicked += AddButton_Clicked;

//            addButtonSection.Add(addButton);
//        }

//        public void AddButton_Clicked(object sender, EventArgs e)
//        {
//            //CommonUIPage.NavigateToPageWithParameters(this, typeof(AddUserPage), null);
//        }

//        void UpdateUserList()
//        {
//            progressDialogHash = CreateProgressDialog(string.Format(i18n.loading), false, null);

//            //CommunicationManager.GetUsers((t) =>
//            //{
//                this.RunOnUiThreadTh(() =>
//                {
//                    DismissProgressDialog(progressDialogHash);

//                    //if (t.CompletedWithoutProblemsWithResult())
//                    //{
//                        var m = new MessageGetUsersListResponse()
//                        {
//                            Users = new List<UserWithoutHash>()
//                            {
//                                new UserWithoutHash()
//                                {
//                                    FullName = "Name 1",
//                                    Login = "abc@dfe.com",
//                                    Permissions = UserPermission.User | UserPermission.Admin,
//                                },
//                                new UserWithoutHash()
//                                {
//                                    FullName = "Name 2",
//                                    Login = "abc@dfe.com",
//                                    Permissions = UserPermission.User,
//                                },
//                                new UserWithoutHash()
//                                {
//                                    FullName = "Name 3",
//                                    Login = "abc@dfe.com",
//                                    Permissions = UserPermission.User | UserPermission.Admin | UserPermission.Service,
//                                },
//                                new UserWithoutHash()
//                                {
//                                    FullName = "Name 4",
//                                    Login = "abc@dfe.com",
//                                    Permissions = UserPermission.User | UserPermission.Admin | UserPermission.Service,
//                                },
//                                new UserWithoutHash()
//                                {
//                                    FullName = "Name 5",
//                                    Login = "abc@dfe.com",
//                                    Permissions = UserPermission.Service | UserPermission.Admin,
//                                },
//                            }.ToArray()
//                        };
//                        UpdateAllUsersSection(m);
//                    //}
//                    //else
//                    //{
//                    //    CreateToast(string.Format(i18n.core_notConnected));
//                    //}
//                });
//            //});
//        }

//		private void UpdateAllUsersSection(MessageGetUsersListResponse m)
//		{
//			if (userManagementSection != null)
//			{
//				userManagementSection.Clear();
//			}

//			var elementList = new List<RolesElement>();

//			foreach (var user in m.Users)
//			{
//			    if (!user.IsDeleted)
//			    {
//			        var login = "(" + user.Login + ")" ?? "";
//                    RolesElement userStringElement;
//                    if (string.IsNullOrWhiteSpace(user.FullName))
//                        userStringElement = new RolesElement(this, login, user.Permissions);
//                    else
//                        userStringElement = new RolesElement(this, String.Format("{0} {1}", user.FullName, login), user.Permissions);			        userStringElement.Clicked += (sender, e) => UserStringElement(user);
//					elementList.Add(userStringElement);
//				}
//			}

//			if (elementList.Count > 0)
//			{
//				elementList = elementList.OrderBy(x => x.Caption).ToList();
//				foreach (var element in elementList)
//				{
//					userManagementSection.Add(element);
//				}
//			}
//#if IOS
//            this.Root.Reload(userManagementSection, UIKit.UITableViewRowAnimation.None);
//            this.Root.Reload(userManagementSection, UIKit.UITableViewRowAnimation.None);
//#endif
//        }

//        private void UserStringElement(UserWithoutHash user)
//        {
//            //var actualUser = Storage.GetLocalUserInfo(Storage.LastUsedLocationId.Value);
//            //bool perspective = false;

//            //if (user == null || user.Id == actualUser.Id)
//            //{
//            //    perspective = true;                
//            //}
//            //Dictionary<string, object> parameters = new Dictionary<string, object>();
//            //var serializedUser = JsonConvert.SerializeObject(user);
//            //parameters.Add(UserManagementPage.userInfoKey, serializedUser);
//            //parameters.Add(UserManagementPage.userPerspectiveKey, perspective);

//            //CommonUIPage.NavigateToPageWithParameters(this, typeof(UserManagementPage), parameters);
//        }

//        public override void OnBack()
//        {
//            GoBack();
//        }

//        public override void CreateControls()
//        {
//        }

//        public override void WillAppear()
//        {
//            base.WillAppear();
//            UpdateUserList();
//        }
//    }
//}
