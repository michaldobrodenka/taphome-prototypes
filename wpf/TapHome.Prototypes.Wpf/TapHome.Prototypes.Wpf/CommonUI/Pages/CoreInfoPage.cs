// todo: GK
//#if ANDROID
//using Android.App;
//using Com.TapHome.Android.Views.Elements;
//using Android.Content;
//using Com.TapHome.Android;
//#endif
//#if IOS
//using Com.TapHome.iOS.CommonUI;
//using Com.TapHome.iOS;
//#endif
//using System;
//using System.Collections.Generic;
//using System.Threading;
//using System.Threading.Tasks;
//using TapHome.Translations;
//using System.Text;
//using System.Globalization;
//using TapHome.Client.Desktop.CommonUI;
//using TapHome.Client.Desktop.CommonUI.Dependencies;
//using TapHome.Client.Desktop.CommonUI.Elements;
//using TapHome.Commons;
//using TapHome.Commons.CommonFrames;

//namespace TapHome.Commons.CommonPages
//{
//#if ANDROID
//    [Activity(Label = "CoreInfoPage", Theme = "@style/TapHomeTheme")]
//#endif
//	public class CoreInfoPage : CommonUIPage
//	{
//		public CoreInfoPage(Dictionary<string, object> parameters) : base(string.Empty, parameters)
//		{
//		}

//		public CoreInfoPage() : base(string.Empty, null)
//		{
//		}

//		CommonSection coreInfoSection, userLogSection;

//		public bool UpdateCoreInProgress;

//		#region TimeZones

//		private List<string> timeZones = new List<string>()
//		{
//			"Africa/Bangui",
//			"Africa/Cairo",
//			"Africa/Casablanca",
//			"Africa/Harare",
//			"Africa/Johannesburg",
//			"Africa/Lagos",
//			"Africa/Monrovia",
//			"Africa/Nairobi",
//			"Africa/Windhoek",
//			"America/Anchorage",
//			"America/Argentina",
//			"America/Asuncion",
//			"America/Bahia",
//			"America/Bogota",
//			"America/Buenos_Aires",
//			"America/Caracas",
//			"America/Cayenne",
//			"America/Chicago",
//			"America/Chihuahua",
//			"America/Cuiaba",
//			"America/Denver",
//			"America/Fortaleza",
//			"America/Godthab",
//			"America/Guatemala",
//			"America/Halifax",
//			"America/Indianapolis",
//			"America/Indiana",
//			"America/La_Paz",
//			"America/Los_Angeles",
//			"America/Mexico_City",
//			"America/Montevideo",
//			"America/New_York",
//			"America/Noronha",
//			"America/Phoenix",
//			"America/Regina",
//			"America/Santa_Isabel",
//			"America/Santiago",
//			"America/Sao_Paulo",
//			"America/St_Johns",
//			"America/Tijuana",
//			"Antarctica/McMurdo",
//			"Atlantic/South_Georgia",
//			"Asia/Almaty",
//			"Asia/Amman",
//			"Asia/Baghdad",
//			"Asia/Baku",
//			"Asia/Bangkok",
//			"Asia/Beirut",
//			"Asia/Calcutta",
//			"Asia/Colombo",
//			"Asia/Damascus",
//			"Asia/Dhaka",
//			"Asia/Dubai",
//			"Asia/Irkutsk",
//			"Asia/Jerusalem",
//			"Asia/Kabul",
//			"Asia/Kamchatka",
//			"Asia/Karachi",
//			"Asia/Katmandu",
//			"Asia/Kolkata",
//			"Asia/Krasnoyarsk",
//			"Asia/Kuala_Lumpur",
//			"Asia/Kuwait",
//			"Asia/Magadan",
//			"Asia/Muscat",
//			"Asia/Novosibirsk",
//			"Asia/Oral",
//			"Asia/Rangoon",
//			"Asia/Riyadh",
//			"Asia/Seoul",
//			"Asia/Shanghai",
//			"Asia/Singapore",
//			"Asia/Taipei",
//			"Asia/Tashkent",
//			"Asia/Tbilisi",
//			"Asia/Tehran",
//			"Asia/Tokyo",
//			"Asia/Ulaanbaatar",
//			"Asia/Vladivostok",
//			"Asia/Yakutsk",
//			"Asia/Yekaterinburg",
//			"Asia/Yerevan",
//			"Atlantic/Azores",
//			"Atlantic/Cape_Verde",
//			"Atlantic/Reykjavik",
//			"Australia/Adelaide",
//			"Australia/Brisbane",
//			"Australia/Darwin",
//			"Australia/Hobart",
//			"Australia/Perth",
//			"Australia/Sydney",
//			"Europe/Amsterdam",
//			"Europe/Athens",
//			"Europe/Belgrade",
//			"Europe/Berlin",
//			"Europe/Brussels",
//			"Europe/Budapest",
//			"Europe/Dublin",
//			"Europe/Helsinki",
//			"Europe/Istanbul",
//			"Europe/Kiev",
//			"Europe/London",
//			"Europe/Minsk",
//			"Europe/Moscow",
//			"Europe/Paris",
//			"Europe/Sarajevo",
//			"Europe/Warsaw",
//			"Indian/Mauritius",
//			"Pacific/Apia",
//			"Pacific/Auckland",
//			"Pacific/Fiji",
//			"Pacific/Guadalcanal",
//			"Pacific/Guam",
//			"Pacific/Honolulu",
//			"Pacific/Pago_Pago",
//			"Pacific/Port_Moresby",
//			"Pacific/Tongatapu"
//		};

//		#endregion

//		private GenericProperty coreProperty;

//		private List<GenericProperty> corePropertiesList = new List<GenericProperty>();

//		private StringElement nameElement;

//		private PropertyElement longitudeElement, latitudeElement;

//		private LabelElement versionElement, coreProcessUptimeElement;

//		private RadioRootElement timezoneElement;

//		private int progressDialogHash, uploadingDialogHash;

//		private bool viewOnly, triedToUpdate;

//		private CommonButton userLogButton;

//		public ClientCommunicationManager CommunicationManager
//		{
//			get { return ApplicationManager.Instance.GetCommunicationManager(Storage.LastUsedLocationId.Value); }
//		}

//		public override void OnInit()
//		{
//			this.title = i18n.myLocation;

//			SetTitle();

//			SetLightTheme();

//			InitSection();

//		    triedToUpdate = false;
//		}

//		private void InitSection()
//		{
//			RemoveAllSections();

//			coreInfoSection = new CommonSection(this, String.Empty, true);
//			userLogSection = new CommonSection(this, string.Empty, true);

//			coreProperty = new GenericProperty();

//			progressDialogHash = CreateProgressDialog(string.Format(i18n.loading), false, null);

//		    AddSection(coreInfoSection);

//            AddInfoElements();
//        }

//		private void AddUserLogButton()
//		{
//			userLogButton = new CommonButton(this, i18n.userManagementLog, CommonButton.Style.Default);
//			userLogButton.Clicked += (sender, e) =>
//			{
//#if IOS
//				this.NavigationController.PushViewController(new UserManagementLogViewController(), true);
//#endif
//#if ANDROID
//                StartActivity(new Intent(this, typeof(UserManagementLogActivity)));
//#endif
//			};
//			userLogSection.Add(userLogButton);
//		}

//        private void AddRestartCoreButton()
//        {
//            var restartCoreButton = new CommonButton(this, i18n.restart_controlUnit, CommonButton.Style.Default);
//            restartCoreButton.Clicked += (sender, e) =>
//            {
//                RestartCore();
//            };

//            var section = new CommonSection(this, String.Empty, true);
//            section.Add(restartCoreButton);
//            AddSection(section);
//        }

//        private void RestartCore()
//        {
//            var progressDialogHash = CreateProgressDialog(string.Format(i18n.saving), false, null);

//            CommunicationManager.RestartCore((t) =>
//            {
//                this.RunOnUiThreadTh(() =>
//                {
//                    DismissProgressDialog(progressDialogHash);

//                    if (t.CompletedWithoutProblemsWithResult())
//                    {
//                        var result = t.Result as MessageGenericResponse;

//                        switch (result.ErrorCode)
//                        {
//                            case CommonErrorCodes.Ok:
//                                UpdateCoreInProgress = true;
//                                ShowRestartInProgressDialog();
//                                break;
//                        }
//                    }

//                    else
//                    {
//                        CreateToast(string.Format(i18n.core_notConnected));
//                    }
//                });
//            });
//        }

//	    private void ShowRestartInProgressDialog()
//	    {
//            this.RunOnUiThreadTh(() =>
//            {
//                Progress<int> progress;

//	            uploadingDialogHash = CreateResultBasedProgressDialog(String.Empty, i18n.restart_inProgress, false, out progress);

//                var task = Task.Factory.StartNew(() =>
//                {
//                    for (int i = 0; i < 90; i++)
//                    {
//                        (progress as IProgress<int>).Report(i);
//                        Thread.Sleep(1000);
//                    }
//                }).ContinueWith((t2) =>
//                {
//                    ChangeResultBasedProgressDialogMessage(uploadingDialogHash, i18n.update_almostDone);

//                    for (int i = 91; i < 100; i++)
//                    {
//                        (progress as IProgress<int>).Report(i);
//                        Thread.Sleep(2000);
//                    }
//                });
//            });
//        }

//	    private void AddInfoElements()
//        {
//            //CommunicationManager.GetCoreProperties((t) =>
//            //{
//                this.RunOnUiThreadTh(() =>
//                {
//                    DismissProgressDialog(progressDialogHash);

//                    //if (t.CompletedWithoutProblemsWithResult())
//                    //{
//                        this.corePropertiesList = new List<GenericProperty>()
//                        {
//                            new GenericProperty()
//                            {
//                                Name = "Name",
//                                Value = "Location name",
//                            },
//                            new GenericProperty()
//                            {
//                                Name = "Latitude",
//                                Value = 12.3,
//                            },
//                            new GenericProperty()
//                            {
//                                Name = "Longitude",
//                                Value = 12.3,
//                            },
//                            new GenericProperty()
//                            {
//                                Name = "Version",
//                                Value = "2017.2.123",
//                            },
//                            new GenericProperty()
//                            {
//                                Name = "Core process uptime",
//                                Value = new TimeSpan(1,2,3,4).ToString(),
//                            },
//                            new GenericProperty()
//                            {
//                                Name = "TimeZoneId",
//                                Value = "Africa/Bangui",
//                            },
//                            new GenericProperty()
//                            {
//                                Name = "OS Image Version",
//                                Value = 5,
//                            },
//                            new GenericProperty()
//                            {
//                                Name = "Current Time",
//                                Value = (UInt32)40,
//                            },
//                        };

//			            var propertyElementInfo = new ServiceSettingsFrame.PropertyInfo();
//                        propertyElementInfo.InputType = ServiceSettingsFrame.PropertyInputType.SignedDecimalNumber;

//                        foreach (var item in corePropertiesList)
//                        {
//                            if (item.Name == "Name")
//                            {
//                                nameElement = new StringElement(this, item.Name, (string) item.Value, i18n.changeName,
//                                    "", !viewOnly);
//                                nameElement.NameChanged += (sender, e) =>
//                                {
//                                    nameElement.Name = (sender as StringElement).Name;
//                                    coreProperty = item;
//                                    coreProperty.Value = (object) nameElement.Name;
//                                    UpdateCoreProperty();
//                                };
//                                coreInfoSection.Add(nameElement);
//                            }

//                            if (item.Name == "Latitude")
//                            {
//                                latitudeElement = new PropertyElement(this, item.Name, item.Value.ToString(), propertyElementInfo);

//								latitudeElement.AlertCaption = i18n.changeLatitude;

//                                latitudeElement.ValueChanged += (sender, e) =>
//                                {
//                                    latitudeElement.Name = (sender as PropertyElement).Name;
//                                    coreProperty = item;
//                                    var latitude = latitudeElement.Name;
//                                    double value;
//                                    double.TryParse(latitude.Replace(",", "."), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out value);
//                                    coreProperty.Value = value;
//                                    UpdateCoreProperty();
//                                };
//								coreInfoSection.Add(latitudeElement);
//                            }

//                            if (item.Name == "Longitude")
//                            {
//                                longitudeElement = new PropertyElement(this, item.Name, item.Value.ToString(), propertyElementInfo);

//								longitudeElement.AlertCaption = null;//i18n.changeLongitude;

//                                longitudeElement.ValueChanged += (sender, e) =>
//                                {
//                                    longitudeElement.Name = (sender as PropertyElement).Name;
//                                    coreProperty = item;
//                                    var longitude = longitudeElement.Name;

//                                    coreProperty.Value = double.Parse(longitude.Replace(",", "."), CultureInfo.InvariantCulture);
//                                    UpdateCoreProperty();
//                                };
//                                coreInfoSection.Add(longitudeElement);
//                            }

//                            if (item.Name == "Version")
//                            {
//                                versionElement = new LabelElement(this, item.Name, (string)item.Value);
//                                coreInfoSection.Add(versionElement);
//                            }

//                            if (item.Name == "Core process uptime")
//                            {
//                                coreProcessUptimeElement = new LabelElement(this, item.Name, TimeSpan.Parse((string)item.Value).ToStringLong());
//                                coreInfoSection.Add(coreProcessUptimeElement);
//                            }

//                            if (item.Name == "TimeZoneId")
//                            {
//                                int index = timeZones.IndexOf(item.Value.ToString());
//                                timezoneElement = new RadioRootElement(this, i18n.core_timeZone, timeZones, index,
//                                    !viewOnly);
//                                timezoneElement.SelectedChanged += (sender, e) =>
//                                {
//                                    coreProperty = item;
//                                    coreProperty.Value = timeZones[timezoneElement.SelectedValueIndex];
//                                    UpdateCoreProperty();
//                                };
//                                coreInfoSection.Add(timezoneElement);
//                            }

//                            if (item.Name == "OS Image Version")
//                            {
//                                versionElement = new LabelElement(this, item.Name, item.Value.ToString());
//                                coreInfoSection.Add(versionElement);
//                            }

//                            if (item.Name == "Current Time")
//                            {
//                                try
//                                {
//                                    var currentTimeElement = new LabelElement(this, item.Name, (new DateTime(2000, 1, 1).Add(TimeSpan.FromSeconds((UInt32)item.Value))).ToString());
//                                    coreInfoSection.Add(currentTimeElement);
//                                }
//                                // TODO KUBO refactor
//                                catch (Exception e)
//                                {
//                                    var currentTimeElement = new LabelElement(this, item.Name, (string)item.Value);
//                                    coreInfoSection.Add(currentTimeElement);
//                                }
//                            }
//                        }

//                        var core = Storage.GetLocalCoreInfo(Storage.LastUsedLocationId.Value);

//                        if (core != null)
//                        {
//                            var ipAddressElement = new LabelElement(this, "IP address", core.IpOnLocalNetwork);
//                            coreInfoSection.Add(ipAddressElement);

//                            var accessTokenElement = new LabelElement(this, "Access Token", FormatAccessToken(core.AccessToken));
//                            coreInfoSection.Add(accessTokenElement);
//                        }

//						if (true)
//                        {
//                            CommonButton proxyConnectButton = new CommonButton(this, "Connect to TapHome for support",
//                                CommonButton.Style.Default);
//                            proxyConnectButton.Clicked += ProxyConnectButton_Clicked;

//                            var section = new CommonSection(this, String.Empty, true);
//                            AddSection(section);
//                            section.Add(proxyConnectButton);
//                        }

//                        // 1. if () // check verzii - kedy vobec ponuknut update, update coru je mozny len ked mam currentVersionInfo a ideme checkovat aj ProtocolVersion?
//                        // 2. currentVersionInfo vypisovat v properties namiesto tej sucasnej divnej verzie ak currentVersionInfo nie je null
//                        // 3. Progress dialog, aspon indeterminate, nezabudnut zrusit ten dialog ked sa pripojim alebo ked sa odpojim
//                        // 4. zkontrolovat ci sedi verzia? ak nesedi tak contact support
//                        // 5. are u sure? niekolko minut trva, funkcie budu nedostupne
//                        // 6. zobrazovat iba servisakom

//                        // testing only
//                        /*if (String.IsNullOrEmpty(version))
//                        {
//                            currentVersionInfo = "2017.0.4879,stable,2017";
//                            embeddedVersionInfo = "2017.0.4880,stable,2017";
//                        }*/

//                        bool updateCore = true;

//                        if (!viewOnly && updateCore)
//                        {
//                            CommonButton updateCoreButton = new CommonButton(this, String.Format(i18n.update_to, "2017.2.1234"), CommonButton.Style.Default);
//                            updateCoreButton.Clicked += UpdateCoreButton_Clicked;

//                            var section = new CommonSection(this, String.Empty, true);
//                            AddSection(section);
//                            section.Add(updateCoreButton);

//                            if (triedToUpdate)
//                            {
//                                triedToUpdate = false;
//                                ShowAlertDialog(String.Empty, "Software update failed.");

//                            }
//                        }

//                        if (true)
//                        {
//                            AddUserLogButton();
//                            AddSection(userLogSection);
//                        }

//                        if (!viewOnly && ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion
//                            >= ClientCommunicationManager.DeviceTypeMinProtocolVersion)
//                        {
//                            AddRestartCoreButton();
//                        }

//                        if (triedToUpdate)
//                        {
//                            triedToUpdate = false;
//                            ShowAlertDialog(i18n.softwareUpdate_complete, i18n.latestSoftware);
//                        }
//                    //}

//                    //else
//                    //{
//                    //    CreateToast(string.Format(i18n.core_notConnected));
//                    //}
//                });
//            //});
//        }

//        private string FormatAccessToken(string accessToken)
//        {
//            if (string.IsNullOrEmpty(accessToken))
//            {
//                return String.Empty;
//            }

//            int loc = 4;

//            StringBuilder token = new StringBuilder(accessToken);

//            for (int ins = loc; ins < token.Length; ins += loc + 1)
//                token.Insert(ins, "-");

//            return token.ToString();
//        }

//        private bool UpdateCore(string currentVersionInfo, string embeddedVersionInfo)
//        {
//            bool updateCore = false;

//            if (!String.IsNullOrEmpty(currentVersionInfo))
//            {
//                try
//                {
//                    if (embeddedVersionInfo.Split(',')[1] == "stable")
//                    {
//                        if (currentVersionInfo.Split(',')[1] == "stable")
//                        {
//                            int embeddedYear = int.Parse(embeddedVersionInfo.Split(',')[0].Split('.')[0]);
//                            int currentYear = int.Parse(currentVersionInfo.Split(',')[0].Split('.')[0]);

//                            if (currentYear < embeddedYear)
//                            {
//                                updateCore = true;
//                            }

//                            else
//                            {
//                                int embeddedVersion =
//                                    int.Parse(embeddedVersionInfo.Split(',')[0].Split('.')[1] +
//                                              embeddedVersionInfo.Split(',')[0].Split('.')[2]);
//                                int currentVersion =
//                                    int.Parse(currentVersionInfo.Split(',')[0].Split('.')[1] +
//                                              currentVersionInfo.Split(',')[0].Split('.')[2]);

//                                int buildNumber = int.Parse(currentVersionInfo.Split(',')[0].Split('.')[2]);

//                                if (currentVersion < embeddedVersion && buildNumber >= 5889)
//                                {
//                                    updateCore = true;
//                                }
//                            }
//                        }

//                        else if (currentVersionInfo.Split(',')[1] == "devel")
//                        {
//                            int embeddedVersion =
//                                    int.Parse(embeddedVersionInfo.Split(',')[0].Split('.')[2]);

//                            int currentVersion =
//                                int.Parse(currentVersionInfo.Split(',')[0].Split('.')[1]);

//                            if (currentVersion < embeddedVersion && currentVersion >= 5889)
//                            {
//                                updateCore = true;
//                            }
//                        }
//                    }
                    
//                }
//                catch { }
//            }

//            return updateCore;
//        }

//        private void UpdateCoreButton_Clicked(object sender, EventArgs e)
//        {
//            var buttons = new AlertDialogButtons();

//            buttons.PositiveButton = new Tuple<string, Action>(i18n.smartrule_phasealarm_phase_result_update, () =>
//            {
//                UpdateCoreInProgress = true;

//                triedToUpdate = true;

//                Progress<int> progress;

//                uploadingDialogHash = CreateResultBasedProgressDialog(i18n.softwareUpdate, i18n.update_uploading, false, out progress, 
//                    "DoNotUnplug", String.Format(i18n.update_doNotUnplug, "3-5"));

//                // testing only
//                //CommunicationManager.UpdateCoreTaskTest(progress).ContinueWith(t =>
                
//                //CommunicationManager.UpdateCoreTask(progress);
//            });

//            buttons.NegativeButton = new Tuple<string, Action>(i18n.general_cancel, () =>
//            {
//            });

//            CreateAlertDialog(String.Format(i18n.update_location, "Storage.LocationName"), String.Format(i18n.update_features, "2017.2.1234") +
//                String.Format("\n\n- {0}\n\n", i18n.update_speedStability) +
//                //String.Format("- {0}\n", i18n.update_permissions) +
//                //String.Format("- {0}\n\n", i18n.update_multiValueSwitch) +
//                i18n.general_question_continue, buttons, "DoNotUnplug", String.Format(i18n.update_doNotUnplug, "3-5"));
//        }

//	    private void ShowAlertDialog(string caption, string message)
//	    {
//	        var buttons = new AlertDialogButtons();

//	        buttons.PositiveButton = new Tuple<string, Action>(i18n.general_ok, () => { });

//	        CreateAlertDialog(caption, message, buttons);
//	    }

//	    private void ProxyConnectButton_Clicked(object sender, EventArgs e)
//        {

//        }

//        private void UpdateCoreProperty()
//        {
//            List<GenericProperty> genericPropertyList = new List<GenericProperty>();
//            genericPropertyList.Add(coreProperty);
//        }

//        private void RefreshMyLocalInfo()
//        {
//            if (Storage.LastUsedLocationId.HasValue)
//            {
//            }
//        }

//        public override void OnBack()
//        {
//            GoBack();
//        }

//        public override void CreateControls()
//        {

//        }
//    }
//}