// todo: GK
//#if ANDROID
//using Android.App;
//using Com.TapHome.Android.Views.Elements;
//using Android;
//using Android.Views;
//using Android.Widget;
//using Com.TapHome.Android;
//#endif
//#if IOS
//using Com.TapHome.iOS.CommonUI;
//using Com.TapHome.iOS;
//using UIKit;
//#endif
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using TapHome.Client.Desktop.CommonUI;
//using TapHome.Client.Desktop.CommonUI.Dependencies;
//using TapHome.Client.Desktop.CommonUI.Elements;
//using TapHome.Commons;
//using TapHome.Translations;

//namespace TapHome.Commons.CommonPages
//{
//#if ANDROID
//    [Activity(Label = "SlaveInterfaces", Theme = "@style/TapHomeTheme")]
//#endif
//    public class SlaveInterfacesPage : CommonUIPage
//    {
//        private CommonSection slaveInterfacesSection;

//        private bool viewOnly;

//        public ClientCommunicationManager CommunicationManager
//        {
//            get
//            {
//                return ApplicationManager.Instance.GetCommunicationManager(Storage.LastUsedLocationId.Value);
//            }
//        }

//        public SlaveInterfacesPage() : base(string.Empty, null)
//        {
//        }

//        public SlaveInterfacesPage(Dictionary<string, object> parameters) : base(string.Empty, parameters)
//        {
//        }

//        public override void WillAppear()
//        {
//            InitSection();
//        }

//        public override void OnInit()
//        {
//            base.OnInit();

//            this.title = i18n.exposeDevices;

//            SetTitle();

//            SetLightTheme();
//        }

//        private void InitSection()
//        {
//            this.RemoveAllSections();

//            //GetSlaveInterfaces();

//            var section = new CommonSection(this, String.Empty, true);
//#if ANDROID
//            int padding = App.IsTablet(this) ? 20 : 40;

//            ImageView image = new ImageView(this);
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
//            layoutParams.Gravity = GravityFlags.Center;
//            image.LayoutParameters = layoutParams;
//            image.SetImageDrawable(ResourceUtils.GetDrawable(this, Com.TapHome.Android.Resource.Drawable.desc_ExposeDevices));
//            image.SetBackgroundColor(ResourceUtils.GetColor(this, Com.TapHome.Android.Resource.Color.background_detail));
//            image.SetPadding(0, padding, 0, 0);
//            section.InsertAt(image, 0);
//            section.FindViewById<LinearLayout>(Com.TapHome.Android.Resource.Id.layout_elementsLayout)
//                .SetBackgroundColor(ResourceUtils.GetColor(this, Com.TapHome.Android.Resource.Color.background_detail));
//            section.SetFooterPadding(padding, padding, padding, padding);
//#endif
//#if IOS
//            UIImageView imageView = new UIImageView(new CoreGraphics.CGRect(200, 10, 100, 80));
//            imageView.Image = UIImage.FromFile("desc_ExposeDevices.png");
//			imageView.ContentMode = UIViewContentMode.Center;
//	        section.HeaderView = imageView;
//#endif
//            section.Footer = String.Format("Your control unit can provide information about devices in this location ({0}) " +
//                "to other control unit or central building management system.", "Storage.LocationName");

//            slaveInterfacesSection = new CommonSection(this, String.Empty, true);

//            if (!viewOnly)
//            {
//                var addInterfaceElement = new AddElement(this, "Add new interface");
//                addInterfaceElement.ElementClicked += (sender, e) =>
//                {
//                    //NavigateToPageWithParameters(this, typeof(AddSlaveInterfacePage), null);
//                };
//                slaveInterfacesSection.Add(addInterfaceElement);
//            }

//            AddSection(section);
//            AddSection(slaveInterfacesSection);

//            GetSlaveInterfaces();
//        }

//        private void GetSlaveInterfaces()
//        {
//            var progressDialogHash = CreateProgressDialog(string.Format(i18n.loading), false, null);

//            //CommunicationManager.GetSlaveInterfaces((t) =>
//            //{
//                this.RunOnUiThreadTh(() =>
//                {
//                    DismissProgressDialog(progressDialogHash);

//                    //if (t.CompletedWithoutProblemsWithResult())
//                    //{
//                        var result = new MessageGetSlaveInterfacesResponse()
//                        {
//                            SlaveInterfaces = new List<SlaveInterface>()
//                            {
//                                new SlaveInterface()
//                                {
//                                    Parameter1 = "/dev/ttyBUS0",
//                                    SlaveInterfaceTypeId = 1,
//                                },
//                                new SlaveInterface()
//                                {
//                                    Parameter1 = "/dev/ttyBUS1",
//                                    SlaveInterfaceTypeId = 2,
//                                },
//                                new SlaveInterface()
//                                {
//                                    Parameter1 = "/dev/ttyBUS2",
//                                    SlaveInterfaceTypeId = 3,
//                                }
//                            }.ToArray(),
//                        };

//                        if (result != null && result.SlaveInterfaces != null)
//                        {
//                            foreach (var slaveInterface in result.SlaveInterfaces)
//                            {
//                                AddStringElement(slaveInterface);
//                            }
//                        }
//                    //}

//                    //else
//                    //{
//                    //    CreateToast(string.Format(i18n.core_notConnected));
//                    //}
//                });
//            //});
//        }

//        private void AddStringElement(SlaveInterface slaveInterface)
//        {
//            var stringElement = new StringElement(this, GetSlaveInterfaceName((SlaveInterfaceType)slaveInterface.SlaveInterfaceTypeId), 
//                RenamePort(slaveInterface.Parameter1), String.Empty, String.Empty, true, false);

//            stringElement.Clicked += (sender, e) =>
//            {
//                //Dictionary<string, object> parameters = new Dictionary<string, object>();
//                //parameters.Add(ExposeDevicesPage.ExtraSlaveInterfaceId, slaveInterface.SlaveInterfaceId);

//                //NavigateToPageWithParameters(this, typeof(ExposeDevicesPage), parameters);
//            };

//            if (viewOnly)
//            {
//                slaveInterfacesSection.Add(stringElement);
//            }

//            else
//            {
//                slaveInterfacesSection.InsertAt(stringElement, slaveInterfacesSection.Elements.Count - 1);
//            }
//        }

//        private string RenamePort(string port)
//        {
//            if (!String.IsNullOrEmpty(port))
//            {
//                port = port
//                    .Replace("/dev/ttyS0", "BUS")
//                    .Replace("/dev/ttyBUS0", "BUS1")
//                    .Replace("/dev/ttyBUS1", "BUS2")
//                    .Replace("/dev/ttyBUS2", "BUS3")
//                    .Replace(",/dev/ttyBUS3", "");
//            }

//            return port;
//        }

//        private string GetSlaveInterfaceName(SlaveInterfaceType slaveInterfaceType)
//        {
//            String name = String.Empty;

//            switch (slaveInterfaceType)
//            {
//                case SlaveInterfaceType.ModbusRS485RTU:
//                    name = "Modbus RTU";
//                    break;
//                case SlaveInterfaceType.ModbusRS485ASCII:
//                    name = "Modbus ASCII";
//                    break;
//                case SlaveInterfaceType.ModbusTCP:
//                    name = "Modbus TCP";
//                    break;
//            }

//            return name;
//        }

//        public override void OnBack()
//        {
//            GoBack();
//        }

//        public override void CreateControls()
//        {
//        }
//    }
//}