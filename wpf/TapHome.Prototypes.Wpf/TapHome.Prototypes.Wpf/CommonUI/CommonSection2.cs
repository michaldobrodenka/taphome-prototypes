﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TapHome.Prototypes.Wpf.CommonUI.Elements;

namespace TapHome.Prototypes.Wpf.CommonUI
{
    public enum SectionLayout
    {
        SettingsWithHeaderOnSeparateRow,
        SettingsWithHeaderInFirstRow,
        List
    }

    public class CommonSection2 : INotifyPropertyChanged
    {
        private string header;

        public string Header
        { get => header;
          set
            {
                if (this.header != value)
                {
                    this.header = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public bool ShowHeader { get => !String.IsNullOrEmpty(this.header); }

        private string footer;

        public string Footer
        {
            get => footer;
            set
            {
                if (this.footer != value)
                {
                    this.footer = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public CommonSection2(object context, string caption, bool lightTheme = false)
        {
            this.Elements = new ObservableCollection<ElementBase2>();
            this.Header = caption;
        }

        public void Add(ElementBase2 element)
        {
            this.Elements.Add(element);
        }
        public void AddAll(ElementBase2[] elements)
        {
            foreach (var element in elements)
            {
                this.Elements.Add(element);
            }
        }

        public ObservableCollection<ElementBase2> Elements { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CommonUIPage2 : INotifyPropertyChanged
    {
        public ObservableCollection<CommonSection2> Sections { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public Dictionary<string, object> Parameters { get; }

        protected CommonUIPage2(string caption, Dictionary<string, object> parameters)
        {
            this.Sections = new ObservableCollection<CommonSection2>();
            this.Parameters = parameters;
        }

        public void AddSection(CommonSection2 section)
        {
            this.Sections.Add(section);
        }

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
