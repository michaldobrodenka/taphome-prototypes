﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace TapHome.Prototypes.Wpf.CommonUI
{
    /// <summary>
    /// Interaction logic for CommonUIHost.xaml
    /// </summary>
    public partial class CommonUIHost : UserControl
    {
        public Stack<CommonUIPage> navigationStack = new Stack<CommonUIPage>();

        public CommonUIHost()
        {
            InitializeComponent();
            this.BackButton.Click += BackButton_Click;
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        public void Push(CommonUIPage page, bool asRoot = false)
        {
            page.Host = this;

            if (asRoot)
            {
                this.navigationStack.Clear();
            }

            this.navigationStack.Push(page);
            page.OnInit();
            page.WillAppear();

            // todo: GK
            Grid.SetRow(page, 1);
            this.grid.Children.Add(page);
            //this.PageContent.Children.Clear();
            //this.PageContent.Children.Add(page);

            this.RefreshBackButton();
        }

        public void GoBack()
        {
            if (this.navigationStack.Count > 1)
            {
                var oldPage = this.navigationStack.Pop();
                this.grid.Children.Remove(oldPage);

                var page = this.navigationStack.Peek();

                // todo: GK
                Grid.SetRow(page, 1);
                this.grid.Children.Add(page);
            }

            this.RefreshBackButton();
        }

        public void RefreshBackButton()
        {
            if (this.navigationStack.Count < 2)
            {
                this.BackButton.IsEnabled = false;
            }
            else
            {
                this.BackButton.IsEnabled = true;
            }
        }
    }
}
