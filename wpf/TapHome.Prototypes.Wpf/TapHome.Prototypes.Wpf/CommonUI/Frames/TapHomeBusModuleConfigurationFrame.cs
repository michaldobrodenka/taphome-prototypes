﻿ //todo: GK
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TapHome.Client.Desktop.CommonUI;
using TapHome.Client.Desktop.CommonUI.Dependencies;
//using TapHome.Client.Desktop.CommonUI.Elements;
using TapHome.Commons;
using TapHome.Prototypes.Wpf.CommonUI;
using TapHome.Prototypes.Wpf.CommonUI.Elements;
using TapHome.Translations;

namespace TapHome.Commons.CommonFrames
{
    // M.P: Only for test
    public class CommonUIFrame2 : CommonUIPage2
    {
        protected object context;
        public CommonUIFrame2(object context)
            : base(String.Empty, null)
        {
            this.context = context;
        }

        public void WillAppear() { OnInit(); }

        public virtual void OnInit()
        { }
    }

    public class TaphomeBusModuleConfigurationFrame : CommonUIFrame2
    {
        public const string DeviceIdExtraKey = "com.taphome.android.TapHomeBusModuleConfigActivity/DEVICE_ID";

        public const string DeviceSerialExtraKey = "DEVICE_SERIAL";

        public class ModuleConfiguration
        {
            public enum DOFunction
            {
                NotConfigured = 0,
                Switch = 1,
                Blind = 2
            };

            public enum DIFunction
            {
                NotConfigured = 0,
                ReedContact = 1,
                PushButton = 2,
                ImpulseCounter = 3,
            };

            public enum AOFunction
            {
                NotConfigured = 0,
                Used = 1,
            };

            public enum NTCFunction
            {
                NotConfigured = 0,
                Used = 1,
            };

            public enum UIFunction
            {
                NotConfigured = 0,
                ReedContact = 1,
                PushButton = 2,
                ImpulseCounter = 3,
                NtcThermometer = 4,
                AnalogInput = 5,
            };

            public enum BLEFunction
            {
                NotConfigured = 0,
                EncoderBlind = 1,
                TimedBlind = 2,
                EncoderSlat = 3,
                EncoderSlide = 4,
                TimedSlide = 5,
            };

            public enum UOFunction
            {
                NotConfigured = 0,
                Switch = 1,
                LedDimmer = 2,
                Blind = 3,
            };

            public class BlindConfiguration
            {
                public int TerminalUp { get; set; }

                public int TerminalDown { get; set; }

                public bool IsConfigured { get; set; }

                public string Type { get; set; }
            }

            public TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction[] DigitalInputs;

            public TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction[] DigitalOutputs;

            public TaphomeBusModuleConfigurationFrame.ModuleConfiguration.AOFunction[] AnalogOutputs;

            public TaphomeBusModuleConfigurationFrame.ModuleConfiguration.NTCFunction[] NtcThermometers;

            public TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction[] UniversalInputs;

            public TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BlindConfiguration[] BlindConfigurations;

            public TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction[] EncoderBlindConfigurations;

            public TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction[] UniversalOutputs;

            public string[] DigitalInputsNames;

            public string[] DigitalOutputsNames;

            public string[] BlindConfigurationsNames;

            public string[] AnalogOutputsNames;

            public string[] NtcThermometersNames;

            public string[] EncoderBlindsNames;

            public string[] UniversalInputsNames;

            public string[] UniversalOutputsNames;

            public string[] CustomDigitalInputsNames;

            public string[] CustomDigitalOutputsNames;

            public string[] CustomBlindConfigurationsNames;

            public string[] CustomAnalogOutputsNames;

            public string[] CustomNtcThermometersNames;

            public string[] CustomEncoderBlindsNames;

            public string[] CustomUniversalInputsNames;

            public string[] CustomUniversalOutputsNames;
        }

        private TaphomeBusModuleConfigurationFrame.ModuleConfiguration configuration;

        private int digitalOutputs, digitalInputs, blinds, analogOutputs, ntcThermometers, encoderBlinds, universalInputs, universalOutputs;

        public string config;

        // CONTROLS //

        //private ProgressIndicator progressIndicatorOverlay;

        private RadioRootElement2[] outputRadioGroups;

        //		private Section[] blindSections = new Section[6];

        private BooleanElement2[] blindConfigedElements;

        private RadioRootElement2[] encoderBlindConfigedElements;

        private DoRadioElement2[] blindRelayUpRadioGroups;

        private DoRadioElement2[] blindRelayDownRadioGroups;

        private RadioRootElement2[] inputRadioGroups;

        private RadioRootElement2[] analogOutputRadioGroups;

        private RadioRootElement2[] ntcThermometersRadioGroups;

        private RadioRootElement2[] universalInputRadioGroups;

        private RadioRootElement2[] universalOutputRadioGroups;

        private CommonSection2[] blindSectionGroups;

        private StringElement2[] blindNameGroups;

        private RadioRootElement2[] blindTypeRadioGroups;

        //		private UIButton blinkLedButton;

        // END OF CONTROLS //

        private Device board;

        public TaphomeBusModuleConfigurationFrame(object context, Device board) : base(context)
        {
            this.board = board;
            base.WillAppear();
        }

        private int savingDialogHashCode;

        private int connectingDialogHashCode;

        //public ServiceSettingsDeviceFrame ServiceSettingsDeviceFrame;

        public Dictionary<string, string> DeviceProperties { get; set; }

        private bool viewOnly;

        public event EventHandler<EventArgs> OnBack;

        public override void OnInit()
        {
            base.OnInit();

            //this.RemoveAllSections();

            var permissions = UserPermissionForDevice.ServiceSettingsCanEdit;// Storage.GetUserPermissionsForDevice(Storage.LastUsedLocationId.Value, board.ID);

            if (permissions.HasFlag(UserPermissionForDevice.ServiceSettingsCanView))
            {
                viewOnly = true;
            }

            if (permissions.HasFlag(UserPermissionForDevice.ServiceSettingsCanEdit))
            {
                viewOnly = false;
            }

            var type = this.board.SerialNumber.Split('-')[0];
            switch (type)
            {
                case "DI3":
                    this.digitalInputs = 3;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "DI32":
                    this.digitalInputs = 32;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "DI24":
                    this.digitalInputs = 24;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "DO12":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 12;
                    this.blinds = 6;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "DI15":
                case "DI15BL15":
                    this.digitalInputs = 15;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "DI15BL15V2":
                    this.digitalInputs = 15;
                    this.digitalOutputs = 30;
                    this.blinds = 15;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "NTC2DI4":
                    this.digitalInputs = 4;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 2;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "NTC2":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 2;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "AO4":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 4;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "CORE14":
                    this.digitalInputs = 4;
                    this.digitalOutputs = 4;
                    this.blinds = 2;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 2;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "BLE15":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 15;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "BLE14":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 14;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "UI24":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 24;
                    this.universalOutputs = 0;
                    break;
                case "NTC2DI2DO2":
                    this.digitalInputs = 2;
                    this.digitalOutputs = 2;
                    this.blinds = 1;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 2;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "CONTROLLER":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 1;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
                case "LEDOC12":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 0;
                    this.blinds = 6;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 0;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 12;
                    break;
                case "WSBT":
                    this.digitalInputs = 0;
                    this.digitalOutputs = 0;
                    this.blinds = 0;
                    this.analogOutputs = 0;
                    this.ntcThermometers = 1;
                    this.encoderBlinds = 0;
                    this.universalInputs = 0;
                    this.universalOutputs = 0;
                    break;
            }
            // TODO:
            //var currentDevices = Storage.GetDevicesById(Storage.LastUsedLocationId.Value).Values.Where(d => d.SerialNumber.Split(':')[0] == this.board.SerialNumber).ToArray();
            GenericDevice[] currentDevices = new GenericDevice[0];
            this.GenerateConfig(currentDevices);
            //this.RunOnUiThreadTh(() =>
            //{
                this.CreateControls();
                this.ConfigToControls();
            //});

            config = this.ExportConfig();

            if (this.board.Model == "TapHomeBusMultiZoneController")
            {
                //DeviceProperties = ((GenericDevice)this.board).DeviceProperties;
                //ServiceSettingsDeviceFrame = new ServiceSettingsDeviceFrame(context, this.board, DeviceProperties, false, false);
                //InsertFrame(ServiceSettingsDeviceFrame, 0);
            }
        }

        private void GenerateConfig(GenericDevice[] currentDevices)
        {
            this.configuration = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration();

            this.configuration.DigitalInputs = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction[this.digitalInputs];
            this.configuration.DigitalOutputs = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction[this.digitalOutputs];
            this.configuration.BlindConfigurations = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BlindConfiguration[this.blinds];
            this.configuration.AnalogOutputs = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.AOFunction[this.analogOutputs];
            this.configuration.NtcThermometers = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.NTCFunction[this.ntcThermometers];
            this.configuration.EncoderBlindConfigurations = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction[this.encoderBlinds];
            this.configuration.UniversalInputs = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction[this.universalInputs];
            this.configuration.UniversalOutputs = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction[this.universalOutputs];

            this.configuration.DigitalInputsNames = new string[this.digitalInputs];
            this.configuration.DigitalOutputsNames = new string[this.digitalOutputs];
            this.configuration.BlindConfigurationsNames = new string[this.blinds];
            this.configuration.AnalogOutputsNames = new string[this.analogOutputs];
            this.configuration.NtcThermometersNames = new string[this.ntcThermometers];
            this.configuration.EncoderBlindsNames = new string[this.encoderBlinds];
            this.configuration.UniversalInputsNames = new string[this.universalInputs];
            this.configuration.UniversalOutputsNames = new string[this.universalOutputs];

            this.configuration.CustomDigitalInputsNames = new string[this.digitalInputs];
            this.configuration.CustomDigitalOutputsNames = new string[this.digitalOutputs];
            this.configuration.CustomBlindConfigurationsNames = new string[this.blinds];
            this.configuration.CustomAnalogOutputsNames = new string[this.analogOutputs];
            this.configuration.CustomNtcThermometersNames = new string[this.ntcThermometers];
            this.configuration.CustomEncoderBlindsNames = new string[this.encoderBlinds];
            this.configuration.CustomUniversalInputsNames = new string[this.universalInputs];
            this.configuration.CustomUniversalOutputsNames = new string[this.universalOutputs];

            for (int i = 0; i < this.digitalInputs; i++)
            {
                this.configuration.DigitalInputs[i] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction.NotConfigured;
                this.configuration.DigitalInputsNames[i] = String.Format("DI{0}", i + 1);
            }

            for (int i = 0; i < this.digitalOutputs; i++)
            {
                this.configuration.DigitalOutputs[i] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.NotConfigured;
                this.configuration.DigitalOutputsNames[i] = String.Format("DO{0}", i + 1);
            }

            for (int i = 0; i < this.analogOutputs; i++)
            {
                this.configuration.AnalogOutputs[i] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.AOFunction.NotConfigured;
                this.configuration.AnalogOutputsNames[i] = String.Format("AO{0}", i + 1);
            }

            for (int i = 0; i < this.ntcThermometers; i++)
            {
                this.configuration.NtcThermometers[i] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.NTCFunction.NotConfigured;
                this.configuration.NtcThermometersNames[i] = String.Format("NTC{0}", i + 1);
            }

            for (int i = 0; i < this.blinds; i++)
            {
                this.configuration.BlindConfigurations[i] = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BlindConfiguration()
                {
                    TerminalUp = 0,
                    TerminalDown = 0,
                    IsConfigured = false,
                    Type = this.universalOutputs > 0 ?
                        TapHomeBusModuleConfig.TypeBlindOnUO : TapHomeBusModuleConfig.TypeBlind,
                };
                this.configuration.BlindConfigurationsNames[i] = i18n.bus_module_blind + (i + 1);
            }

            for (int i = 0; i < this.encoderBlinds; i++)
            {
                this.configuration.EncoderBlindConfigurations[i] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.NotConfigured;

                this.configuration.EncoderBlindsNames[i] = i18n.bus_module_blind + (i + 1);
            }

            for (int i = 0; i < this.universalInputs; i++)
            {
                this.configuration.UniversalInputs[i] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.NotConfigured;
                this.configuration.UniversalInputsNames[i] = String.Format("UI{0}", i + 1);
            }

            for (int i = 0; i < this.universalOutputs; i++)
            {
                this.configuration.UniversalOutputs[i] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.NotConfigured;
                this.configuration.UniversalOutputsNames[i] = String.Format("UO{0}", i + 1);
            }

            foreach (var device in currentDevices)
            {
                if (!device.SerialNumber.Contains<char>(':'))
                    continue;
                int index = 0;

                if (int.TryParse(device.SerialNumber.Split('-').Last(), out index))
                {
                    index--;
                }
                //index = int.Parse(device.SerialNumber.Split('-').Last()) - 1;

                string feature = (device.SerialNumber.Split(':')[1]).Split('-')[0];

                switch (feature)
                {
                    case TapHomeBusModuleConfig.TypeReedContact:
                        if (this.universalInputs > 0)
                        {
                            this.configuration.UniversalInputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.ReedContact;
                            this.configuration.CustomUniversalInputsNames[index] = device.Name;
                        }
                        else
                        {
                            this.configuration.DigitalInputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction.ReedContact;
                            this.configuration.CustomDigitalInputsNames[index] = device.Name;
                        }
                        break;

                    case TapHomeBusModuleConfig.TypeImpulseCounter:
                        if (this.universalInputs > 0)
                        {
                            this.configuration.UniversalInputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.ImpulseCounter;
                            this.configuration.CustomUniversalInputsNames[index] = device.Name;
                        }
                        else
                        {
                            this.configuration.DigitalInputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction.ImpulseCounter;
                            this.configuration.CustomDigitalInputsNames[index] = device.Name;
                        }
                        break;

                    case TapHomeBusModuleConfig.TypeBlind:
                    case TapHomeBusModuleConfig.TypeSlide:
                        var str = device.GetProperty<string>("TerminalUp", "DO0");

                        if (str.StartsWith("DO") || str.StartsWith("AO"))
                            str = str.Remove(0, 2);

                        var relayUp = int.Parse(str);

                        str = device.GetProperty<string>("TerminalDown", "DO0");

                        if (str.StartsWith("DO") || str.StartsWith("AO"))
                            str = str.Remove(0, 2);

                        var relayDown = int.Parse(str);

                        this.configuration.BlindConfigurations[index] = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BlindConfiguration();
                        this.configuration.BlindConfigurations[index].IsConfigured = true;
                        this.configuration.BlindConfigurations[index].TerminalUp = relayUp;
                        this.configuration.BlindConfigurations[index].TerminalDown = relayDown;
                        this.configuration.BlindConfigurations[index].Type = feature == TapHomeBusModuleConfig.TypeBlind ?
                            TapHomeBusModuleConfig.TypeBlind : TapHomeBusModuleConfig.TypeSlide;
                        this.configuration.CustomBlindConfigurationsNames[index] = device.Name;
                        break;

                    case TapHomeBusModuleConfig.TypePushButton:
                        if (this.universalInputs > 0)
                        {
                            this.configuration.UniversalInputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.PushButton;
                            this.configuration.CustomUniversalInputsNames[index] = device.Name;
                        }
                        else
                        {
                            this.configuration.DigitalInputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction.PushButton;
                            this.configuration.CustomDigitalInputsNames[index] = device.Name;
                        }
                        break;

                    case TapHomeBusModuleConfig.TypeSwitch:
                        this.configuration.DigitalOutputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.Switch;
                        this.configuration.CustomDigitalOutputsNames[index] = device.Name;
                        break;

                    case TapHomeBusModuleConfig.TypeAnalogOutput:
                        this.configuration.AnalogOutputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.AOFunction.Used;
                        this.configuration.CustomAnalogOutputsNames[index] = device.Name;
                        break;

                    case TapHomeBusModuleConfig.TypeAnalogInput:
                        this.configuration.UniversalInputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.AnalogInput;
                        this.configuration.CustomUniversalInputsNames[index] = device.Name;
                        break;

                    case TapHomeBusModuleConfig.TypeThermometer:
                        if (this.universalInputs > 0)
                        {
                            this.configuration.UniversalInputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.NtcThermometer;
                            this.configuration.CustomUniversalInputsNames[index] = device.Name;
                        }
                        else
                        {
                            this.configuration.NtcThermometers[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.NTCFunction.Used;
                            this.configuration.CustomNtcThermometersNames[index] = device.Name;
                        }
                        break;

                    case TapHomeBusModuleConfig.TypeEncoderBlind:
                        this.configuration.EncoderBlindConfigurations[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderBlind;
                        this.configuration.CustomEncoderBlindsNames[index] = device.Name;
                        break;
                    case TapHomeBusModuleConfig.TypeCurrentLimitedBlind:
                        this.configuration.EncoderBlindConfigurations[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.TimedBlind;
                        this.configuration.CustomEncoderBlindsNames[index] = device.Name;
                        break;
                    case TapHomeBusModuleConfig.TypeEncoderSlat:
                        this.configuration.EncoderBlindConfigurations[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderSlat;
                        this.configuration.CustomEncoderBlindsNames[index] = device.Name;
                        break;
                    case TapHomeBusModuleConfig.TypeBlindOnUO:
                    case TapHomeBusModuleConfig.TypeSlideOnUO:
                        str = device.GetProperty<string>("TerminalUp", "UO0");

                        if (str.StartsWith("UO"))
                            str = str.Remove(0, 2);

                        relayUp = int.Parse(str);

                        str = device.GetProperty<string>("TerminalDown", "UO0");

                        if (str.StartsWith("UO"))
                            str = str.Remove(0, 2);

                        relayDown = int.Parse(str);

                        this.configuration.BlindConfigurations[index] = new TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BlindConfiguration();
                        this.configuration.BlindConfigurations[index].IsConfigured = true;
                        this.configuration.BlindConfigurations[index].TerminalUp = relayUp;
                        this.configuration.BlindConfigurations[index].TerminalDown = relayDown;
                        this.configuration.BlindConfigurations[index].Type = feature == TapHomeBusModuleConfig.TypeBlindOnUO ?
                            TapHomeBusModuleConfig.TypeBlindOnUO : TapHomeBusModuleConfig.TypeSlideOnUO;
                        this.configuration.CustomBlindConfigurationsNames[index] = device.Name;
                        break;
                    case TapHomeBusModuleConfig.TypeLedDimmer:
                        this.configuration.UniversalOutputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.LedDimmer;
                        this.configuration.CustomUniversalOutputsNames[index] = device.Name;
                        break;
                    case TapHomeBusModuleConfig.TypeSwitchOnUO:
                        this.configuration.UniversalOutputs[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.Switch;
                        this.configuration.CustomUniversalOutputsNames[index] = device.Name;
                        break;
                    case TapHomeBusModuleConfig.TypeCurrentLimitedSlide:
                        this.configuration.EncoderBlindConfigurations[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.TimedSlide;
                        this.configuration.CustomEncoderBlindsNames[index] = device.Name;
                        break;
                    case TapHomeBusModuleConfig.TypeEncoderSlide:
                        this.configuration.EncoderBlindConfigurations[index] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderSlide;
                        this.configuration.CustomEncoderBlindsNames[index] = device.Name;
                        break;
                        // do NOT delete
                        /*case TapHomeBusModuleConfig.TypeAnalogInput:
                            this.configuration.UniversalInputs[index] = ModuleConfiguration.UIFunction.AnalogInput;
                            this.configuration.CustomUniversalInputsNames[index] = device.Name;
                            break;*/

                }
            }
        }

        public void CreateControls()
        {
            //SetLightTheme();
            // OUTPUTS
            this.outputRadioGroups = new RadioRootElement2[this.digitalOutputs];
            var values = new List<string>();
            values.Add(i18n.bus_module_not_configured);
            values.Add(i18n.dashboard_deviceSwitch);
            var outputsSection = new CommonSection2(context, i18n.bus_module_outputs, true);

            for (int i = 0; i < this.digitalOutputs; i++)
            {
                var radioGroup = new DeviceRadioRootElement2(context, this.configuration.DigitalOutputsNames[i], this.configuration.CustomDigitalOutputsNames[i], values, 0, !viewOnly);
                radioGroup.SelectedChanged += HandleOutputRadioChanged;
                radioGroup.DeviceNameChanged += HandleOutputDeviceNameChanged;
                this.outputRadioGroups[i] = radioGroup;
            }
            outputsSection.AddAll(this.outputRadioGroups);
            List<string> relaysList = new List<string>();
            for (int i = 0; i < this.digitalOutputs; i++)
                relaysList.Add(this.configuration.DigitalOutputsNames[i]);

            // ANALOG OUTPUTS
            this.analogOutputRadioGroups = new RadioRootElement2[this.analogOutputs];
            values = new List<string>();
            values.Add(i18n.bus_module_not_configured);
            values.Add(i18n.bus_module_configured);
            var analogOutputsSection = new CommonSection2(context, i18n.bus_module_analog_outputs, true);

            for (int i = 0; i < this.analogOutputs; i++)
            {
                var radioGroup = new DeviceRadioRootElement2(context, this.configuration.AnalogOutputsNames[i], this.configuration.CustomAnalogOutputsNames[i], values, 0, !viewOnly);
                radioGroup.SelectedChanged += HandleAnalogOutputRadioChanged;
                radioGroup.DeviceNameChanged += HandleAnalogOutputDeviceNameChanged;
                this.analogOutputRadioGroups[i] = radioGroup;
            }
            analogOutputsSection.AddAll(this.analogOutputRadioGroups);

            // NTC
            this.ntcThermometersRadioGroups = new RadioRootElement2[this.ntcThermometers];
            values = new List<string>();
            values.Add(i18n.bus_module_not_configured);
            values.Add(i18n.bus_module_configured);
            var temperatureSensorsSection = new CommonSection2(context, "NTC", true);

            for (int i = 0; i < this.ntcThermometers; i++)
            {
                var radioGroup = new DeviceRadioRootElement2(context, this.configuration.NtcThermometersNames[i], this.configuration.CustomNtcThermometersNames[i], values, 0, !viewOnly);
                radioGroup.SelectedChanged += HandleTemperatureSensorsRadioChanged;
                radioGroup.DeviceNameChanged += HandleTemperatureSensorDeviceNameChanged;
                this.ntcThermometersRadioGroups[i] = radioGroup;
            }
            temperatureSensorsSection.AddAll(this.ntcThermometersRadioGroups);

            // BLINDS
            this.blindConfigedElements = new BooleanElement2[this.blinds];
            this.blindNameGroups = new StringElement2[this.blinds];
            this.blindRelayUpRadioGroups = new DoRadioElement2[this.blinds];
            this.blindRelayDownRadioGroups = new DoRadioElement2[this.blinds];
            this.blindTypeRadioGroups = new RadioRootElement2[this.blinds];

            values = new List<string>();
            values.Add("Blind");
            values.Add("Slide");

            for (int i = 0; i < this.blinds; i++)
            {
                blindConfigedElements[i] = new BooleanElement2(context, i18n.bus_module_configured, false, String.Empty, !viewOnly);
                blindConfigedElements[i].ValueChanged += HandleBlindConfiguredElementChanged;
                blindNameGroups[i] = new StringElement2(context, i18n.create_account_name, this.configuration.CustomBlindConfigurationsNames[i], i18n.detail_smartRules_rename);
                blindNameGroups[i].NameChanged += HandleBlindNameChanged;
                blindRelayUpRadioGroups[i] = new DoRadioElement2(context, i18n.bus_module_up, new List<int>(), this.universalOutputs > 0 ? this.configuration.UniversalOutputsNames.ToList()
                                                                : this.configuration.DigitalOutputsNames.ToList(), this.universalOutputs > 0 ? "UO" : "DO");
                blindRelayUpRadioGroups[i].SelectedChanged += HandleBlindRelayUpRadioChanged;
                blindRelayDownRadioGroups[i] = new DoRadioElement2(context, i18n.bus_module_down, new List<int>(), this.universalOutputs > 0 ? this.configuration.UniversalOutputsNames.ToList()
                                                                : this.configuration.DigitalOutputsNames.ToList(), this.universalOutputs > 0 ? "UO" : "DO");
                blindRelayDownRadioGroups[i].SelectedChanged += HandleBlindRelayDownRadioChanged;
                blindTypeRadioGroups[i] = new RadioRootElement2(context, i18n.limitingCondition_type, values, 0, !viewOnly);
                blindTypeRadioGroups[i].SelectedChanged += HandleBlindTypeRadioChanged;
            }

            // ENCODER BLINDS

            var encoderBlindsSection = new CommonSection2(context, i18n.bus_module_blind, true);

            values = new List<string>();
            values.Add(i18n.bus_module_not_configured);
            values.Add("Encoder blind");
            values.Add("Timed blind");
            values.Add("Encoder slat");
            values.Add("Encoder slide");
            values.Add("Timed slide");

            this.encoderBlindConfigedElements = new RadioRootElement2[this.encoderBlinds];
            for (int i = 0; i < this.encoderBlinds; i++)
            {
                var radioGroup = new DeviceRadioRootElement2(context, this.configuration.EncoderBlindsNames[i], this.configuration.CustomEncoderBlindsNames[i], values, 0, !viewOnly);

                radioGroup.SelectedChanged += HandleEncoderBlindConfiguredElementChanged;
                radioGroup.DeviceNameChanged += HandleEncoderBlindDeviceNameChanged;
                this.encoderBlindConfigedElements[i] = radioGroup;
            }

            encoderBlindsSection.AddAll(this.encoderBlindConfigedElements);

            // INPUTS
            values = new List<string>();
            values.Add(i18n.bus_module_not_configured);
            values.Add(i18n.bus_module_reed_contact);
            values.Add(i18n.dashboard_devicePushButton);
            values.Add(i18n.bus_module_impulse_computer);

            var inputsSection = new CommonSection2(context, i18n.bus_module_inputs, true);
            this.inputRadioGroups = new RadioRootElement2[this.digitalInputs];
            for (int i = 0; i < this.digitalInputs; i++)
            {
                var radioGroup = new DeviceRadioRootElement2(context, this.configuration.DigitalInputsNames[i], this.configuration.CustomDigitalInputsNames[i], values, 0, !viewOnly);

                radioGroup.SelectedChanged += HandleInputRadioChanged;
                radioGroup.DeviceNameChanged += HandleDigitalInputDeviceNameChanged;
                this.inputRadioGroups[i] = radioGroup;
            }
            inputsSection.AddAll(this.inputRadioGroups);

            // UNIVERSAL INPUTS
            this.universalInputRadioGroups = new RadioRootElement2[this.universalInputs];
            values = new List<string>();
            values.Add(i18n.bus_module_not_configured);
            values.Add(i18n.bus_module_reed_contact);
            values.Add(i18n.dashboard_devicePushButton);
            values.Add(i18n.bus_module_impulse_computer);
            values.Add("NTC Thermometer");
            values.Add("Analog Input");
            //values.Add(i18n.bus_module_analogInput);
            //values.Add(i18n.bus_module_ntcThermometer);
            var universalInputsSection = new CommonSection2(context, "Universal Inputs", true);

            for (int i = 0; i < this.universalInputs; i++)
            {
                var radioGroup = new DeviceRadioRootElement2(context, this.configuration.UniversalInputsNames[i], this.configuration.CustomUniversalInputsNames[i], values, 0, !viewOnly);
                radioGroup.SelectedChanged += HandleUniversalInputsRadioChanged;
                radioGroup.DeviceNameChanged += HandleUniversalInputDeviceNameChanged;
                this.universalInputRadioGroups[i] = radioGroup;
            }
            universalInputsSection.AddAll(this.universalInputRadioGroups);

            // UNIVERSAL OUTPUTS
            this.universalOutputRadioGroups = new RadioRootElement2[this.universalOutputs];
            values = new List<string>();
            values.Add(i18n.bus_module_not_configured);
            values.Add(i18n.dashboard_deviceSwitch);
            values.Add("LED Dimmer");
            var universalOutputsSection = new CommonSection2(context, "Outputs", true);

            for (int i = 0; i < this.universalOutputs; i++)
            {
                var radioGroup = new DeviceRadioRootElement2(context, this.configuration.UniversalOutputsNames[i], this.configuration.CustomUniversalOutputsNames[i], values, 0, !viewOnly);
                radioGroup.SelectedChanged += HandleUniversalOutputRadioChanged;
                radioGroup.DeviceNameChanged += HandleUniversalOutputDeviceNameChanged;
                this.universalOutputRadioGroups[i] = radioGroup;
            }
            universalOutputsSection.AddAll(this.universalOutputRadioGroups);

            if (this.digitalInputs > 0)
            {
                this.AddSection(inputsSection);
            }

            if (this.digitalOutputs > 0)
            {
                this.AddSection(outputsSection);
            }

            if (this.analogOutputs > 0)
            {
                this.AddSection(analogOutputsSection);
            }

            if (this.ntcThermometers > 0)
            {
                this.AddSection(temperatureSensorsSection);
            }

            if (this.universalInputs > 0)
            {
                this.AddSection(universalInputsSection);
            }

            if (this.encoderBlinds > 0)
            {
                this.AddSection(encoderBlindsSection);
            }

            if (this.universalOutputs > 0)
            {
                this.AddSection(universalOutputsSection);
            }

            this.blindSectionGroups = new CommonSection2[this.blinds];
            for (int i = 0; i < this.blinds; i++)
            {
                var blindSection = new CommonSection2(context, this.configuration.BlindConfigurationsNames[i], true);

                blindSection.Add(this.blindConfigedElements[i]);
                blindSection.Add(this.blindNameGroups[i]);
                blindSection.Add(this.blindTypeRadioGroups[i]);
                blindSection.Add(this.blindRelayUpRadioGroups[i]);
                blindSection.Add(this.blindRelayDownRadioGroups[i]);

                this.blindSectionGroups[i] = blindSection;

                this.AddSection(blindSection);
            }

            if (!viewOnly)
            {
                CommonButton2 deleteButton = new CommonButton2(context, i18n.delete, CommonButton2.Style.Delete);
                deleteButton.Clicked += HandleDeleteButtonClicked;

                var buttonSection = new CommonSection2(context, String.Empty, true);
                buttonSection.Add(deleteButton);
                this.AddSection(buttonSection);

                // do NOT delete - or Dominik will show his revenge
                //Storage.GetTapHomeBusModuleConfigClipboard(Storage.LastUsedLocationId.Value) == null
                /*CommonButton2 copyButton = new CommonButton2(this, "Copy", CommonButton2.Style.Default);
                copyButton.Clicked += CopyButton_Clicked;

                var copyPasteSection = new CommonSection2(this, String.Empty);
                copyPasteSection.Add(copyButton);

                this.AddSection(copyPasteSection);

                var clipboard = Storage.GetTapHomeBusModuleConfigClipboard();

                // if clipboard is not empty and not the same device
                if (clipboard != null &&
                    !(clipboard.LocationId == Storage.LastUsedLocationId.Value && this.board.ID == clipboard.DeviceId) &&
                    (clipboard.SerialNumber.Split('-')[0] == this.board.SerialNumber.Split('-')[0]))
                {
                    String copiedModuleName = String.Empty;

                    var copiedModule = Storage.GetDevice(Storage.LastUsedLocationId.Value, clipboard.DeviceId);

                    if (copiedModule != null)
                    {
                        copiedModuleName = copiedModule.Name;
                    }

                    var pasteDescSection = new CommonSection2(this, String.Empty);
                    pasteDescSection.Footer = String.Format("Copied module {0} will be pasted on this module {1}. " +
                        "This will create same devices as on the copied module.", copiedModuleName, this.board.Name);
                    this.AddSection(pasteDescSection);

                    CommonButton2 pasteButton = new CommonButton2(this, "Paste", CommonButton2.Style.Default);
                    pasteButton.Clicked += PasteButton_Clicked;

                    var pasteSection = new CommonSection2(this, String.Empty);
                    pasteSection.Add(pasteButton);
                    this.AddSection(pasteSection);

                    if (ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion >=
                        ClientCommunicationManager.DeviceTypeMinProtocolVersion)
                    {
                        var replaceDescSection = new CommonSection2(this, String.Empty);
                        replaceDescSection.Footer = String.Format("Replace this module {0} with copied module {1}. Every device from the original module " +
                                "will be assigned to this module", this.board.Name, copiedModuleName);
                        this.AddSection(replaceDescSection);

                        CommonButton2 replaceButton = new CommonButton2(this, "Replace", CommonButton2.Style.Default);
                        replaceButton.Clicked += ReplaceButton_Clicked;

                        var replaceSection = new CommonSection2(this, String.Empty);
                        replaceSection.Add(replaceButton);
                        this.AddSection(replaceSection);
                    }
                }*/

                //if (ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion >=
                //        ClientCommunicationManager.DeviceTypeMinProtocolVersion)
                {
                    CommonButton2 replaceButton = new CommonButton2(context, i18n.general_replace, CommonButton2.Style.Default);
                    replaceButton.Clicked += ReplaceButton_Clicked;

                    var replaceSection = new CommonSection2(context, String.Empty, true);
                    replaceSection.Add(replaceButton);
                    this.AddSection(replaceSection);
                }
            }
        }

        private void HandleBlindTypeRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            int blindIndex = Array.IndexOf(this.blindTypeRadioGroups, sender);
            var type = (sender as RadioRootElement2).SelectedValueIndex;

            switch (type)
            {
                case 0:
                    this.configuration.BlindConfigurations[blindIndex].Type = this.universalOutputs > 0 ?
                        TapHomeBusModuleConfig.TypeBlindOnUO : TapHomeBusModuleConfig.TypeBlind;
                    break;

                case 1:
                    this.configuration.BlindConfigurations[blindIndex].Type = this.universalOutputs > 0 ?
                        TapHomeBusModuleConfig.TypeSlideOnUO : TapHomeBusModuleConfig.TypeSlide;
                    break;

                default:
                    this.configuration.BlindConfigurations[blindIndex].Type = this.universalOutputs > 0 ?
                        TapHomeBusModuleConfig.TypeBlindOnUO : TapHomeBusModuleConfig.TypeBlind;
                    break;
            }

            this.ConfigToControls();
        }

        private void ReplaceButton_Clicked(object sender, EventArgs e)
        {
            ////ShowWarningDialog(false);
            //Dictionary<string, object> parameters = new Dictionary<string, object>();
            //parameters.Add(ReplaceModulePage.DeviceIdExtraKey, this.board.ID);

            //CommonUIPage.NavigateToPageWithParameters((context), typeof(ReplaceModulePage), parameters);
        }

        // do NOT delete - or Dominik will show his revenge
        /*private void CopyButton_Clicked(object sender, EventArgs e)
        {
            CreateToast("Module has been copied to clipboard.");

            var clipboard = new TapHomeBusModuleConfigClipboard()
            {
                DeviceId = this.board.ID,
                SerialNumber = this.board.SerialNumber,
                LocationId = Storage.LastUsedLocationId.Value,
                CreatedOnUtc = DateTime.UtcNow,
                ModuleConfigurationJson  = this.ExportConfig()
            };

            var currentDevices = Storage.GetDevicesById(Storage.LastUsedLocationId.Value).Values.Where(d => d.SerialNumber.Split(':')[0] == this.board.SerialNumber).ToArray();

            var devicesContainer = new DeviceListContainer()
            {
                Devices = currentDevices
            };

            var devicesData = ProtobufSerializer.Serialize(devicesContainer);

            clipboard.ModuleDevicesSerialized = devicesData;

            Storage.PutTapHomeBusModuleConfigClipboard(clipboard);
        }

        private void PasteModule()
        {
            int progressDialogHash = CreateProgressDialog(string.Format(i18n.saving), false, null);

            var clipboard = Storage.GetTapHomeBusModuleConfigClipboard();

            var communicationManager = ApplicationManager.Instance.GetCurrentCommunicationManager();

            if (communicationManager == null)
            {
                DismissProgressDialog(progressDialogHash);
                return;
            }

            var clipboardDevices = ProtobufSerializer.Deserialize<DeviceListContainer>(clipboard.ModuleDevicesSerialized);

            communicationManager.TapHomeBusConfig(this.board.ID, clipboard.ModuleConfigurationJson, t =>
            {
                this.RunOnUiThreadTh(() =>
                {
                    DismissProgressDialog(progressDialogHash);

                    if (t.CompletedWithoutProblemsWithResult())
                    {
                        if (t.Result is MessageTapHomeBusConfigResponse)
                        {
                            var m = t.Result as MessageTapHomeBusConfigResponse;

                            CreateToast(String.Format("Added {0} devices, removed {1} devices",
                                m.AddedDevices == null ? 0 : m.AddedDevices.Count(),
                                m.RemovedDevices == null ? 0 : m.RemovedDevices.Count()));

                            if (m.AddedDevices != null && m.AddedDevices.Length > 0)
                            {
                                Storage.PutDevicesIncrementally(Storage.LastUsedLocationId.Value, m.AddedDevices);
                            }

                            var currentDevices =
                                Storage.GetDevicesById(Storage.LastUsedLocationId.Value)
                                    .Values.Where(d => d.SerialNumber.Split(':')[0] == this.board.SerialNumber)
                                    .ToArray();

                            foreach (var clipboardDevice in clipboardDevices.Devices)
                            {
                                var device = currentDevices.Where(d =>
                                    d.SerialNumber.Split(':').Length > 1 &&
                                    clipboardDevice.SerialNumber.Split(':').Length > 1 &&
                                    d.SerialNumber.Split(':')[1] == clipboardDevice.SerialNumber.Split(':')[1]).FirstOrDefault();

                                if (device == null) // device not found?
                                    continue;

                                //device
                                ApplicationManager.Instance.GetCurrentCommunicationManager().UpdateDeviceProperties(
                                    device.ID,
                                    this.board.Name,
                                    clipboardDevice.DeviceProperties,
                                    (t1) =>
                                    {
                                        this.RunOnUiThreadTh(() =>
                                        {
                                            bool wasOk = t.CompletedWithoutProblemsWithResult();

                                            if (wasOk)
                                                CreateToast("Device " + device.SerialNumber + " properties updated");
                                            else
                                                CreateToast("Error updating Device " + device.SerialNumber + " properties");
                                        });
                                    });
                            }

                            this.GoBack();
                        }
                        else if (t.Result is MessageGenericResponse)
                        {
                            CreateToast(i18n.error + ": " + (t.Result as MessageGenericResponse).ErrorMessage);
                        }
                        else
                        {
                            CreateToast(i18n.bus_module_unknown_response_from_core);
                        }

                        // navigate from
                        StopBlinkingLed();
                        GoBack();
                    }
                    else
                    {
                        var responseMessage = t.Result != null ? t.Result as MessageGenericResponse : null;
                        string errorMessage = responseMessage != null ? responseMessage.ErrorMessage : String.Empty;

                        HandleApiError(true, errorMessage);
                        StopBlinkingLed();
                        GoBack();
                    }
                });
            });
            DismissProgressDialog(progressDialogHash);
        }*/

        private void HandleDeleteButtonClicked(object sender, EventArgs e)
        {
            //var buttons = new CommonUIFrame.AlertDialogButtons();

            //buttons.PositiveButton = new Tuple<string, Action>(i18n.delete, () =>
            //{
            //    DeleteDevice(false);
            //});

            //buttons.NegativeButton = new Tuple<string, Action>(i18n.general_cancel, () =>
            //{
            //});

            //CreateAlertDialog(i18n.device_delete, String.Format(i18n.deleteDevice, this.board.Name), buttons);
        }

        private void DeleteDevice(bool forceDelete)
        {
            //connectingDialogHashCode = CreateProgressDialog(i18n.saving, false, null);

            //try
            //{
            //    ApplicationManager.Instance.GetCurrentCommunicationManager().DeleteDevice(this.board.ID, forceDelete, (t) =>
            //    {
            //        this.RunOnUiThreadTh(() =>
            //        {
            //            DismissProgressDialog(connectingDialogHashCode);

            //            if (t.CompletedWithoutProblemsWithResult())
            //            {
            //                var result = t.Result as MessageGenericResponse;
            //                switch (result.ErrorCode)
            //                {
            //                    case CommonErrorCodes.Ok:
            //                        CreateToast(i18n.device_deleted);
            //                        OnBack?.Invoke(this, null);
            //                        break;

            //                    case CommonErrorCodes.DeserializationException:
            //                        CreateToast((t.Exception == null ? i18n.error_unknown : t.Exception.ToString()));
            //                        break;

            //                    case CommonErrorCodes.Exception:
            //                        string ids = result.ErrorMessage;

            //                        if (ids.IndexOf("{") < 0 || ids.IndexOf("}") < 0)
            //                        {
            //                            CreateToast((t.Exception == null ? i18n.error_unknown : t.Exception.ToString()));
            //                        }
            //                        else
            //                        {
            //                            ids = ids.Substring(ids.IndexOf("{") + 1);
            //                            ids = ids.Substring(0, ids.IndexOf("}"));

            //                            List<int> idsList = ids.Split(',').Select(int.Parse).ToList();

            //                            ShowForceDeleteDialog(idsList);
            //                        }
            //                        break;

            //                }
            //            }
            //            else
            //            {
            //                CreateToast(i18n.core_notConnected);
            //            }
            //        });
            //    });
            //}
            //catch (CoreVersionTooLowException e1)
            //{
            //    CreateToast(e1.Message);
            //}
        }

        private void ShowForceDeleteDialog(List<int> devicesIds)
        {
            //var buttons = new CommonUIFrame.AlertDialogButtons();

            //buttons.PositiveButton = new Tuple<string, Action>(i18n.delete, () =>
            //{
            //    DeleteDevice(true);
            //});

            //buttons.NegativeButton = new Tuple<string, Action>(i18n.general_cancel, () =>
            //{
            //});

            //string message = "These devices have assigned smartrules: \n\n";

            //foreach (var deviceId in devicesIds)
            //{
            //    var device = Storage.GetDevice(Storage.LastUsedLocationId.Value, deviceId);
            //    message += "- " + device.Name + "\n";
            //}

            //message += "\nDo you want to proceed deleting this module?";

            //CreateAlertDialog(i18n.device_delete, message, buttons);
        }

        private void HandleDigitalInputDeviceNameChanged(object sender, DeviceRadioRootElement2.DeviceNameChangedEventArgs e)
        {
            HandleDigitalInputChanged(sender);
        }

        private void HandleInputRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            HandleDigitalInputChanged(sender);
        }

        private void HandleEncoderBlindDeviceNameChanged(object sender, DeviceRadioRootElement2.DeviceNameChangedEventArgs e)
        {
            HandleEncoderBlindChanged(sender);
        }


        private void HandleEncoderBlindChanged(object sender)
        {
            int blindIndex = Array.IndexOf(this.encoderBlindConfigedElements, sender);

            var blindFunction = (sender as RadioRootElement2).SelectedValueIndex;
            var blindName = (sender as DeviceRadioRootElement2).Name;

            this.configuration.CustomEncoderBlindsNames[blindIndex] = blindName;

            switch (blindFunction)
            {
                case 0:
                    this.configuration.EncoderBlindConfigurations[blindIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.NotConfigured;
                    break;

                case 1:
                    this.configuration.EncoderBlindConfigurations[blindIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderBlind;
                    break;

                case 2:
                    this.configuration.EncoderBlindConfigurations[blindIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.TimedBlind;
                    break;

                case 3:
                    this.configuration.EncoderBlindConfigurations[blindIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderSlat;
                    break;

                case 4:
                    this.configuration.EncoderBlindConfigurations[blindIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderSlide;
                    break;

                case 5:
                    this.configuration.EncoderBlindConfigurations[blindIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.TimedSlide;
                    break;

                default:
                    this.configuration.EncoderBlindConfigurations[blindIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.NotConfigured;
                    break;
            }

            this.ConfigToControls();
        }
        private void HandleDigitalInputChanged(object sender)
        {
            int inputIndex = Array.IndexOf(this.inputRadioGroups, sender);
            var inputFunction = (sender as RadioRootElement2).SelectedValueIndex;
            var inputName = (sender as DeviceRadioRootElement2).Name;

            this.configuration.DigitalInputs[inputIndex] = (TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction)inputFunction;
            this.configuration.CustomDigitalInputsNames[inputIndex] = inputName;

            this.ConfigToControls();
        }

        private void HandleBlindNameChanged(object sender, StringElement2.TextViewHelperEventArgs e)
        {
            HandleBlindNameChanged(sender);
        }

        private void HandleBlindNameChanged(object sender)
        {
            int blindIndex = Array.IndexOf(this.blindNameGroups, sender);
            var blindName = (sender as StringElement2).Value;

            this.configuration.CustomBlindConfigurationsNames[blindIndex] = blindName;

            this.ConfigToControls();
        }

        private void HandleBlindRelayUpRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            int blindIndex = Array.IndexOf(this.blindRelayUpRadioGroups, sender);
            var relayNumber = (sender as DoRadioElement2).SelectedRelayNumber;
            this.configuration.BlindConfigurations[blindIndex].TerminalUp = relayNumber;

            this.ConfigToControls();
        }

        private void HandleBlindRelayDownRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            int blindIndex = Array.IndexOf(this.blindRelayDownRadioGroups, sender);
            var relayNumber = (sender as DoRadioElement2).SelectedRelayNumber;
            this.configuration.BlindConfigurations[blindIndex].TerminalDown = relayNumber;

            this.ConfigToControls();
        }

        private void HandleEncoderBlindConfiguredElementChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            HandleEncoderBlindChanged(sender);
        }

        private void HandleBlindConfiguredElementChanged(object sender, EventArgs e)
        {
            int blindIndex = Array.IndexOf(this.blindConfigedElements, sender);
            bool isConfigured = (sender as BooleanElement2).Value;

            this.configuration.BlindConfigurations[blindIndex].IsConfigured = isConfigured;

            this.ConfigToControls();
        }

        private void HandleOutputDeviceNameChanged(object sender, DeviceRadioRootElement2.DeviceNameChangedEventArgs e)
        {
            HandleOutputDeviceChanged(sender);
        }

        private void HandleOutputRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            HandleOutputDeviceChanged(sender);
        }

        private void HandleOutputDeviceChanged(object sender)
        {
            int outputIndex = Array.IndexOf(this.outputRadioGroups, sender);
            var outputFunction = (sender as RadioRootElement2).SelectedValueIndex;
            var outputName = (sender as DeviceRadioRootElement2).Name;

            this.configuration.CustomDigitalOutputsNames[outputIndex] = outputName;

            switch (outputFunction)
            {
                case 0:
                    this.configuration.DigitalOutputs[outputIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.NotConfigured;
                    break;

                case 1:
                    this.configuration.DigitalOutputs[outputIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.Switch;
                    break;

                default:
                    this.configuration.DigitalOutputs[outputIndex] = TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.NotConfigured;
                    break;

            }

            this.ConfigToControls();
        }

        private void HandleAnalogOutputDeviceNameChanged(object sender, DeviceRadioRootElement2.DeviceNameChangedEventArgs e)
        {
            HandleAnalogOutputChanged(sender);
        }

        private void HandleAnalogOutputRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            HandleAnalogOutputChanged(sender);
        }

        private void HandleUniversalOutputDeviceNameChanged(object sender, DeviceRadioRootElement2.DeviceNameChangedEventArgs e)
        {
            HandleUniversalOutputChanged(sender);
        }

        private void HandleUniversalOutputRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            HandleUniversalOutputChanged(sender);
        }

        private void HandleUniversalOutputChanged(object sender)
        {
            int outputIndex = Array.IndexOf(this.universalOutputRadioGroups, sender);
            var outputFunction = (sender as RadioRootElement2).SelectedValueIndex;
            var outputName = (sender as DeviceRadioRootElement2).Name;

            this.configuration.UniversalOutputs[outputIndex] = (TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction)outputFunction;
            this.configuration.CustomUniversalOutputsNames[outputIndex] = outputName;

            this.ConfigToControls();
        }

        private void HandleAnalogOutputChanged(object sender)
        {
            int outputIndex = Array.IndexOf(this.analogOutputRadioGroups, sender);
            var outputFunction = (sender as RadioRootElement2).SelectedValueIndex;
            var outputName = (sender as DeviceRadioRootElement2).Name;

            this.configuration.AnalogOutputs[outputIndex] = (TaphomeBusModuleConfigurationFrame.ModuleConfiguration.AOFunction)outputFunction;
            this.configuration.CustomAnalogOutputsNames[outputIndex] = outputName;

            this.ConfigToControls();
        }

        private void HandleTemperatureSensorDeviceNameChanged(object sender, DeviceRadioRootElement2.DeviceNameChangedEventArgs e)
        {
            HandleTemperatureSensorChanged(sender);
        }

        private void HandleTemperatureSensorsRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            HandleTemperatureSensorChanged(sender);
        }

        private void HandleTemperatureSensorChanged(object sender)
        {
            int outputIndex = Array.IndexOf(this.ntcThermometersRadioGroups, sender);
            var outputFunction = (sender as RadioRootElement2).SelectedValueIndex;
            var outputName = (sender as DeviceRadioRootElement2).Name;

            this.configuration.NtcThermometers[outputIndex] = (TaphomeBusModuleConfigurationFrame.ModuleConfiguration.NTCFunction)outputFunction;
            this.configuration.CustomNtcThermometersNames[outputIndex] = outputName;

            this.ConfigToControls();
        }

        private void HandleUniversalInputDeviceNameChanged(object sender, DeviceRadioRootElement2.DeviceNameChangedEventArgs e)
        {
            HandleUniversalInputChanged(sender);
        }

        private void HandleUniversalInputsRadioChanged(object sender, RadioRootElement2.RadioRootSelectedChangedEventArgs e)
        {
            HandleUniversalInputChanged(sender);
        }

        private void HandleUniversalInputChanged(object sender)
        {
            int inputIndex = Array.IndexOf(this.universalInputRadioGroups, sender);
            var inputFunction = (sender as RadioRootElement2).SelectedValueIndex;
            var inputName = (sender as DeviceRadioRootElement2).Name;

            this.configuration.UniversalInputs[inputIndex] = (TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction)inputFunction;
            this.configuration.CustomUniversalInputsNames[inputIndex] = inputName;

            this.ConfigToControls();
        }

        private void ConfigToControls()
        {
            List<int> availableRelays = new List<int>();

            for (int i = 0; i < this.digitalOutputs; i++)
            {
                switch (configuration.DigitalOutputs[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.NotConfigured:
                        this.outputRadioGroups[i].SelectedValueIndex = 0;
                        this.outputRadioGroups[i].Enabled = !viewOnly;
                        availableRelays.Add(i + 1);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.Switch:
                        this.outputRadioGroups[i].SelectedValueIndex = 1;
                        this.outputRadioGroups[i].Enabled = !viewOnly;
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.Blind:
                        this.outputRadioGroups[i].SelectedValueIndex = 0;
                        this.outputRadioGroups[i].Enabled = false;
                        break;
                }
            }

            for (int i = 0; i < this.digitalInputs; i++)
            {
                this.inputRadioGroups[i].SelectedValueIndex = (int)this.configuration.DigitalInputs[i];
                //                switch (configuration.DigitalInputs[i])
                //                {
                //                    case ModuleConfiguration.DIFunction.NotConfigured:
                //                        this.inputRadioGroups[i].SelectedValueIndex= 0;
                //                }
            }

            for (int i = 0; i < this.analogOutputs; i++)
            {
                this.analogOutputRadioGroups[i].Enabled = !viewOnly;
                this.analogOutputRadioGroups[i].SelectedValueIndex = (int)this.configuration.AnalogOutputs[i];
            }

            for (int i = 0; i < this.ntcThermometers; i++)
            {
                this.ntcThermometersRadioGroups[i].Enabled = !viewOnly;
                this.ntcThermometersRadioGroups[i].SelectedValueIndex = (int)this.configuration.NtcThermometers[i];
            }

            for (int i = 0; i < this.universalOutputs; i++)
            {
                switch (configuration.UniversalOutputs[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.NotConfigured:
                        this.universalOutputRadioGroups[i].SelectedValueIndex = 0;
                        this.universalOutputRadioGroups[i].Enabled = !viewOnly;
                        availableRelays.Add(i + 1);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.Switch:
                        this.universalOutputRadioGroups[i].SelectedValueIndex = 1;
                        this.universalOutputRadioGroups[i].Enabled = !viewOnly;
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.LedDimmer:
                        this.universalOutputRadioGroups[i].SelectedValueIndex = 2;
                        this.universalOutputRadioGroups[i].Enabled = !viewOnly;
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.Blind:
                        this.universalOutputRadioGroups[i].SelectedValueIndex = 0;
                        this.universalOutputRadioGroups[i].Enabled = false;
                        break;
                }
            }

            for (int i = 0; i < this.blinds; i++)
            {
                var blindConfig = this.configuration.BlindConfigurations[i];
                this.blindConfigedElements[i].Value = blindConfig.IsConfigured;


                if (blindConfig.IsConfigured)
                {
                    this.blindRelayUpRadioGroups[i].Enabled = !viewOnly;
                    this.blindRelayDownRadioGroups[i].Enabled = !viewOnly;
                    this.blindNameGroups[i].Enabled = !viewOnly;
                    this.blindConfigedElements[i].Enabled = !viewOnly;
                    this.blindTypeRadioGroups[i].Enabled = !viewOnly;

                    switch (blindConfig.Type)
                    {
                        case TapHomeBusModuleConfig.TypeBlind:
                        case TapHomeBusModuleConfig.TypeBlindOnUO:
                            this.blindTypeRadioGroups[i].SelectedValueIndex = 0;
                            break;

                        case TapHomeBusModuleConfig.TypeSlide:
                        case TapHomeBusModuleConfig.TypeSlideOnUO:
                            this.blindTypeRadioGroups[i].SelectedValueIndex = 1;
                            break;

                        default:
                            this.blindTypeRadioGroups[i].SelectedValueIndex = 0;
                            break;
                    }

                    if (blindConfig.TerminalUp > 0)
                    {
                        availableRelays.Remove(blindConfig.TerminalUp);
                        if (universalOutputs > 0)
                        {
                            this.universalOutputRadioGroups[blindConfig.TerminalUp - 1].Enabled = false;
                            this.universalOutputRadioGroups[blindConfig.TerminalUp - 1].DisabledDetailText =
                                String.Format(i18n.bus_module_blind + " {0} " + i18n.bus_module_up, i + 1);
                        }
                        else
                        {
                            this.outputRadioGroups[blindConfig.TerminalUp - 1].Enabled = false;
                            this.outputRadioGroups[blindConfig.TerminalUp - 1].DisabledDetailText =
                                String.Format(i18n.bus_module_blind + " {0} " + i18n.bus_module_up, i + 1);
                        }
                    }

                    if (blindConfig.TerminalDown > 0)
                    {
                        availableRelays.Remove(blindConfig.TerminalDown);
                        if (universalOutputs > 0)
                        {
                            this.universalOutputRadioGroups[blindConfig.TerminalDown - 1].Enabled = false;
                            this.universalOutputRadioGroups[blindConfig.TerminalDown - 1].DisabledDetailText =
                                String.Format(i18n.bus_module_blind + " {0} " + i18n.bus_module_down, i + 1);
                        }
                        else
                        {
                            this.outputRadioGroups[blindConfig.TerminalDown - 1].Enabled = false;
                            this.outputRadioGroups[blindConfig.TerminalDown - 1].DisabledDetailText =
                                String.Format(i18n.bus_module_blind + " {0} " + i18n.bus_module_down, i + 1);
                        }
                    }
                }
                else
                {
                    this.blindRelayUpRadioGroups[i].Enabled = false;
                    this.blindRelayDownRadioGroups[i].Enabled = false;
                    this.blindNameGroups[i].Enabled = false;
                    this.blindTypeRadioGroups[i].Enabled = false;
                    this.blindConfigedElements[i].Enabled = !viewOnly && availableRelays.Any();

                    this.blindRelayUpRadioGroups[i].DisabledDetailText = "-";
                    this.blindRelayDownRadioGroups[i].DisabledDetailText = "-";
                }
            }

            for (int i = 0; i < this.encoderBlinds; i++)
            {
                this.encoderBlindConfigedElements[i].SelectedValueIndex = (int)this.configuration.EncoderBlindConfigurations[i];
                this.encoderBlindConfigedElements[i].Enabled = true;
            }

            for (int i = 0; i < this.universalInputs; i++)
            {
                this.universalInputRadioGroups[i].Enabled = !viewOnly;
                this.universalInputRadioGroups[i].SelectedValueIndex = (int)this.configuration.UniversalInputs[i];
            }

            RefreshAvailableRelays(availableRelays);
        }

        void RefreshAvailableRelays(List<int> availableRelays)
        {
            // set available relays for pickers in blinds
            for (int i = 0; i < this.blinds; i++)
            {
                var blindConfig = this.configuration.BlindConfigurations[i];
                if (blindConfig.IsConfigured)
                {
                    if (blindConfig.TerminalUp > 0)
                    {
                        List<int> newRelays = new List<int>(availableRelays);
                        newRelays.Add(blindConfig.TerminalUp);
                        newRelays.Sort();
                        this.blindRelayUpRadioGroups[i].ReplaceRelays(newRelays);
                        this.blindRelayUpRadioGroups[i].SelectedRelayNumber = blindConfig.TerminalUp;
                    }
                    else
                    {
                        this.blindRelayUpRadioGroups[i].ReplaceRelays(availableRelays);
                    }

                    if (blindConfig.TerminalDown > 0)
                    {
                        List<int> newRelays = new List<int>(availableRelays);
                        newRelays.Add(blindConfig.TerminalDown);
                        newRelays.Sort();
                        this.blindRelayDownRadioGroups[i].ReplaceRelays(newRelays);
                        this.blindRelayDownRadioGroups[i].SelectedRelayNumber = blindConfig.TerminalDown;
                    }
                    else
                    {
                        this.blindRelayDownRadioGroups[i].ReplaceRelays(availableRelays);
                    }
                }
                else
                {
                    this.blindRelayUpRadioGroups[i].ReplaceRelays(availableRelays);
                    this.blindRelayDownRadioGroups[i].ReplaceRelays(availableRelays);
                }
            }
        }

        public string ExportConfig()
        {
            var result = new TapHomeBusModuleConfig()
            {
                Devices = new List<TapHomeBusModuleConfig.BusDeviceInfo>(),
            };

            // OUTPUTS
            for (int i = 0; i < this.digitalOutputs; i++)
            {
                switch (this.configuration.DigitalOutputs[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.NotConfigured:
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DOFunction.Switch:
                        var deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeSwitch,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.DigitalOutputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomDigitalOutputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;
                }
            }

            // BLINDS
            for (int i = 0; i < this.blinds; i++)
            {
                var blindConfig = this.configuration.BlindConfigurations[i];

                if (blindConfig.IsConfigured)
                {
                    //var type = this.universalOutputs > 0
                    //    ? TapHomeBusModuleConfig.TypeBlindOnUO
                    //    : TapHomeBusModuleConfig.TypeBlind;

                    var output = this.universalOutputs > 0
                        ? "UO"
                        : "DO";

                    var deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                    {
                        Index = i + 1,
                        Type = blindConfig.Type
                    };

                    deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                    deviceInfo.ExtraAttributes.Add(TapHomeBusModuleConfig.TypeBlindTerminalUp, output + blindConfig.TerminalUp.ToString());
                    deviceInfo.ExtraAttributes.Add(TapHomeBusModuleConfig.TypeBlindTerminalDown, output + blindConfig.TerminalDown.ToString());
                    if (!String.IsNullOrEmpty(this.configuration.CustomBlindConfigurationsNames[i]))
                    {
                        deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomBlindConfigurationsNames[i];
                    }
                    result.Devices.Add(deviceInfo);
                }
            }

            // ENCODER BLINDS
            for (int i = 0; i < this.encoderBlinds; i++)
            {
                switch (this.configuration.EncoderBlindConfigurations[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.NotConfigured:
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderBlind:
                        var deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeEncoderBlind,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.EncoderBlindsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomEncoderBlindsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.TimedBlind:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeCurrentLimitedBlind,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.EncoderBlindsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomEncoderBlindsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderSlat:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeEncoderSlat,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.EncoderBlindsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomEncoderBlindsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.EncoderSlide:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeEncoderSlide,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.EncoderBlindsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomEncoderBlindsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.BLEFunction.TimedSlide:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeCurrentLimitedSlide,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.EncoderBlindsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomEncoderBlindsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;
                }
            }

            // INPUTS
            for (int i = 0; i < this.digitalInputs; i++)
            {
                switch (this.configuration.DigitalInputs[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction.NotConfigured:
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction.PushButton:
                        var deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypePushButton,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomDigitalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomDigitalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction.ReedContact:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeReedContact,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomDigitalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomDigitalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.DIFunction.ImpulseCounter:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeImpulseCounter,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomDigitalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomDigitalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;
                }
            }

            // ANALOG OUTPUTS
            for (int i = 0; i < this.analogOutputs; i++)
            {
                switch (this.configuration.AnalogOutputs[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.AOFunction.NotConfigured:
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.AOFunction.Used:
                        var deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeAnalogOutput,
                        };
                        if (!String.IsNullOrEmpty(this.configuration.CustomAnalogOutputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomAnalogOutputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;
                }
            }

            // NTC
            for (int i = 0; i < this.ntcThermometers; i++)
            {
                switch (this.configuration.NtcThermometers[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.NTCFunction.NotConfigured:
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.NTCFunction.Used:
                        var deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeThermometer,
                        };
                        if (!String.IsNullOrEmpty(this.configuration.CustomNtcThermometersNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomNtcThermometersNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;
                }
            }

            // UNIVERSAL INPUTS
            for (int i = 0; i < this.universalInputs; i++)
            {
                switch (this.configuration.UniversalInputs[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.NotConfigured:
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.PushButton:
                        var deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypePushButton,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomUniversalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomUniversalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.ReedContact:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeReedContact,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomUniversalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomUniversalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.ImpulseCounter:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeImpulseCounter,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomUniversalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomUniversalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    /*case ModuleConfiguration.UIFunction.AnalogInput:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            //Type = TapHomeBusModuleConfig.TypeAnalogInput,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomUniversalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomUniversalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;*/

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.AnalogInput:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeAnalogInput,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomUniversalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomUniversalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;


                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UIFunction.NtcThermometer:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeThermometer,
                        };

                        if (!String.IsNullOrEmpty(this.configuration.CustomUniversalInputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomUniversalInputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;
                }
            }

            // UNIVERSAL OUTPUTS
            for (int i = 0; i < this.universalOutputs; i++)
            {
                switch (this.configuration.UniversalOutputs[i])
                {
                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.NotConfigured:
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.Switch:
                        var deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeSwitchOnUO,
                        };
                        if (universalOutputs > 0)
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomUniversalOutputsNames[i];
                        }
                        else
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomAnalogOutputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;

                    case TaphomeBusModuleConfigurationFrame.ModuleConfiguration.UOFunction.LedDimmer:
                        deviceInfo = new TapHomeBusModuleConfig.BusDeviceInfo()
                        {
                            Index = i + 1,
                            Type = TapHomeBusModuleConfig.TypeLedDimmer,
                        };
                        if (!String.IsNullOrEmpty(this.configuration.CustomUniversalOutputsNames[i]))
                        {
                            deviceInfo.ExtraAttributes = new Dictionary<string, string>();
                            deviceInfo.ExtraAttributes[TapHomeBusModuleConfig.DeviceName] = this.configuration.CustomUniversalOutputsNames[i];
                        }
                        result.Devices.Add(deviceInfo);
                        break;
                }
            }

            string resultString = "";//JsonConvert.SerializeObject(result);

            return resultString;
        }
    }
}
