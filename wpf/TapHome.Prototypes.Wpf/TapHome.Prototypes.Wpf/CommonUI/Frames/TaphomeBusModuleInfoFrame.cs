﻿// todo: GK
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using TapHome.Client.Desktop.CommonUI;
//using TapHome.Client.Desktop.CommonUI.Dependencies;
//using TapHome.Client.Desktop.CommonUI.Elements;
//using TapHome.Commons;
//using TapHome.Translations;

//namespace TapHome.Commons.CommonFrames
//{
//	public class TaphomeBusModuleInfoFrame : CommonUIFrame
//	{
//		private Device board;

//		public ClientCommunicationManager CommunicationManager
//		{
//			get { return ApplicationManager.Instance.GetCommunicationManager(Storage.LastUsedLocationId.Value); }
//		}

//		private int initProgressDialogHash, uploadingDialogHash;

//		private LabelElement boardHwRevision;
//		private LabelElement boardSwVersion;
//		private LabelElement protocolVersion;
//		private LabelElement uptime;
//		private LabelElement vcc;
//		private LabelElement temperature;

//		private CommonSection mainSection;

//		private bool viewOnly;

//		private string fileName;

//		private Progress<int> progress;

//		private int currentProgress;

//		public TaphomeBusModuleInfoFrame(Object context, Device board)
//			: base(context)
//		{
//			this.board = board;
//		}

//		private void InitSections()
//		{
//			this.RemoveAllSections();

//			SetLightTheme();

//			var permissions = Storage.GetUserPermissionsForDevice(Storage.LastUsedLocationId.Value, board.ID);

//			if (permissions.HasFlag(UserPermissionForDevice.ServiceSettingsCanView))
//			{
//				viewOnly = true;
//			}

//			if (permissions.HasFlag(UserPermissionForDevice.ServiceSettingsCanEdit))
//			{
//				viewOnly = false;
//			}

//			mainSection = new CommonSection(context, null, true);
//			AddSection(mainSection);

//			protocolVersion = new LabelElement(context, i18n.taphomebus_info_protocolVersion, i18n.taphomebus_info_unknown);
//			boardHwRevision = new LabelElement(context, i18n.taphomebus_info_hwRevision, i18n.taphomebus_info_unknown);
//			boardSwVersion = new LabelElement(context, i18n.taphomebus_info_swRevision, i18n.taphomebus_info_unknown);
//			uptime = new LabelElement(context, i18n.taphomebus_info_uptime, i18n.taphomebus_info_unknown);
//			vcc = new LabelElement(context, i18n.taphomebus_info_vcc, i18n.taphomebus_info_unknown);
//			temperature = new LabelElement(context, i18n.taphomebus_info_temperature, i18n.taphomebus_info_unknown);

//			mainSection.Add(boardHwRevision);
//			mainSection.Add(boardSwVersion);
//			mainSection.Add(protocolVersion);
//			mainSection.Add(uptime);
//			mainSection.Add(vcc);
//			mainSection.Add(temperature);
//		}

//		public override void FrameDidAppear()
//		{
//			InitSections();

//			currentProgress = 0;

//			initProgressDialogHash = CreateProgressDialog(string.Format(i18n.loading), true, null);

//			Storage.OnModuleVccAndTemperatureReceived += Storage_OnModuleVccAndTemperatureReceived;
//			Storage.OnModuleUptimeReceived += Storage_OnModuleUptimeReceived;
//			Storage.OnUpdateProgressReceived += Storage_OnUpdateProgressReceived;

//			CommunicationManager.GetModuleInfo(
//				this.board.ID,
//				(t) =>
//				{
//					this.RunOnUiThreadTh(() =>
//					{
//						if (t.CompletedWithoutProblemsWithResult())
//						{
//							var m = t.Result as MessageGetModuleInfoResponse;
//							if (m != null)
//							{
//								protocolVersion.LabelValue = m.ProtocolVersion.ToString();
//								protocolVersion.Refresh();

//								boardHwRevision.LabelValue = FormatValue.GetFormattedRevision(m.BoardHwRevision);
//								boardHwRevision.Refresh();

//								boardSwVersion.LabelValue = m.BoardSwVersion.ToString();
//								boardSwVersion.Refresh();

//								RefreshUpdateSection(m.BoardSwVersion, m.ArchitectureTypeId == 0 ? ArchitectureType.Xmega : (ArchitectureType)m.ArchitectureTypeId);
//							}

//							DismissProgressDialog(initProgressDialogHash);
//						}
//						else
//						{
//							DismissProgressDialog(initProgressDialogHash);
//							CreateToast(string.Format(i18n.core_notConnected));
//						}
//					});
//				});
//		}

//		private void RefreshUpdateSection(int firmwareVersion, ArchitectureType architectureType)
//		{
//			int version = 0;

//			bool updateFirmware = UpdateFirmware(firmwareVersion, architectureType, out version);

//			if (!viewOnly && updateFirmware &&
//				ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion >=
//				ClientCommunicationManager.FirmwareUpdateMinProtocolVersion)
//			{
//				CommonButton updateFirmwareButton = new CommonButton(context,
//					String.Format(i18n.update_to, version), CommonButton.Style.Default);
//				updateFirmwareButton.Clicked += UpdateFirmwareButton_Clicked;

//				var section = new CommonSection(context, String.Empty, true);
//				AddSection(section);
//				section.Add(updateFirmwareButton);
//			}
//		}

//		private void ShowAlertDialog(string caption, string message)
//		{
//			var buttons = new AlertDialogButtons();

//			buttons.PositiveButton = new Tuple<string, Action>(i18n.general_ok, () => { });

//			CreateAlertDialog(caption, message, buttons);
//		}

//		private void UpdateFirmwareButton_Clicked(object sender, EventArgs e)
//		{
//			var buttons = new AlertDialogButtons();

//			buttons.PositiveButton = new Tuple<string, Action>(i18n.smartrule_phasealarm_phase_result_update, () =>
//			{
//				uploadingDialogHash = CreateResultBasedProgressDialog(i18n.softwareUpdate, i18n.update_uploading, false, out progress,
//					"DoNotUnplug", String.Format(i18n.update_doNotUnplug, "1-2"));

//				// testing only  
//				//CommunicationManager.UpdateModuleFirmwareTest(progress, board.ID);
//				CommunicationManager.UpdateModuleFirmwareTask(progress, fileName, this.board.ID);
//			});

//			buttons.NegativeButton = new Tuple<string, Action>(i18n.general_cancel, () =>
//			{
//			});

//			CreateAlertDialog(String.Format(i18n.update_location, this.board.Name), string.Empty,
//				buttons, "DoNotUnplug", String.Format(i18n.update_doNotUnplug, "1-2"));
//		}

//		private bool UpdateFirmware(int firmwareVersion, ArchitectureType architectureType, out int version)
//		{
//			bool updateCore = false;

//			fileName = String.Empty;
//			version = 0;

//			var firmwareInfo = TaphomeBusFirmwareExtensions.GetCurrentFirmwareInfo(this.board.SerialNumber.Split('-')[0], architectureType);

//			if (firmwareInfo.Version > firmwareVersion)
//			{
//				updateCore = true;
//				fileName = firmwareInfo.FileName;
//				version = firmwareInfo.Version;
//			}

//			return updateCore;
//		}

//		private void Storage_OnUpdateProgressReceived(object sender, Storage.UpdateProgressArgs e)
//		{
//			if (Storage.LastUsedLocationId != null && e.LocationId != Storage.LastUsedLocationId.Value)
//			{
//				return;
//			}

//			if (this.board.ID != e.DeviceId)
//			{
//				return;
//			}

//			this.RunOnUiThreadTh(() =>
//			{
//				switch (e.UpdateCode)
//				{
//					case CommonErrorCodes.UpdateStarted:
//						FirmwareUpdateStarted();
//						break;

//					case CommonErrorCodes.UpdateFailed:
//						DismissProgressDialog(uploadingDialogHash);
//						CreateToast("Update failed.");
//						break;

//					case CommonErrorCodes.AlreadyInProgress:
//						if (currentProgress > 12)
//						{
//							return;
//						}
//						DismissProgressDialog(uploadingDialogHash);
//						CreateToast("Firmware update already in progress.");
//						break;

//					case CommonErrorCodes.UpdateFinished:
//						DismissProgressDialog(uploadingDialogHash);
//						CreateToast("Update successful.");
//						FrameDidAppear();
//						break;
//				}
//			});
//		}

//		private void FirmwareUpdateStarted()
//		{
//			if (uploadingDialogHash == 0)
//			{
//				return;
//			}

//			ChangeResultBasedProgressDialogMessage(uploadingDialogHash, i18n.update_inProgress);

//			Task.Factory.StartNew(() =>
//			{
//				for (int i = 11; i < 90; i++)
//				{
//					currentProgress = i;
//					(progress as IProgress<int>).Report(i);
//					Thread.Sleep(250);
//				}
//			}).ContinueWith((t2) =>
//			{
//				ChangeResultBasedProgressDialogMessage(uploadingDialogHash, i18n.update_almostDone);

//				for (int i = 91; i < 100; i++)
//				{
//					currentProgress = i;
//					(progress as IProgress<int>).Report(i);
//					Thread.Sleep(500);
//				}
//			});
//		}

//		public override void FrameDidDisappear()
//		{
//			base.FrameDidDisappear();

//			Storage.OnModuleVccAndTemperatureReceived -= Storage_OnModuleVccAndTemperatureReceived;
//			Storage.OnModuleUptimeReceived -= Storage_OnModuleUptimeReceived;
//			Storage.OnUpdateProgressReceived -= Storage_OnUpdateProgressReceived;
//		}

//		private void Storage_OnModuleUptimeReceived(object sender, Storage.ModuleUptimeArgs e)
//		{
//			this.RunOnUiThreadTh(() =>
//			{
//				uptime.LabelValue = ConvertUptimeToString(e.Uptime);
//				uptime.Refresh();
//			});
//		}

//		private void Storage_OnModuleVccAndTemperatureReceived(object sender, Storage.ModuleVccAndTemperatureArgs e)
//		{
//			this.RunOnUiThreadTh(() =>
//			{
//				vcc.LabelValue = FormatValue.GetFormattedModuleVcc(e.Vcc);
//				vcc.Refresh();

//				temperature.LabelValue = FormatValue.GetFormattedModuleTemperature(e.Temperature);
//				temperature.Refresh();
//			});
//		}

//		private string ConvertUptimeToString(long uptime)
//		{
//			return TimeSpan.FromMilliseconds(uptime).ToStringLong();
//		}
//	}
//}
