﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using TapHome.Prototypes.Wpf.CommonUI.Elements;

namespace TapHome.Prototypes.Wpf.CommonUI
{
    /// <summary>
    /// Interaction logic for CommonSection.xaml
    /// </summary>
    public partial class CommonSection : StackPanel
    {
        public List<Control> Elements = new List<Control>();

        private string header, footer;

        private int elementsWidth = -2;

        public string Footer
        {
            get
            {
                return this.footer;
            }
            set
            {
                this.footer = value;

                if (this.footerLabel != null)
                {
                    this.footerLabel.Content = value;

                    RefreshFooterVisibility();
                }
            }
        }

        public string Header
        {
            get
            {
                return this.header;
            }
            set
            {
                this.header = value;
                if (this.headerLabel != null)
                {
                    this.headerLabel.Content = value;
                }
            }
        }

        // todo: GK - staci iba caption v konstruktore a treba aj nastaivt
        public CommonSection(object context, string caption, bool lightTheme = false)
        {
            this.InitializeComponent();

            this.Header = caption;
        }

        public void Add(Control view)
        {
            this.Elements.Add(view);

            //var footerIndex = this.mainStackPanel.Children.IndexOf(this.footerLabel);
            this.mainStackPanel.Children.Add(view);
            UpdateElementSeparators();
        }

        // hide footer when empty (to save vertical space)
        private void RefreshFooterVisibility()
        {
            if (String.IsNullOrEmpty(this.footer))
            {
                this.footerLabel.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.footerLabel.Visibility = Visibility.Visible;
            }
        }

        //public void Clear()
        //{
        //    this.Elements.Clear();
        //    this.mainStackPanel.Items.Clear();
        //    this.mainStackPanel.Items.Add(this.headerStackLayoutItem); // put back header & footer
        //    this.mainStackPanel.Items.Add(this.footerStackLayoutItem);
        //}

        // todo: GK - skusit s niecim inym ako Panel .....
        public void InsertAt(Control view, int index)
        {
            this.Elements.Insert(index, view);

            this.mainStackPanel.Children.Insert(index, view);
            UpdateElementSeparators();
        }

        public void AddAll(IEnumerable<Control> controls)
        {
            foreach (var control in controls)
            {
                this.Add(control);
            }
            UpdateElementSeparators();
        }

        //private int DesiredElementWidth()
        //{
        //    if (this.Width < this.mainStackPanel.Padding.Left * 2)
        //        return -1;

        //    return this.Width - this.mainStackPanel.Padding.Left * 2 - 2;
        //}

        //public void SetWidth(int width)
        //{
        //    //if (!Application.Instance.Platform.IsWpf)
        //    if (!CommonUIHost.UseManualResizing())
        //        return;

        //    if (this.elementsWidth == width)
        //        return;

        //    if (width < this.stackLayout.Padding.Left * 2)
        //        return;

        //    this.Width = width - 2;// -this.stackLayout.Padding.Left * 2;


        //    this.headerLabel.Width = DesiredElementWidth();// width - this.stackLayout.Padding.Left * 2;
        //    this.footerLabel.Size = new Size(DesiredElementWidth(), -1);// width - this.stackLayout.Padding.Left * 2;

        //    foreach (var control in this.Elements)
        //    {
        //        control.Size = new Size(DesiredElementWidth(), -1);
        //    }

        //    this.Size = new Size(width - 2, -1); // recompute height

        //    this.elementsWidth = width;
        //}
        private void UpdateElementSeparators()
        {
            for (var i = 0; i < Elements.Count; i++)
            {
                var currentElement = Elements[i] as ElementBase;
                if (currentElement == null)
                    continue;

                var isFirstElement = i == 0;
                var isLastElement = i - 1 == Elements.Count;

                currentElement.LineUpVisible = isFirstElement;
                currentElement.LineDownVisible = !isLastElement;
            }
        }

    }
}
