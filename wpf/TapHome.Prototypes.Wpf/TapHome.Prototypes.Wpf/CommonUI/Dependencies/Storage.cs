﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
	public enum ArchitectureType
	{
		None = 0,
		Xmega = 1,
		STM = 2,
	}

	[Flags]
	public enum UserPermissionForDevice : int
	{
		None = 0x00,
		IsSet = 0x01,
		VisibilityAndValuesCanView = 0x02,
		VisibilityAndValuesCanEdit = 0x04,
		SmartRulePrioritiesCanView = 0x08,
		SmartRulePrioritiesCanEdit = 0x10,
		ServiceSettingsCanView = 0x20,
		ServiceSettingsCanEdit = 0x40,
		ManualOverrideCanView = 0x80,
		ManualOverrideCanEdit = 0x100,
		ManualOverrideSettingsCanView = 0x200,
		ManualOverrideSettingsCanEdit = 0x400,
		NameAppearanceCanView = 0x800,
		NameAppearanceCanEdit = 0x1000,
		StatisticsCanView = 0x2000,
		StatisticsCanEdit = 0x4000,
		ChartsCanView = 0x8000,
		ChartsCanEdit = 0x10000,
		UserPermissionsCanView = 0x20000,
		UserPermissionsCanEdit = 0x40000,

		AccessForbidden = IsSet | None,
		ViewPermissions = IsSet | VisibilityAndValuesCanEdit | SmartRulePrioritiesCanView | ManualOverrideCanEdit | StatisticsCanView | ChartsCanView,
		EditPermissions = IsSet | VisibilityAndValuesCanEdit | SmartRulePrioritiesCanEdit | ServiceSettingsCanView | ManualOverrideCanEdit | ManualOverrideSettingsCanEdit | NameAppearanceCanEdit | StatisticsCanEdit | ChartsCanView | UserPermissionsCanView,
		AdvancedPermissions = IsSet | VisibilityAndValuesCanEdit | SmartRulePrioritiesCanEdit | ServiceSettingsCanEdit | ManualOverrideCanEdit | ManualOverrideSettingsCanEdit | NameAppearanceCanEdit | StatisticsCanEdit | ChartsCanView | UserPermissionsCanEdit,
	}

	public class Storage
	{

		public class ModuleVccAndTemperatureArgs : EventArgs
		{
			public readonly int DeviceId;
			public readonly Guid LocationId;

			public readonly int Vcc;

			public readonly int Temperature;

			public ModuleVccAndTemperatureArgs(Guid locationId, int vcc, int temperature, int deviceId)
			{
				LocationId = locationId;
				Vcc = vcc;
				Temperature = temperature;
				DeviceId = deviceId;
			}
		}
		public static event EventHandler<ModuleVccAndTemperatureArgs> OnModuleVccAndTemperatureReceived;

		public class ModuleUptimeArgs : EventArgs
		{
			public readonly int DeviceId;
			public readonly Guid LocationId;

			public readonly long Uptime;
			public ModuleUptimeArgs(Guid locationId, long uptime, int deviceId)
			{
				LocationId = locationId;
				Uptime = uptime;
				DeviceId = deviceId;
			}
		}
		public static event EventHandler<ModuleUptimeArgs> OnModuleUptimeReceived;

		public class UpdateProgressArgs : EventArgs
		{
			public readonly Guid LocationId;
			public readonly int DeviceId;
			public readonly int UpdateCode;
			public readonly string ErrorMessage;

			public UpdateProgressArgs(Guid locationId, int deviceId, int updateCode, string errorMessage)
			{
				this.LocationId = locationId;
				this.DeviceId = deviceId;
				this.UpdateCode = updateCode;
				this.ErrorMessage = errorMessage;
			}
		}
		public static event EventHandler<UpdateProgressArgs> OnUpdateProgressReceived;
		
		public static GenericDevice GetDeviceBySN(object value, string deviceSerial)
		{
			return new GenericDevice();
		}

	    public static Guid? LastUsedLocationId
	    {
	        get
	        {
	            return new Guid();
	        }
            set
	        {
	            
	        }
	    }

	    public static UserPermissionForDevice GetUserPermissionsForDevice(Guid value, int deviceId)
		{
			return UserPermissionForDevice.AdvancedPermissions;
		}

		public static GenericDevice GetDevice(Guid value, int deviceId)
		{
			return new GenericDevice();
		}

		public static Dictionary<int, GenericDevice> GetDevicesById(Guid locationId)
		{
			return new Dictionary<int, GenericDevice>
			{
				{ 1, new GenericDevice() },
				{ 2, new GenericDevice() },
			};
		}

	    public static LocationInfo GetLocalCoreInfo(Guid locationId)
	    {
	        return new LocationInfo()
	        {
	            AccessToken = "************1234",
                AuthorizationToken = new Guid(),
                IpOnLocalNetwork = "192.168.100.123",
                LocationId = locationId,
                Name = "Location name",
	        };
	    }
    }
}
	
