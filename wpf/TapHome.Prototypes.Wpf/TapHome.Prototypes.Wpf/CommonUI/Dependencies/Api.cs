using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{

    public class CommunicationsConstants
    {
        const string HeaderPrefix = "X-TapHome-";
        public const string HeaderAuthToken = HeaderPrefix + "AuthToken";
        public const string HeaderLastTimeUpdated = HeaderPrefix + "LastTimeUpdated";
        public const string HeaderOverwriteWarning = HeaderPrefix + "OverwriteWarning";
        public const string HeaderMissing = HeaderPrefix + "Missing";
        public const string HeaderAllDevices = HeaderPrefix + "AllDevices";
        public const string HeaderMobileDeviceId = HeaderPrefix + "MobileDeviceId";
        public const string HeaderTime = HeaderPrefix + "Time";

        public const string KeyAction = "action";
        public const string KeyActionSwitch = "switch";
        public const string KeyActionMultiSwitch = "multi-switch";
        public const string KeyActionTemperature = "temperature";
        public const string KeyActionFanLevel = "fan-level";
        public const string KeyActionDimLevel = "dim-level";
        public const string KeyActionBlindLevel = "blind-level";
        public const string KeyActionSlideLevel = "slide-level";
        public const string KeyActionBlindSlope = "blind-slope";
        public const string KeyActionHueValue = "hue-value";
        public const string KeyActionPushButton = "push-button";
        public const string KeyActionPushButtonHeld = "push-button-held";
        public const string KeyActionAlarmMode = "alarm";
        public const string KeyActionHueValueAndState = "hue-value-and-state";
        public const string KeyActionManualAction = "manual-action";
        public const string KeyActionInstallMode = "install-mode";
        public const string KeyActionModuleConfig = "set-module-config";
        public const string KeyActionModuleBlinkLedFewTimes = "module-led-few";
        public const string KeyActionModuleStartBlinkingLed = "module-led-start";
        public const string KeyActionModuleStopBlinkingLed = "module-led-stop";
        public const string KeyActionMultiValueSwitch = "multi-value-switch";
        public const string KeyActionBlindStop = "blind-stop";
        public const string KeyActionBlindForceUpForTime = "blind-force-up-for-time";
        public const string KeyActionBlindForceDownForTime = "blind-force-down-for-time";
        public const string KeyActionBlindForceUpIndefinetelyStart = "blind-force-up-indefinetely-start";
        public const string KeyActionBlindForceUpIndefinetelyStop = "blind-force-up-indefinetely-stop";
        public const string KeyActionBlindForceDownIndefinetelyStart = "blind-force-down-indefinetely-start";
        public const string KeyActionBlindForceDownIndefinetelyStop = "blind-force-down-indefinetely-stop";
        public const string KeyActionBlindDeclareCurrentPositionAs = "blind-declare-current-position-as";
        public const string KeyActionVariableSetValue = "variable-set-value";
        public const string KeyPlatform = "platform";
        public const string KeyPushId = "push-id";
        public const string KeyName = "name";
        public const string KeyLogin = "login";
        public const string KeyPassword = "password";
        public const string KeyInstallation = "installation";
        public const string KeyAccessToken = "token";
        public const string KeyLastTimeUpdated = "lastTimeUpdated";
        public const string KeyConfig = "config";
        public const string KeySmartRules = "smartRules";
        public const string KeySmartRulesState = "smartRulesState";
        public const string KeyRuleId = "rule-id";
        public const string KeyLogs = "logs";
        public const string KeyHistogram = "histogram";
        public const string KeyTitle = "title";
        public const string KeyMessage = "message";
        public const string KeyMobileDeviceIds = "mobile-device-ids";
        public const string KeyIcon = "icon";
        public const string KeyInterface = "interface";
        public const string KeyDeviceIds = "deviceIds";
        public const string KeySilent = "silent";
        public const string KeyType = "type";
        public const string KeyData = "data";
        public const string KeyDeviceId = "device-id";
        public const string KeyActivate = "activate";
        public const string KeyModel = "model";
        public const string KeyDuration = "duration";
        public const string KeyModuleConfig = "module-config";

        public const string KeyArgState = "state";
        public const string KeyArgValue = "value";
        public const string KeyArgSwitchIndex = "switch-index";
        public const string KeyArgTemperature = "temperature";
        public const string KeyArgLevel = "level";
        public const string KeyArgMode = "mode";
        public const string KeyArgPlatformAndroid = "Android";
        public const string KeyArgPlatformIOS = "IOS";
    }
}
