using System;
using System.Collections.Generic;
using System.Linq;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{

    public class FirmawareInfo
    {
        public string Type;
        public ArchitectureType ArchitectureType;
        public int Version;
        public string FileName;
    }

    public static class TaphomeBusFirmwareExtensions
    {
        public static FirmawareInfo GetCurrentFirmwareInfo(string type, ArchitectureType architectureType)
        {
            int version = 0;
            string fileName = String.Empty;

            var firmwareVersions = GetCurrentFirmwareVersions();

            var result = firmwareVersions.FirstOrDefault(f => f.Type == type && f.ArchitectureType == architectureType);

            if (result != null)
            {
                return result;
            }

            return new FirmawareInfo()
            {
                Version = version,
                FileName = fileName,
            };
        }

        private static List<FirmawareInfo> GetCurrentFirmwareVersions()
        {
            var result = new List<FirmawareInfo>();

            var versionsString = "asd";

            if (string.IsNullOrEmpty(versionsString))
            {
                return result;
            }

            try
            {
                var pairs = versionsString.Replace("\r","").Split('\n');
                var arrays = pairs.Select(item => item.Split(',')).Where(s => s.Length > 1);

                foreach (var array in arrays)
                {
                    result.Add(new FirmawareInfo()
                    {
                        Type = array[0],
                        ArchitectureType = array[1] == "STM" ? ArchitectureType.STM : ArchitectureType.Xmega,
                        FileName = array[2],
                        Version = int.Parse(array[3]),
                    });
                }
            }
            catch { }

            return result;
        }
    }
}
