﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
    public class CommonErrorCodes
    {
        //public const int NA = 0,
        public const int Ok = 1;
        public const int Unauthorized = 2;
        public const int Exception = 3;
        public const int ParameterTooShortOrLong = 4;
        public const int AlreadyExist = 5;
        public const int DoesNotExist = 6;
        public const int InvalidRequest = 7;
        public const int MissingEmail = 8;
        public const int WrongEmailFormat = 9;
        public const int MissingPassword = 10;
        public const int PasswordTooShort = 11;
        public const int WrongEmailOrPassword = 12;
        public const int MissingParameter = 13;
        public const int AlreadyInProgress = 14;
        public const int DeserializationException = 15;
        public const int IncompatibleCoreSmartRuleVersion = 16;
        public const int UpdateStarted = 17;
        public const int UpdateFinished = 18;
        public const int UpdateFailed = 19;
	}
	
}
