using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
#if ANDROID
using Android.App;
using Com.TapHome.Android.Utils;
#endif
//using TapHome.Commons;
//using WebSocket4Net;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
	public class ConnectionClosedException : Exception
	{
		public ConnectionClosedException(string message) : base(message)
		{
		}
	}

	public class InvalidResponseException : Exception
	{
		public InvalidResponseException(string message) : base(message)
		{
		}
	}

	public class CoreVersionTooLowException : Exception
	{
		public CoreVersionTooLowException(string message) : base(message)
		{
		}
	}

	public class AwaitingRequestDescription
	{
		public int RequesstId { get; set; }

		public DateTime RequestedOnUtc { get; set; }

		public MessageBase RequestMessage { get; set; }
	}

	public class BroadcastEventArgs : EventArgs
	{
		public Guid LocationId { get; set; }

		public MessageBase BroadcastMessage { get; set; }
	}

	public class RequestStatus
	{
		public int CurrentRequest { get; set; }

		public Dictionary<int, AwaitingRequestDescription> AwaitingRequest
		{
			get { return awaitingRequest; }

			set { awaitingRequest = value; }
		}

		private Dictionary<int, AwaitingRequestDescription> awaitingRequest = new Dictionary<int, AwaitingRequestDescription>();
	}


	public class ClientCommunicationManager
	{
#if ANDROID_APP
        private const string Platform = WebApiWrapper.KeyArgPlatformAndroid;
#elif IOS
        private const string Platform = WebApiWrapper.KeyArgPlatformIOS;
#elif DESKTOP
        private const string Platform = WebApiWrapper.KeyArgPlatformDesktop;
#endif

		private const int PingInterval = 6000;

		public static int LimitingConditionMinProtocolVersion = 2;

		public static int StatisticsMinProtocolVersion = 3;

		public static int DeviceTypeMinProtocolVersion = 4;

		public static int SlaveInterfacesMinProtocolVersion = 5;

		public static int CcuManagementMinProtocolVersion = 6;

		public static int PreciseAngleMinProtocolVersion = 7;

		public static int VariableMinProtocolVersion = 8;

		public static int NewMaunalOverridesMinProtocolVersion = 9;

		public static int FirmwareUpdateMinProtocolVersion = 10;

		public static int SafetyTriggerProtocolVersion = 11;

		public static int DashboardManagementMinProtocolVersion = 12;

		public const int ClientProtocolVersion = 12;

		// EVENTS

		public static event EventHandler<BroadcastEventArgs> OnBroadcastMessage;

		public event EventHandler<MessageBase> OnNewMessage;

		public event EventHandler OnError;

		public event EventHandler OnConnectionClosed;

		public event EventHandler OnConnected;

		// PROPERTIES

		public Guid LocationId { get; private set; }


		public int CurrentCoreProtocolVersion { get; private set; }

		public string CoreVersionInfo { get; private set; }

		// PRIVATE

		private enum DesiredConnectionMode
		{
			ShouldNotBeConnected,
			ShouldBeConnected,
		}

		private bool cloudSocketConnectingInitiated = false;

		private bool localSocketConnectingInitiated = false;

		private object requestLock = new object();

		private object connecDisconnectLock = new object();

		public ClientCommunicationManager(Guid locationId)
		{
		    CurrentCoreProtocolVersion = 100;

		}
		public void UpdateDeviceSerialNumber(int deviceId, string serialNumber, Action<Task<MessageBase>> responseHandler)
		{
			//MessageUpdateDeviceSerialNumberRequest message = new MessageUpdateDeviceSerialNumberRequest()
			//{
			//	DeviceId = deviceId,
			//	SerialNumber = serialNumber,
			//};

			//var task = this.SendMessageAsync(message);
			//task.FlushExceptions();
			//task.ContinueWith(responseHandler);
		}

		public void DeleteDevice(int deviceId, bool forceDelete, Action<Task<MessageBase>> responseHandler)
		{
			//if (this.CurrentCoreProtocolVersion < MessageDeleteDeviceRequest.MinVersionWithDeleteSupport)
			//{
			//	throw new CoreVersionTooLowException(String.Format("Core protocol version is {0}, required is {1}, please upgrade Core or contact support", this.CurrentCoreProtocolVersion, MessageDeleteDeviceRequest.MinVersionWithDeleteSupport));
			//}

			//MessageDeleteDeviceRequest message = new MessageDeleteDeviceRequest()
			//{
			//	DeviceId = deviceId,
			//	ForceDelete = forceDelete
			//};

			//var task = this.SendMessageAsync(message);
			//task.FlushExceptions();
			//task.ContinueWith(responseHandler);
		}

		public void RestartCore(Action<Task<MessageBase>> responseHandler)
		{
			//MessageRestartCoreRequest message = new MessageRestartCoreRequest()
			//{
			//};

			//var task = this.SendMessageAsync(message);
			//task.FlushExceptions();
			//task.ContinueWith(responseHandler);
		}

		public void UpdateDeviceName(int deviceId, string deviceName, Action<Task<MessageBase>> responseHandler)
		{
			//MessageUpdateDeviceNameRequest message = new MessageUpdateDeviceNameRequest()
			//{
			//	DeviceId = deviceId,
			//	Name = deviceName,
			//};

			//var task = this.SendMessageAsync(message);
			//task.FlushExceptions();
			//task.ContinueWith(responseHandler);
		}

		public void UpdateDeviceProperties(int deviceId, string deviceName, Dictionary<string, string> newDeviceProperties, Action<Task<MessageBase>> responseHandler)
		{
			//MessageUpdateDevicePropertiesRequest message = new MessageUpdateDevicePropertiesRequest()
			//{
			//	DeviceId = deviceId,
			//	DeviceProperties = newDeviceProperties,
			//	DeviceName = deviceName,
			//};

			//var task = this.SendMessageAsync(message);
			//task.FlushExceptions();
			//task.ContinueWith(responseHandler);
		}

		public void TapHomeBusConfig(int deviceId, string configJson, Action<Task<MessageBase>> responseHandler)
		{
			//MessageTapHomeBusConfigRequest message = new MessageTapHomeBusConfigRequest()
			//{
			//	DeviceId = deviceId,
			//	ConfigJson = configJson
			//};

			//var task = this.SendMessageAsync(message);
			//task.FlushExceptions();
			//task.ContinueWith(responseHandler);
		}

		public void DeviceAction(string keyAction, int deviceId, string deviceName, Action<Task<MessageBase>> responseHandler, params object[] parameters)
		{
			//MessageDeviceAction message = new MessageDeviceAction()
			//{
			//	Action = keyAction,
			//	DeviceId = deviceId,
			//	DeviceName = deviceName,
			//	Parameters = parameters
			//};

			//var task = SendMessageAsync(message);
			//task.FlushExceptions();
			//task.ContinueWith(responseHandler);
		}
		public Task UpdateModuleFirmwareTask(Progress<int> progress, string file, int moduleId)
		{
			return Task.Factory.StartNew(() =>
			{
				//using (var stream = FirmwareUpdateResources.GetFirmwarePackageStream(file))
				//{
				//	int streamLength = (int)stream.Length;

				//	while (stream.Position != stream.Length)
				//	{
				//		var updateTask = this.UpdateModuleFirmwareWithPackageFromStream(stream, file, moduleId);

				//		try
				//		{
				//			updateTask.Wait(10000);
				//		}
				//		catch (Exception e)
				//		{
				//			(progress as IProgress<int>).Report(-1); // update error
				//			break;
				//		}

				//		int streamPosition = (int)stream.Position; // we store this to use it in another thread

				//		updateTask.ContinueWith(t =>
				//		{
				//			if (t.CompletedWithoutProblemsWithResult())
				//			{
				//				(progress as IProgress<int>).Report((int)(10 * (((float)streamPosition) / (float)streamLength)));
				//				// if 100% Core will restart, indeterminate progress bar until connected
				//			}
				//			else
				//			{
				//				//Console.WriteLine("Exception: " + t.Exception);
				//			}
				//		});
				//	}
				//}
			});
		}

		public void GetModuleInfo(int deviceId, Action<Task<MessageBase>> responseHandler)
		{
			//MessageGetModuleInfoRequest message = new MessageGetModuleInfoRequest()
			//{
			//	ModuleId = deviceId
			//};

			//var task = this.SendMessageAsync(message);
			//task.FlushExceptions();
			//task.ContinueWith(responseHandler);
		}
	}
}
