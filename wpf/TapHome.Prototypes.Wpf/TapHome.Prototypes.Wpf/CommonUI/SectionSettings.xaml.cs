﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TapHome.Prototypes.Wpf.CommonUI.Elements;

namespace TapHome.Prototypes.Wpf.CommonUI
{
    /// <summary>
    /// Interaction logic for SectionElements.xaml
    /// </summary>
    public partial class SettingsSection : UserControl
    {
        public SettingsSection()
        {
            InitializeComponent();

            this.elementsItemsControl.ItemTemplateSelector = new SectionElementsDataTemplateSelector();
        }
    }

    public class SectionElementsDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement elemnt = container as FrameworkElement;
            if (item is SliderElement2)
            {
                return elemnt.FindResource("SliderElementTemplate") as DataTemplate;
            }
            else if (item is DeviceRadioRootElement2)
            {
                return elemnt.FindResource("DeviceRadioRootElementTemplate") as DataTemplate;
            }
            else if (item is DoRadioElement2)
            {
                return elemnt.FindResource("DoRadioElementTemplate") as DataTemplate;
            }
            else if (item is RangeSliderElement2)
            {
                return elemnt.FindResource("RangeSliderElementTemplate") as DataTemplate;
            }
            else if (item is StringElementWithMenu2)
            {
                return elemnt.FindResource("StringElementWithMenuTemplate") as DataTemplate;
            }
            else if (item is StringElement2)
            {
                return elemnt.FindResource("StringElementTemplate") as DataTemplate;
            }
            else if (item is DateElement2)
            {
                return elemnt.FindResource("DateElementTemplate") as DataTemplate;
            }
            else if (item is LabelElement2)
            {
                return elemnt.FindResource("LabelElementTemplate") as DataTemplate;
            }
            else if (item is BooleanElement2)
            {
                return elemnt.FindResource("BooleanElementTemplate") as DataTemplate;
            }
            else if (item is RadioRootElement2)
            {
                if ((item as RadioRootElement2).Values.Count > 3)
                {
                    return elemnt.FindResource("RadioRootComboBoxElementTemplate") as DataTemplate;
                }
                else
                {
                    return elemnt.FindResource("RadioRootRadioListElementTemplate") as DataTemplate;
                }
            }
            else if (item is CommonButton2)
            {
                return elemnt.FindResource("CommonButtonTemplate") as DataTemplate;
            }
            else if (item is RolesElement2)
            {
                return elemnt.FindResource("RolesElementTemplate") as DataTemplate;
            }
            else if (item is PasswordElement2)
            {
                return elemnt.FindResource("PasswordElementTemplate") as DataTemplate;
            }
            else if (item is EquationElement2)
            {
                return elemnt.FindResource("EquationElementTemplate") as DataTemplate;
            }
            return elemnt.FindResource("ItemTemplate") as DataTemplate;
        }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class BoolToVisibilityConverter : IValueConverter
    {
        public Visibility TrueValue { get; set; }
        public Visibility FalseValue { get; set; }

        public BoolToVisibilityConverter()
        {
            // set defaults
            TrueValue = Visibility.Visible;
            FalseValue = Visibility.Collapsed;
        }

        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return null;
            return (bool)value ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (Equals(value, TrueValue))
                return true;
            if (Equals(value, FalseValue))
                return false;
            return null;
        }
    }
}
