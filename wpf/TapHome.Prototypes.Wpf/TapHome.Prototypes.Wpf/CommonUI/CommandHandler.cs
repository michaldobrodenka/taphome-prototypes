﻿using System;
using System.Windows.Input;

namespace TapHome.Prototypes.Wpf.CommonUI
{
    public class CommandHandler : ICommand
    {
        private Action action;
        private Action<object> actionWithParameter;
        private bool canExecute;
        public CommandHandler(Action action, bool canExecute)
        {
            this.action = action;
            this.canExecute = canExecute;
        }
        public CommandHandler(Action<object> action, bool canExecute)
        {
            this.actionWithParameter = action;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter) => canExecute;
        public void Execute(object parameter)
        {
            actionWithParameter?.Invoke(parameter);
            action?.Invoke();
        }
        public event EventHandler CanExecuteChanged;
    }
}
