using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
    public class ApplicationManager
    {
        private const int DisconnectAfterPauseDelay = 90;

        public static ApplicationManager Instance { get; private set; }

        private object connectLock = new object();

        public event EventHandler<Guid> OnConnectionClosed;

        public event EventHandler<Guid> OnConnected;
        
        public event EventHandler<Guid> OnLocationChanged;

        public event EventHandler OnLoggedOut;

        private bool IsInForeground;

        private Timer disconnectTimer;


        private Dictionary<Guid, ClientCommunicationManager> communicationManagers;

        const int maxMessages = 500;
		

        public static readonly object SystemLogLock = new object();

        public ApplicationManager()
        {
            communicationManagers = new Dictionary<Guid, ClientCommunicationManager>();
            communicationManagers[Storage.LastUsedLocationId.Value] = new ClientCommunicationManager(Storage.LastUsedLocationId.Value);
        }

        public static void Init()
        {
            Instance = new ApplicationManager();

        }



        //        public IDeviceManager GetCurrentDeviceManager()
        //        {
        //#if ANDROID || IOS
        //            return (IDeviceManager)this.Log; // currently ILogger is also IDeviceManager, need to be nicer in future
        //#endif
        //        }



        public ClientCommunicationManager GetCurrentCommunicationManager()
	    {
		    if (Storage.LastUsedLocationId != null)
		    {
			    return this.GetCommunicationManager(Storage.LastUsedLocationId.Value);
		    }
		    else
		    {
			    return null;
		    }
	    }


		public ClientCommunicationManager GetCommunicationManager(Guid locationId)
        {
            if (!this.communicationManagers.ContainsKey(locationId))
            {
                var communicationManger = new ClientCommunicationManager(locationId);
                this.communicationManagers[locationId] = communicationManger;
				
            }

            return this.communicationManagers[locationId];
        }

        private void ScheduleDisconnectTimer()
        {
            this.disconnectTimer.Change(DisconnectAfterPauseDelay * 1000, Timeout.Infinite);
        }

        private void StopDisconnectTimer()
        {
            this.disconnectTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }




    }
}