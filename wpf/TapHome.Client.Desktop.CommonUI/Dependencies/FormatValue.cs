using System;
using System.Collections.Generic;
using System.Globalization;
using TapHome.Translations;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
    public static class FormatValue
    {
        
        public static int GetWeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        
        public static string GetFormattedBoolYesNo(bool value)
        {
            return value ? i18n.detail_deviceSettings_true : i18n.detail_deviceSettings_false;
        }

        private static string GetFormattedValuePercentage(double value, bool multiply)
        {
            if (multiply)
                value *= 100;

            return Math.Round(value) + " %";
        }

        private static string GetFormattedValueTemperature(double value)
        {
            return String.Format(i18n.device_variable_format_desiredtemperature, value);
        }
        
        private static string GetFormattedValueDateTime(double value)
        {
            return DateTime.FromOADate(value).ToLocalTime().ToString();
        }

        // 2 Decimal Places - Default
        private static string GetFormattedValueDefault(double value)
        {
            return String.Format("{0:0.00}", value);
        }

        // Module values
        public static string GetFormattedModuleVcc(int vcc)
        {
            return string.Format("{0:0.00} V", (double)vcc/1000);
        }

        public static string GetFormattedPartsPreMilion(double value)
        {
            return string.Format("{0:0} ppm", value);
        }

        public static string GetFormattedDegrees(double value)
        {
            return string.Format("{0} {1}", Math.Round(value), '\u00B0');
        }

        public static string GetFormattedMetersPerSecond(double value)
        {
            return string.Format("{0:0.00} m/s", value);
        }

        public static string GetFormattedDecibels(double value)
        {
            return string.Format("{0:0.0} db", value);
        }

        public static string GetFormattedMilibars(double value)
        {
            return string.Format("{0} mb", value);
        }

        public static string GetFormattedMilimeters(double value)
        {
            return string.Format("{0:0.0} mm", value);
        }

        public static string GetFormattedModuleTemperature(int temperature)
        {
            return GetTemperatureString(((double)temperature/100).ToString("0.00"));
        }

        public static string GetFormattedValueNoUnits(double value)
        {
            return string.Format("{0:0.00}", value);
        }

        public static string GetFormattedValueNoUnitsRounded(double value)
        {
            return string.Format("{0:0}", value);
        }

        public static string GetFormattedValueKilimetersPerHour(double value)
        {
            return string.Format("{0:0.00} km/h", value);
        }

        public static string GetFormattedRevision(int hwRevision)
        {
            return String.Format("{0}.{1}", hwRevision / 10, hwRevision%10);
        }

        public static string GetTemperatureString(string temperature)
        {
			return string.Format("{0} {1}C", temperature, '\u00B0'); 	// unicode degree sign
        }

        private static string GetFormattedValueEnergy(double value)
        {
            if (value < 1)
                return $"{value * 1000:###} Wh";

            return $"{value:##.###} kWh";
        }

        private static string GetFormattedValuePower(double value)
        {
            if (value < 1)
                return $"{value * 1000:###} W";

            return $"{value:##.###} kW";
        }
    }

    #region DateExtensions
    static class DateExtensions
    {
        public static DateTime ToLocalDateTime(this TimeSpan timeSpan)
        {
            return DateTime.Today.Add(timeSpan);
        }

        public static string ToCultureString(this TimeSpan timeSpan)
        {
            return timeSpan.ToLocalDateTime().ToString("t", CultureInfo.CurrentUICulture);
        }

        public static string ToCultureString(this DateTime dateTime)
        {
            return dateTime.ToString("t", CultureInfo.CurrentUICulture);
        }

    }
    #endregion


    #region TimespanExtensions
    static class TimespanExtensions
    {
        public static string ToStringLong(this TimeSpan timeSpan)
        {
            return string.Format("{0}d {1}h {2}m {3}s", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
        }
    }
    #endregion

}
