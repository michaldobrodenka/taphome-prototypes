//using ProtoBuf;
using System;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
    public enum SlaveInterfaceType
    {
        ModbusRS485RTU = 1,
        ModbusRS485ASCII = 2,
        ModbusTCP = 3,
    }

    //[ProtoContract]
    public class SlaveInterface
    {
        //[ProtoMember(1)] 
        public int SlaveInterfaceId { get; set; }

        //[ProtoMember(2)]
        public int SlaveInterfaceTypeId { get; set; }

        //[ProtoMember(3)]
        public string Parameter1 { get; set; }

        //[ProtoMember(4)]
        public string Parameter2 { get; set; }

        //[ProtoMember(5)]
        public string Parameter3 { get; set; }

        //[ProtoMember(6)]
        public string Parameter4 { get; set; }

        //[ProtoMember(7)]
        public string Parameter5 { get; set; }
    }
}
