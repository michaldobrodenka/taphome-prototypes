﻿using System;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
	public class DeviceVariable
	{
		public int ID { get; set; }
		public string Variable { get; set; }
		public string Abbr { get; set; }
	}

	public class GenericDevice : Device
	{
	    public GenericDevice()
	    {
	        var random = new Random();
	        ID = (int) random.NextDouble();
	        Model = Guid.NewGuid().ToString();
	        SerialNumber = "DO12-3538-3206-1D02";
            Name = Guid.NewGuid().ToString();
        }

		public T GetProperty<T>(string name, T defaultValue)
		{
			try
			{
				return (T)Convert.ChangeType(DeviceProperties[name], typeof(T));
			}
			catch (Exception e)
			{
				return defaultValue;
			}
		}
	}
}
