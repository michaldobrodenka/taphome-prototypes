﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
	// Protocol versions:
	// 0 - first production
	// 1 - added delete device
	// 2 - Limiting condition added
	// 3 - aggregated values
	public static class TaskExtenstions
	{
#if !SERVICES
		public static bool CompletedWithoutProblemsWithResult<T>(this Task<T> task)
		{
			bool wasOk = task.Exception == null && task.IsCompleted && !task.IsCanceled && !task.IsFaulted && task.Result != null; // task.Exception je prva podmienka kvoli getnutiu Exception pre finalizer thread

			return wasOk;
		}
#endif
		public static void FlushExceptions(this Task task)
		{
			task.ContinueWith(t =>
				{
					var aggException = t.Exception.Flatten();
					foreach (var exception in aggException.InnerExceptions)
					{
#if !CORE_APP && !WEBAPP && !SERVICES
						//global::TapHome.Commons.ApplicationManager.Instance.Log.Log(Model.LogLevel.Warning, "Task exception: " + exception);
#endif
#if WEBAPP
                    Console.WriteLine("Task exception: " + exception);
#elif CORE_APP
                    TapHomeManager.Current.Log(Model.LogLevel.Warning, "Task exception: " + exception);
#endif
					}
				},
				TaskContinuationOptions.OnlyOnFaulted);
		}
	}

	public abstract class MessageBase
	{
		public abstract MessageType MessageType { get; }

		public virtual int MinProtocolVersion
		{
			get { return 0; }
		}

		//[ProtoMember(1)]
		public int RequestsId { get; set; }
	}

	public enum MessageType : byte
	{
		None = 0,
		Request = 1,
		Response = 2,
		Broadcast = 3
	}

	[Flags]
	public enum ResponseFlags : short
	{
		None = 0x00,
		Handled = 0x01,
		Failed = 0x02,
	}

	public enum LoginResult : int
	{
		None = 0,
		Ok = 1,
		WrongPassword = 2,
		WrongLogin = 3,
		WrongAuthToken = 4,
		Deleted = 5,
	}

	public enum ReconnectResult : int
	{
		None = 0,
		Ok = 1,
		WrongSession = 2,
		TooManyConnections = 3
	}

	public enum ForgottenPasswordResult : int
	{
		Admin = 0,
		NotAdmin = 1,
		DoesNotExist = 2,
		IsDeleted = 3,
		WrongAccessToken = 4,
		MissingAccessToken = 5,
	}


    //[ProtoContract]
    public class GenericProperty
    {
        //[ProtoMember(1)]
        public string Name { get; set; }

        //[ProtoMember(2)]
        public object ProtoValue { get; set; }

        public object Value
        {
            get
            {
                return this.ProtoValue;
            }
            set
            {
                this.ProtoValue = value;
            }
        }

        //[ProtoMember(3)]
        public bool IsReadOnly { get; set; }

        //[ProtoMember(4)]
        public bool AllowEmpty { get; set; }
    }

    //[ProtoContract]
    public class LocationInfo
    {
        //[ProtoMember(1)]
        public Guid LocationId { get; set; }

        //[ProtoMember(2)]
        public string Name { get; set; }

        //[ProtoMember(5)]
        public Guid AuthorizationToken { get; set; } // nastavuje sa iba ked to ide z Cloudu

        //[ProtoMember(3)]
        public string IpOnLocalNetwork { get; set; }

        //[ProtoMember(4)]
        public string AccessToken { get; set; }
    }

    //[ProtoContract]
    public class MessageGetUsersListResponse : MessageBase
    {
        public override MessageType MessageType { get { return MessageType.Response; } }

        //[ProtoMember(1)]
        public UserWithoutHash[] Users { get; set; }
    }

    //[ProtoContract]
    public class MessageGenericResponse : MessageBase
	{
		public override MessageType MessageType { get { return MessageType.Response; } }

		//[ProtoMember(1)]
		//public short ResponseFlagsId { get; set; }

		//public ResponseFlags ResponseFlags
		//{
		//	get
		//	{
		//		return (ResponseFlags)this.ResponseFlagsId;
		//	}
		//	set
		//	{
		//		this.ResponseFlagsId = (short)value;
		//	}
		//}

		//[ProtoMember(2)]
		public string ErrorMessage { get; set; }

		//[ProtoMember(3)]
		public int ErrorCode { get; set; }

		//public object[] Parameters
		//{
		//	get
		//	{
		//		if (this.ProtoParameters == null)
		//			return null;

		//		//List<object> result;

		//		//result = new List<object>();

		//		//foreach (var p in this.ProtoParameters)
		//		//{
		//		//    result.Add(p.Value);
		//		//}

		//		//return result.ToArray();

		//		return this.ProtoParameters.Select(p => p.Value).ToArray();
		//	}
		//	set
		//	{
		//		this.ProtoParameters = new List<ProtoParameterBase>();

		//		foreach (var o in value)
		//		{
		//			if (o is string)
		//			{
		//				this.ProtoParameters.Add(
		//					new ProtoGenericParameter<string>()
		//					{
		//						GenericValue = (string)o,
		//					});
		//			}
		//			else if (o is Int32)
		//			{
		//				this.ProtoParameters.Add(
		//					new ProtoGenericParameter<int>()
		//					{
		//						GenericValue = (int)o,
		//					});
		//			}
		//			else if (o is long)
		//			{
		//				this.ProtoParameters.Add(
		//					new ProtoGenericParameter<long>()
		//					{
		//						GenericValue = (long)o,
		//					});
		//			}
		//			else if (o is double)
		//			{
		//				this.ProtoParameters.Add(
		//					new ProtoGenericParameter<double>()
		//					{
		//						GenericValue = (double)o,
		//					});
		//			}
		//			else if (o is bool)
		//			{
		//				this.ProtoParameters.Add(
		//					new ProtoGenericParameter<bool>()
		//					{
		//						GenericValue = (bool)o,
		//					});
		//			}
		//			//else if (o is AlarmMode)
		//			//{
		//			//    this.ProtoParameters.Add(
		//			//        new ProtoGenericParameter<AlarmMode>()
		//			//        {
		//			//            GenericValue = (AlarmMode)o,
		//			//        });
		//			//}
		//			//else if (o is ClickKind)
		//			//{
		//			//    this.ProtoParameters.Add(
		//			//        new ProtoGenericParameter<ClickKind>()
		//			//        {
		//			//            GenericValue = (ClickKind)o,
		//			//        });
		//			//}
		//			else if (o is Guid)
		//			{
		//				this.ProtoParameters.Add(
		//					new ProtoGenericParameter<Guid>()
		//					{
		//						GenericValue = (Guid)o,
		//					});
		//			}
		//			else if (o is UInt32)
		//			{
		//				this.ProtoParameters.Add(
		//					new ProtoGenericParameter<UInt32>()
		//					{
		//						GenericValue = (UInt32)o,
		//					});
		//			}
		//			else throw new ArgumentException("Unsupported type - can not serialize.");

		//		}
		//	}
		//}

		//[ProtoMember(4)]
		//public List<ProtoParameterBase> ProtoParameters { get; set; }
	}

	//[ProtoContract]
	public class MessageTapHomeBusConfigResponse : MessageBase
	{
		public override MessageType MessageType { get { return MessageType.Response; } }

		//[ProtoMember(1)]
		public int DeviceId { get; set; }

		//[ProtoMember(2)]
		public GenericDevice[] AddedDevices { get; set; }

		//[ProtoMember(3)]
		public GenericDevice[] RemovedDevices { get; set; }
	}

	//[ProtoContract]
	public class MessageGetModuleInfoResponse : MessageBase
	{
		public override MessageType MessageType { get { return MessageType.Response; } }

		public override int MinProtocolVersion { get { return 6; } }

		//[ProtoMember(1)]
		public int BoardHwRevision;

		//[ProtoMember(2)]
		public int BoardSwVersion;

		//[ProtoMember(3)]
		public byte ProtocolVersion;

		//[ProtoMember(4)]
		public int ArchitectureTypeId;
	}

    public class MessageGetSlaveInterfacesResponse : MessageBase
    {
        public override MessageType MessageType { get { return MessageType.Response; } }

        public override int MinProtocolVersion { get { return 5; } }

        public SlaveInterface[] SlaveInterfaces { get; set; }
    }
}