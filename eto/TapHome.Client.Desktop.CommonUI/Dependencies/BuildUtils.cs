

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
	public enum CompanyType
	{
		TapHome = 0,
		OlaControls = 1,
	    TapHomeNext = 2,
    }

    public static class BuildUtils
    {
#if OLACONTROLS
        public const string AssemblyTitle = "OlaControls Android Client";
        public const string CompanyName = "OlaControls";
        public const string AppName = "OlaControls";
        public const string AppIcon = "@drawable/Launcher_Ola";
        public const string CcuIcon = "icon_ccu_OlaControls";
        public const string PackageName = "com.olarms.android";
        public const string PackageNameOffline = "com.olarms.android";
        public const string PushServiceSenderId = "156621242989";
        public const string DemoAccountEmail = "demo@olacontrols.com";
        public const string DemoAccountPassword = "Olads";
        public const CompanyType Company = CompanyType.OlaControls;
        public const int AppStoreID = 1272706068;
        public const string HoyckeyAppAndroidId = "a0c0628a1c044efc853cce460f74e0dd";
#elif TAPHOMENEXT
        public const string AssemblyTitle = "TapHome Android Client";
        public const string CompanyName = "TapHome";
        public const string AppName = "TapHome Next";
        public const string AppIcon = "@drawable/Launcher_TapHome";
        public const string CcuIcon = "icon_ccu_TapHome";
        public const string PackageName = "com.taphome.android.next";
        public const string PackageNameOffline = "com.taphome.android.next";
        public const string PushServiceSenderId = "367173346603";
        public const string DemoAccountEmail = "demo@taphome.com";
        public const string DemoAccountPassword = "pass";
        public const CompanyType Company = CompanyType.TapHomeNext;
        public const int AppStoreID = 0;
        public const string HoyckeyAppAndroidId = "84c54cc06d0b4753ae0cfdfe590e400c";
#else
        public const string AssemblyTitle = "TapHome Android Client";
        public const string CompanyName = "TapHome";
        public const string AppName = "@string/app_name";
        public const string AppIcon = "@drawable/Launcher_TapHome";
        public const string CcuIcon = "icon_ccu_TapHome";
        public const string PackageName = "com.taphome.android";
        public const string PackageNameOffline = "com.taphome.android.offline";
        public const string PushServiceSenderId = "182283819639";
        public const string DemoAccountEmail = "demo@taphome.com";
        public const string DemoAccountPassword = "pass";
        public const CompanyType Company = CompanyType.TapHome;
        public const int AppStoreID = 1081828287;
        public const string HoyckeyAppAndroidId = "a7af8828c6c44fe68d8cf38a5822059c";
#endif

        public static string ReplaceTapHomeInText(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }

#if OLACONTROLS
            return text.Replace("TapHome", CompanyName).Replace("taphome", CompanyName);
#endif
            return text;
        }
    }
}
