﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
	public class TapHomeBusModuleConfig
	{
		// device types
		public const string TypeReedContact = "Reed";
		public const string TypePushButton = "PushButton";
		public const string TypeSwitch = "Switch";
		public const string TypeSwitchOnUO = "SwitchOnUO";
		public const string TypeBlind = "Blind";
		public const string TypeBlindOnUO = "BlindOnUO";
		public const string TypeEncoderBlind = "EncoderBlind";
		public const string TypeCurrentLimitedBlind = "CurrentLimitedBlind";
		public const string TypeImpulseCounter = "ImpulseCounter";
		public const string TypeThermometer = "Thermometer";
		public const string TypeAnalogOutput = "AnalogOutput";
		public const string TypeAnalogInput = "AnalogInput";
		public const string TypeLedDimmer = "LedDimmer";
		public const string TypeEncoderSlat = "EncoderSlat";
		public const string TypeSlide = "Slide";
		public const string TypeSlideOnUO = "SlideOnUO";
		public const string TypeEncoderSlide = "EncoderSlide";
		public const string TypeCurrentLimitedSlide = "CurrentLimitedSlide";

		// extra attributes
		public const string TypeBlindTerminalUp = "TerminalUp";
		public const string TypeBlindTerminalDown = "TerminalDown";
		public const string DeviceName = "DeviceName";

		public class BusDeviceInfo
		{
			public int Index { get; set; }
			public string Type { get; set; }
			public Dictionary<string, string> ExtraAttributes { get; set; }
		}

		public List<BusDeviceInfo> Devices { get; set; }
	}
}
