﻿using System;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{

    [Flags]
    public enum UserPermission : int
    {
        None = 0x00,
        User = 0x01,        // normal user
        Admin = 0x02,       // Device admin
        Service = 0x04,     // Device service 
    }

    /// <summary>
    /// To be used as DTO
    /// </summary>
    //[ProtoContract]
    public class UserWithoutHash
    {
        //[PrimaryKey]
        //[ProtoMember(1)]
        public Guid Id { get; set; }

        //[ProtoMember(2)]
        public string Login { get; set; }

        //[ProtoMember(3)]
        public string Email { get; set; }

        //[ProtoMember(4)]
        public string FullName { get; set; }

        //[ProtoMember(5)]
        public long CreatedOnTUtc { get; set; }

        //[Ignore]
        public DateTime CreatedOnUtc
        {
            get { return new DateTime(this.CreatedOnTUtc); }
            set { this.CreatedOnTUtc = value.Ticks; }
        }

        //[ProtoMember(6)]
        public UserPermission Permissions { get; set; }

        //[ProtoMember(7)]
        public bool IsLocalAccount { get; set; }

        //[ProtoMember(8)]
        public long? DeletedOnTUtc { get; set; }

        //[Ignore]
        //public DateTime? DeletedOnUtc
        //{
        //    get { return this.DeletedOnTUtc.ToNullableDateTime(); }
        //    set { this.DeletedOnTUtc = value.ToNullableTicks(); }
        //}

        //[ProtoMember(9)]
        public long? ValidFromTUtc { get; set; }

        //[Ignore]
        //public DateTime? ValidFromUtc
        //{
        //    get { return this.ValidFromTUtc.ToNullableDateTime(); }
        //    set { this.ValidFromTUtc = value.ToNullableTicks(); }
        //}

        //[ProtoMember(10)]
        public long? ValidUntilTUtc { get; set; }

        //[Ignore]
        //public DateTime? ValidUntilUtc
        //{
        //    get { return this.ValidUntilTUtc.ToNullableDateTime(); }
        //    set { this.ValidUntilTUtc = value.ToNullableTicks(); }
        //}

        //[ProtoMember(11)]
        public long? FirstLoginOnTUtc { get; set; }

        //[Ignore]
        //public DateTime? FirstLoginOnUtc
        //{
        //    get { return this.FirstLoginOnTUtc.ToNullableDateTime(); }
        //    set { this.FirstLoginOnTUtc = value.ToNullableTicks(); }
        //}

        //[ProtoMember(12)]
        public bool HasTemporaryId { get; set; }

        //[ProtoMember(13)]
        public Guid? OldTemporaryId { get; set; }

        //[ProtoMember(14)]
        public string ExtraData { get; set; } //json

        //[ProtoMember(15)]
        public int UserPermissionsForDevicesChangesetId { get; set; }

        //[ProtoMember(16)]
        public int UserPermissionsForTriggersChangesetId { get; set; }

        //[ProtoMember(17)]
        public int UserPermissionsGeneralChangesetId { get; set; }

        public bool IsDeleted
        {
            get { return false; }
        }
    }
}
