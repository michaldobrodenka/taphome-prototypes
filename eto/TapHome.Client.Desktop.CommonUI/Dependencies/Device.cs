﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Client.Desktop.CommonUI.Dependencies
{
	public class Device
	{
		public int ID { get; set; }

		public string Name { get; set; }

		public string SerialNumber { get; set; }

		public string Model { get; set; }

		public Dictionary<string, string> DeviceProperties { get; set; }

		public DeviceIcon Icon { get; set; }
	}

	public enum DeviceOperationMode
	{
		ManualOnly = 0, // vysedene Acko, neda sa zmenit
		Manual = 1,
		Automatic = 2
	}
	public enum ValueLogType : int
	{
		NA = 0,
		State = 1,
		Brightness = 2,
		Humidity = 3,
		Proximity = 4,
		/// <summary>
		/// Real temperature for thermostats
		/// </summary>
		RealTemperature = 5,
		/// <summary>
		/// The desired temperature for thermostats
		/// </summary>
		DesiredTemperature = 6,
		DeviceStatus = 7,
		[Obsolete("Shouldn't be used anywhere", true)]
		Working = 8,
		HueValue = 9,
		BlindsSlope = 10,
		ConvectorThermostatFanLevel = 11,
		Raining = 12,
		RainCounter = 13,
		WindSpeed = 14,
		WindDirection = 15,
		WindDirectionRange = 16,
		SunshineDuration = 17,
		ThermostatMode = 18, // 0 = heating, 1 = cooling
		AlarmMode = 19,
		AlarmState = 20,
		AlarmActiveDeviceCount = 21, // pocet zariadeni kotre prave "aktivuju" alarm
		OperationMode = 22, // current operation mode of the device (for values see DeviceOperationMode enum below)
		ManualTimeout = 23, // utc time as double when manual override timeout expires
		Co2 = 24,
		Pressure = 25,
		Noise = 26,
		ThermostatControlMode = 27, // automatic = 0, manual = 1
		SnowCounter = 28,
		WindGust = 29,
		Cloudiness = 30,
		WeatherCondition = 31,
		ForecastTargetTime = 32,
		[Obsolete("Should use device properties instead")]
		MinTemperature = 33,
		[Obsolete("Should use device properties instead")]
		MaxTemperature = 34,
		SmartRuleUpdate = 35, // ID of smart rule to invoke
		ManualOverrideUpdate = 36, // ID of manual override to invoke
		ValveState = 37, // <0; 100>
		ButtonHeldState = 38, // true if button is held
		Startup = 39, // invoked on startup by clock
		HueDegrees = 40, // <0; 360>
		Saturation = 41, // <0; 1>
		AnalogOutputValue = 42,
		IsWindowOpen = 43,
		ReedContact = 44, // Reed contant ?
		Smoke = 45,
		BlindsLevel = 46,
		FloodState = 47,
		SwitchState = 48,
		MultiValueSwitchState = 49,
		SlideLevel = 50,
		Motion = 51,
		ButtonPressed = 52,
		Tilt = 53,
		Voc = 54,
		AnalogInputValue = 55,
		TotalImpulseCount = 56,
		CurrentHourImpulseCount = 57,
		LastMeasuredFrequency = 58,
		ElectricityConsumption = 59,
		ElectricityDemand = 60,
		TotalElectricityConsumption = 61,
		VariableState = 62,
		IsSafetySmartruleActive = 63,
	}

	public class ValueLog
	{
		public ValueLogType Type { get; set; }

		public int ChangedOnTHUtc { get; set; }

		public DateTime ChangedOnUtc
		{
			get { return DateTime.Now; }
			set { this.ChangedOnTHUtc = 20; }
		}

		public double Value { get; set; }

		//[ProtoMember(4)]
		public double PreviousValue { get; set; }

		//[ProtoMember(5)]
		public DeviceValueAggregation DeviceValueAggregation { get; set; }
	}
	public enum DeviceValueStatisticsFunction : byte
	{
		// FUNCTION
		CurrentValue = 0, // > 0 Aggregated value
		Min = 1,
		Max = 2,
		Avg = 3,
		Count = 4,
		SumOrIntegral = 5, // musi byt podporovane cez HW, nie? podporuje kto vlastne? elektromer a imp counter?  
	}
	public enum ValueHistoryPeriodType : byte
	{
		Instant = 0,
		// TIME INTERVAL TYPE
		PeriodToNow = 1, // last 365 days 
		CurrentPeriod = 2, // from 1.1. this year
	}

	public enum ValueHistoryPeriod : byte
	{
		// TIME INTERVAL
		InstantValue = 0,
		Period5Min = 10, // zacina sa ked time/5m == 0 - priebezny priemer sa prepocitava sa ke 
		Period10Min = 15, // zacina sa ked time/10m == 0
		Period15Min = 20, // zacina sa ked time/15m == 0
		Period30Min = 25, // zacina sa ked time/30m == 0
		Period1H = 30, // zacina sa ked time/1 hod == 0
		Period3H = 35, // zacina sa ked time/3 hod == 0 ---- potialto z instantValue
		Period12H = 40, // zacina sa ked time/12 hod == 0 ----- po hodinach
		Period1D = 45, // zacina sa kazdy den o polnoci v nastavenej casovej zone ----- po hodinach
		Period2D = 50,
		Period3D = 55, // zacina sa cely den o polnoci, ak od poslednej dokoncenej periody je viac ako 2 dni ---- po hodinach
		Period1W = 60, // zacina sa o polnoci medzi nedelou a pondelkom v nastavenej casovej zone Pozn: asi kaslime na ostatne krajiny zatial ---- po hodinach
		Period1M = 65, // zacina sa o polnoci 1. kazdy mesiac. Ak berieme posledny mesiac, berieme poslednych 30 dni ---- po dnoch
		Period1Q = 70, // zacina sa o polnoci 1. v januari, aprili, juli, oktobri. Ak berieme posledny stvrtrok, berieme poslednych 90 dni ---- po dnoch
		Period1Y = 75, // zacina sa 1.1. kazdy rok o polnociu v nastavenej casovej zone, ak sa berie posledny rok, tak poslednych 365 dni ---- po dnoch
	}
	public class DeviceValueAggregation
	{
		public DeviceValueStatisticsFunction StatisticsFunction { get; set; }
		public ValueHistoryPeriodType PeriodType { get; set; }
		public ValueHistoryPeriod Period { get; set; }

		public static readonly DeviceValueAggregation InstantValueAggregation = new DeviceValueAggregation
		{
			StatisticsFunction = DeviceValueStatisticsFunction.CurrentValue,
			PeriodType = ValueHistoryPeriodType.Instant,
			Period = ValueHistoryPeriod.InstantValue,
		};

		public bool IsInstant()
		{
			return Period == ValueHistoryPeriod.InstantValue ||
				   StatisticsFunction == DeviceValueStatisticsFunction.CurrentValue ||
				   PeriodType == ValueHistoryPeriodType.Instant;
		}

		public override int GetHashCode()
		{
			return (((int)this.StatisticsFunction) << 16) | (((int)this.PeriodType) << 8) | ((int)this.Period);
		}

		public static int GetHashCode(DeviceValueStatisticsFunction statisticsFunction, ValueHistoryPeriodType periodType, ValueHistoryPeriod period)
		{
			return (((int)statisticsFunction) << 16) | (((int)periodType) << 8) | ((int)period);
		}

		public static DeviceValueAggregation ParseHashCode(int hashCode)
		{
			return new DeviceValueAggregation
			{
				StatisticsFunction = (DeviceValueStatisticsFunction)(hashCode >> 16),
				PeriodType = (ValueHistoryPeriodType)(hashCode >> 8),
				Period = (ValueHistoryPeriod)(hashCode),
			};
		}

		public override bool Equals(System.Object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast return false.
			DeviceValueAggregation p = obj as DeviceValueAggregation;
			if ((System.Object)p == null)
			{
				return false;
			}

			// Return true if the fields match:
			return this.Equals(p);
		}

		public bool Equals(DeviceValueAggregation p)
		{
			// If parameter is null return false:
			if ((object)p == null)
			{
				return false;
			}

			// Return true if the fields match:
			return (this.StatisticsFunction == p.StatisticsFunction) && (this.PeriodType == p.PeriodType) && (this.Period == p.Period);
		}

		public bool Equals(DeviceValueAggregationExtended p)
		{
			// If parameter is null return false:
			if ((object)p == null)
			{
				return false;
			}

			// Return true if the fields match:
			return (this.StatisticsFunction == p.StatisticsFunction) && (this.PeriodType == p.PeriodType) && (this.Period == p.Period);
		}

		public override string ToString()
		{
			return $"Period {this.Period}, Period Type {this.PeriodType}, Statistic Fn {this.StatisticsFunction}";
		}
	}
	[Flags]
	public enum DeviceValueAggregationUsage
	{
		IsRequired = 0x01,
		IsEnabledByUser = 0x02,
		IsHistoryRecordingRequested = 0x04,
		IsSubAggregation = 0x08,
		IsProvidedByDevice = 0x10, // do we need to compute it in DeviceValueHistory Manager?
	}
	public class DeviceValueAggregationExtended : DeviceValueAggregation
	{
		public DeviceValueAggregationUsage AggregationUsage { get; set; }

		public int EnabledOnTHUtc { get; set; }

		public DateTime EnabledOnUtc
		{
			get { return DateTime.Now; }
			set { this.EnabledOnTHUtc = 3; }
		}

		//[ProtoMember(3)]
		public Guid EnabledByUserId { get; set; }

		//[ProtoMember(4)]
		public Guid HistoryRecordingEnabledByUserId { get; set; }

		public int HistoryRecordingEnabledOnTHUtc { get; set; }

		public DateTime HistoryRecordingEnabledOnUtc
		{
			get { return DateTime.Now; }
			set { this.HistoryRecordingEnabledOnTHUtc = 3; }
		}

		public DateTime LastRecalculationOnUtc { get; set; }

		public List<int> HistorySubAggregationsHashCodes { get; set; }

		public long LastHistoryCloudFlushOnTUtc { get; set; }
	}
	public enum DeviceIcon : int
	{
		Unknown = 0,
		MotionSensor = 1,
		WindowSensor = 2,
		FloodSensor = 3,
		SmokeSensor = 4,
		PushButton = 5,
		Relay = 6,
		Light = 7,
		Dimmer = 8,
		Thermostat = 9,
		Blinds = 10,
		HueBulb = 11,
		AwayMode = 12,
		LightScene = 13,
		WeekSchedule = 14,
		Alarm = 15,
		Notification = 16,
		TiltSensor = 17,
		GenericSensor = 18,
		TemperatureSensor = 19,
		WeatherSensor = 20,
		Astronomy = 21,
		Timer = 22,
		WindSpeedSensor = 23,
		BrightnessSensor = 24,
		LightSceneSequence = 25,
		ProtectBlindsFromWind = 26,
		ProtectFromOverheat = 27,
		PresenceSimulation = 28,
		Watchdog = 29,
		Evaluation = 30,
		KeepValues = 31,
		SwitchTrigger = 32,
		Co2Sensor = 33,
		PressureSensor = 34,
		NoiseSensor = 35,
		RainSensor = 36,
		Humidity = 37,
		LimitFloorTemperature = 38,
		ReduceHeating = 39,
		LightingTimer = 40,
		PidController = 41,
		DelayedAirVent = 42,
		DisableBoiler = 43,
		EquithermicRegulation = 44,
		SensorNotification = 45,
		Link = 46,
		DeviceStatusNotification = 47,
		BoilerPidController = 48,
		SoilMoisture = 49,
		Formula = 50,
		CoolingMode = 51,
		HeatingMode = 52,
		CoolingHeatingMode = 53,
		DayNightMode = 54,
		MaxVentilationMode = 55,
		PartyMode = 56,
		HolidayMode = 57,
		BlindAdjustmentAccordingToSun = 58,
		PushButtonDimming = 59,
		Emergency = 60,
		Voc = 61,
		ElectricityMeter = 62,
		ImpulseCounter = 63,
		DailySchedule = 64,
		Sequencer = 65,
		MultiValueSwitch = 66,
		Variable = 67,
	}

	public interface IDeviceManager
	{
		IEnumerable<Device> Devices { get; }
		Device GetDevice(int id);
		//Device GetDevice(long id);
		Device GetClockDevice();
	}
}
