using System;
using System.Linq;
using System.Collections.Generic;
using TapHome.Client.Desktop.CommonUI.Dependencies;

#if ANDROID || IOS
using Com.TapHome.Communication;
using TapHome.Commons;
#endif

namespace TapHome.Core.Lib.IFTTT.Actions
{
    public class Trigger
    {
        public long ID { get; set; }
    }
    public class DeviceValueEventHandlingStatus { }
	public abstract class DeviceBaseAction : BaseAction
	{
		public bool ShouldValueBeSet { get; set; }
		public bool ShouldOperationModeBeSet { get; set; }
		public DeviceOperationMode OperationMode { get; set; }

		protected DeviceBaseAction()
		{
			ShouldValueBeSet = true;
			ShouldOperationModeBeSet = false;
			OperationMode = DeviceOperationMode.Automatic;
		}

		private void SetDeviceOperationMode(Device device)
		{
#if CORE_APP
            var currentOperationMode = device.OperationMode;
            if (currentOperationMode == OperationMode)
                return;

            //if (OperationMode == DeviceOperationMode.Automatic)
            //{
            //    TapHomeManager.Current.Ifttt.OverrideManager.CancelOverride(device);
            //}
            //else
            //{
            //    TapHomeManager.Current.Ifttt.OverrideManager.CreateOverride(device, ValueLogType.NA);
            //}
            device.OperationMode = OperationMode;

            //TapHomeManager.Current.Ifttt.ReevalSmartRules();
            TapHomeManager.Current.Ifttt.ReevalDevice(device);
#endif
		}

		public override bool IsNoAction()
		{
			return !ShouldOperationModeBeSet;
		}

		public override bool ShouldOverrideManualOverride()
		{
			return ShouldOperationModeBeSet && OperationMode == DeviceOperationMode.Automatic;
		}
	}

	public abstract class BaseAction
	{
		public const string FormatMain = "main";
		public const string FormatInfo = "info";

		public static readonly string AllDevices = "all";

		/// <summary>A list of device names that are affected by this action.</summary>
		public virtual HashSet<int> OutputDeviceIDs { get; private set; }

		/// <summary>If this flag is set, trigger should fire this action only once. What exactly this means depends on trigger itself. Default value is false.</summary>
		public bool OneTimeOnly { get; set; }
        
        public Trigger SourceTrigger { get; set; }

        public DeviceValueEventHandlingStatus HandlingStatus { get; set; }

        public long SourceTriggerId
        {
            get
            {
                return this.SourceTrigger == null ? 0 : this.SourceTrigger.ID;
            }
        }
        
		public BaseAction()
		{
			OutputDeviceIDs = new HashSet<int>();
			OneTimeOnly = false;
		}

		public DeviceIcon GetIcon(IDeviceManager deviceManager)
		{
			if (OutputDeviceIDs.Count == 0)
				return GetDefaultIcon();
			bool hasDevice = false;
			DeviceIcon deviceIcon = DeviceIcon.Unknown;
			foreach (int deviceID in OutputDeviceIDs)
			{
				Device dev = deviceManager.GetDevice(deviceID);
				if (dev == null)
					continue;

				hasDevice = true;
				if (deviceIcon == DeviceIcon.Unknown)
					deviceIcon = dev.Icon;
				else if (deviceIcon != dev.Icon)
					return GetDefaultIcon();
			}
			return hasDevice ? deviceIcon : GetDefaultIcon();
		}

		protected virtual DeviceIcon GetDefaultIcon() { return DeviceIcon.Unknown; }
        

		/*public static void AppendDeviceIDs(IEnumerable<long> deviceIDs, HashSet<long> setToAppend)
		{
			foreach (var deviceID in deviceIDs)
				setToAppend.Add(deviceID);
		}*/

		public static BaseAction GetDefaultAction(string intrface)
		{
            //if (intrface == typeof(HueBulb).Name)
            //    return new HueAction();
            //else if (intrface == typeof(Dimmer).Name)
            //    return new DimmerAction();
            if (intrface == "Switch")
                return new SwitchAction();
//            else if (intrface == typeof(Thermostat).Name)
//                return new ThermostatAction();
//            else if (intrface == typeof(Blinds).Name || intrface == typeof(AdvancedBlinds).Name)
//            {
//                // default action is don't set level, however BlindsAction.ctor() must set "Set action" for backward compat.
//                return new BlindsAction() { LevelAction = BlindsAction.LevelActionType.NoAction };
//            }
//            else if (intrface == typeof(Slide).Name || intrface == typeof(AdvancedSlide).Name)
//            {
//                // default action is don't set level, however BlindsActio.ctor() must set "Set action" for backward compat.
//                return new BlindsAction() { LevelAction = BlindsAction.LevelActionType.NoAction, SlopeAction = BlindsAction.SlopeActionType.NA };
//            }
//            else if (intrface == typeof(Slat).Name || intrface == typeof(AdvancedSlat).Name)
//            {
//                return new BlindsAction() { LevelAction = BlindsAction.LevelActionType.NA, SlopeAction = BlindsAction.SlopeActionType.NoAction };
//            }
//            else if (intrface == typeof(PushButton).Name)
//				return new PushButtonAction();
//            else if (intrface == typeof(Alarm).Name)
//                return new AlarmAction();
//            else if (intrface == typeof(MultiValueSwitch).Name
//#if ANDROID || IOS
//                && ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion >= ClientCommunicationManager.DeviceTypeMinProtocolVersion
//#endif
//)
//                return new MultiValueSwitchAction() { ActionType = MultiValueSwitchAction.MultiValueSwitchActionType.None};
//            else if (intrface == typeof(Variable).Name
//#if ANDROID || IOS
//                && ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion >= ClientCommunicationManager.VariableMinProtocolVersion
//#endif
//)
//                return new VariableAction();

            else
				return null;
		}

        public static BaseAction GetDefaultOffAction(string intrface)
        {
            //if (intrface == typeof(HueBulb).Name)
            //    return new HueAction()
            //    {
            //        ActionType = SwitchAction.SwitchActionType.Off,
            //        Brightness = 0,
            //    };
            //else if (intrface == typeof(Dimmer).Name)
            //    return new DimmerAction()
            //    {
            //        ActionType = SwitchAction.SwitchActionType.Off,
            //        DimLevel = 0,
            //        MinDimLevel = 0,
            //        MaxDimLevel = 0
            //    };
            if (intrface == "Switch")
                return new SwitchAction()
                {
                    ActionType = SwitchAction.SwitchActionType.Off,
                };
//            else if (intrface == typeof(Thermostat).Name)
//                return new ThermostatAction();
//            else if (intrface == typeof(Blinds).Name || intrface == typeof(AdvancedBlinds).Name)
//            {
//                // default action is don't set level, however BlindsAction.ctor() must set "Set action" for backward compat.
//                return new BlindsAction() { LevelAction = BlindsAction.LevelActionType.NoAction };
//            }
//            else if (intrface == typeof(Slide).Name)
//            {
//                // default action is don't set level, however BlindsActio.ctor() must set "Set action" for backward compat.
//                return new BlindsAction() { LevelAction = BlindsAction.LevelActionType.NoAction, SlopeAction = BlindsAction.SlopeActionType.NA };
//            }
//            else if (intrface == typeof(PushButton).Name)
//                return new PushButtonAction();
//            else if (intrface == typeof(Alarm).Name)
//                return new AlarmAction();
//            else if (intrface == typeof(MultiValueSwitch).Name
//#if ANDROID || IOS
//                && ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion >= ClientCommunicationManager.DeviceTypeMinProtocolVersion
//#endif
//)
//                return new MultiValueSwitchAction() { ActionType = MultiValueSwitchAction.MultiValueSwitchActionType.Off };

//            else
                return null;
        }

        public static BaseAction GetDefaultOnAction(string intrface)
        {
            //if (intrface == typeof(HueBulb).Name)
            //    return new HueAction()
            //    {
            //        ActionType = SwitchAction.SwitchActionType.On,
            //    };
            //else if (intrface == typeof(Dimmer).Name)
            //    return new DimmerAction()
            //    {
            //        ActionType = SwitchAction.SwitchActionType.On,
            //        MaxDimLevel = 1,
            //        MinDimLevel = 1,
            //        DimLevel = 1
            //    };
            if (intrface == "Switch")
                return new SwitchAction()
                {
                    ActionType = SwitchAction.SwitchActionType.On,
                };
//            else if (intrface == typeof(Thermostat).Name)
//                return new ThermostatAction();
//            else if (intrface == typeof(Blinds).Name || intrface == typeof(AdvancedBlinds).Name)
//            {
//                // default action is don't set level, however BlindsAction.ctor() must set "Set action" for backward compat.
//                return new BlindsAction() { LevelAction = BlindsAction.LevelActionType.NoAction };
//            }
//            else if (intrface == typeof(Slide).Name)
//            {
//                // default action is don't set level, however BlindsActio.ctor() must set "Set action" for backward compat.
//                return new BlindsAction() { LevelAction = BlindsAction.LevelActionType.NoAction, SlopeAction = BlindsAction.SlopeActionType.NA };
//            }
//            else if (intrface == typeof(PushButton).Name)
//                return new PushButtonAction();
//            else if (intrface == typeof(Alarm).Name)
//                return new AlarmAction();
//            else if (intrface == typeof(MultiValueSwitch).Name
//#if ANDROID || IOS
//                && ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion >= ClientCommunicationManager.DeviceTypeMinProtocolVersion
//#endif
//)
//                return new MultiValueSwitchAction() { ActionType = MultiValueSwitchAction.MultiValueSwitchActionType.On };

//            else
                return null;
        }

        public virtual bool IsNoAction()
	    {
	        return false;
	    }

		/// <summary>
		/// Create clone of current action leaving OutputDeviceIDs empty
		/// </summary>
		public virtual BaseAction CloneAction()
		{
		    return null;
		}


        /// <summary>
        /// Update properties of action
        /// </summary>
        public abstract BaseAction UpdateAction(BaseAction baseAction);

	    public bool AffectsDevice(int deviceId)
	    {
	        return OutputDeviceIDs.Contains(deviceId);
	    }

        public bool AffectsDevice(List<int> deviceId)
        {
            return deviceId.Any(a => OutputDeviceIDs.Contains(a));
        }
        
        
        
	    public bool ActionEqualsWithOutputDevices(BaseAction other)
	    {
	        return this.Equals(other) && OutputDeviceIDs.All(o => other.OutputDeviceIDs.Contains(o)) &&
	               other.OutputDeviceIDs.All(o => OutputDeviceIDs.Contains(o));

	    }

	    private bool OutputDeviceIdsEqual(BaseAction action)
	    {
            var firstNotSecond = this.OutputDeviceIDs.Except(action.OutputDeviceIDs).ToList();
            var secondNotFirst = action.OutputDeviceIDs.Except(this.OutputDeviceIDs).ToList();

            return !firstNotSecond.Any() && !secondNotFirst.Any();
        }

	    public virtual bool ShouldInvoke(ValueLog valueLog)
        {
            return true;
        }

	    public virtual bool IsOneTimeOnly()
	    {
	        return OneTimeOnly;
	    }

	    public virtual bool ShouldOverrideManualOverride()
	    {
	        return false;
	    }
	}
}
