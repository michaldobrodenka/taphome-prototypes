using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TapHome.Client.Desktop.CommonUI.Dependencies;

namespace TapHome.Core.Lib.IFTTT.Actions
{
	public class SwitchAction : DeviceBaseAction
    {
        public SwitchAction()
        {
        }

		public enum SwitchActionType { None, On, Off, Toggle, StartAnimating, StopAnimating }

		public SwitchActionType ActionType { get; set; }

        public static bool ActionsContainDeviceId(List<BaseAction> actions, int deviceId)
        {
            bool containsAwayMode = false;

            foreach (var action in actions)
            {
                if (action.OutputDeviceIDs.Contains(deviceId))
                {
                    containsAwayMode = true;
                }
            }

            return containsAwayMode;
        }

		public override string ToString()
		{
			return string.Format("SwitchAction {0}{1}{2} {3}", ActionType, IsOneTimeOnly() ? " once" : string.Empty, base.ToString(), string.Join(",", OutputDeviceIDs));
		}

		protected override DeviceIcon GetDefaultIcon() { return DeviceIcon.PushButton; }
        
        public override bool IsNoAction()
        {
            return base.IsNoAction() && ActionType == SwitchActionType.None;
        }

        public override BaseAction UpdateAction(BaseAction baseAction)
        {
            throw new NotImplementedException();
        }

        public override bool IsOneTimeOnly()
        {
            return OneTimeOnly || ActionType == SwitchActionType.Toggle;
        }
    }
}
