﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Eto.Forms;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.Core.Lib.IFTTT.Actions;
using TapHome.Translations;

namespace TapHome.Client.Desktop.CommonUI.Actions
{
    public partial class ActionDialog
    {
        public enum Result
        {
            OK, Remove, Cancel,
        }

	    private class ActionDialogViewModel : INotifyPropertyChanged
	    {
		    private bool shouldSetValue;
		    public bool ShouldSetValue
		    {
				get { return shouldSetValue; }
			    set
			    {
				    if (shouldSetValue == value)
					    return;

				    shouldSetValue = value;
					OnPropertyChanged();
				    OnPropertyChanged(nameof(ShouldShowMoreButton));
				}
			}

		    private bool shouldSetOperationMode;
		    public bool ShouldSetOperationMode
			{
			    get { return shouldSetOperationMode; }
			    set
			    {
				    if (shouldSetOperationMode == value)
					    return;

				    shouldSetOperationMode = value;
				    OnPropertyChanged();
				    OnPropertyChanged(nameof(ShouldShowMoreButton));
				}
			}

		    private bool shouldShowMore;
		    public bool ShouldShowMore
		    {
			    get { return shouldShowMore; }
			    set
			    {
				    if (shouldShowMore == value)
					    return;

				    shouldShowMore = value;
				    OnPropertyChanged();
				    OnPropertyChanged(nameof(ShowMoreButtonText));
				    OnPropertyChanged(nameof(ShouldShowMoreButton));
				}
			}
			
		    public bool ShouldShowMoreButton
		    {
			    get { return !(ShouldShowMore && ShouldSetOperationMode || !ShouldSetValue); }
		    }

		    public string ShowMoreButtonText
			{
			    get { return shouldShowMore ? i18n.smartrule_action_showLess : i18n.smartrule_action_showMore; }
		    }

			#region INotifyPropertyChanged implementation

			public event PropertyChangedEventHandler PropertyChanged;

		    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		    {
			    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		    }

		    #endregion
		}

	    private ActionDialogViewModel viewModel;
		private BaseAction baseAction;

	    public ActionDialog(BaseAction action)
	    {
		    InitializeComponent(action);
			SetTitle(action);

		    viewModel = new ActionDialogViewModel
		    {
			    ShouldSetValue = ((DeviceBaseAction) action).ShouldValueBeSet,
			    ShouldSetOperationMode = ((DeviceBaseAction)action).ShouldOperationModeBeSet,
				ShouldShowMore = ((DeviceBaseAction)action).ShouldOperationModeBeSet,
			};
		    baseAction = action;

			SetBindings();

			showMoreButton.Click += ShowMoreButton_Click;
			DefaultButton.Click += DefaultButton_Click;
			removeButton.Click += RemoveButton_Click;
			AbortButton.Click += AbortButton_Click;
	    }

		private void SetTitle(BaseAction action)
	    {
		    var deviceCount = action.OutputDeviceIDs.Count;

			var title = string.Empty;
		    if (deviceCount == 1)
			{
				// todo: get device
				//Device dev = context.GetApp().GetDevice(Action.OutputDeviceIDs.FirstOrDefault()) as Device;
				var d = new GenericDevice();
				title = string.Format(i18n.smartrules_action_title_one, d.Name);
		    }
		    else
		    {
			    // todo: title for multiple devices
		    }

		    Title = title;
		}

	    private void SetBindings()
	    {
		    DataContext = viewModel;

		    setValueCheckBox.CheckedBinding.BindDataContext((ActionDialogViewModel vm) => vm.ShouldSetValue);
			actionFrame.Bind(a => a.Visible, Binding.Property(viewModel, vm => vm.ShouldSetValue));
			
		    setOperationModeCheckBox.CheckedBinding.BindDataContext((ActionDialogViewModel vm) => vm.ShouldSetOperationMode);
			operationModeStackLayout.Bind(s => s.Visible, Binding.Property(viewModel, vm => vm.ShouldSetOperationMode));

		    showMoreButton.Bind(b => b.Visible, Binding.Property(viewModel, vm => vm.ShouldShowMoreButton));

		    setValueCheckBox.Bind(cb => cb.Visible, Binding.Property(viewModel, vm => vm.ShouldShowMore));
		    setOperationModeCheckBox.Bind(cb => cb.Visible, Binding.Property(viewModel, vm => vm.ShouldShowMore));
			showMoreButton.Bind(b => b.Text, Binding.Property(viewModel, vm => vm.ShowMoreButtonText));
		}

	    private void ShowMoreButton_Click(object sender, System.EventArgs e)
	    {
		    viewModel.ShouldShowMore = !viewModel.ShouldShowMore;
	    }

		private void DefaultButton_Click(object sender, System.EventArgs e)
		{
			Close(Result.OK);
		}

	    private void RemoveButton_Click(object sender, System.EventArgs e)
		{
			Close(Result.Remove);
		}

		private void AbortButton_Click(object sender, System.EventArgs e)
		{
			Close(Result.Cancel);
		}
	}
}
