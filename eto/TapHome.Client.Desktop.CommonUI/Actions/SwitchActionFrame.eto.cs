﻿using Eto.Forms;
using TapHome.Core.Lib.IFTTT.Actions;
using TapHome.Translations;

namespace TapHome.Client.Desktop.CommonUI.Actions
{
    public partial class SwitchActionFrame : ActionFrame
    {
	    protected RadioButtonList switchActionsRadioButtonList;

		protected override void InitializeComponent()
		{
			base.InitializeComponent();

		    switchActionsRadioButtonList = new RadioButtonList
		    {
				Orientation = Orientation.Vertical,

				Items =
				{
					new ListItem { Key = SwitchAction.SwitchActionType.On.ToString(), Text = i18n.smartrule_action_switch_on },
					new ListItem { Key = SwitchAction.SwitchActionType.Off.ToString(), Text = i18n.smartrule_action_switch_off },
					new ListItem { Key = SwitchAction.SwitchActionType.Toggle.ToString(), Text = i18n.smartrule_action_switch_toggle },
				},
		    };

		    Items.Add(new StackLayoutItem(switchActionsRadioButtonList));
	    }
    }
}
