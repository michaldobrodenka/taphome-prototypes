﻿using System;
using Eto.Forms;
using TapHome.Core.Lib.IFTTT.Actions;

namespace TapHome.Client.Desktop.CommonUI.Actions
{
	// todo: GK - bindings
    public partial class SwitchActionFrame
    {
	    private SwitchAction action;

	    public SwitchActionFrame(SwitchAction action)
	    {
		    InitializeComponent();

		    this.action = action;
		    switchActionsRadioButtonList.SelectedKey = action.ActionType.ToString();

			//DataContext = action;

			//switchActionsRadioButtonList.SelectedKeyBinding.BindDataContext(Binding.Property((SwitchAction a) => a.ActionType).OfType<string>());
			switchActionsRadioButtonList.SelectedKeyChanged += SwitchActionsRadioButtonList_SelectedKeyChanged;
		}

		private void SwitchActionsRadioButtonList_SelectedKeyChanged(object sender, System.EventArgs e)
		{
			action.ActionType = (SwitchAction.SwitchActionType)Enum.Parse(typeof(SwitchAction.SwitchActionType), switchActionsRadioButtonList.SelectedKey);
		}
	}
}
