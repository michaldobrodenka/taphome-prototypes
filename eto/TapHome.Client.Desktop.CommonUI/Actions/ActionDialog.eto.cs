﻿using Eto.Drawing;
using Eto.Forms;
using TapHome.Core.Lib.IFTTT.Actions;
using TapHome.Translations;

namespace TapHome.Client.Desktop.CommonUI.Actions
{
    public partial class ActionDialog : Dialog<ActionDialog.Result>
    {
		//private class OperationModeRadioButtons
		//{
		//    public RadioButtonList
		//}

	    protected Scrollable scrollable;
        protected StackLayout mainStackLayout;

        protected ActionFrame actionFrame;
        protected StackLayout operationModeStackLayout;

        protected CheckBox setValueCheckBox;
        protected CheckBox setOperationModeCheckBox;

        protected RadioButtonList operationModeRadioButtonList;

	    protected Button removeButton;
	    protected Button showMoreButton;
		protected TableLayout buttonsLayout;

        void InitializeComponent(BaseAction action)
        {
	        actionFrame = ActionFrame.GetActionFrame(action);

            operationModeRadioButtonList = new RadioButtonList
            {
				Orientation = Orientation.Vertical,

                Items =
                {
                    new ListItem { Text = i18n.device_mode_Automatic, Key = "A", },
                    new ListItem { Text = i18n.device_mode_Manual, Key = "M" },
                }, 

				SelectedIndex = 0,
			};

            operationModeStackLayout = new StackLayout
			{
				Padding = new Padding(10),
				Spacing = 5,

                Items =
                {
                    new StackLayoutItem(operationModeRadioButtonList),
                },
            };

            setValueCheckBox = new CheckBox { Text = i18n.smartrule_action_multivalueswitch_setvalue };
            setOperationModeCheckBox = new CheckBox{ Text = i18n.smartrule_action_setOperationMode };

	        DefaultButton = new Button { Text = "OK" };
	        removeButton = new Button { Text = i18n.smartrule_action_remove };
			AbortButton = new Button { Text = "C&ancel" };
	        buttonsLayout = new TableLayout { Rows = { new TableRow(DefaultButton, removeButton, AbortButton) }, Spacing = new Size(5, 5) };
	        showMoreButton = new Button { Text = "Show more" };

			mainStackLayout = new StackLayout
			{
				Padding = new Padding(10),
				Spacing = 5,
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,

                Items =
                {
                    new StackLayoutItem(setValueCheckBox),
                    new StackLayoutItem(actionFrame),
                    new StackLayoutItem(setOperationModeCheckBox),
	                new StackLayoutItem(operationModeStackLayout),
	                new StackLayoutItem(showMoreButton),
					new StackLayoutItem(buttonsLayout),
				}
            };

	        scrollable = new Scrollable { Content = mainStackLayout };

	        Resizable = true;
	        MinimumSize = new Size(0, 200);
			Content = scrollable;
        }
    }
}
