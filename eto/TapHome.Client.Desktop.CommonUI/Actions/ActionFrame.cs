﻿
using TapHome.Core.Lib.IFTTT.Actions;

namespace TapHome.Client.Desktop.CommonUI.Actions
{
    public partial class ActionFrame
    {
	    public static ActionFrame GetActionFrame(BaseAction action)
	    {
		    ActionFrame frame;
			//if (action is ThermostatAction)
			//{
			//	frame = new TemperatureActionFrame(this, action as ThermostatAction);
			//}
			//else if (action is DimmerAction)
			//{
			//	frame = new DimmerActionFrame(this, action as DimmerAction);
			//}
			//else if (action is HueAction)
			//{
			//	frame = new HueActionFrame(this, action as HueAction);
			//}
			//else if (action is SwitchAction)
			{
				frame = new SwitchActionFrame(action as SwitchAction);
			}
			//else if (action is BlindsAction)
			//{
			//	var device = Storage.GetDevice(Storage.LastUsedLocationId.Value, action.OutputDeviceIDs.FirstOrDefault());

			//	frame = new BlindsActionFrame(this, ((device != null) && device.IsInterface(typeof(Slide))), action as BlindsAction);
			//}
			//else if (action is PushButtonAction)
			//{
			//	frame = new PushButtonActionFrame(this, action as PushButtonAction);
			//}
			//else if (action is MultiValueSwitchAction)
			//{
			//	frame = new MultivalueSwitchActionFrame(this, action as MultiValueSwitchAction);
			//}
			//else if (action is AlarmAction)
			//{
			//	frame = new AlarmActionFrame(this, action as AlarmAction);
			//}

		    return frame;
	    }
    }
}
