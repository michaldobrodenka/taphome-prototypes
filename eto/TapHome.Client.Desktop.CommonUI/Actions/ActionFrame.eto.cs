﻿using Eto.Drawing;
using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Actions
{
    public partial class ActionFrame : StackLayout
	{
		protected virtual void InitializeComponent()
		{
			Padding = new Padding(10);
			Spacing = 5;
		}
    }
}
