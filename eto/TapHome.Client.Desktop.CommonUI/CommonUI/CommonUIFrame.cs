﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eto.Forms;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.Core.Lib.IFTTT.Actions;

namespace TapHome.Client.Desktop.CommonUI
{
	public class CommonUIFrame : CommonUIPage
	{
		public string Name;

		protected object context;

		private StackLayout mainLayout;

		private bool created;

		// todo: can prolly be removed
		private string requestTag;

		//public class AlertDialogButtons
		//{
		//	public Tuple<string, Action> NeutralButton { get; set; }
		//	public Tuple<string, Action> PositiveButton { get; set; }
		//	public Tuple<string, Action> NegativeButton { get; set; }
		//}

		//private List<ProgressDialog> progressDialogs;

		//private List<AlertDialog> resultBasedProgressDialogs;

		public CommonUIFrame(object context) : base("", null)
		{
			//LayoutInflater inflater = LayoutInflater.From((Context)context);
			//inflater.Inflate(Resource.Layout.TapHomeBusModuleConfig, this, true);

			//this.mainLayout = this.FindViewById<LinearLayout>(Resource.Id.MainLayout);
			//OnInit();

			//progressDialogs = new List<ProgressDialog>();

			//resultBasedProgressDialogs = new List<AlertDialog>();
		}

		//public static void NavigateToPageWithParameters(Object parent, Type pageType, Dictionary<string, object> parameters, bool asRoot = false)
		//{
		//	//if (typeof(pageType) is CommonUIPage)

		//	var newPage = (CommonUIPage)Activator.CreateInstance(pageType, new object[] { parameters });

		//	(parent as CommonUIPage).Host.Push(newPage, asRoot);
		//}

		//public void RunOnUiThreadTh(Action action)
		//{
		//	//(this.context as Activity).RunOnUiThread(action);
		//	Application.Instance.Invoke(action);
		//}

		//public IDeviceManager CurrentApp
		//{
		//	get
		//	{
		//		return this.GetApp();
		//	}
		//}

		//public void SetLightTheme()
		//{
		//	if (this.mainLayout != null)
		//	{
		//		this.mainLayout.SetBackgroundColor(ResourceUtils.GetColor(context, Resource.Color.background_detail));
		//	}
		//}

		public void ReloadData() { }

		protected void RefreshOptionsMenu()
		{
			//(this.context as Activity)?.InvalidateOptionsMenu();
		}

		//protected void AddSection(CommonSection section)
		//{
		//	this.mainLayout.Items.Add(new StackLayoutItem(section));
		//}

		//protected void InsertSection(CommonSection section, int index)
		//{
		//	this.mainLayout.Items.Insert(index, new StackLayoutItem(section));
		//}

		//protected void AddFrame(FrameLayout frame, int x = 0, int y = 0)
		//{
		//	this.mainLayout.AddView(frame);
		//}

		//protected void AddFrame(LinearLayout frame, int x = 0, int y = 0)
		//{
		//	this.mainLayout.AddView(frame);
		//}

		//protected void AddChartFrame(FrameLayout frame)
		//{
		//	this.mainLayout.AddView(frame);
		//	mainLayout.SetBackgroundColor(Color.White);
		//}

		protected void MoveSection(int fromIndex, int toIndex)
		{
			var section = this.mainLayout.Items[fromIndex];

			this.mainLayout.Items.RemoveAt(fromIndex);

			this.mainLayout.Items.Insert(toIndex, section);
		}

        //protected void RemoveSection(CommonSection section)
        //{
        //	var index = this.mainLayout.Controls.ToList().IndexOf(section);

        //	if (index >= 0)
        //	{
        //		this.mainLayout.Items.RemoveAt(index);
        //	}
        //}

        //protected void RemoveAllSections()
        //{
        //	this.mainLayout.Items.Clear();
        //}

        //protected override void OnAttachedToWindow()
        //{
        //	base.OnAttachedToWindow();

        //	if (!created)
        //	{
        //		created = true;
        //		this.OnInit();
        //	}

        //	WillAppear();

        //	FrameDidAppear();

        //	OnBegin();
        //}

        //protected override void OnDetachedFromWindow()
        //{
        //	base.OnDetachedFromWindow();

        //	WillDisappear();

        //	FrameDidDisappear();

        //	OnEnd();
        //}

        public virtual void WillAppear() { OnInit(); }

        public virtual void WillDisappear() { }

        public override void CreateControls()
		{
			
		}

		public virtual void FrameDidAppear() {  }

		public virtual void FrameDidDisappear() { }

		public virtual void OnEnd() { }

		public virtual void OnBegin() { }

		protected delegate void MenuItemSelected(object sender, MenuItemSelectedArgs args);

		protected event MenuItemSelected OnMenuItemSelected;

		public virtual void FireMenuItemSelected(int selectedId)
		{
			this.OnMenuItemSelected?.Invoke(this, new MenuItemSelectedArgs()
			{
				Id = selectedId
			});
		}

		public class MenuItemSelectedArgs : EventArgs
		{
			public int Id;
		}

		public bool MenuItemsEnabled;
		public int SelectedMenuItem;

		public Dictionary<int, string> menuItems = new Dictionary<int, string>();

		protected void SetUpMenuItems(int selectedItem = 0)
		{
			menuItems.Clear();
			MenuItemsEnabled = true;
			SelectedMenuItem = selectedItem;
		}

		protected void AddMenuItem(int itemId, string item)
		{
			if (menuItems.ContainsKey(itemId))
				return;

			menuItems.Add(itemId, item);
		}

		protected void InsertFrame(CommonUIFrame frame, int index)
		{
			this.mainLayout.Items.Insert(index, frame);
		}

		//public void CreateToast(string text)
		//{
		//	AndroidUtils.ShowLongToast(Context, text);
		//}

		//public int CreateProgressDialog(string message, bool cancelable, Action onKeyListener)
		//{
		//	if ((context as CommonUIPage) != null)
		//	{
		//		return (context as CommonUIPage).CreateProgressDialog(message, cancelable, onKeyListener);
		//	}

		//	// for now
		//	int hashCode = 0;

		//	try
		//	{
		//		var dialog = new ProgressDialog(this.context);
		//		dialog.SetMessage(message);
		//		dialog.SetCancelable(cancelable);

		//		dialog.Show();

		//		if (progressDialogs == null)
		//		{
		//			progressDialogs = new List<ProgressDialog>();
		//		}

		//		progressDialogs.Add(dialog);

		//		if (onKeyListener != null)
		//			dialog.SetOnKeyListener(new OnKeyListener(() => onKeyListener.Invoke()));

		//		hashCode = dialog.GetHashCode();
		//	}
		//	catch { }

		//	return hashCode;
		//}

		//public void DismissProgressDialog(int hashCode)
		//{
		//	if ((context as CommonUIPage) != null)
		//	{
		//		(context as CommonUIPage).DismissProgressDialog(hashCode);
		//		return;
		//	}

		//	// for now
		//	var progressDialog = progressDialogs?.Where(pd => pd.GetHashCode() == hashCode).FirstOrDefault();

		//	if (progressDialog != null)
		//	{
		//		try
		//		{
		//			progressDialog.Dismiss();
		//			progressDialogs.Remove(progressDialog);
		//		}
		//		catch { }
		//	}
		//}

		//public virtual void OnInit() { }

		//public void CreateAlertDialog(string caption, string message, CommonUIPage.AlertDialogButtons buttons, string footerImage = "", string footerMessage = "")
		//{
		//	var buttns = new CommonUIPage.AlertDialogButtons();
		//	buttns.PositiveButton = buttons.PositiveButton;
		//	buttns.NegativeButton = buttons.NegativeButton;
		//	buttns.NeutralButton = buttons.NeutralButton;

		//	CreateAlertDialog(caption, message, buttns, footerImage, footerMessage);
		//}

		//public int CreateResultBasedProgressDialog(string title, string message, bool cancelable, out Progress<int> progress, string footerImage = "", string footerMessage = "")
		//{
		//	return CreateResultBasedProgressDialog(title, message, cancelable, out progress, footerImage, footerMessage);
		//}

		//public void ChangeResultBasedProgressDialogMessage(int hashCode, string message)
		//{
		//	ChangeResultBasedProgressDialogMessage(hashCode, message);
		//}

		public void CreateBottomSheetDialog(string caption, List<SheetItem> sheetItems)
		{
			//var rootView = LayoutInflater.From(context).Inflate(Resource.Layout.BottomSheetDialog, null);
			//var layout = rootView.FindViewById<LinearLayout>(Resource.Id.layout_main);
			//var list = rootView.FindViewById<ListView>(Resource.Id.listView);
			//var title = rootView.FindViewById<TextView>(Resource.Id.text_caption);

			//title.Visibility = ViewStates.Visible;
			//title.Text = caption;

			//var simpleAdapter = new BottomSheetAdapter(sheetItems);

			//list.SetPadding(0, 0, 0, 0);
			//list.Adapter = simpleAdapter;

			//var bottomSheet = new BottomSheetDialog(context);
			//bottomSheet.SetContentView(layout);

			//simpleAdapter.SelectedItem += (sen, ev) =>
			//{
			//	bottomSheet.Dismiss();
			//};

			//bottomSheet.Show();
		}

		public void ShowActionPickerDialog(string intrface, int[] deviceIDs, object sender, string requestTag = "")
		{
			this.requestTag = requestTag;

			var action = BaseAction.GetDefaultAction(intrface);
			if (action == null)
				return;

			foreach (int deviceId in deviceIDs)
			{
				action.OutputDeviceIDs.Add(deviceId);
			}

			ShowActionPickerDialog(action, true, sender);
		}

		public void ShowActionPickerDialog(BaseAction action, bool isCreateDialog, object sender, string requestTag = "", bool isDeleteEnabled = true)
		{
			if (String.IsNullOrEmpty(this.requestTag))
			{
				this.requestTag = requestTag;
			}
		}

		private void AddActions(BaseAction action)
		{
			List<BaseAction> actions = new List<BaseAction>();

			for (int i = 0; i < action.OutputDeviceIDs.Count; i++)
			{
				BaseAction clonedAction = action.CloneAction();
				clonedAction.OutputDeviceIDs.Clear();
				clonedAction.OutputDeviceIDs.Add(action.OutputDeviceIDs.ElementAt(i));
				actions.Add(clonedAction);
			}

			OnAddAction(actions, this.requestTag);
		}

		public virtual void OnAddAction(List<BaseAction> newActions, string requestTag)
		{
		}

		public virtual void OnUpdateAction(BaseAction action, object sender, string requestTag)
		{
		}

		public virtual void OnDeleteAction(BaseAction action, object sender, string requestTag)
		{
		}

		public virtual void OnAddVariables(List<DeviceVariable> variables, bool variablesOnInput)
		{
		}

		//public sealed class OnKeyListener : Java.Lang.Object, IDialogInterfaceOnKeyListener
		//{
		//	private readonly Action action;

		//	public OnKeyListener(Action action)
		//	{
		//		this.action = action;
		//	}

		//	public bool OnKey(IDialogInterface dialog, Keycode keyCode, KeyEvent keyEvent)
		//	{
		//		this.action();

		//		if (keyCode == Keycode.Back)
		//			return true;

		//		return false;
		//	}
		//}
		public override void OnBack()
		{
			
		}
	}
}
