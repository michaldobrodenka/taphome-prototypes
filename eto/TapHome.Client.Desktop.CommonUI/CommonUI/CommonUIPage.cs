﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using System.Linq;
using Com.TapHome.CommonUI;

namespace TapHome.Client.Desktop.CommonUI
{
    public abstract partial class CommonUIPage
	{
		public event EventHandler<EventArgs> RenameClicked;

		public class AlertDialogButtons
		{
			public Tuple<string, Action> NeutralButton { get; set; }
			public Tuple<string, Action> PositiveButton { get; set; }
			public Tuple<string, Action> NegativeButton { get; set; }
		}

		public class AlertDialogElementsButtons
		{
			public Tuple<string, Action<string>> PositiveButton { get; set; }
			public Tuple<string, Action> NegativeButton { get; set; }
		}

		public class AlertDialogElements
		{
			public Tuple<string, string> TextField { get; set; }
		}

		protected string title;

        public Dictionary<string, object> Parameters { get; private set; }

        public CommonUIHost Host { get; set; }

        public CommonMainList MainList { get; private set; }

        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                title = value;
                // todo refresh
            }
        }
        
        protected CommonUIPage(string caption, Dictionary<string, object> parameters)
        {
            this.Parameters = parameters;
            InitializeComponent();
        }

        public static void NavigateToPageWithParameters(Object parent, Type pageType, Dictionary<string, object> parameters, bool asRoot = false)
        {
            //if (typeof(pageType) is CommonUIPage)

            var newPage = (CommonUIPage)Activator.CreateInstance(pageType, new object[] { parameters });

            (parent as CommonUIPage).Host.Push(newPage, asRoot);
        }

        protected object GetParameter(string id, Type type)
        {
            if (Parameters != null)
            {
                if (Parameters.ContainsKey(id))
                {
                    return Parameters.FirstOrDefault((o) => o.Key == id).Value;
                }
            }

            if (type == typeof(Int32))
            {
                return -1;
            }
            else if (type == typeof(string))
            {
                return String.Empty;
            }
            else if (type == typeof(long))
            {
                return -1L;
            }
            else if (type == typeof(bool))
            {
                return false;
            }

            throw new InvalidOperationException("Given type is not supported!");
        }

        private int maxSectionWidth = -2;

        protected void AddSection(CommonSection section)
        {
	        //if (Application.Instance.Platform.IsWpf)
			if(CommonUIHost.UseManualResizing())
	        {
		        section.SetWidth(this.VisibleRect.Width);
		        if (maxSectionWidth < this.VisibleRect.Width)
			        maxSectionWidth = this.VisibleRect.Width;
	        }

	        this.stackLayout.Items.Add(new StackLayoutItem(section));

			//if (Application.Instance.Platform.IsWpf)
			if(CommonUIHost.UseManualResizing())
			{
				if (this.VisibleRect.Width < this.maxSectionWidth)
					this.RefreshWidth();
			}
        }

        protected void SetMainList(CommonMainList mainList)
        {
            if (mainList == null)
                return;

            this.MainList = mainList;
            // todo events in Resume and Stop
            //this.mainLayout.AddView(MainList.GetView());
            this.stackLayout.Items.Add(new StackLayoutItem(mainList.GetView()));
        }

        protected void InsertSection(CommonSection section, int index)
        {
            this.stackLayout.Items.Insert(index, new StackLayoutItem(section));
        }

        protected void RemoveSection(CommonSection section)
        {
            var index = this.stackLayout.Controls.ToList().IndexOf(section);

            if (index >= 0)
            {
                this.stackLayout.Items.RemoveAt(index);
            }
        }

        protected void RemoveAllSections()
        {
            this.stackLayout.Items.Clear();
        }

		public abstract void CreateControls();

		public virtual void OnInit() { }

        public virtual void WillAppear() { }

        public virtual void WillDisappear() { }

	    public void CreateAlertDialog(string caption, string message, AlertDialogButtons buttons, string footerImage = "", string footerMessage = "")
	    {
			//try
			//{
			// caption = BuildUtils.ReplaceTapHomeInText(caption);
			// message = BuildUtils.ReplaceTapHomeInText(message);

			// var dialogBuilder = new global::Android.App.AlertDialog.Builder(this);

			// if (!String.IsNullOrEmpty(caption))
			// {
			//  dialogBuilder.SetTitle(caption);
			// }

			// if (String.IsNullOrEmpty(footerImage) && String.IsNullOrEmpty(footerMessage))
			// {
			//  if (!String.IsNullOrEmpty(message))
			//  {
			//   dialogBuilder.SetMessage(message);
			//  }
			// }

			// else
			// {
			//  dialogBuilder.SetView(GetDialogMessageLayout(message, footerImage, footerMessage));
			// }

			// ShowAlertDialog(buttons, dialogBuilder);
			//}
			//catch { }
		}

		public void CreateAlertDialogWithElements(string caption, string message, AlertDialogElementsButtons buttons, AlertDialogElements elements)
		{
			//try
			//{
			//	caption = BuildUtils.ReplaceTapHomeInText(caption);
			//	message = BuildUtils.ReplaceTapHomeInText(message);

			//	var dialogBuilder = new global::Android.App.AlertDialog.Builder(this);

			//	View dialogLayout = LayoutInflater.From(this).Inflate(Resource.Layout.Dialog, null);
			//	LinearLayout layout = dialogLayout.FindViewById<LinearLayout>(Resource.Id.dialog);
			//	EditText renameEdit = new EditText(this);

			//	if (!String.IsNullOrEmpty(caption))
			//	{
			//		dialogBuilder.SetTitle(caption);
			//	}
			//	if (!String.IsNullOrEmpty(message))
			//	{
			//		dialogBuilder.SetMessage(message);
			//	}

			//	if (elements != null)
			//	{
			//		renameEdit = new EditText(this) { Text = elements.TextField.Item2, InputType = global::Android.Text.InputTypes.TextVariationEmailAddress };
			//		layout.AddView(renameEdit);
			//	}

			//	dialogBuilder.SetView(dialogLayout);

			//	Tuple<string, Action<string>> posititiveButton = buttons.PositiveButton;
			//	Tuple<string, Action> negativeButton = buttons.NegativeButton;

			//	if (buttons.PositiveButton != null)
			//	{
			//		dialogBuilder.SetPositiveButton(posititiveButton.Item1, (s, e) =>
			//		{
			//			posititiveButton.Item2.Invoke(renameEdit.Text);
			//		});
			//	}

			//	if (buttons.NegativeButton != null)
			//	{
			//		dialogBuilder.SetNegativeButton(negativeButton.Item1, (s, e) =>
			//		{
			//			negativeButton.Item2.Invoke();
			//		});
			//	}

			//	dialogBuilder.Show();
			//}
			//catch { }
		}

		public int CreateProgressDialog(string message, bool cancelable, Action onKeyListener)
		{
			int hashCode = 0;

			//try
			//{
			//	var dialog = new ProgressDialog(this);
			//	dialog.SetMessage(message);
			//	dialog.SetCancelable(cancelable);

			//	dialog.Show();

			//	if (progressDialogs == null)
			//	{
			//		progressDialogs = new List<ProgressDialog>();
			//	}

			//	progressDialogs.Add(dialog);

			//	if (onKeyListener != null)
			//		dialog.SetOnKeyListener(new OnKeyListener(() => onKeyListener.Invoke()));

			//	hashCode = dialog.GetHashCode();
			//}
			//catch { }

			return hashCode;
		}

		public int CreateResultBasedProgressDialog(string title, string message, bool cancelable, out Progress<int> progress, string footerImage = "", string footerMessage = "")
		{
			int hashCode = 0;

			progress = new Progress<int>();

			//try
			//{
			//	var dialog = new AlertDialog.Builder(this);
			//	dialog.SetTitle(title);

			//	LayoutInflater inflater = (LayoutInflater)this.GetSystemService(LayoutInflaterService);

			//	View view = inflater.Inflate(Resource.Layout.ResultBasedProgressDialog, null);

			//	view.FindViewById<LinearLayout>(Resource.Id.layout_main).AddView(GetDialogMessageLayout(message, footerImage, footerMessage), 0);

			//	ProgressBar progressBar = view.FindViewById<ProgressBar>(Resource.Id.progressBar);
			//	TextView textViewProgress = view.FindViewById<TextView>(Resource.Id.textView_progress);

			//	textViewProgress.Text = "0%";

			//	dialog.SetView(view);
			//	dialog.SetCancelable(cancelable);

			//	AlertDialog progressDialog = dialog.Create();

			//	progress = new Progress<int>((i) =>
			//	{
			//		this.RunOnUiThreadTh(() =>
			//		{
			//			try
			//			{
			//				progressBar.Progress = i;
			//				textViewProgress.Text = String.Format("{0}%", i);
			//			}
			//			catch { }
			//		});
			//	});

			//	progressDialog.Show();

			//	resultBasedProgressDialogs.Add(progressDialog);

			//	hashCode = dialog.GetHashCode();
			//}
			//catch { }

			return hashCode;
		}

		public void ChangeResultBasedProgressDialogMessage(int hashCode, string message)
		{
			//var progressDialog = resultBasedProgressDialogs.FirstOrDefault();

			//if (progressDialog != null && messageText != null)
			//{
			//	try
			//	{
			//		messageText.Text = message;
			//	}
			//	catch
			//	{
			//	}
			//}
		}

		public void DismissProgressDialog(int hashCode)
		{
			//var progressDialog = progressDialogs?.Where(pd => pd.GetHashCode() == hashCode).FirstOrDefault();

			//if (progressDialog != null)
			//{
			//	try
			//	{
			//		progressDialog.Dismiss();
			//		progressDialogs.Remove(progressDialog);
			//	}
			//	catch { }
			//}

			//var resultBasedProgressDialog = resultBasedProgressDialogs?.FirstOrDefault();

			//if (resultBasedProgressDialog != null)
			//{
			//	try
			//	{
			//		resultBasedProgressDialog.Dismiss();
			//		resultBasedProgressDialogs.Remove(resultBasedProgressDialog);
			//	}
			//	catch { }
			//}

			//messageText = null;
		}

		public void SetTitle()
		{
		}

		public void SetLightTheme()
		{
			
		}

		public void CreateToast(string message)
		{

		}

		public abstract void OnBack();

		public void GoBack()
		{
            Host.GoBack();
		}

		public void RunOnUiThreadTh(Action action)
		{
			Application.Instance.Invoke(action);
		}
	}
}
