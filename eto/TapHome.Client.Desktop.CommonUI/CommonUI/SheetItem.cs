﻿using System;

namespace TapHome.Client.Desktop.CommonUI
{
	public class SheetItem
	{
		public string Caption { get; set; }
		public string IconId { get; set; }
		public bool Disabled { get; set; }
		public bool Selected { get; set; }
		public Action OnSelected { get; set; }
		public bool IsSelectable { get; set; }
	}
}
