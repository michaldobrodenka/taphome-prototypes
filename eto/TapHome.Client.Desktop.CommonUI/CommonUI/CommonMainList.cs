﻿using Eto.Forms;
using System;
using System.Collections.Generic;
using System.Text;

namespace Com.TapHome.CommonUI
{
    public class CommonMainList
    {
        private Control listView;
        //private Context context;

        //public MainListViewModel ViewModel;

        public CommonMainList(object context)
        {
            //this.context = context as Context;
            //ViewModel = new MainListViewModel();
        }

        public Control GetView()
        {
            this.listView = ControlProvider.GetListControl();

            return this.listView;

            //LayoutInflater inflater = LayoutInflater.From(context);
            //var view = inflater.Inflate(Resource.Layout.LayoutMainList, null, false);

            //this.mainLayout = view.FindViewById<RelativeLayout>(Resource.Id.mainListLayout);
            //listView = mainLayout.FindViewById<ExpandableListView>(Resource.Id.expandableListView);
            //listView.SetScrollContainer(false);

            //Adapter = new MainListAdapter(ViewModel, context, listView);
            //listView.SetAdapter(Adapter);
            //listView.GroupClick += (s, e) => { };

            //for (int i = 0; i < ViewModel.DataStore.SectionsCount; i++)
            //{
            //    listView.ExpandGroup(i);
            //}

            //return mainLayout;
        }

    }
}
