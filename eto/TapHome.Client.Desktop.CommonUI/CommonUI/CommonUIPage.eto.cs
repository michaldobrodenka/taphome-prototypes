﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.Client.Desktop.CommonUI
{
    partial class CommonUIPage : Panel
    {
        private Scrollable scrollable;

        private StackLayout stackLayout;

        public Rectangle VisibleRect
        {
            get
            {
                return this.scrollable.VisibleRect;
            }
        }

        void InitializeComponent()
        {
            this.MinimumSize = new Size(280, 250);

            this.stackLayout = new StackLayout()
            {
                Padding = 0,
                Orientation = Orientation.Vertical,
				HorizontalContentAlignment = CommonUIHost.UseManualResizing()  ? HorizontalAlignment.Left : HorizontalAlignment.Stretch,
			};

            this.scrollable = new Scrollable() { Content = this.stackLayout };
            this.scrollable.SizeChanged += Scrollable_SizeChanged;
            this.Content = this.scrollable;
        }

        private void Scrollable_SizeChanged(object sender, EventArgs e)
        {
			RefreshWidth();
        }

        public void RefreshWidth()
        {
			this.scrollable.Content.Size = new Size(this.scrollable.VisibleRect.Width, -1);

			if (!CommonUIHost.UseManualResizing())
			 return
;
            this.scrollable.UpdateScrollSizes();

            foreach (var control in this.Children)
            {
                var section = control as CommonSection;

                if (section != null)
                {
                    section.SetWidth(this.scrollable.VisibleRect.Width);
                }
            }

            maxSectionWidth = this.VisibleRect.Width;
        }

        private int previousWidth = -2;

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            if (previousWidth == -2)
            {
                this.RefreshWidth();
                this.previousWidth = this.Width;
            }
        }
    }
}
