﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TapHome.Client.Desktop.CommonUI;

namespace TapHome.Commons.CommonFrames
{
	public class ServiceSettingsFrame : CommonUIFrame
	{
	    public enum PropertyInputType
	    {
	        Boolean = 0,
	        Number = 1,
	        SignedDecimalNumber = 2,
	        UnsignedDecimalNumber = 3,
	        SignedNumber = 4,
	        Icon = 5,
	        String = 6,
	        Slider = 7,
	    }

	    public enum PropertyOutputType
	    {
	        None = 0,
	        Second = 1,
	        Percentage = 2,
	        DegreeCelsius = 3,
	        DegreeFahrenheit = 4,
	        Angle = 5,
	        Speed = 6,
	        MilliSecond = 7,
	        Voltage = 8,
	        MilliAmpers = 9,
	    }

	    public class PropertyInfo
	    {
	        public PropertyInputType InputType { get; set; }
	        public PropertyOutputType OutputType { get; set; }
	        public double? MinValue { get; set; }
	        public double? MaxValue { get; set; }
	    }

        public ServiceSettingsFrame(object context) : base(context)
		{
		}
	}
}