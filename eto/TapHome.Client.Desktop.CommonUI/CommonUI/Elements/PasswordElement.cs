﻿using System;
using TapHome.DesktopClient.Wpf.Dialogs;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    public partial class PasswordElement
    {
        public class ChangePasswordEventArgs : EventArgs
        {
            public string Password { get; set; }
        }

        public class GetPasswordEventArgs : EventArgs
        {
            public string OldPassword { get; set; }
            public string NewPassword { get; set; }
        }

        public event EventHandler<ChangePasswordEventArgs> PasswordChanged;

        public event EventHandler<GetPasswordEventArgs> PasswordGet;

        public string Password
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public string oldPasscode { get; set; }

        private bool numericInput;

        private string value;

        private string caption;

        private bool hiddenPassword;

        //bool password - pismena alebo len cisla
        public PasswordElement(object context, string password, bool numericInput = true, string caption = null, bool showHiddenPassword = false)
			: base(context, "Change password", "••••••••")
        {
            Password = password;

            this.numericInput = numericInput;
            this.caption = caption;
            this.hiddenPassword = showHiddenPassword;

            InitializeComponent();
        }

	    protected override void mainStackLayout_Click(object sender, EventArgs e)
	    {
		    Clicked();
	    }

	    void Clicked()
        {
            if (numericInput)
            {
            }
            else
            {
                var dialog = new PasswordDialog();
                dialog.ShowModal(this);

                if (dialog.Result != null)
                {
                    this.oldPasscode = dialog.Result.OldPassword;
                    this.Password = dialog.Result.NewPassword;
                    this.FireGetPassword();
                }
            }
        }

        void FireGetPassword()
        {
            this.PasswordGet?.Invoke(this, new GetPasswordEventArgs { NewPassword = this.Password, OldPassword = this.oldPasscode });
        }
    }
}
