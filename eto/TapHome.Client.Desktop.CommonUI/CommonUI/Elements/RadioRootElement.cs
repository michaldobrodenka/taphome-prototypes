﻿using System;
using System.Collections.Generic;
using TapHome.DesktopClient.Wpf.Dialogs;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    public partial class RadioRootElement
    {
	    protected class RadioRootElementViewModel : LabelElementViewModel
		{
			public RadioRootElementViewModel(string caption, bool enabled) : base(caption, enabled) { }
			
			private int selectedValueIndex;
		    public int SelectedValueIndex
		    {
			    get { return selectedValueIndex; }
			    set
			    {
					if (selectedValueIndex == value)
						return;

				    selectedValueIndex = value;
				    OnPropertyChanged();
				    OnPropertyChanged(nameof(Value));
				}
		    }

			public override string Value
			{
				get
				{
					if (SelectedValueIndex > -1 && SelectedValueIndex < Values.Count)
						return Values[SelectedValueIndex];
					else
						return string.Empty;
				}
			}

			public string DisabledDetailText { get; set; }
			public List<string> Values { get; set; }
	    }

		public class RadioRootSelectedChangedEventArgs : EventArgs
        {
            public int Selected { get; set; }
            //public string Name { get; set; }
        }

        public event EventHandler<RadioRootSelectedChangedEventArgs> SelectedChanged;

        public event EventHandler<EventArgs> RemoveClicked;

		public int SelectedValueIndex
        {
            get { return this.viewModel.SelectedValueIndex; }
            set { this.viewModel.SelectedValueIndex = value; }
        }
		
        public new bool Enabled
        {
            get { return this.viewModel.Enabled; }
            set { this.viewModel.Enabled = value; }
        }
		
        public string DisabledDetailText
        {
            get { return this.viewModel.DisabledDetailText; }
            set { this.viewModel.DisabledDetailText = value; }
        }

        protected bool removeButton;

	    protected RadioRootElementViewModel viewModel;
		protected RadioRootElementViewModel ViewModel
		{
			get { return viewModel; }
			set
			{
				viewModel = value;
				LabelViewModel = viewModel;
			}
		}

		protected RadioRootElement(object context) : base(context, string.Empty, string.Empty)
        {
        }

        public RadioRootElement(object context, string caption, List<string> values, int selectedValueIndex = 0, bool enabled = false, bool removeButton = false)
            : base(context, caption, string.Empty)
        {
            Init(context, caption, enabled);

	        this.removeButton = removeButton;

			ViewModel = new RadioRootElementViewModel(caption, enabled)
	        {
		        DisabledDetailText = null,
		        Values = values,
				SelectedValueIndex = selectedValueIndex,
	        };

	        SetBindings();
        }

        public void ReplaceValues(List<string> newItems)
        {
            int newSelectedIndex = 0;

            if (this.viewModel.SelectedValueIndex >= 0 && this.viewModel.Values != null)
                newSelectedIndex = newItems.IndexOf(this.viewModel.Values[this.viewModel.SelectedValueIndex]);

            if (newSelectedIndex == -1)
                newSelectedIndex = 0;

            this.viewModel.Values = newItems;
            this.SelectedValueIndex = newSelectedIndex;
        }

        protected void Init(object context, string caption, bool enabled)
        {
            this.InitializeComponent();
		}

	    protected override void mainStackLayout_Click(object sender, EventArgs e)
	    {
		    OnClick();
	    }

	    public void OnClick()// CommonUIPage page = null)
        {
            if (!this.Enabled)
                return;

            int checkedItemIndex = this.viewModel.SelectedValueIndex;

            ShowRadioDialog(checkedItemIndex);
        }

        protected virtual void ShowRadioDialog(int checkedItemIndex)
        {
	        Action thirdButtonAction = null;
	        if (removeButton)
		        thirdButtonAction = FireRemoveClicked;

			var dialog = new RadioDialog(this.viewModel.Caption, this.viewModel.Values.ToArray(), this.viewModel.SelectedValueIndex, removeButton, thirdButtonAction);

	        var result = dialog.ShowModal(this);

	        if (result >= 0)
	        {
		        this.viewModel.SelectedValueIndex = result;
		        this.FireSelectedChanged();
	        }
		}

		void FireSelectedChanged()
        {
            SelectedChanged?.Invoke(this, new RadioRootSelectedChangedEventArgs { Selected = this.SelectedValueIndex/*, Name = this.deviceName*/ });
        }

        void FireRemoveClicked()
        {
            this.RemoveClicked?.Invoke(this, new EventArgs { });
        }
    }
}
