﻿using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	partial class BooleanElement : LabelElement
	{
		private CheckBox valueCheckBox;

		void InitializeComponent()
		{
			firstRowLayout.Remove(valueLabel);

			valueCheckBox = new CheckBox();
            firstRowLayout.Items.Add(new StackLayoutItem(valueCheckBox, HorizontalAlignment.Right, false));
		}
	}
}
