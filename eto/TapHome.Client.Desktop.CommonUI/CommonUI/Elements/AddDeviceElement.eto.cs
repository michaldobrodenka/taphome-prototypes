﻿using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	partial class AddDeviceElement : Panel
	{
		private StackLayout mainLayout;
		private Label addTextView;

		void InitializeComponent()
		{
			addTextView = new Label { Text = "add" };
			mainLayout = new StackLayout
			{
				Padding = 5,

				Items =
				{
					//new UniversalIcon(icon_small_normal_smartrule_add),
					addTextView,
					//new UniversalIcon(icon_more),
				},
			};

			Content = mainLayout;
		}
	}
}
