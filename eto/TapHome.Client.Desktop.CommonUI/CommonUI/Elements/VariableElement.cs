﻿using System;
using TapHome.Client.Desktop.CommonUI.Dependencies;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public partial class VariableElement
	{
		public class VariableEventArgs : EventArgs
		{
			public readonly DeviceVariable Variable;
			public VariableEventArgs(DeviceVariable variable)
			{
				Variable = variable;
			}
		}

		public event EventHandler<VariableEventArgs> VariableClick;

		private DeviceVariable variable;
		private bool showValue;

		protected VariableElement(object context) : base(context, string.Empty, string.Empty)
		{
		}

		public VariableElement(object context, DeviceVariable variable, bool showValue = true)
			: base(context, string.Empty, string.Empty)
		{
			InitializeComponent();

			this.variable = variable;
			this.showValue = showValue;
			
			CreateVariable(variable);
		}

		protected override void mainStackLayout_Click(object sender, EventArgs e)
		{
			VariableClick?.Invoke(sender, new VariableEventArgs(variable));
		}

		// todo: GK - po pridani do realneho projektu doplnit
		private void CreateVariable(DeviceVariable variable)
		{
			LabelViewModel.Caption = $"MegaDevice > {variable.Variable}";
			LabelViewModel.Value = $"{variable.Abbr} = 0.5";
			//Device device = Storage.GetDevice(Storage.LastUsedLocationId.Value, variable.ID);

			//DeviceProperty property = null;
			//if (device != null) property = DeviceProperties.GetProperties(device.Interface).FirstOrDefault(p => p.Name.Equals(variable.Variable));

			//string itemValue = "";
			//string itemName = device == null ?
			//	Context.GetString(Resource.String.device_not_found) : device.Name;

			//if (device == null)
			//{

			//}
			//else if (property == null)
			//{
			//	itemName = Context.GetString(Resource.String.device_variable_missing) + " " + itemName;

			//}
			//else
			//{
			//	itemName += " > " + GetVariableName(Context, property.Name);

			//	if (showValue)
			//		itemValue = variable.Abbr + " = " + property.FormatValue(Context.GetApp(), device);
			//}

			//view.FindViewById<TextView>(Resource.Id.textView_name).Text = itemName;
			//view.FindViewById<TextView>(Resource.Id.textView_variable).Text = itemValue;

			//return view;
		}
	}
}
