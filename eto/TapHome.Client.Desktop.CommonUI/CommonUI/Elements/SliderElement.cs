﻿using System;
using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public partial class SliderElement
	{
		public class ValueChangedEventArgs : EventArgs
		{
			public int Progress;
			public int sliderId;
		}

		public event EventHandler<ValueChangedEventArgs> ValueChanged;

		public bool Enabled
		{
			set { LabelViewModel.Enabled = value; }
		}

		public int MaxValue
		{
			set { slider.MaxValue = value; }
		}

		public string ValueText
		{
			set { LabelViewModel.Value = value; }
		}

		public int ValueSelected
		{
			get { return int.Parse(LabelViewModel.Value); }
			set { LabelViewModel.Value = value.ToString(); }
		}

		public int id;

        public SliderElement(object context, string title = "", int id = 0) : base(context, title, "0")
		{
			Init();
			this.id = id;

            slider.ValueChanged += Slider_ValueChanged;

			slider.Bind(Binding.Property((Slider s) => s.Value).ToType<string>(), Binding.Property(LabelViewModel, (vm) => vm.Value));
			slider.Bind(Binding.Property((Slider s) => s.Enabled), Binding.Property(LabelViewModel, (vm) => vm.Enabled));
		}

        private void Init()
		{
			InitializeComponent();
		}

	    private void Slider_ValueChanged(object sender, EventArgs e)
	    {
	        var handler = this.ValueChanged;

	        if (handler != null)
	        {
	            handler(this, new ValueChangedEventArgs { Progress = slider.Value, sliderId = this.id });
	        }
        }
    }
}
