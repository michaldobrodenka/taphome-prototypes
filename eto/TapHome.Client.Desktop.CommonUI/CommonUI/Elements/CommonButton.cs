﻿using System;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public partial class CommonButton
	{
		public event EventHandler<EventArgs> Clicked;
		public event EventHandler<EventArgs> LongClickStart;
		public event EventHandler<EventArgs> LongClickStop;

		public enum Style
		{
			Default = 0,
			Delete = 1,
		}

		private bool held;

		public CommonButton(object context, string caption, Style style)
		{
			held = false;

			if (style == Style.Delete)
			{
				// todo: GK - set color
				//Drawable background = button.Background;
				//PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.Red, PorterDuff.Mode.SrcAtop);
				//background.SetColorFilter(filter);
			}

			InitializeComponent();

			button.Text = caption;
			button.Click += CommonButton_Click;
			// todo: GK - long click?
			//button.LongClick += CommonButton_LongClick;
			//button.Touch += CommonButton_Touch;
		}

		//private void CommonButton_Touch(object sender, TouchEventArgs e)
		//{
		//	e.Handled = false;
		//	if (held)
		//	{
		//		e.Handled = true;
		//		if (e.Event.Action == MotionEventActions.Up || e.Event.Action == MotionEventActions.Cancel)
		//		{
		//			held = false;
		//			this.LongClickStop?.Invoke(this, new EventArgs { });
		//		}
		//	}
		//}

		//private void CommonButton_LongClick(object sender, LongClickEventArgs e)
		//{
		//	if (!held)
		//	{
		//		held = true;
		//		this.LongClickStart?.Invoke(this, new EventArgs { });
		//	}
		//}

		private void CommonButton_Click(object sender, EventArgs e)
		{
			this.Clicked?.Invoke(this, new EventArgs { });
		}
	}
}
