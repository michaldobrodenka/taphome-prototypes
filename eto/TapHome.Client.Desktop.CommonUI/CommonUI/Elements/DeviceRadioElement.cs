﻿using System;
using System.Collections.Generic;
using TapHome.DesktopClient.Wpf.Dialogs;
using TapHome.Translations;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public class DeviceRadioRootElement : RadioRootElement
	{
		public class DeviceNameChangedEventArgs : EventArgs
		{
			public int Selected { get; set; }
			public string Name { get; set; }
		}

		private string generatedName;
		private string deviceName;
		private string originalName;

		public string Name
		{
			get
			{
				return this.deviceName;
			}
			set
			{
				this.deviceName = value;
				this.RefreshName(generatedName, value);
			}
		}

		public event EventHandler<DeviceNameChangedEventArgs> DeviceNameChanged;

		private RadioDialog radioDialog;
		private bool IsRadioDialogOpen { get { return radioDialog != null; } }

		protected DeviceRadioRootElement(object context) : base(context)
		{
		}

		public DeviceRadioRootElement(object context, string caption, string name, List<string> values, int selectedValueIndex = 0, bool enabled = false)
			: base(context, caption, values, selectedValueIndex, enabled)
		{
			this.RefreshName(caption, name);
		}

		private void RefreshName(string caption, string name)
		{
			if (String.IsNullOrEmpty(name))
			{
				this.viewModel.Caption = caption;
			}
			else
			{
				this.viewModel.Caption = caption + " (" + name + ")";
				this.originalName = name;
			}

			this.generatedName = caption;
			this.deviceName = name;
		}
        
		protected override void ShowRadioDialog(int checkedItemIndex)
        {
            radioDialog = new RadioDialog(this.viewModel.Caption, this.viewModel.Values.ToArray(), this.viewModel.SelectedValueIndex, viewModel.SelectedValueIndex != 0, this.ShowRenameDialog);
	        radioDialog.SelectedIndexChanged += (sender, args) =>
	        {
		        radioDialog.ShowThirdButton = args.Selected != 0;
	        };

			checkedItemIndex = radioDialog.ShowModal(this);

	        radioDialog = null;

			if (checkedItemIndex >= 0)
            {
                if (this.viewModel.SelectedValueIndex != checkedItemIndex)
                {
                    if (!string.IsNullOrEmpty(this.originalName))
                    {
                        this.deviceName = this.originalName;
                        this.viewModel.Caption = this.generatedName + " (" + this.deviceName + ")";
                    }
                    else
                    {
                        this.viewModel.Caption = generatedName;
                        this.deviceName = null;
                    }
                }

                this.viewModel.SelectedValueIndex = checkedItemIndex;
				
                this.FireDeviceNameChanged();
			}
		}
		
        private void ShowRenameDialog()
        {
			var renameDialog = new StringDialog(i18n.detail_smartRules_rename, deviceName);
			var newName = renameDialog.ShowModal(ParentWindow);
			if (string.IsNullOrEmpty(newName))
				return;

			viewModel.Caption = $"{generatedName} ({newName})";
			originalName = newName;
			deviceName = newName;

			if (IsRadioDialogOpen)
				radioDialog.Caption = viewModel.Caption;
			
			FireDeviceNameChanged();
		}

		void FireDeviceNameChanged()
		{
			this.DeviceNameChanged?.Invoke(this, new DeviceNameChangedEventArgs { Selected = this.SelectedValueIndex, Name = this.deviceName });
		}
	}
}
