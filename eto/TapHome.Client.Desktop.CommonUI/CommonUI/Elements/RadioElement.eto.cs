﻿using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	partial class RadioElement : LabelElement
	{
		private RadioButton valueRadioButton;

		void InitializeComponent()
		{
			firstRowLayout.Remove(valueLabel);

			valueRadioButton = new RadioButton();
			firstRowLayout.Items.Add(new StackLayoutItem(valueRadioButton, HorizontalAlignment.Right, false));
		}
	}
}
