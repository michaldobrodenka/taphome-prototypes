﻿using System;
using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public partial class RadioElement
	{
		private class RadioElementViewModel : LabelElementViewModel
		{
			public RadioElementViewModel(string caption, bool enabled) : base(caption, enabled) { }

			private bool radiovalue;
			public bool RadioValue
			{
				get { return radiovalue; }
				set
				{
					if (this.radiovalue == value)
						return;

					this.radiovalue = value;
					OnPropertyChanged();
				}
			}
		}

		public event EventHandler ValueChanged;

		public new bool Value
		{
			get { return ViewModel.RadioValue; }
			set { ViewModel.RadioValue = value; }
		}

		private RadioElementViewModel viewModel;
		private RadioElementViewModel ViewModel
		{
			get { return viewModel; }
			set
			{
				viewModel = value;
				LabelViewModel = value;
			}
		}

		public RadioElement(object context, string caption, bool value, string valueText = "", string description = "", bool enabled = true)
			: base(context, caption, string.Empty, description)
		{
			InitializeComponent();

			ViewModel = new RadioElementViewModel(caption, enabled)
			{
				Description = description,
				RadioValue = value,
			};

			SetBindings();

			valueRadioButton.CheckedChanged += ValueRadioButton_CheckedChanged;
		}

		private void ValueRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			this.ValueChanged?.Invoke(this, null);
		}

		protected override void SetBindings()
		{
			// todo: skaredy hack - SetBindings sa vola aj v konstruktore LabelElementu preto tento check
			if (valueRadioButton == null)
				return;

			base.SetBindings();

			valueRadioButton.Bind(Binding.Property((RadioButton rb) => rb.Checked), Binding.Property(ViewModel, (vm) => vm.RadioValue));
		}

		protected override void mainStackLayout_Click(object sender, EventArgs e)
		{
			if (!Enabled || ViewModel.RadioValue)
				return;

			ViewModel.RadioValue = !ViewModel.RadioValue;
		}
	}
}
