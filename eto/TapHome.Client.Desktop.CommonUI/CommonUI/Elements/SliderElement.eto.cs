﻿using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	partial class SliderElement : LabelElement
	{
        private Slider slider;

		void InitializeComponent()
        {
            slider = new Slider();

			secondRowLayout.Items.Add(new StackLayoutItem(slider, true));
		}
	}
}
