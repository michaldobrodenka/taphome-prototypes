﻿
using System;

namespace TapHome.Client.Desktop.CommonUI.CommonUI.Elements
{	
	public partial class StringElementWithMenu
	{
		public EventHandler MenuClicked;

		public StringElementWithMenu(object context, string caption, string value, string title = "", string message = "", bool enabled = false, bool isEditable = true, string description = "", string image = "", bool showMoreIcon = true) 
			: base(context, caption, value, title, message, enabled, isEditable, description, image, showMoreIcon)
		{
			InitializeComponent();

			menuLabel.MouseDown += MenuLabel_MouseDown;
		}

		private void MenuLabel_MouseDown(object sender, Eto.Forms.MouseEventArgs e)
		{
			e.Handled = true;

			MenuClicked?.Invoke(sender, e);
		}
	}
}
