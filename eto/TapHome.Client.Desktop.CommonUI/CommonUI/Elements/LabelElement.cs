﻿using System;
using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public partial class LabelElement
	{
		protected class LabelElementViewModel : ElementBaseViewModel
		{
		    public LabelElementViewModel(string caption, bool enabled) : base(caption, enabled) { }

		    protected string value;
		    public virtual string Value
		    {
		        get { return value; }
		        set
		        {
		            if (this.value == value)
		                return;

		            this.value = value;
		            OnPropertyChanged();
                }
		    }

		    private string description;
		    public string Description
		    {
		        get { return description; }
		        set
		        {
		            if (description == value)
		                return;

		            description = value;
		            OnPropertyChanged();
		            OnPropertyChanged("HasDescription");
		        }
		    }

		    public bool HasDescription { get { return !String.IsNullOrEmpty(this.Description); } }
        }

	    public string Value
	    {
	        get { return labelViewModel.Value; }
	        set { labelViewModel.Value = value; }
	    }

        public string LabelValue
		{
			get { return labelViewModel.Value; }
			set { labelViewModel.Value = value; }
		}
		public string Description
		{
			get { return labelViewModel.Description; }
			set { labelViewModel.Description = value; }
		}

		private LabelElementViewModel labelViewModel;
		protected LabelElementViewModel LabelViewModel
		{
			get { return labelViewModel; }
			set
			{
				labelViewModel = value;
				BaseViewModel = labelViewModel;
			}
		}

		public LabelElement(object context, string caption, string value, string description = "", bool enabled = true)
		    : base(context, caption, enabled)
        {
            InitializeComponent();

			LabelViewModel = new LabelElementViewModel(caption, enabled)
	        {
		        Value = value,
		        Description = description,
	        };

	        SetBindings();
		}

	    protected override void SetBindings()
	    {
	        base.SetBindings();

            valueLabel.TextBinding.BindDataContext((LabelElementViewModel vm) => vm.Value);
	        valueLabel.Bind(Binding.Property((Label l) => l.Enabled), Binding.Property(labelViewModel, (vm) => vm.Enabled));

            descriptionLabel.TextBinding.BindDataContext((LabelElementViewModel vm) => vm.Description);
	        descriptionLabel.Bind(Binding.Property((Label l) => l.Visible), Binding.Property(labelViewModel, (vm) => vm.HasDescription));
	        descriptionLabel.Bind(Binding.Property((Label l) => l.Enabled), Binding.Property(labelViewModel, (vm) => vm.Enabled));
        }

        public void Refresh()
		{

		}
	}
}
