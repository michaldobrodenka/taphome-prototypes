﻿using System;
using TapHome.Commons.CommonFrames;
using TapHome.DesktopClient.Wpf.Dialogs;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    public partial class PropertyElement
    {
        public class PropertyEventArgs : EventArgs
        {
            public string NumberValue { get; set; }
            public string OldValue { get; set; }
        }

        public event EventHandler<PropertyEventArgs> ValueChanged;

        public string Name
        {
            get
            {
                return GetValueWithoutFormat(LabelViewModel.Value);
            }
            set
            {
                LabelViewModel.Value = GetValueWithFormat(value);
            }
        }

        public string AlertCaption;

        private ServiceSettingsFrame.PropertyInfo propInfo;

        private string oldValue;

        public PropertyElement(object context, string caption, string value, ServiceSettingsFrame.PropertyInfo propInfo, string description = "", bool enabled = true) 
            : base(context, caption, value, description, enabled)
        {
            this.oldValue = value;
            this.propInfo = propInfo;

            InitializeComponent();

            SetBindings();

            LabelViewModel.Value = GetValueWithFormat(value);
        }

        protected override void mainStackLayout_Click(object sender, EventArgs e)
        {
            ShowEditDialog();
        }

        private void ShowEditDialog()
        {
            var dialog = new NumericStepperDialog(LabelViewModel.Caption, double.Parse(this.Name), this.propInfo);
            dialog.ShowModal(this);

            if (dialog.Result != null)
            {
                this.Name = dialog.Result.ToString();
                this.FireValueChanged();
            }
        }

        private string GetValueWithFormat(string text)
        {
            if (propInfo == null)
            {
                return text;
            }

            switch (propInfo.OutputType)
            {
                case ServiceSettingsFrame.PropertyOutputType.Second:
                    return text + " s";
                case ServiceSettingsFrame.PropertyOutputType.MilliSecond:
                    return text + " ms";
                case ServiceSettingsFrame.PropertyOutputType.Percentage:
                    return text + " %";
                case ServiceSettingsFrame.PropertyOutputType.DegreeFahrenheit:
                    return text + " °F";
                case ServiceSettingsFrame.PropertyOutputType.DegreeCelsius:
                    return text + " °C";
                case ServiceSettingsFrame.PropertyOutputType.Angle:
                    return text + " °";
                case ServiceSettingsFrame.PropertyOutputType.Voltage:
                    return text + " V";
                case ServiceSettingsFrame.PropertyOutputType.MilliAmpers:
                    return text + " mA";
                case ServiceSettingsFrame.PropertyOutputType.None:
                    return text;
                default:
                    return text;
            }
        }

        private string GetValueWithoutFormat(string text)
        {
            if (propInfo == null)
            {
                return text;
            }

            switch (propInfo.OutputType)
            {
                case ServiceSettingsFrame.PropertyOutputType.Second:
                    return text.Substring(0, text.Length - 2);
                case ServiceSettingsFrame.PropertyOutputType.MilliSecond:
                    return text.Substring(0, text.Length - 3);
                case ServiceSettingsFrame.PropertyOutputType.Percentage:
                    return text.Substring(0, text.Length - 2);
                case ServiceSettingsFrame.PropertyOutputType.DegreeFahrenheit:
                    return text.Substring(0, text.Length - 3);
                case ServiceSettingsFrame.PropertyOutputType.DegreeCelsius:
                    return text.Substring(0, text.Length - 3);
                case ServiceSettingsFrame.PropertyOutputType.Angle:
                    return text.Substring(0, text.Length - 2);
                case ServiceSettingsFrame.PropertyOutputType.Voltage:
                    return text.Substring(0, text.Length - 2);
                case ServiceSettingsFrame.PropertyOutputType.None:
                    return text;
                default:
                    return text;
            }
        }

        void FireValueChanged()
        {
            this.ValueChanged?.Invoke(this, new PropertyEventArgs { NumberValue = this.Name, OldValue = this.oldValue });
        }
    }
}
