﻿using Eto.Drawing;
using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    partial class ElementBase : Panel
    {
        protected StackLayout mainStackLayout;
        protected StackLayout firstRowLayout;
        protected StackLayout secondRowLayout;
        protected Label captionLabel;

        void InitializeComponent()
        {
            captionLabel = new Label();

            firstRowLayout = new StackLayout
            {
                Orientation = Orientation.Horizontal,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,

				Items =
                {
                    new StackLayoutItem(captionLabel, HorizontalAlignment.Left, true),
                },
            };

            secondRowLayout = new StackLayout
            {
                Orientation = Orientation.Horizontal,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
            };

            mainStackLayout = new StackLayout
            {
                Padding = 5,
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,

                Items =
                {
                    new StackLayoutItem(firstRowLayout),
                    new StackLayoutItem(secondRowLayout),
                },
            };

            Content = mainStackLayout;
        }
    }
}
