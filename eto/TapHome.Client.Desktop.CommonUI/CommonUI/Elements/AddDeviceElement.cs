﻿using System;
using System.Collections.Generic;
using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    // todo: GK - dokoncit
	public partial class AddDeviceElement
	{
		private static readonly Type[] supportedActionTypesBeforeDeviceType = new Type[]
		{
			//typeof(PushButton),
			//typeof(global::TapHome.Core.Lib.Model.Switch),
			//typeof(Dimmer),
			//typeof(Thermostat),
			//typeof(Blinds),
			//typeof(AdvancedBlinds),
			//typeof(Slide),
			//typeof(HueBulb),
			//typeof(Slat),
			//typeof(AdvancedSlat),
			//typeof(AdvancedSlide),
		};

		private static readonly Type[] supportedActionTypes = new Type[]
		{
			//typeof(PushButton),
			//typeof(global::TapHome.Core.Lib.Model.Switch),
			//typeof(Dimmer),
			//typeof(Thermostat),
			//typeof(Blinds),
			//typeof(AdvancedBlinds),
			//typeof(Slide),
			//typeof(HueBulb),
			//typeof(MultiValueSwitch),
			//typeof(Alarm),
			//typeof(Slat),
			//typeof(AdvancedSlat),
			//typeof(AdvancedSlide),
			//typeof(Variable),
		};

		public static Type[] SupportedActionTypes
		{
			get
			{
				//if (ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion <
				//	ClientCommunicationManager.DeviceTypeMinProtocolVersion)
				{
					return supportedActionTypesBeforeDeviceType;
				}

				//else
				//{
				//	return supportedActionTypes;
				//}
			}
		}

		public static readonly Type[] SupportedInterfaces = new Type[]
		{
			//typeof(TiltSensor), typeof(GenericSensor), typeof(MotionSensor), typeof(MotionAndBrightnessSensor),
			//typeof(WindowSensor), typeof(FloodSensor), typeof(SmokeSensor), typeof(TemperatureSensor),
			//typeof(WindSpeedSensor), typeof(BrightnessSensor), typeof(WindSpeedAndBrightnessSensor), typeof(WeatherSensor),
			//typeof(global::TapHome.Core.Lib.Model.Switch), typeof(Dimmer), typeof(Thermostat), typeof(ConvectorThermostat),
			//typeof(Blinds), typeof(AdvancedBlinds), typeof(Slide), typeof(HueBulb), typeof(Alarm),
			//typeof(AtmosphericPressureSensor), typeof(Co2Sensor), typeof(NoiseSensor), typeof(RainCounterSensor),
			//typeof(WeatherForecast), typeof(PushButton), typeof(MultiValueSwitch), typeof(Co2AndVocSensor), typeof(AnalogInput),
			//typeof(Slat), typeof(AdvancedSlat), typeof(AdvancedSlide), typeof(Variable),
		};

		public enum Behaviour
		{
			None = 0x00,
			DisableOtherInterfaces = 0x01,
			DisableOthers = 0x02,
			CloseOnFirstClick = 0x04,
			CheckDisabledIds = 0x08,

			PickSingleDevice = DisableOthers | CloseOnFirstClick,
			PickSingleInterface = DisableOtherInterfaces | CloseOnFirstClick,
			PickMultiple = None
		}

		public class DeviceListEventArgs : EventArgs
		{
			public int[] SelectedDevices;
			public string Interface;
			public string RequestTag;
		}

		public event EventHandler<DeviceListEventArgs> DeviceListResult;

		public string Name
		{
			get
			{
				return this.caption;
			}
			set
			{
				this.caption = value;
			}
		}

		private string caption;

		private string requestTag;

		//private DeviceListActivity.Behaviour behaviour;

		public IEnumerable<int> DisableIds;

		private string[] interfaces;

		private Dictionary<string, int> interfaceGroups;

		private bool useDeviceType = false;

		//private DeviceType deviceType;

		//private TriggerType safetySmartRuleTriggerType;

		protected AddDeviceElement(object context) //: base(context)
		{
		}

		public AddDeviceElement(object context, string caption, Behaviour behaviour, string requestTag, params string[] interfaces)
			//: base(context, null, Resource.Style.TextView_ActionAdd)
		{
			InitValues(caption, behaviour, requestTag, null, interfaces);
		}

		//public AddDeviceElement(Context context, string caption, Behaviour behaviour, string requestTag, TriggerType safetySmartRuleTriggerType, params string[] interfaces)
		//	: base(context, null, Resource.Style.TextView_ActionAdd)
		//{
		//	this.safetySmartRuleTriggerType = safetySmartRuleTriggerType;
		//	InitValues(caption, behaviour, requestTag, null, interfaces);
		//}

		public AddDeviceElement(object context, string caption, Behaviour behaviour, string requestTag, Dictionary<string, int> interfaceGroups, params string[] interfaces)
			//: base(context, null, Resource.Style.TextView_ActionAdd)
		{
			InitValues(caption, behaviour, requestTag, interfaceGroups, interfaces);
		}

		public AddDeviceElement(object context, string caption, Behaviour behaviour, string requestTag/*, DeviceType deviceType = DeviceType.None*/)
			//: base(context, null, Resource.Style.TextView_ActionAdd)
		{
			useDeviceType = true;
			InitValues(caption, behaviour, requestTag, null, interfaces/*, deviceType*/);
		}

		private void InitValues(string caption, Behaviour behaviour, string requestTag, Dictionary<string, int> interfaceGroups,
			string[] interfaces/*, DeviceType deviceType = DeviceType.None*/)
		{
			this.requestTag = requestTag;
			this.Name = caption;
			//this.behaviour = (DeviceListActivity.Behaviour)behaviour;
			this.interfaces = interfaces;
			this.interfaceGroups = interfaceGroups;
			//this.deviceType = deviceType;

			this.Init();

			//this.mainLayout.Click += MainLayout_Click;
			this.mainLayout.MouseDown += MainLayout_MouseDown;
		}

		private void MainLayout_MouseDown(object sender, MouseEventArgs e)
		{
			// todo: GK - navigate to DeviceListActivity
		}

		protected void Init()
		{
			//LayoutInflater inflater = (LayoutInflater)Context.GetSystemService(Context.LayoutInflaterService);
			//inflater.Inflate(Resource.Layout.AddDeviceElement, this);

			//this.mainLayout = this.FindViewById<LinearLayout>(Resource.Id.layout_main);
			//this.addTextView = this.FindViewById<TextView>(Resource.Id.text_add);
			//this.addTextView.SetBackgroundColor(ResourceUtils.GetColor(Context, global::Android.Resource.Color.Transparent));

			InitializeComponent();

			this.addTextView.Text = Name;
		}

		private void MainLayout_Click(object sender, EventArgs e)
		{
			//CommonUIPage.CurrentAddDeviceElement = this;

			//Intent intent;

			//if (!useDeviceType)
			//{
			//	intent = DeviceListActivity.NewInstance(
			//		Context,
			//		requestTag,
			//		behaviour,
			//		DisableIds,
			//		interfaceGroups,
			//		safetySmartRuleTriggerType,
			//		interfaces);
			//}

			//else
			//{
			//	intent = DeviceListActivity.NewInstance(
			//		Context,
			//		requestTag,
			//		behaviour,
			//		DisableIds,
			//		deviceType,
			//		true);
			//}

			//((Activity)Context).StartActivityForResult(intent, CommonUIPage.RequestCodeDeviceList);
		}

		void FireDeviceListResult(DeviceListEventArgs e)
		{
			this.DeviceListResult?.Invoke(this, e);
		}

		////public void OnDeviceListResult(DeviceListActivity.ResultList e)
		////{
		////	CommonUIPage.CurrentAddDeviceElement = null;

		////	FireDeviceListResult(new DeviceListEventArgs() { RequestTag = e.RequestTag, Interface = e.Interface, SelectedDevices = e.SelectedDevices });
		////}
	}
}
