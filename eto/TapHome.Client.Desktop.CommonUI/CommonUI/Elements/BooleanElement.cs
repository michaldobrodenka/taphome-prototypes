﻿using System;
using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public partial class BooleanElement
	{
		protected class BooleanElementViewModel : LabelElementViewModel
		{
            public BooleanElementViewModel(string caption, bool enabled) : base(caption, enabled) { }

		    private bool isChecked;
		    public bool IsChecked
		    {
		        get { return isChecked; }
		        set
		        {
		            if (this.isChecked == value)
		                return;

		            this.isChecked = value;
		            OnPropertyChanged();
		        }
            }
		}

		public event EventHandler ValueChanged;
        
		public new bool Value
		{
			get { return viewModel.IsChecked; }
			set { viewModel.IsChecked = value; }
		}

        private BooleanElementViewModel viewModel;
		private BooleanElementViewModel ViewModel
		{
			get { return viewModel; }
			set
			{
				viewModel = value;
				LabelViewModel = value;
			}
		}

        private string image;

		public BooleanElement(object context, string caption, bool value, string description = "", bool enabled = true, string image = "")
            : base(context, caption, string.Empty, description, enabled)
		{
			this.image = image;

		    InitializeComponent();

			ViewModel = new BooleanElementViewModel(caption, enabled)
			{
				IsChecked =  value,
			};

			SetBindings();

            valueCheckBox.CheckedChanged += ValueCheckBox_CheckedChanged;
		}

	    protected override void mainStackLayout_Click(object sender, EventArgs e)
	    {
	        if (!Enabled)
	            return;

	        ViewModel.IsChecked = !ViewModel.IsChecked;

            ValueChanged?.Invoke(sender, e);
	    }

	    protected override void SetBindings()
	    {
			// todo: skaredy hack - SetBindings sa vola aj v konstruktore LabelElementu preto tento check
		    if (valueCheckBox == null)
			    return;

            base.SetBindings();

            valueCheckBox.CheckedBinding.BindDataContext((BooleanElementViewModel vm) => vm.IsChecked);
			valueCheckBox.Bind(Binding.Property((CheckBox cb) => cb.Enabled), Binding.Property(ViewModel, (vm) => vm.Enabled));
		}

		private void ValueCheckBox_CheckedChanged(object sender, EventArgs e)
		{
            Value = this.valueCheckBox.Checked ?? false; // todo: GK: binding funguje az po evente asi, toto je hack, ale funguje, co s tym?

			ValueChanged?.Invoke(this, null);
		}
    }
}
