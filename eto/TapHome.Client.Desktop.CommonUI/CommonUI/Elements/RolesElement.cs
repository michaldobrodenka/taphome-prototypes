﻿using System;
using System.Collections.Generic;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.DesktopClient.Wpf.Dialogs;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    public partial class RolesElement
    {
        public class RolesElementSelectedChangedEventArgs : System.EventArgs
        {
            public bool[] Selected { get; set; }
        }

        public event EventHandler<RolesElementSelectedChangedEventArgs> SelectedChanged;

        public event EventHandler<EventArgs> Clicked;

        private List<string> values;
        private bool[] selectedIndexValues;
        private UserPermission permissions;
        private bool isEditable;

        public RolesElement(object context, string caption, UserPermission permissions) 
            : base(context, caption, null)
        {
            this.permissions = permissions;
            isEditable = false;
            
            InitializeComponent();

            UpdateValue();
        }

        public RolesElement(object context, string caption, UserPermission permissions, List<string> values, bool[] selectedIndexValues)
            : base(context, caption, null)
        {
            this.permissions = permissions;
            this.values = values;
            this.selectedIndexValues = selectedIndexValues;
            isEditable = true;

            InitializeComponent();

            UpdateValue();
        }

        private void UpdateValue()
        {
            LabelViewModel.Value = GetRolesString();
        }

        protected override void mainStackLayout_Click(object sender, EventArgs e)
        {
            if (isEditable)
            {
                ShowEditDialog();
            }
            else
            {
                FireClicked();
            }
        }

        private string GetRolesString()
        {
            string value = string.Empty;

            if (permissions.HasFlag(UserPermission.User))
            {
                value += "U ";
            }
            if (permissions.HasFlag(UserPermission.Admin))
            {
                value += "A ";
            }
            if (permissions.HasFlag(UserPermission.Service))
            {
                value += "S ";
            }

            return value;
        }

        void FireClicked()
        {
            this.Clicked?.Invoke(this, new EventArgs { });
        }

        void ShowEditDialog()
        {
            var dialog = new RolesDialog(permissions);
            dialog.ShowModal(this);

            if (dialog.Result != null)
            {
                permissions = UserPermission.None;

                if (dialog.Result.User)
                {
                    permissions |= UserPermission.User;
                }

                if (dialog.Result.Admin)
                {
                    permissions |= UserPermission.Admin;
                }

                if (dialog.Result.Service)
                {
                    permissions |= UserPermission.Service;
                }

                this.selectedIndexValues[0] = dialog.Result.User;
                this.selectedIndexValues[1] = dialog.Result.Admin;
                this.selectedIndexValues[2] = dialog.Result.Service;

                UpdateValue();

                SelectedChanged?.Invoke(this, new RolesElementSelectedChangedEventArgs { Selected = this.selectedIndexValues });
            }
        }
    }
}
