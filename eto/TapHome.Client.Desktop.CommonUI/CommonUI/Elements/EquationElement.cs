﻿using System;
using Eto.Forms;
using TapHome.DesktopClient.Wpf.Dialogs;
using TapHome.Translations;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public partial class EquationElement
	{
		public class EquationEventArgs : EventArgs
		{
			public string Equation { get; set; }
		}

		public event EventHandler<EquationEventArgs> EquationChanged;

		public string Equation
		{
			get { return BaseViewModel.Caption; }
			set { BaseViewModel.Caption = value; }
		}

		protected EquationElement(object context) : base(context, string.Empty)
		{
		}

		public EquationElement(object context, string caption) : base(context, caption)
		{
			InitializeComponent();

			BaseViewModel = new ElementBaseViewModel(caption, true);
			SetBindings();

			this.mainStackLayout.MouseDown += MainStackLayout_MouseDown;
			this.mainStackLayout.MouseDoubleClick += MainStackLayout_MouseDoubleClick;
		}

		private void MainStackLayout_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			e.Handled = true;
		}

		private void MainStackLayout_MouseDown(object sender, MouseEventArgs e)
		{
			ShowRenameDialog();
		}

		public void ShowRenameDialog()
		{
			var footer = $"{i18n.smartrule_evaluation_formula_helper}\n{i18n.smartrule_evaluation_formula_helper2}\n{i18n.smartrule_evaluation_formula_helper3}";
			// i18n escapes \n
			footer = footer.Replace("\\n", "\n");

			var dialog = new StringDialogWithFooter(i18n.smartrule_evaluation_formula_define, footer, BaseViewModel.Caption);
			dialog.ShowModal(this);

			if (dialog.Result != null)
			{
				BaseViewModel.Caption = dialog.Result;
				FireEquationChanged();
			}
		}

		void FireEquationChanged()
		{
			var handler = this.EquationChanged;

			if (handler != null)
			{
				handler(this, new EquationEventArgs { Equation = BaseViewModel.Caption });
			}
		}
	}
}
