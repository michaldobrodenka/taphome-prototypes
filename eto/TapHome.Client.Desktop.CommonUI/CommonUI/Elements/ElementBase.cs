﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    public partial class ElementBase
    {
        protected class ElementBaseViewModel : INotifyPropertyChanged
        {
            public ElementBaseViewModel(string caption, bool enabled)
            {
                Caption = caption;
                Enabled = enabled;
            }

            protected string caption;

            public string Caption
            {
                get { return caption; }
                set
                {
                    if (caption == value)
                        return;

                    caption = value;
                    OnPropertyChanged();
                }
            }

            protected bool enabled;
            public bool Enabled
            {
                get { return enabled; }
                set
                {
                    if (enabled == value)
                        return;

                    enabled = value;
                    OnPropertyChanged();
                }
            }

            #region INotifyPropertyChanged implementation

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }

            #endregion
        }

        public string Caption
        {
            get { return baseViewModel.Caption; }
            set { baseViewModel.Caption = value; } 
        }

        public bool Enabled
        {
            get { return baseViewModel.Enabled; }
            set { baseViewModel.Enabled = value; }
        }

        private ElementBaseViewModel baseViewModel;
	    protected ElementBaseViewModel BaseViewModel
	    {
		    get { return baseViewModel; }
			set { baseViewModel = value; }
	    }

        public ElementBase(object context, string caption, bool enabled = true)
        {
            InitializeComponent();

            baseViewModel = new ElementBaseViewModel(caption, enabled);

	        mainStackLayout.MouseDoubleClick += mainStackLayout_MouseDoubleClick;
			mainStackLayout.MouseDown += mainStackLayout_Click;
		}

		private void mainStackLayout_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			e.Handled = true;
		}

		protected virtual void mainStackLayout_Click(object sender, EventArgs e) { }

        protected virtual void SetBindings()
        {
            DataContext = baseViewModel;
            
            captionLabel.TextBinding.BindDataContext((ElementBaseViewModel vm) => vm.Caption);
            captionLabel.Bind(Binding.Property((Label l) => l.Enabled), Binding.Property(baseViewModel, (vm) => vm.Enabled));
        }
    }
}
