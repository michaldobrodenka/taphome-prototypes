﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.Core.Lib.IFTTT.Actions;

namespace TapHome.Client.Desktop.CommonUI.CommonUI.Elements
{
    public partial class AddActionElement
	{
		public class ActionEventArgs : EventArgs
		{
			public readonly BaseAction Action;
			public ActionEventArgs(BaseAction action)
			{
				Action = action;
			}
		}

		public event EventHandler<ActionEventArgs> ActionClick;

		private BaseAction action;
		public BaseAction Action
		{
			get
			{
				return this.action;
			}
			set
			{
				this.action = value;
				LabelValue = action.ToString();
			}
		}

		public AddActionElement(object context, BaseAction action) 
			: base(context, Storage.GetDevice(Storage.LastUsedLocationId.Value, action.OutputDeviceIDs.FirstOrDefault()).Name, action.ToString())
		{
			this.action = action;
		}

	    protected override void mainStackLayout_Click(object sender, EventArgs e)
		{
			if (ActionClick != null)
				ActionClick(this, new ActionEventArgs(action));
		}
    }
}
