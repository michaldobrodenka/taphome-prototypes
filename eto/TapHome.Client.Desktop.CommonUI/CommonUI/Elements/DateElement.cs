﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Eto.Forms;
using TapHome.DesktopClient.Wpf.Dialogs;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public partial class DateElement
	{
		private class DateElementViewModel : LabelElementViewModel
		{
			public DateElementViewModel(string caption) : base(caption, true) { }

			private DateTime selectedDate;
			public DateTime SelectedDate
			{
				get { return selectedDate; }
				set
				{
					if (selectedDate == value)
						return;

					selectedDate = value;
					OnPropertyChanged();
					OnPropertyChanged(nameof(Value));
				}
			}

			public override string Value
			{
				get { return Value = selectedDate.ToString("dd.MM.yyyy"); } 
			}
		}

		public class SelectedDateChangedEventArgs : EventArgs
		{
			public DateTime SelectedDate { get; set; }
		}

		public event EventHandler<SelectedDateChangedEventArgs> SelectedDateChanged;

		public DateTime MinimumDate
		{
			get { return minimumDate; }
			set { minimumDate = value; }
		}

		public DateTime MaximumDate
		{
			get { return maximumDate; }
			set { maximumDate = value; }
		}

		public DateTime SelectedDate
		{
			get { return viewModel.SelectedDate; }
			set { viewModel.SelectedDate = value; }
		}

		private DateTime minimumDate, maximumDate;

		private DateElementViewModel viewModel;
		private DateElementViewModel ViewModel
		{
			get { return viewModel; }
			set
			{
				viewModel = value;
				LabelViewModel = value;
			}
		}

		public DateElement(object context, string caption) 
			: base(context, caption, string.Empty)
		{
			InitializeComponent();

			ViewModel = new DateElementViewModel(caption);

			SetBindings();
			
			mainStackLayout.MouseDown += MainStackLayout_MouseDown;
		}

		private void MainStackLayout_MouseDown(object sender, MouseEventArgs e)
		{
			var dateTimePickerDialog = new DateTimePickerDialog(viewModel.Caption, MinimumDate, MaximumDate, viewModel.SelectedDate);
			var newSelectedDate = dateTimePickerDialog.ShowModal(ParentWindow);
			if (newSelectedDate != null)
			{
				viewModel.SelectedDate = newSelectedDate.Value;
			}
		}

		void FireSelectedDateChanged()
		{
			var handler = this.SelectedDateChanged;

			if (handler != null)
			{
				handler(this, new SelectedDateChangedEventArgs { SelectedDate = viewModel.SelectedDate });
			}
		}
	}
}
