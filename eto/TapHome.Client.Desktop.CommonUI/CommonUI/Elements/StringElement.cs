﻿using System;
using Eto.Forms;
using TapHome.DesktopClient.Wpf.Dialogs;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    public partial class StringElement
    {
        public class TextViewHelperEventArgs : EventArgs
        {
            public string Name { get; set; }
        }

        public event EventHandler<TextViewHelperEventArgs> NameChanged;
		
        public string Name
        {
            get { return LabelViewModel.Value; }
			set { LabelViewModel.Value = value; }
        }

        private bool isEditable;
        public bool IsEditable
        {
            get { return this.isEditable; }
            set { this.isEditable = value; }
        }

        private bool showMoreIcon;
        public bool ShowMoreIcon
        {
            get { return this.showMoreIcon; }
            set { this.showMoreIcon = value; }
        }

        public event EventHandler<EventArgs> Clicked;

        public Action SetClickedEvent
        {
            set
            {
                Clicked += (sender, e) => { Console.WriteLine(); value(); };
            }
        }

        private ImageView imageView;

        private string value;
        private string message;
        private string image;

        public StringElement(object context, string caption, string value, string title = "", string message = "", bool enabled = false, bool isEditable = true, string description = "", string image = "", bool showMoreIcon = true)
            : base(context, caption, value, description)
        {
            this.Name = value;
            this.message = message;
            this.isEditable = isEditable;
            this.image = image;
            this.showMoreIcon = showMoreIcon;
			
            this.InitializeComponent();
        }

        void FireClicked()
        {
            this.Clicked?.Invoke(this, new EventArgs { });
        }

        private void ShowRenameDialog()
        {
            var dialog = new StringDialog(message, this.Name);
            dialog.ShowModal(this);

            if (dialog.Result != null)
            {
                this.Name = dialog.Result;
                this.FireNameChanged();
            }
        }

        void FireNameChanged()
        {
            this.NameChanged?.Invoke(this, new TextViewHelperEventArgs { Name = this.Name });
		}

	    protected override void mainStackLayout_Click(object sender, EventArgs e)
	    {
		    if (!Enabled)// && !(this is StringElementWithMenu))
			    return;

		    if (isEditable)
		    {
			    ShowRenameDialog();
		    }
		    else
		    {
			    FireClicked();
		    }
	    }
	}
}


