﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	public class DoRadioElement : RadioRootElement
	{
		private string relayName;
		public int SelectedRelayNumber
		{
			get
			{
				return this.outputValueIndices[this.SelectedValueIndex];
			}
			set
			{
				KeyValuePair<int, int> selectedKv = this.outputValueIndices.FirstOrDefault(kv => kv.Value == value);

				if (!selectedKv.Equals(default(KeyValuePair<int, int>)))
				{
					this.SelectedValueIndex = selectedKv.Key;
				}
				else
				{
					this.SelectedValueIndex = 0; // none 
				}
			}
		}

		Dictionary<int, int> outputValueIndices; // index, relay number

		protected DoRadioElement(object context) : base(context)
		{
		}

		public DoRadioElement(object context, string caption, List<int> relays, List<string> values, string relayName)
			: base(context, caption, values)
		{
			this.relayName = relayName;

			this.Init(context, caption, true);

			this.outputValueIndices = new Dictionary<int, int>();

			this.ReplaceRelays(relays);
		}

		public void ReplaceRelays(List<int> newRelays)
		{
			this.outputValueIndices.Clear();

			var stringValues = GenerateRelayValues(newRelays);

			this.ReplaceValues(stringValues);
		}

		private List<string> GenerateRelayValues(IList<int> relays)
		{
			var stringValues = new List<string>();
			stringValues.Add("None");
			this.outputValueIndices.Add(0, 0);
			for (int i = 1; i < relays.Count + 1; i++)
			{
				var relayNumber = relays[i - 1];
				stringValues.Add(String.Format("{0}{1}", relayName, relayNumber));
				this.outputValueIndices.Add(i, relayNumber);
			}

			return stringValues;
		}
	}
}
