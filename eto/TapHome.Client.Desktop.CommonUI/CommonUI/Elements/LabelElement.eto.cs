﻿using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	partial class LabelElement : ElementBase
    {
		protected Label valueLabel;
	    protected Label descriptionLabel;

        void InitializeComponent()
		{
            valueLabel = new Label();
            descriptionLabel = new Label();
            descriptionLabel.Wrap = WrapMode.Character;

            firstRowLayout.Items.Add(new StackLayoutItem(valueLabel, HorizontalAlignment.Right, false));
            secondRowLayout.Items.Add(descriptionLabel);

            Content = mainStackLayout;
        }
	}
}
