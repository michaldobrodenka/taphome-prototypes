﻿using System;

namespace TapHome.Client.Desktop.CommonUI.CommonUI.Elements
{	
	public partial class BooleanElementWithMenu
	{
		public EventHandler MenuClicked;

		public BooleanElementWithMenu(object context, string caption, bool value, string description = "", bool enabled = true, string image = "") 
			: base(context, caption, value, description, enabled, image)
		{
			InitializeComponent();

			menuLabel.MouseDown += MenuLabel_MouseDown;
		}

		private void MenuLabel_MouseDown(object sender, Eto.Forms.MouseEventArgs e)
		{
			e.Handled = true;

			MenuClicked?.Invoke(sender, e);
		}
	}
}
