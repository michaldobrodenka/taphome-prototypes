﻿using Eto.Forms;
using TapHome.Client.Desktop.CommonUI.Elements;

namespace TapHome.Client.Desktop.CommonUI.CommonUI.Elements
{	
	partial class StringElementWithMenu : StringElement
	{
		// todo: icon or smth else
		private Label menuLabel;

		void InitializeComponent()
		{
			menuLabel = new Label { Text = "  :" };
			firstRowLayout.Items.Add(new StackLayoutItem(menuLabel));
		}
	}
}
