﻿using Eto.Forms;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
	partial class CommonButton : Panel
	{
		private Button button;

		void InitializeComponent()
		{
			button = new Button();

			Content = button;
		}
	}
}
