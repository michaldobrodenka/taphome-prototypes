﻿using System;

namespace TapHome.Client.Desktop.CommonUI.Elements
{
    public partial class AddElement
    {
        public event EventHandler<EventArgs> ElementClicked;

        public AddElement(object context, string caption) : base(context, "+ " + caption)
        {
            InitializeComponent();

            SetBindings();
        }

        protected override void mainStackLayout_Click(object sender, EventArgs e)
        {
            this.ElementClicked?.Invoke(this, new EventArgs { });
        }
    }
}
