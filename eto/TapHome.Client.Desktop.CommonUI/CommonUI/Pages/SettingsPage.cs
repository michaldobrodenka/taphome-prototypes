using System;
using System.Linq;
using System.Collections.Generic;
using TapHome.Client.Desktop.CommonUI;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.Client.Desktop.CommonUI.Elements;
using TapHome.Translations;

#if IOS
using Com.TapHome.iOS.CommonUI;
using Com.TapHome.iOS;
using UIKit;
#endif
#if ANDROID
using Android.App;
using Com.TapHome.Android.Views.Elements;
using Com.TapHome.Android;
#endif

namespace TapHome.Commons.CommonPages
{
#if ANDROID
    [Activity(Label = "SettingsPage", Theme = "@style/TapHomeTheme")]
#endif
	public class SettingsPage : CommonUIPage
	{
		private CommonSection userPermissionLocationSection, devicesSection, dashboardSection, logoutSection, versionSection;
		
		private enum MenuSections
		{
			userPermission = 0,
			devicesSection = 1,
			dashboardSection = 2,
			logoutSection = 3,
			versionSection = 4,
		}

        public const string variableTypeExtra = "com.taphome.android.SettingsPage.EXTRA_IS_VARIABLE";

        public SettingsPage(Dictionary<string, object> parameters) : base(string.Empty, parameters)
        {
        }

        public SettingsPage() : base(string.Empty, null)
        {
        }

        public override void OnInit()
        {
            base.OnInit();
			SetLightTheme();
            this.title = i18n.settings;
            SetTitle();
            InitSections();
        }

		public void NavigateToPage()
		{
		}

		public override void CreateControls()
		{
		}

		public override void OnBack()
		{
			GoBack();
		}

        private void InitSections()
        {
			this.RemoveAllSections();
			
			#if IOS
				var versionString = i18n.softwareVersion + ": " + Foundation.NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleShortVersionString") /*+ NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleVersion")*/;
			#endif
			#if ANDROID
				var versionString = i18n.softwareVersion + ": " + Application.Context.PackageManager.GetPackageInfo(Application.Context.PackageName, 0).VersionName;;
			#endif
            var versionString = i18n.softwareVersion + ": " + "123";;
			userPermissionLocationSection = new CommonSection(this, " ", true);
			devicesSection = new CommonSection(this, " ", true);
			dashboardSection = new CommonSection(this, " ", true);
			logoutSection = new CommonSection(this, " ", true);
			versionSection = new CommonSection(this, versionString, true);

            CreateData();
            AddSectionsToPage();
        }
		
		private void CreateData()
		{
		    if (Storage.LastUsedLocationId == null)
		    {
		        return;
		    }

			var permissions = new bool[] {
				//my profile alway true
			    true,
				//all users
			    true,
				//permissions
			    true,
				//my location
			    true,
				//add location
				true,
				//hardware
			    true,
				//virtual devices
			    true,
				//expose devices
			    true,
				//dashboard
			    true,
				//logout
				true,
                //virtual variables
			    true,
            };

			//item1 name, item2 image path, item3 tapped, item4 permission, item5 section)
			var elementData = new Tuple<string, string, Action, bool, MenuSections>[]
			{
				new Tuple<string ,string ,Action ,bool, MenuSections>
					(i18n.myProfile, "icon_settings_profile", MyProfile_Tapped, permissions[0], MenuSections.userPermission),
				new Tuple<string ,string ,Action ,bool, MenuSections>
					(i18n.allUsers,"icon_settings_all_users", AllUsers_Tapped, permissions[1], MenuSections.userPermission),
				new Tuple<string ,string ,Action ,bool, MenuSections>
					(i18n.detail_deviceSettings_Permissions, "icon_settings_permissions", Permissions_Tapped, permissions[2], MenuSections.userPermission),
				new Tuple<string, string, Action, bool, MenuSections>
					(i18n.myLocation, "icon_settings_location", MyLocation_Tapped, permissions[3], MenuSections.userPermission),
				new Tuple<string, string, Action, bool, MenuSections>
					(i18n.addLocation, "icon_settings_add_location", AddLocation_Tapped, permissions[4], MenuSections.userPermission),
				new Tuple<string, string, Action, bool, MenuSections>
					(i18n.hardware, "icon_settings_hardware", Hardware_Tapped, permissions[5], MenuSections.devicesSection),
				new Tuple<string, string, Action, bool, MenuSections>
					(i18n.virtualDevices, "icon_settings_virtual", VirtualDevices_Tapped, permissions[6], MenuSections.devicesSection),
                new Tuple<string, string, Action, bool, MenuSections>
                    (i18n.virtualVariables, "icon_settings_variables", VirtualVariables_Tapped, permissions[10], MenuSections.devicesSection),
                new Tuple<string, string, Action, bool, MenuSections>
					(i18n.exposeDevices, "icon_settings_expose", ExposeDevices_Tapped, permissions[7], MenuSections.devicesSection),
				new Tuple<string, string, Action, bool, MenuSections>
					(i18n.dashboard, "icon_settings_dashboard", Dashboard_Tapped, permissions[8], MenuSections.dashboardSection),
				new Tuple<string, string, Action, bool, MenuSections>
					(i18n.dashboard_logout, "icon_settings_logout", Logout_Tapped, permissions[9], MenuSections.logoutSection),
            };

			PopulateSections(elementData);
		}

		private void PopulateSections(Tuple<string, string, Action, bool, MenuSections>[] data)
		{	//item1 name, item2 image path, item3 tapped, item4 permission, item5 section)
			foreach (var element in data)
			{
				var permission = element.Item4;
				if (permission)
				{
					var section = element.Item5;
					if (MenuSections.userPermission == section)
					{
						userPermissionLocationSection.Add(CreateStringElement(element.Item1, element.Item2, element.Item3));
					}
					else if (MenuSections.devicesSection == section)
					{
						devicesSection.Add(CreateStringElement(element.Item1, element.Item2, element.Item3));
					}
					else if (MenuSections.dashboardSection == section)
					{
						dashboardSection.Add(CreateStringElement(element.Item1, element.Item2, element.Item3));
					}
					else if (MenuSections.logoutSection == section)
					{
						logoutSection.Add(CreateStringElement(element.Item1, element.Item2, element.Item3));
					}
				} 
			}
		}

		private StringElement CreateStringElement(string name, string imagePath, Action tapAction)
		{
			return new StringElement(this, name, null, "", "", true, false, "", imagePath) { SetClickedEvent = tapAction };
		}
		
		private void AddSectionsToPage()
		{
			if (userPermissionLocationSection.Elements.Count > 0)
				AddSection(userPermissionLocationSection);
			if (devicesSection.Elements.Count > 0)
				AddSection(devicesSection);
			if (dashboardSection.Elements.Count > 0)
				AddSection(dashboardSection);
			if (logoutSection.Elements.Count > 0)
				AddSection(logoutSection);
			//always add
			AddSection(versionSection);
		}

        private void MyProfile_Tapped ()
		{
		    if (Storage.LastUsedLocationId == null)
		    {
		        return;
		    }

        }

		private void AllUsers_Tapped ()
		{
		    CommonUIPage.NavigateToPageWithParameters(this, typeof(AllUsersManagementPage), null);
        }

		private void Permissions_Tapped()
		{

        }

        private void MyLocation_Tapped()
		{
		    CommonUIPage.NavigateToPageWithParameters(this, typeof(CoreInfoPage), null);
        }

        private void VirtualDevices_Tapped()
        {
			if (Storage.LastUsedLocationId == null)
			{
				return;
			}

        }

		void AddLocation_Tapped ()
		{
			#if IOS
				var vc = new Com.TapHome.iOS.ViewControllers.CoreSearchViewController();
				this.NavigationController.PushViewController(vc, true);
			#endif
			#if ANDROID
                StartActivity(new Android.Content.Intent(this, typeof(SearchForCoresActivity)));
			#endif
		}

		void Hardware_Tapped()
		{

		}

		void ExposeDevices_Tapped()
		{
		    CommonUIPage.NavigateToPageWithParameters(this, typeof(SlaveInterfacesPage), null);
        }

		public void Dashboard_Tapped()
        {
			#if IOS
				var vc = new DashBoardSettingsViewController(0);
				this.NavigationController.PushViewController(vc, true);
			#endif
			#if ANDROID
                StartActivity(new Android.Content.Intent(this, typeof(EditDashboardActivity)));
			#endif
        }

        public void Logout_Tapped()
        {
			#if IOS
				if (DashBoardViewController.sharedInstance != null)
				{
					DashBoardViewController.sharedInstance.Logout();
				}
			#endif
			#if ANDROID
                ApplicationManager.Instance.Logout();
			#endif
        }
        
        public void VirtualVariables_Tapped()
        {
            if (Storage.LastUsedLocationId == null)
            {
                return;
            }

        #if ANDROID
            var intent = new Android.Content.Intent(this, typeof(CentralUnitActivity));
            intent.PutExtra("ccuId", cc.Value.CcuId);
            intent.PutExtra(variableTypeExtra, Android.OS.Bundle.Empty);
            this.StartActivity(intent);
        #endif
        #if IOS
                var vc = new Com.TapHome.iOS.ViewControllers.CCUInfoViewController(true, cc) { ShowOnlyGlobalVariables = true};

                vc.NavigationItem.BackBarButtonItem = new UIBarButtonItem(Util.App.GetString("general_back"), UIBarButtonItemStyle.Bordered, (sender2, args) =>
                {
                    NavigationController.PopViewController(true);
                });

                this.NavigationController.PushViewController(vc, true);
        #endif
        }
	}
}
