﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TapHome.Client.Desktop.CommonUI;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.Commons;
using TapHome.Commons.CommonFrames;
using TapHome.Translations;

namespace TapHome.Commons.CommonPages
{
#if ANDROID
    [Activity(Label = "DeviceHistoryStatisticsSettingsPageWithFrames", Theme = "@style/TapHomeTheme.NoActionBar")]
#endif
	public class TapHomeBusModuleConfigurationPageWithFrames : CommonUIPageWithFrames
	{
		public const string DeviceIdExtraKey = "DeviceId";
		public const string DeviceSerialExtraKey = "DeviceSerial";

		private GenericDevice board;

		private TaphomeBusModuleConfigurationFrame configurationFrame;

		private TaphomeBusModuleInfoFrame infoFrame;

		private int savingDialogHashCode;

		public TapHomeBusModuleConfigurationPageWithFrames() : base(null)
		{
		}

		public TapHomeBusModuleConfigurationPageWithFrames(Dictionary<string, object> parameters) : base(parameters)
		{
		}

		public override void OnInit()
		{
			base.OnInit();

			var deviceId = (int)GetParameter(DeviceIdExtraKey, typeof(Int32));

			var deviceSerial = GetParameter(DeviceSerialExtraKey, typeof(string));

			if (!String.IsNullOrEmpty((string)deviceSerial))
			{
				var thisDevice = Storage.GetDeviceBySN(Storage.LastUsedLocationId.Value, (string)deviceSerial);
				deviceId = thisDevice.ID;
			}

			var permissions = Storage.GetUserPermissionsForDevice(Storage.LastUsedLocationId.Value, deviceId);

			if ((!permissions.HasFlag(UserPermissionForDevice.ServiceSettingsCanView)
				&& !permissions.HasFlag(UserPermissionForDevice.ServiceSettingsCanEdit)))
			{
				CreateToast(i18n.permissions_warning);
				GoBack();
				return;
			}

			this.board = Storage.GetDevice(Storage.LastUsedLocationId.Value, deviceId);

			if (board == null)
			{
				GoBack();
				return;
			}

			StartBlinkingLed();

			this.title = board.Name;

			this.SetTitle();

			SetLightTheme();

			List<CommonUIFrame> frames = new List<CommonUIFrame>();

			configurationFrame = new TaphomeBusModuleConfigurationFrame(this, board) { Name = "taphomebus_menu_configuration" };

			configurationFrame.OnBack += (sender, e) =>
			{
				GoBack();
			};

			frames.Add(configurationFrame);

			if (ApplicationManager.Instance.GetCurrentCommunicationManager().CurrentCoreProtocolVersion >=
				ClientCommunicationManager.CcuManagementMinProtocolVersion)
			{
				infoFrame = new TaphomeBusModuleInfoFrame(this, board) { Name = "taphomebus_menu_info" };
				frames.Add(infoFrame);
			}

			AddFrames(frames);

			this.RenameClicked += HandleRenameClicked;
		}

		private void HandleRenameClicked(object sender, EventArgs e)
		{
			var elements = new AlertDialogElements();

			elements.TextField = new Tuple<string, string>(i18n.detail_smartRules_rename, this.board.Name);

			var buttons = new AlertDialogElementsButtons();

			buttons.PositiveButton = new Tuple<string, Action<string>>(i18n.general_ok, (string name) =>
			{
				this.board.Name = name;
				this.title = name;
				SetTitle();

				ApplicationManager.Instance.GetCurrentCommunicationManager().UpdateDeviceName(
						this.board.ID,
						this.board.Name,
						(t) =>
						{
							bool wasOk = t.CompletedWithoutProblemsWithResult();

							if (!wasOk)
							{
								CreateToast(i18n.error_unknown);
							}
						});
			});

			buttons.NegativeButton = new Tuple<string, Action>(i18n.general_cancel, () =>
			{
			});

			CreateAlertDialogWithElements(i18n.detail_smartRules_rename, String.Empty, buttons, elements);
		}

		public override void OnBack()
		{
			if (this.board.Model == "TapHomeBusMultiZoneController")
			{
				SaveServiceSettings();
			}

			else
			{
				CheckChanges();
			}
		}

		private void CheckChanges()
		{
			var str = configurationFrame.ExportConfig();

			if (configurationFrame.config != str)
			{
				var buttons = new AlertDialogButtons();

				buttons.PositiveButton = new Tuple<string, Action>(i18n.general_save, () =>
				{
					savingDialogHashCode = CreateProgressDialog(i18n.saving, false, null);

					this.SaveChanges(str);

					StopBlinkingLed();
				});

				buttons.NegativeButton = new Tuple<string, Action>(i18n.general_dismiss, () =>
				{
					StopBlinkingLed();
					GoBack();
				});

				CreateAlertDialog(i18n.general_save, i18n.saveChanges, buttons);
			}

			else
			{
				StopBlinkingLed();
				GoBack();
			}
		}

		private void SaveServiceSettings()
		{
			StopBlinkingLed();
#if IOS
            if (configurationFrame.ServiceSettingsDeviceFrame != null)
            {
                configurationFrame.ServiceSettingsDeviceFrame.OnEnd();
            }
#endif

			if ((configurationFrame.ServiceSettingsDeviceFrame != null && configurationFrame.ServiceSettingsDeviceFrame.IsChanged))
			{
				var buttons = new AlertDialogButtons();

				buttons.PositiveButton = new Tuple<string, Action>(i18n.general_save, () =>
				{
					savingDialogHashCode = CreateProgressDialog(i18n.saving, false, null);

					var str = configurationFrame.ExportConfig();

					ApplicationManager.Instance.GetCurrentCommunicationManager().UpdateDeviceProperties(
					this.board.ID,
					this.board.Name,
					this.board.DeviceProperties,
					(t) =>
					{
						this.RunOnUiThreadTh(() =>
						{
							if (configurationFrame.config == str)
							{
								DismissProgressDialog(savingDialogHashCode);

								bool wasOk = t.CompletedWithoutProblemsWithResult();

								CreateToast(!wasOk ? i18n.error : i18n.detail_deviceSettings_update_success);

								GoBack();
							}
							else
							{
								SaveChanges(str);
							}
						});
					});
				});

				buttons.NegativeButton = new Tuple<string, Action>(i18n.general_dismiss, () =>
				{
					GoBack();
				});

				CreateAlertDialog(i18n.general_save, i18n.saveChanges, buttons);
			}

			else
			{
				CheckChanges();
			}
		}

		private void SaveChanges(string str)
		{
			var communicationManager = ApplicationManager.Instance.GetCurrentCommunicationManager();

			if (communicationManager == null)
				return;

			communicationManager.TapHomeBusConfig(this.board.ID, str, t =>
			{
				this.RunOnUiThreadTh(() =>
				{
					DismissProgressDialog(savingDialogHashCode);

					if (t.CompletedWithoutProblemsWithResult())
					{
						if (t.Result is MessageTapHomeBusConfigResponse)
						{
							var m = t.Result as MessageTapHomeBusConfigResponse;

							CreateToast(String.Format("Added {0} devices, removed {1} devices", m.AddedDevices == null ? 0 : m.AddedDevices.Count(), m.RemovedDevices == null ? 0 : m.RemovedDevices.Count()));
						}
						else if (t.Result is MessageGenericResponse)
						{
							CreateToast(i18n.error + ": " + (t.Result as MessageGenericResponse).ErrorMessage);
						}
						else
						{
							CreateToast(i18n.bus_module_unknown_response_from_core);
						}

						// navigate from
						GoBack();
					}
					else
					{
						var responseMessage = t.Result != null ? t.Result as MessageGenericResponse : null;
						string errorMessage = responseMessage != null ? responseMessage.ErrorMessage : String.Empty;

						HandleApiError(true, errorMessage);
						GoBack();
					}
				});
			});
		}

		protected void HandleApiError(bool communicationError, string content)
		{
			var message = content;

			if (communicationError)
			{
				message = i18n.error;
			}

			var buttons = new AlertDialogButtons();
			buttons.NeutralButton = new Tuple<string, Action>(i18n.general_ok, () => { });

			CreateAlertDialog(String.Empty, BuildUtils.ReplaceTapHomeInText(message), buttons);
		}

		private void StartBlinkingLed()
		{
			var communicationManager = ApplicationManager.Instance.GetCurrentCommunicationManager();

			if (communicationManager == null)
				return;

			communicationManager.DeviceAction(CommunicationsConstants.KeyActionModuleStartBlinkingLed, (int)this.board.ID, this.board.Name, delegate (Task<MessageBase> t) { t.FlushExceptions(); });
		}

		private void StopBlinkingLed()
		{
			var communicationManager = ApplicationManager.Instance.GetCurrentCommunicationManager();

			if (communicationManager == null)
				return;

			communicationManager.DeviceAction(CommunicationsConstants.KeyActionModuleStopBlinkingLed, (int)this.board.ID, this.board.Name, delegate (Task<MessageBase> t) { t.FlushExceptions(); });
		}

		public override void CreateControls()
		{
		}

		public override void WillAppear()
		{
			var device = Storage.GetDevice(Storage.LastUsedLocationId.Value, board.ID);
			if (device == null) GoBack();
		}
	}
}
