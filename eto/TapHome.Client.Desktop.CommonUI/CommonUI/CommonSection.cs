﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.Client.Desktop.CommonUI
{
    public partial class CommonSection
    {
        //public CommonSection()
        //{
        //    InitializeComponent();
        //}

        public List<Control> Elements = new List<Control>();

        private string header, footer;

        private int elementsWidth = -2;

        public string Footer
        {
            get
            {
                return this.footer;
            }
            set
            {
                this.footer = value;

                if (this.footerLabel != null)
                {
                    this.footerLabel.Text = value;

                    RefreshFooterVisibility();
                }
            }
        }

        public string Header
        {
            get
            {
                return this.header;
            }
            set
            {
                this.header = value;
                if (this.headerLabel != null)
                {
                    this.headerLabel.Text = value;
                }
            }
        }

        // todo: GK - staci iba caption v konstruktore a treba aj nastaivt
        public CommonSection(object context, string caption, bool lightTheme = false)
        {
            this.header = caption;

            this.InitializeComponent();
		}

	    public void Add(Control view)
	    {
             view.Size = new Size(this.DesiredElementWidth(), -1);
            this.Elements.Add(view);

            var footerIndex = this.stackLayout.Items.IndexOf(this.footerStackLayoutItem);
            this.stackLayout.Items.Insert(footerIndex,view);
        }

        // hide footer when empty (to save vertical space)
        private void RefreshFooterVisibility()
        {
            if (String.IsNullOrEmpty(this.footer))
            {
                this.footerLabel.Visible = false;
            }
            else
            {
                this.footerLabel.Visible = true;
            }
        }

        public void Clear()
        {
            this.Elements.Clear();
            this.stackLayout.Items.Clear();
            this.stackLayout.Items.Add(this.headerStackLayoutItem); // put back header & footer
            this.stackLayout.Items.Add(this.footerStackLayoutItem);
        }

        public void InsertAt(Control view, int index)
        {
            view.Size = new Size(this.DesiredElementWidth(), -1);
            this.Elements.Insert(index, view);

            this.stackLayout.Items.Insert(index, view);
        }

        public void AddAll(IEnumerable<Control> controls)
		{
			foreach (var control in controls)
			{
                this.Add(control);
		    }
	    }

        private int DesiredElementWidth()
        {
            if (this.Width < this.stackLayout.Padding.Left * 2)
                return -1;

            return this.Width - this.stackLayout.Padding.Left * 2-2;
        }

        public void SetWidth(int width)
        {
			//if (!Application.Instance.Platform.IsWpf)
			if(!CommonUIHost.UseManualResizing())
				return;

            if (this.elementsWidth == width)
                return;

            if (width < this.stackLayout.Padding.Left * 2)
                return;

            this.Width = width-2;// -this.stackLayout.Padding.Left * 2;


            this.headerLabel.Width = DesiredElementWidth();// width - this.stackLayout.Padding.Left * 2;
            this.footerLabel.Size = new Size(DesiredElementWidth(),-1);// width - this.stackLayout.Padding.Left * 2;

            foreach (var control in this.Elements)
            {
                control.Size = new Size(DesiredElementWidth(), -1);
            }

            this.Size = new Size(width - 2,-1); // recompute height

            this.elementsWidth = width;
        }
    }
}
