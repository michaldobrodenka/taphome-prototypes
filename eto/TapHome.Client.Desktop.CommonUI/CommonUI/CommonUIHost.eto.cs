﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.Client.Desktop.CommonUI
{
    partial class CommonUIHost : Panel
    {
        public StackLayout mainlayout;
        public StackLayout toolBar;
        public Button backButton;
        public Label titleLabel;
        public Panel rightToolbarSpace;

        public Panel pageContent;

        void InitializeComponent()
        {
            this.backButton = new Button() { Text = "Back" };

            this.titleLabel = new Label() { Text = "title", TextAlignment = TextAlignment.Center };

            this.rightToolbarSpace = new Panel() { BackgroundColor = Colors.AliceBlue };

            this.toolBar = new StackLayout()
            {
                Orientation = Orientation.Horizontal,
                Padding = 3,
                BackgroundColor = Colors.White,
            };
            this.toolBar.Items.Add(new StackLayoutItem(this.backButton));
            this.toolBar.Items.Add(new StackLayoutItem(this.titleLabel, true));
            this.toolBar.Items.Add(new StackLayoutItem(this.rightToolbarSpace));

            this.pageContent = new Panel();

            this.mainlayout = new StackLayout()
            {
                Orientation = Orientation.Vertical,
                Padding = 3,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
                Items =
                {
                    new StackLayoutItem(this.toolBar),
                    new StackLayoutItem(this.pageContent, true)
                }
            };

            Content = this.mainlayout;
        }
    }
}
