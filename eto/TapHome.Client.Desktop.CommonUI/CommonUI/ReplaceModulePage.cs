﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.Client.Desktop.CommonUI.Elements;
using TapHome.Translations;

namespace TapHome.Client.Desktop.CommonUI
{
	public class ReplaceModulePage : CommonUIPage
	{
		public const string DeviceIdExtraKey = "DeviceIdExtraKey";

		private CommonSection moduleSection;

		private Device oldModule;

		private string newModuleSerialNumber;

		private bool selectFirst;

		private List<GenericDevice> devicesToReplace;

		private List<RadioElement> radioElements;

		private int progressDialogHash;

		private int availableModules;

		public ReplaceModulePage() : base(string.Empty, null)
		{
		}

		public ReplaceModulePage(Dictionary<string, object> parameters) : base(string.Empty, parameters)
		{
		}

		public override void CreateControls()
		{
			
		}

		public override void OnInit()
		{
			base.OnInit();

			this.title = i18n.module_replace;

			SetTitle();

			SetLightTheme();

			InitSection();
		}

		public override void OnBack()
		{
		}

		private void InitSection()
		{
			this.RemoveAllSections();

			availableModules = 0;

			radioElements = new List<RadioElement>();

			selectFirst = true;

			var deviceId = (int)GetParameter(ReplaceModulePage.DeviceIdExtraKey, typeof(Int32));

			oldModule = Storage.GetDevice(Storage.LastUsedLocationId.Value, deviceId);

			if (oldModule == null)
			{
				ShowErrorMessage(i18n.error_unknown);
				return;
			}

			devicesToReplace = Storage.GetDevicesById(Storage.LastUsedLocationId.Value).Values.
				Where(d => d.SerialNumber.Split(':')[0] == oldModule.SerialNumber && d.SerialNumber != oldModule.SerialNumber).ToList();

			var section = new CommonSection(this, String.Empty, true);

			String footerMessage = String.Format(i18n.module_replace_info, (oldModule.Name + " (" + oldModule.SerialNumber + ")"));

			foreach (var device in devicesToReplace)
			{
				if (device == null)
				{
					continue;
				}

				footerMessage += String.Format("\n- {0}", device.Name);
			}

			section.Footer = footerMessage;

#if ANDROID
            int padding = App.IsTablet(this) ? 20 : 40;

            section.SetFooterPadding(padding, padding, padding, padding);
#endif

			this.AddSection(section);

			moduleSection = new CommonSection(this, i18n.module_select, true);

			var modules =
				Storage.GetDevicesById(Storage.LastUsedLocationId.Value)
					.Values.Where(d => d.Model == oldModule.Model && d.ID != oldModule.ID)
					.ToList();

			if (modules.Count == 0)
			{
				ShowErrorMessage(String.Format("No other {0} found.", oldModule.SerialNumber.Split('-')[0]));
				return;
			}

			foreach (var module in modules)
			{
				AddRadioElement(module);
			}

			AddSection(moduleSection);

			if (availableModules > 0)
			{
				CommonButton replaceButton = new CommonButton(this, i18n.general_replace, CommonButton.Style.Default);
				replaceButton.Clicked += ReplaceButton_Clicked;

				var replaceSection = new CommonSection(this, String.Empty, true);
				replaceSection.Add(replaceButton);
				this.AddSection(replaceSection);
			}
		}

		private void ShowErrorMessage(string message)
		{
			CreateToast(message);
			OnBack();
		}

		private void ReplaceButton_Clicked(object sender, EventArgs e)
		{
			ReplaceModule();
		}

		private void AddRadioElement(Device device)
		{
			int numberOfDevices = Storage.GetDevicesById(Storage.LastUsedLocationId.Value).Values
				.Count(d => d.SerialNumber.Split(':')[0] == device.SerialNumber && d.SerialNumber != device.SerialNumber);

			bool enabled = false;

			string value = numberOfDevices == 0
				? "<New>"
				: String.Format(i18n.smartrule_actiontext_device_count, numberOfDevices.ToString());

			if (numberOfDevices == 0)
			{
				availableModules++;

				if (selectFirst)
				{
					newModuleSerialNumber = device.SerialNumber;
					selectFirst = false;
					enabled = true;
				}
			}

			var radioElement = new RadioElement(this, device.Name, enabled, value, device.SerialNumber);

			radioElement.ValueChanged += (sender, e) =>
			{
				if (!radioElement.Value)
				{
					return;
				}

				if (numberOfDevices > 0)
				{
					radioElement.Value = false;
					ShowWarningDialog(numberOfDevices);
				}

				else
				{
					newModuleSerialNumber = device.SerialNumber;
					UpdateRadioElements(radioElement);
				}
			};

			radioElements.Add(radioElement);

			moduleSection.Add(radioElement);
		}

		private void UpdateRadioElements(RadioElement sender)
		{
			foreach (var element in radioElements)
			{
				if (element == sender)
				{
					continue;
				}

				element.Value = false;
			}
		}

		private void ReplaceModule()
		{
			progressDialogHash = CreateProgressDialog(string.Format(i18n.saving), false, null);

			var communicationManager = ApplicationManager.Instance.GetCurrentCommunicationManager();

			if (communicationManager == null)
			{
				DismissProgressDialog(progressDialogHash);
				return;
			}

			var task = Task.Factory.StartNew(() =>
			{
				foreach (var device in devicesToReplace)
				{
					if (!device.SerialNumber.Contains(oldModule.SerialNumber) || device.SerialNumber == oldModule.SerialNumber)
					{
						continue;
					}

					var newSerialNumber = device.SerialNumber.Replace(oldModule.SerialNumber,
						newModuleSerialNumber);

					ApplicationManager.Instance.GetCurrentCommunicationManager().UpdateDeviceSerialNumber(
						device.ID,
						newSerialNumber,
						(t) =>
						{
							this.RunOnUiThreadTh(() =>
							{
								bool wasOk = t.CompletedWithoutProblemsWithResult();

								if (wasOk)
									CreateToast("Device " + device.Name + " updated");
								else
									CreateToast("Error updating device " + device.Name);
							});
						});
				}
			}).ContinueWith((t) =>
			{
				DeleteDevice(false);
			});
		}

		private void DeleteDevice(bool forceDelete)
		{
			try
			{
				ApplicationManager.Instance.GetCurrentCommunicationManager().DeleteDevice(this.oldModule.ID, forceDelete, (t) =>
				{
					this.RunOnUiThreadTh(() =>
					{
						DismissProgressDialog(progressDialogHash);

						if (t.CompletedWithoutProblemsWithResult())
						{
							var result = t.Result as MessageGenericResponse;
							switch (result.ErrorCode)
							{
								case CommonErrorCodes.Ok:
									CreateToast("Replace module complete.");
									GoBack();
#if IOS
                                    GoBack();
#endif

									ApplicationManager.Instance.GetCurrentCommunicationManager().RestartCore((t3) =>
									{
										this.RunOnUiThreadTh(() =>
										{
											bool wasOk = t3.CompletedWithoutProblemsWithResult();

											if (!wasOk)
												CreateToast("Error restarting core");
										});
									});

									break;

								case CommonErrorCodes.DeserializationException:
									CreateToast((t.Exception == null ? i18n.error_unknown : t.Exception.ToString()));
									break;

								case CommonErrorCodes.Exception:
									string ids = result.ErrorMessage;

									if (ids.IndexOf("{") < 0 || ids.IndexOf("}") < 0)
									{
										CreateToast((t.Exception == null ? i18n.error_unknown : t.Exception.ToString()));
									}
									else
									{
										ids = ids.Substring(ids.IndexOf("{") + 1);
										ids = ids.Substring(0, ids.IndexOf("}"));

										List<int> idsList = ids.Split(',').Select(int.Parse).ToList();

										ShowForceDeleteDialog(idsList);
									}
									break;

							}
						}
						else
						{
							CreateToast(i18n.core_notConnected);
						}
					});
				});
			}
			catch (CoreVersionTooLowException e1)
			{
				CreateToast(e1.Message);
			}
		}

		private void ShowForceDeleteDialog(List<int> devicesIds)
		{
			var buttons = new AlertDialogButtons();

			buttons.PositiveButton = new Tuple<string, Action>(i18n.delete, () =>
			{
				DeleteDevice(true);
			});

			buttons.NegativeButton = new Tuple<string, Action>(i18n.general_cancel, () =>
			{
			});

			string message = "These devices have assigned smartrules: \n\n";

			foreach (var deviceId in devicesIds)
			{
				var device = Storage.GetDevice(Storage.LastUsedLocationId.Value, deviceId);
				message += "- " + device.Name + "\n";
			}

			message += "\nDo you want to proceed deleting this module?";

			CreateAlertDialog(i18n.device_delete, message, buttons);
		}

		private void ShowWarningDialog(int numberOfDevices)
		{
			var buttons = new AlertDialogButtons();

			buttons.PositiveButton = new Tuple<string, Action>(i18n.general_ok, () => { });

			CreateAlertDialog(String.Empty, String.Format(i18n.module_replace_warning, (oldModule.Name + " (" + oldModule.SerialNumber + ")"),
				numberOfDevices.ToString()), buttons);
		}

		//public override void OnBack()
		//{
		//	GoBack();
		//}

		//public override void CreateControls()
		//{
		//}
	}
}
