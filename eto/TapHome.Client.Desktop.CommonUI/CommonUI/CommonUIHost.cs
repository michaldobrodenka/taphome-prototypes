﻿using System;
using System.Collections.Generic;

namespace TapHome.Client.Desktop.CommonUI
{
    public partial class CommonUIHost
    {
        public Stack<CommonUIPage> navigationStack = new Stack<CommonUIPage>();

        public CommonUIHost()
        {
            InitializeComponent();

            this.backButton.Click += BackButton_Click;
        }

		public static bool UseManualResizing()
		{
			return false; 
		}

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            this.rightToolbarSpace.Size = this.backButton.Size;

			navigationStack.Peek().RefreshWidth();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        public void Push(CommonUIPage page, bool asRoot = false)
        {
            page.Host = this;

            if (asRoot)
            {
                this.navigationStack.Clear();
            }

            this.navigationStack.Push(page);
            page.OnInit();
            page.WillAppear();

            this.pageContent.Content = page;

            this.RefreshBackButton();
        }

        public void GoBack()
        {
            if (this.navigationStack.Count > 1)
            {
                this.navigationStack.Pop();

                var page = this.navigationStack.Peek();

                this.pageContent.Content = page;
            }

            this.RefreshBackButton();
        }

        public void RefreshBackButton()
        {
            if (this.navigationStack.Count < 2)
            {
                this.backButton.Enabled = false;
            }
            else
            {
                this.backButton.Enabled = true;
            }
        }
    }
}
