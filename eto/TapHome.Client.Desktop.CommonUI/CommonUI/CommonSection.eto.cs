﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.Client.Desktop.CommonUI
{
    partial class CommonSection : Panel
    {
        private StackLayout stackLayout;

        private Label headerLabel;
        private StackLayoutItem headerStackLayoutItem;

        private Label footerLabel;
        private StackLayoutItem footerStackLayoutItem;

        void InitializeComponent()
        {
            this.headerLabel = new Label() { Text = this.header };
			this.footerLabel = new Label() { Text = this.footer, Size = new Size(10,-1) };
            this.headerStackLayoutItem = new StackLayoutItem(this.headerLabel, expand: false);
            this.footerStackLayoutItem = new StackLayoutItem(this.footerLabel, expand: false);

            this.stackLayout = new StackLayout
			{
				Padding = new Padding(4, 2),
				Orientation = Orientation.Vertical,
				VerticalContentAlignment = VerticalAlignment.Center,
				//HorizontalContentAlignment = Application.Instance.Platform.IsWpf ? HorizontalAlignment.Left : HorizontalAlignment.Stretch,
				HorizontalContentAlignment = CommonUIHost.UseManualResizing()  ? HorizontalAlignment.Left : HorizontalAlignment.Stretch,
				//HorizontalContentAlignment = HorizontalAlignment.Left,
                Items =
                {
                    this.headerStackLayoutItem,
                    this.footerStackLayoutItem
                }
            };

            this.RefreshFooterVisibility();

            Content = this.stackLayout;
        }
    }
}
