﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EmbedWpfInEto
{
    /// <summary>
    /// Interaction logic for MyNativeControl.xaml
    /// </summary>
    public partial class MyNativeControl : UserControl
    {
        public MyNativeControl()
        {
            InitializeComponent();
            List<User> items = new List<User>();
            for (int i = 0; i < 500; i++)
            {
                var item = new User { Name = "Name " + i, Age = i % 100, Mail = "sdfgsdfg", MailVisible = i % 3 == 0 ? Visibility.Collapsed : Visibility.Visible };

                items.Add(item);
            }

            items.Add(new User() { Name = "John Doe", Age = 42, Mail = "john@doe-family.com", Group = "1" });
            items.Add(new User() { Name = "Jane Doe", Age = 39, Mail = "jane@doe-family.com", Group = "1" });
            items.Add(new User() { Name = "Sammy Doe", Age = 7, Mail = "sammy.doe@gmail.com", Group = "1" });


            ICollectionView items2 = CollectionViewSource.GetDefaultView(items);
            items2.GroupDescriptions.Add(new PropertyGroupDescription("Group"));

            lvUsers.ItemsSource = items;
        }
    }

    public class User
    {
        public string Group { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public string Mail { get; set; }

        public Visibility MailVisible { get; set; }
    }
}