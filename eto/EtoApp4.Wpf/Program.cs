﻿using System;
using Eto;
using Eto.Forms;
using EtoApp4.Main;
using TapHome.Client.Desktop.CommonUI;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.Commons;
using TapHome.Commons.CommonPages;

namespace EtoApp4.Wpf
{
    public class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            int t = 0;

            //new Application(Platforms.Wpf).Run(new CommonUITest());

            var application = new Application(Platforms.Wpf);

            ApplicationManager.Init();

            var page = new CommonUITest(null);

            //var page = new SettingsPage();

            var host = new CommonUIHost();
            host.Push(page);

            var form = new Form();
            form.Content = host;
	        //form.Content = new RangeSlider();

			application.Run(form);
        }
    }
}
