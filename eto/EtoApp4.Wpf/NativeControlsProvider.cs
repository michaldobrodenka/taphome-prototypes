﻿using EmbedWpfInEto;
using Eto.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.TapHome.CommonUI
{
    public class ControlProvider
    {
        public static Control GetListControl()
        {
            var listControl = new MyNativeControl();

            var etoControl = listControl.ToEto();
            etoControl.Tag = "MainList";

            return etoControl;
        }
    }
}
