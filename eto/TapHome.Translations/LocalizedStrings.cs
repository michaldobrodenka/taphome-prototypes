﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TapHome.Translations
{
    public class LocalizedStrings
    {
        public LocalizedStrings()
        {
        }

        private static i18n localizedResources = new i18n();

        public i18n i18n
        {
            get
            {
                return localizedResources;
            }
        }
    }
}
