﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using TapHome.Client.Desktop.CommonUI;

namespace EtoApp4.Main
{
    partial class CommonUITest : CommonUIPage
    {
        CommonSection section, section2;

        void InitializeComponent()
        {
            Title = "My Form";
            this.Size = new Size(300, 400);

            this.section = new CommonSection(this, "Header", true);
            this.AddSection(this.section);
        }
    }
}
