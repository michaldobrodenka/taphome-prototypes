﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace EtoApp4.Main
{
    public partial class Panel2
    {
        public Panel2()
        {
            InitializeComponent();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            this.TreeGridView.Columns[0].Width = this.TreeGridView.Size.Width / 2-1;
            this.TreeGridView.Columns[1].Width = this.TreeGridView.Size.Width - this.TreeGridView.Columns[0].Width-2;
        }
    }
}
