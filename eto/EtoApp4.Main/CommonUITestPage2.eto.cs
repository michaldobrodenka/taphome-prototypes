﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using Eto.Forms;
using Eto.Drawing;
using TapHome.Client.Desktop.CommonUI;
using TapHome.Client.Desktop.CommonUI.Elements;

namespace EtoApp4.Main
{
    partial class CommonUITestPage2 : CommonUIPage
    {
        CommonSection section, section2;

        private Scrollable scrollable;

        void InitializeComponent()
        {
            Title = "My Form";
            this.Size = new Size(300, 400);

            this.section = new CommonSection(this, "Header", true);

            this.scrollable = new Scrollable();
            this.scrollable.Content = this.section;

            Content = this.scrollable;

            this.section.Add(new StringElement(new Context(), "Parameter", (string)this.GetParameter("p", typeof(string))));
        }
    }
}
