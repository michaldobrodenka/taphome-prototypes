﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace EtoApp4.Main
{
    partial class EmbedDialog : Dialog
    {
        void InitializeComponent()
        {
            Title = "My Dialog";

            // buttons
            DefaultButton = new Button { Text = "OK" };
            DefaultButton.Click += (sender, e) => Close();

            AbortButton = new Button { Text = "C&ancel" };
            AbortButton.Click += (sender, e) => Close();

            var buttons = new TableLayout { Rows = { new TableRow(null, DefaultButton, AbortButton) }, Spacing = new Size(5, 5) };

            Content = new StackLayout
            {
                Padding = new Padding(10),
                Spacing = 5,
                Items =
                {
                    new StackLayoutItem(new EmbedPanel(), HorizontalAlignment.Stretch, expand: true),
                    new StackLayoutItem(buttons, HorizontalAlignment.Right)
                }
            };
        }
    }
}
