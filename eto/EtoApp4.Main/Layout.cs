﻿using System;
using System.Linq;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using System.Threading;
using System.Threading.Tasks;

namespace EtoApp4.Main
{
    public partial class Layout
    {
        private Label label1, label2, label3, label4, label5;
        private Label[] labels = new Label[5];

        private Dictionary<Control, StackLayoutItem> controlsToStackLayoutItems = new Dictionary<Control, StackLayoutItem>();

        public Layout()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Task.Run(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Thread.Sleep(200);

                    Application.Instance.Invoke(() =>
                    {
                        var label = new Label() { Text = "Label " + i, TextAlignment = TextAlignment.Center };

                        this.labels[i] = label;

                        this.InsertControl(label, 0);
                    });
                }

                Application.Instance.Invoke(() => this.RemoveControl(this.labels[0]));
                Thread.Sleep(200);
                Application.Instance.Invoke(() => this.RemoveControl(this.labels[2]));
                Thread.Sleep(200);
                Application.Instance.Invoke(() => this.RemoveControl(this.labels[3]));
                Thread.Sleep(200);
                Application.Instance.Invoke(() => this.RemoveControl(this.labels[1]));
                Thread.Sleep(200);
                Application.Instance.Invoke(() => this.RemoveControl(this.labels[4]));
            });
        }

        public void AddControl(Control control)
        {
            var item = new StackLayoutItem(control);
            this.controlsToStackLayoutItems[control] = item;
            this.stackLayout.Items.Add(item);
        }

        public void InsertControl(Control control, int index)
        {
            var item = new StackLayoutItem(control);
            this.controlsToStackLayoutItems[control] = item;
            this.stackLayout.Items.Insert(index,control);
        }

        public void RemoveControl(Control control)
        {
            var index = this.stackLayout.Controls.ToList().IndexOf(control);

            if (index >= 0)
            {
                this.stackLayout.Items.RemoveAt(index);
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Instance.Quit();
        }
    }
}
