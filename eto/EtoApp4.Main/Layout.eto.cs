﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace EtoApp4.Main
{
    partial class Layout : Form
    {
        private StackLayout stackLayout;

        void InitializeComponent()
        {
            Title = "My Form";
            Size = new Size(300, 500);
            this.stackLayout = new StackLayout()
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
                BackgroundColor = Colors.AliceBlue,
            };

            Content = this.stackLayout;
        }
    }
}
