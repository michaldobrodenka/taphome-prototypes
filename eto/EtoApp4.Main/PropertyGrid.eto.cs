﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace EtoApp4.Main
{
    partial class PropertyGrid : Form
    {
        private TreeGridView propertyGridView;

        void InitializeComponent()
        {
            Title = "My Form";

            this.propertyGridView = new TreeGridView();

            Content = this.propertyGridView;
        }
    }
}
