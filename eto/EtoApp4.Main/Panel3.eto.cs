﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace EtoApp4.Main
{
    partial class Panel3 : Form
    {
        private ListBox listBox;

        void InitializeComponent()
        {
            Title = "My Form";

            this.listBox = new ListBox();
            this.listBox.Items.Add(new ListItem() { Text = "Item 1" });
            this.listBox.Items.Add(new ListItem() { Text = "Item 2" });
            this.listBox.Items.Add(new ListItem() { Text = "Item 3" });
            this.listBox.Items.Add(new ListItem() { Text = "Item 4" });

            Content = this.listBox;
        }
    }
}
