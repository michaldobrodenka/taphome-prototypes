﻿using System;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
	public partial class StringDialog
	{
		private class StringDialogViewModel
		{
			public string Value { get; set; }
		}

		private StringDialogViewModel viewModel;

		public StringDialog(string title, string initialValue = null, Func<string, bool> validate = null)
		{
			InitializeComponent();

			Title = title;
			valueLabel.Text = title;

			viewModel = new StringDialogViewModel
			{
				Value = initialValue,
			};
			DataContext = viewModel;

			valueTextBox.TextBinding.BindDataContext((StringDialogViewModel vm) => vm.Value);

			if (validate != null)
			{
				valueTextBox.TextChanged += (sender, args) => DefaultButton.Enabled = validate(viewModel.Value);
				DefaultButton.Enabled = validate("");
			}

			DefaultButton.Click += DefaultButton_Click;
			AbortButton.Click += AbortButton_Click;
		}

		protected void DefaultButton_Click(object sender, EventArgs e)
		{
			Result = viewModel.Value;
			Close();
		}

		protected void AbortButton_Click(object sender, EventArgs e)
		{
			Result = null;
			Close();
		}
	}
}
