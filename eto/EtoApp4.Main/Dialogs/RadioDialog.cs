﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    public partial class RadioDialog
	{
		private class RadioDialogViewModel : INotifyPropertyChanged
		{
			public string Caption { get; set; }
			public string[] Values { get; set; }
			public int SelectedIndex { get; set; }

			private bool showThirdButton;
			public bool ShowThirdButton
			{
				get { return showThirdButton; }
				set
				{
					if (showThirdButton == value)
						return;

					showThirdButton = value;
					OnPropertyChanged();
				}
			}

			public Action ThirdButtonAction { get; set; }

			#region INotifyPropertyChanged implementation

			public event PropertyChangedEventHandler PropertyChanged;

			private void OnPropertyChanged([CallerMemberName] string propertyName = null)
			{
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}

			#endregion
		}

		public class SelectedIndexChangedEventArgs : EventArgs
		{
			public int Selected { get; set; }
		}
		public event EventHandler<SelectedIndexChangedEventArgs> SelectedIndexChanged;

		public string Caption
		{
			get { return viewModel.Caption;  }
			set
			{
				viewModel.Caption = value;
				Title = value;
			}
		}

		public bool ShowThirdButton
		{
			get { return viewModel.ShowThirdButton; }
			set { viewModel.ShowThirdButton = value; }
		}

		private RadioDialogViewModel viewModel;

		public RadioDialog(string caption, string[] values, int selectedIndex, bool showThirdButton, Action thirdButtonAction)
        {
	        this.viewModel = new RadioDialogViewModel
	        {
				Caption = caption,
				Values = values,
				SelectedIndex = selectedIndex,
				ShowThirdButton = showThirdButton,
				ThirdButtonAction = thirdButtonAction,
	        };

	        this.Title = caption;

            InitializeComponent();

	        for (var i = 0; i < values.Length; i++)
	        {
		        var value = values[i];
		        this.radioButtonList.Items.Add(new ListItem() { Text = value, Tag = i });
	        };

	        this.DataContext = this.viewModel;
			
	        this.radioButtonList.SelectedIndexBinding.BindDataContext(Binding.Property((RadioDialogViewModel vm) => vm.SelectedIndex));
			this.radioButtonList.SelectedValueChanged += RadioButtonList_SelectedValueChanged;

			this.DefaultButton.Click += (sender, e) => Close(viewModel.SelectedIndex);
	        this.AbortButton.Click += (sender, e) => { Close(-1); };

			this.thirdButton.Click += ThirdButton_Click;
	        this.thirdButton.Bind(Binding.Property((Button b) => b.Visible), Binding.Property(viewModel, (vm) => vm.ShowThirdButton));
		}

		private void RadioButtonList_SelectedValueChanged(object sender, EventArgs e)
		{
			this.SelectedIndexChanged?.Invoke(sender, new SelectedIndexChangedEventArgs {Selected = radioButtonList.SelectedIndex});

			// todo: resize somehow to fit all three buttons if needed
		}

		private void ThirdButton_Click(object sender, EventArgs e)
		{
			this.viewModel.ThirdButtonAction?.Invoke();
		}

		protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            // preventing too tall window in case of many items
            if (this.oldSize.Height == -1 && this.mainLayout.Height > 500)
            {
               this.Size = new Size(this.Content.Width+25, 500);
            }

            this.oldSize = this.Size;
        }
    }
}
