﻿using Eto.Forms;
using Eto.Drawing;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    partial class RolesDialog : Dialog<RolesDialog.RolesDialogResult>
    {
        private StackLayout mainStackLayout;
        private CheckBox userCheckBox;
        private CheckBox adminCheckBox;
        private CheckBox serviceCheckBox;

        void InitializeComponent()
        {
            userCheckBox = new CheckBox();
            adminCheckBox = new CheckBox();
            serviceCheckBox = new CheckBox();

            var userStackLayout = new StackLayout
            {
                Orientation = Orientation.Horizontal,

                Items =
                {
                    new StackLayoutItem(new Label { Text = "User" }, HorizontalAlignment.Left, true),
                    new StackLayoutItem(userCheckBox, HorizontalAlignment.Right, false),
                }
            };

            var adminStackLayout = new StackLayout
            {
                Orientation = Orientation.Horizontal,

                Items =
                {
                    new StackLayoutItem(new Label { Text = "Admin" }, HorizontalAlignment.Left, true),
                    new StackLayoutItem(adminCheckBox, HorizontalAlignment.Right, false),
                }
            };

            var serviceStackLayout = new StackLayout
            {
                Orientation = Orientation.Horizontal,

                Items =
                {
                    new StackLayoutItem(new Label { Text = "Service" }, HorizontalAlignment.Left, true),
                    new StackLayoutItem(serviceCheckBox, HorizontalAlignment.Right, false),
                }
            };

            // buttons
            DefaultButton = new Button { Text = "OK" };

            AbortButton = new Button { Text = "Cancel" };

            var buttons = new TableLayout { Rows = { new TableRow(null, DefaultButton, AbortButton) }, Spacing = new Size(5, 5) };

            mainStackLayout = new StackLayout
            {
                Padding = new Padding(10),
                Spacing = 5,
                Items =
                {
                    new StackLayoutItem(userStackLayout, expand: true),
                    new StackLayoutItem(adminStackLayout, expand: true),
                    new StackLayoutItem(serviceStackLayout, expand: true),
                    new StackLayoutItem(buttons, HorizontalAlignment.Right)
                }
            };

            Content = mainStackLayout;
        }
    }
}
