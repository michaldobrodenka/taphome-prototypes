﻿using System;
using TapHome.Client.Desktop.CommonUI.Dependencies;
using TapHome.Translations;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    public partial class RolesDialog
    {
        public class RolesDialogResult
        {
            public bool User { get; set; }
            public bool Admin { get; set; }
            public bool Service { get; set; }
        }

        private class RolesDialogViewModel
        {
            public bool User { get; set; }
            public bool Admin { get; set; }
            public bool Service { get; set; }
        }

        private RolesDialogViewModel viewModel;

        public RolesDialog(UserPermission permissions)
        {
            InitializeComponent();

            viewModel = new RolesDialogViewModel();

            userCheckBox.Checked = permissions.HasFlag(UserPermission.User);
            adminCheckBox.Checked = permissions.HasFlag(UserPermission.Admin);
            serviceCheckBox.Checked = permissions.HasFlag(UserPermission.Service);

            userCheckBox.CheckedBinding.BindDataContext((RolesDialogViewModel vm) => vm.User);
            adminCheckBox.CheckedBinding.BindDataContext((RolesDialogViewModel vm) => vm.Admin);
            serviceCheckBox.CheckedBinding.BindDataContext((RolesDialogViewModel vm) => vm.Service);

            DataContext = viewModel;

            DefaultButton.Click += (sender, e) =>
            {
                if ((viewModel.Service && !permissions.HasFlag(UserPermission.Service)) ||
                    (!viewModel.Service && permissions.HasFlag(UserPermission.Service)))
                {
                    var buttons = new AlertDialogButtons();
                    buttons.PositiveButton = new Tuple<string, Action>("Ok", () =>
                    {
                        Result = new RolesDialogResult()
                        {
                            User = viewModel.User,
                            Admin = viewModel.Admin,
                            Service = viewModel.Service,
                        };
                        Close();
                    });

                    var dialog = new AlertDialog(i18n.warning, i18n.warning_changingServicePermission, buttons);
                    dialog.ShowModal(this);
                    return;
                }

                Result = new RolesDialogResult()
                {
                    User = viewModel.User,
                    Admin = viewModel.Admin,
                    Service = viewModel.Service,
                };
                Close();
            };

            AbortButton.Click += (sender, e) =>
            {
                Result = null;
                Close();
            };
        }
    }
}
