﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
	partial class StringDialog : Dialog<string>
	{
		protected StackLayout mainStackLayout;
		protected Label valueLabel;
		protected TextBox valueTextBox;

		void InitializeComponent()
		{
			valueLabel = new Label();
			valueTextBox = new TextBox();

			this.DefaultButton = new Button { Text = "OK" };
			this.AbortButton = new Button { Text = "C&ancel" };
			var buttonsLayout = new TableLayout { Rows = { new TableRow(null, DefaultButton, AbortButton) }, Spacing = new Size(5, 5) };

			mainStackLayout = new StackLayout
			{
				Padding = 5,
				Spacing = 5,
				Orientation = Orientation.Vertical,
				HorizontalContentAlignment = HorizontalAlignment.Stretch,
				BackgroundColor = Colors.AliceBlue,

				Items =
				{
					new StackLayoutItem(valueLabel),
					new StackLayoutItem(valueTextBox),
					new StackLayoutItem(buttonsLayout)
				},
			};

			Content = mainStackLayout;
			Resizable = true;
		}
	}
}
