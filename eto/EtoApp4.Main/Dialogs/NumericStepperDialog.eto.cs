﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    partial class NumericStepperDialog : Dialog<double?>
    {
        private NumericStepper numericStepper;
        private Label captionLabel;

        void InitializeComponent()
        {
            numericStepper = new NumericStepper();
            captionLabel = new Label();

            // buttons
            DefaultButton = new Button { Text = "OK" };
            AbortButton = new Button { Text = "Cancel" };
            
            var buttons = new TableLayout { Rows = { new TableRow(null, DefaultButton, AbortButton) }, Spacing = new Size(5, 5) };

            Content = new StackLayout
            {
                Padding = new Padding(10),
                Spacing = 5,
                Items =
                {
                    new StackLayoutItem(captionLabel, expand: true),
                    new StackLayoutItem(numericStepper, expand: true),
                    new StackLayoutItem(buttons, HorizontalAlignment.Right)
                }
            };
        }
    }
}
