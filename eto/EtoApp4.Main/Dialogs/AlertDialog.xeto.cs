﻿using System;
using Eto.Forms;
//using Eto.Serialization.Xaml;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
	public class AlertDialogButtons
	{
		public Tuple<string, Action> NeutralButton { get; set; }
		public Tuple<string, Action> PositiveButton { get; set; }
		public Tuple<string, Action> NegativeButton { get; set; }
	}

	public class AlertDialog : Dialog
	{
		protected Label TitleLabel;
		protected Label MessageLabel;
		protected Button PositiveButton;
		protected Button NeutralButton;
		protected Button NegativeButton;

		private AlertDialogButtons buttons;

		public AlertDialog(string title, string message, AlertDialogButtons buttons)
		{
			//XamlReader.Load(this);

			this.buttons = buttons;

			TitleLabel.Visible = !string.IsNullOrEmpty(title);
			MessageLabel.Visible = !string.IsNullOrEmpty(message);
			PositiveButton.Visible = buttons.PositiveButton != null;
			NeutralButton.Visible = buttons.NeutralButton != null;
			NegativeButton.Visible = buttons.NegativeButton != null;

			TitleLabel.Text = title;
			MessageLabel.Text = message;

			if (buttons.PositiveButton != null)
			{
				PositiveButton.Text = buttons.PositiveButton.Item1;
			}

			if (buttons.NeutralButton != null)
			{
				NeutralButton.Text = buttons.NeutralButton.Item1;
			}

			if (buttons.NegativeButton != null)
			{
				NegativeButton.Text = buttons.NegativeButton.Item1;
			}
		}

		protected void PositiveButton_Click(object sender, EventArgs e)
		{
			Close();
			buttons.PositiveButton.Item2?.Invoke();
		}

		protected void NeutralButton_Click(object sender, EventArgs e)
		{
			Close();
			buttons.NeutralButton.Item2?.Invoke();
		}

		protected void NegativeButton_Click(object sender, EventArgs e)
		{
			Close();
			buttons.NegativeButton.Item2?.Invoke();
		}
	}
}
