﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using TapHome.Commons.CommonFrames;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    public partial class NumericStepperDialog
    {
        private class NumericStepperDialogViewModel
        {
            public double Value { get; set; }
        }

        private NumericStepperDialogViewModel viewModel;

        public NumericStepperDialog(string caption, double value, ServiceSettingsFrame.PropertyInfo propertyInfo)
        {
            InitializeComponent();

            viewModel = new NumericStepperDialogViewModel();

            numericStepper.ValueBinding.BindDataContext((NumericStepperDialogViewModel vm) => vm.Value);

            DataContext = viewModel;

            captionLabel.Text = caption;

            numericStepper.Value = value;

            DefaultButton.Click += (sender, e) =>
            {
                Result = viewModel.Value;
                Close();
            };

            AbortButton.Click += (sender, e) =>
            {
                Result = null;
                Close();
            };

            if (propertyInfo == null)
            {
                return;
            }

            switch (propertyInfo.InputType)
            {
                case ServiceSettingsFrame.PropertyInputType.SignedDecimalNumber:
                    numericStepper.DecimalPlaces = 1;
                    break;
                case ServiceSettingsFrame.PropertyInputType.UnsignedDecimalNumber:
                    numericStepper.DecimalPlaces = 1;
                    numericStepper.MinValue = 0;
                    break;
                case ServiceSettingsFrame.PropertyInputType.SignedNumber:
                    numericStepper.DecimalPlaces = 0;
                    break;
                default:
                    numericStepper.DecimalPlaces = 0;
                    numericStepper.MinValue = 0;
                    break;
            }

            if (propertyInfo.MinValue != null)
            {
                numericStepper.MinValue = propertyInfo.MinValue.Value;
            }

            if (propertyInfo.MaxValue != null)
            {
                numericStepper.MaxValue = propertyInfo.MaxValue.Value;
            }
        }
    }
}
