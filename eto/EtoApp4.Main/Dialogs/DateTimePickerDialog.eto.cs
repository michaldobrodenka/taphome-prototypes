﻿using System;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
	partial class DateTimePickerDialog : Dialog<DateTime?>
	{
		private StackLayout mainStackLayout;
		private Label valueLabel;
		private DateTimePicker dateTimePicker;

		void InitializeComponent()
		{
			valueLabel = new Label();
			dateTimePicker = new DateTimePicker();

			DefaultButton = new Button { Text = "OK" };
			AbortButton = new Button { Text = "C&ancel" };
			var buttonsLayout = new TableLayout { Rows = { new TableRow(null, DefaultButton, AbortButton) }, Spacing = new Size(5, 5) };

			mainStackLayout = new StackLayout
			{
				Padding = 5,
				Spacing = 5,
				Orientation = Orientation.Vertical,
				HorizontalContentAlignment = HorizontalAlignment.Stretch,
				BackgroundColor = Colors.AliceBlue,

				Items =
				{
					new StackLayoutItem(valueLabel),
					new StackLayoutItem(dateTimePicker),
					new StackLayoutItem(buttonsLayout)
				},
			};

			Content = mainStackLayout;
			Resizable = true;
		}
	}
}
