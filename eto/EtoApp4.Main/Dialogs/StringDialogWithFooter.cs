﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
	public class StringDialogWithFooter : StringDialog
	{
		private string footer;
		private Label footerLabel;

		public StringDialogWithFooter(string title, string footer, string initialValue = null, Func<string, bool> validate = null)
			: base(title, initialValue, validate)
		{
			this.footer = footer;

			footerLabel = new Label { Text = footer };

			mainStackLayout.Items.Add(new StackLayoutItem { Control = footerLabel, Expand = true });
		}
	}
}
