﻿
using System;
using TapHome.Translations;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    public partial class PasswordDialog
    {
        public class PasswordDialogResult
        {
            public string OldPassword { get; set; }
            public string NewPassword { get; set; }
            public string RepeatPassword { get; set; }
        }

        private class PasswordDialogViewModel
        {
            public string OldPassword { get; set; }
            public string NewPassword { get; set; }
            public string RepeatPassword { get; set; }
        }

        private PasswordDialogViewModel viewModel;

        public PasswordDialog()
        {
            viewModel = new PasswordDialogViewModel();

            InitializeComponent();

            oldPasswordBox.TextBinding.BindDataContext((PasswordDialogViewModel vm) => vm.OldPassword);
            newPasswordBox.TextBinding.BindDataContext((PasswordDialogViewModel vm) => vm.NewPassword);
            repeatPasswordBox.TextBinding.BindDataContext((PasswordDialogViewModel vm) => vm.RepeatPassword);

            DataContext = viewModel;

            this.DefaultButton.Click += (sender, e) =>
            {
                if (viewModel.NewPassword != viewModel.RepeatPassword)
                {
                    var buttons = new AlertDialogButtons();
                    buttons.PositiveButton = new Tuple<string, Action>("Ok", null);

                    var dialog2 = new AlertDialog(string.Empty, i18n.smartrule_phasealarm_passcode_mismatch, buttons);
                    dialog2.ShowModal(this);
                    return;
                };

                Result = new PasswordDialogResult()
                {
                    OldPassword = viewModel.OldPassword,
                    NewPassword = viewModel.NewPassword,
                };

                Close();
            };

            this.AbortButton.Click += (sender, e) =>
            {
                Result = null;
                Close();
            };
        }
    }
}
