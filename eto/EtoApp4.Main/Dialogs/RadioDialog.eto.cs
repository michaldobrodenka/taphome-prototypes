﻿using System;
using Eto.Forms;
using Eto.Drawing;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    partial class RadioDialog : Dialog<int>
    {
        private Action thirdButtonAction;

        private Size oldSize;

        private StackLayout mainLayout;

        private RadioButtonList radioButtonList;
        private Scrollable scrollable;

        private Button thirdButton;

        void InitializeComponent()
		{
			this.MinimumSize = new Size(80, 10);
			this.Resizable = true;

			this.radioButtonList = new RadioButtonList() { Orientation = Orientation.Vertical, Padding = 5};			
            this.scrollable = new Scrollable() { Content = this.radioButtonList };

            // buttons
            this.DefaultButton = new Button { Text = "OK" };
            this.AbortButton = new Button { Text = "C&ancel" };
            this.thirdButton = new Button() { Text = "Rename" };

            var buttonsLayout = new TableLayout { Rows = { new TableRow(null, DefaultButton, thirdButton, AbortButton) }, Spacing = new Size(5, 5) };

            this.mainLayout = new StackLayout
            {
                Padding = new Padding(10),
                Spacing = 5,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
                Items =
                {
                    new StackLayoutItem(this.scrollable, expand: true),
                    new StackLayoutItem(buttonsLayout, HorizontalAlignment.Right)
                }
            };

            this.Content = mainLayout;
			this.oldSize = this.Size;
		}
	}
}
