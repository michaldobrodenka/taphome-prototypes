﻿using Eto.Forms;
using Eto.Drawing;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    partial class PasswordDialog : Dialog<PasswordDialog.PasswordDialogResult>
    {
        private StackLayout mainStackLayout;

        private PasswordBox oldPasswordBox;
        private PasswordBox newPasswordBox;
        private PasswordBox repeatPasswordBox;

        void InitializeComponent()
        {
            oldPasswordBox = new PasswordBox();
            newPasswordBox = new PasswordBox();
            repeatPasswordBox = new PasswordBox();

            DefaultButton = new Button { Text = "OK" };
            AbortButton = new Button { Text = "Cancel" };

            var buttons = new TableLayout { Rows = { new TableRow(DefaultButton, AbortButton) }, Spacing = new Size(5, 5) };

            mainStackLayout = new StackLayout
            {
                Padding = new Padding(10),
                Spacing = 5,
				HorizontalContentAlignment = HorizontalAlignment.Stretch,

                Items =
                {
                    new StackLayoutItem(new Label { Text = "Enter old password" }),
                    new StackLayoutItem(oldPasswordBox),
                    new StackLayoutItem(new Label { Text = "Enter new password" }),
                    new StackLayoutItem(newPasswordBox),
                    new StackLayoutItem(new Label { Text = "Confirm new password" }),
                    new StackLayoutItem(repeatPasswordBox),
                    new StackLayoutItem(buttons, HorizontalAlignment.Right)
                }
            };

            Content = mainStackLayout;
        }
    }
}
