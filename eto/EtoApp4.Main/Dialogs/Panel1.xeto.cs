﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using Eto.Serialization.Xaml;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
    public class Panel1 : Panel
    {
        public Panel1()
        {
            XamlReader.Load(this);
        }
    }
}
