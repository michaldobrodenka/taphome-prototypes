﻿using System;

namespace TapHome.DesktopClient.Wpf.Dialogs
{
	public partial class DateTimePickerDialog
	{
		private class DateTimePickerDialogViewModel
		{
			public DateTime? DateTime { get; set; }
		}

		private DateTimePickerDialogViewModel viewModel;

		public DateTimePickerDialog(string caption, DateTime minDate, DateTime maxDate, DateTime selectedDate)
		{
			InitializeComponent();

			Title = caption;
			valueLabel.Text = caption;

			viewModel = new DateTimePickerDialogViewModel
			{
				DateTime = selectedDate,
			};
			DataContext = viewModel;

			dateTimePicker.MinDate = minDate;
			dateTimePicker.MaxDate = maxDate;
			dateTimePicker.ValueBinding.BindDataContext((DateTimePickerDialogViewModel vm) => vm.DateTime);

			DefaultButton.Click += DefaultButton_Click;
			AbortButton.Click += AbortButton_Click;
		}

		protected void DefaultButton_Click(object sender, EventArgs e)
		{
			Result = viewModel.DateTime;
			Close();
		}

		protected void AbortButton_Click(object sender, EventArgs e)
		{
			Result = null;
			Close();
		}
	}
}
