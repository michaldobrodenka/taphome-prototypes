﻿using Eto.Drawing;
using Eto.Forms;

namespace EtoApp4.Main
{
	partial class RangeSlider : Panel
	{
		private Slider lowerSlider;
		private Slider upperSlider;

		void InitializeComponent()
		{
			lowerSlider = new Slider { MinValue = 0, MaxValue = 100, Value = 20, };
			upperSlider = new Slider { MinValue = 0, MaxValue = 100, Value = 80, };

			var layout = new StackLayout();
            layout.Padding = 5;
            layout.Spacing = -24;
			layout.Items.Add(lowerSlider);
			layout.Items.Add(upperSlider);

			Content = layout;
		}
	}
}
