﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace EtoApp4.Main
{
    partial class EmbedPanel : Panel
    {
        private ListBox listBox;

        void InitializeComponent()
        {
            this.listBox = new ListBox();
            this.listBox.Items.Add(new ListItem() { Text = "Item 1" });
            this.listBox.Items.Add(new ListItem() { Text = "Item 2" });
            this.listBox.Items.Add(new ListItem() { Text = "Item 3" });
            this.listBox.Items.Add(new ListItem() { Text = "Item 4" });
            this.listBox.Items.Add(new ListItem() { Text = "Item 5" });

            Content = this.listBox;
        }
    }
}
