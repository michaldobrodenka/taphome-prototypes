﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Threading;

namespace EtoApp4.Main
{
    public class TreeItem : ITreeGridItem, ITreeGridStore<ITreeGridItem>
    {
        public ITreeGridItem this[int index]
        {
            get
            {
                return this.Childs[index];
            }
        }

        public ObservableCollection<TreeItem> Childs;

        public void AddChildren(TreeItem item)
        {
            if (this.Childs == null)
            {
                this.Childs = new ObservableCollection<TreeItem>();
            }

            this.Childs.Add(item);
        }

        public bool Expanded { get; set; }

        public bool Expandable
        {
            get
            {
                return this.Count > 0;
            }
        }

        public ITreeGridItem Parent { get; set; }

        public string Text { get; set; }

        public int Count
        {
            get
            {
                if (this.Childs == null)
                    return 0;

                return this.Childs.Count;
            }
        }
    }

    partial class Panel2 : Form
    {
        private TreeGridView TreeGridView;

        private ObservableCollection<TreeItem> items = new ObservableCollection<TreeItem>();

        void InitializeComponent()
        {
            Title = "My Form";
            this.Size = new Size(200, 200);

            var parent = new TreeItem() { Expanded = false, Parent = null, Text = "Parent" };

            var child1 = new TreeItem() { Parent = parent, Text = "Child1" };
            var child12 = new TreeItem() { Parent = parent, Text = "Child12" };
            var child13 = new TreeItem() { Parent = parent, Text = "Child13" };
            var child14 = new TreeItem() { Parent = parent, Text = "Child14" };
            parent.AddChildren(child1);
            child1.AddChildren(child12);
            child1.AddChildren(child13);
            child1.AddChildren(child14);

            var child2 = new TreeItem() { Parent = parent, Text = "Child2" };
            var child22 = new TreeItem() { Parent = parent, Text = "Child22" };
            var child23 = new TreeItem() { Parent = parent, Text = "Child23" };
            var child24 = new TreeItem() { Parent = parent, Text = "Child24" };
            parent.AddChildren(child2);
            child2.AddChildren(child22);
            child2.AddChildren(child23);
            child2.AddChildren(child24);

         

            Task.Run(() =>
            {
                Thread.Sleep(2000);
                Application.Instance.Invoke(() =>
                {
                    var child25 = new TreeItem() { Parent = parent, Text = "Child25" };
                    child2.AddChildren(child25);
                    this.TreeGridView.ReloadItem(child25);

                    var child251 = new TreeItem() { Parent = parent, Text = "Child251" };

                    Task.Run(() =>
                    {
                        Thread.Sleep(2000);
                        Application.Instance.Invoke(() =>
                        {
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" }); parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });
                            parent.AddChildren(new TreeItem() { Parent = parent, Text = "Child1" });

                            child25.AddChildren(child251);
                            this.TreeGridView.ReloadItem(child251);
                            this.TreeGridView.ReloadItem(parent);

                        });
                    });
                });
            });

            items.Add(parent);
            items.Add(new TreeItem() { Parent = parent, Text = "Item" });
            items.Add(new TreeItem() { Parent = parent, Text = "Item1" });
            items.Add(new TreeItem() { Parent = parent, Text = "Item2" });
            items.Add(new TreeItem() { Parent = parent, Text = "Item3" });
            items.Add(new TreeItem() { Parent = parent, Text = "Item4" });
            items.Add(new TreeItem() { Parent = parent, Text = "Item5" });

            this.TreeGridView = new TreeGridView();
            this.TreeGridView.Columns.Add(new GridColumn() { AutoSize = true, HeaderText = "Text", DataCell = new TextBoxCell { Binding = Binding.Property<TreeItem, string>(r => r.Text) } });
            this.TreeGridView.Columns.Add(new GridColumn() { AutoSize = true, HeaderText = "Text", DataCell = new TextBoxCell { Binding = Binding.Property<TreeItem, string>(r => r.Text) } });
            //this.MainGrid.Columns.Add(new GridColumn() { AutoSize = true, HeaderText = "Name", DataCell = new TextBoxCell { Binding = Binding.Property<GenericDevice, string>(r => r.Name) } });
            //this.TreeGridView.DataStore = new TreeGridItemCollection(items);
            this.TreeGridView.DataStore = parent;

            this.Content = this.TreeGridView;
        }
    }
}
