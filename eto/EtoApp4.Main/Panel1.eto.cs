﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using System.Collections.ObjectModel;

namespace EtoApp4.Main
{
    class MyPoco
    {
        public string Text { get; set; }

        public bool Check { get; set; }
    }

    partial class Panel1 : Form
    {
        private GridView grid;

        void InitializeComponent()
        {
            Title = "My Form";
            this.Size = new Size(400, 400);

            var collection = new ObservableCollection<MyPoco>();
            collection.Add(new MyPoco { Text = "Row 1", Check = true });
            collection.Add(new MyPoco { Text = "Row 2", Check = false });

            grid = new GridView { DataStore = collection };

            grid.Columns.Add(new GridColumn
            {
                DataCell = new TextBoxCell { Binding = Binding.Property<MyPoco, string>(r => r.Text) },
                HeaderText = "Text"
            });

            grid.Columns.Add(new GridColumn
            {
                DataCell = new CheckBoxCell { Binding = Binding.Property<MyPoco, bool?>(r => r.Check) },
                HeaderText = "Check"
            });
            Content = grid;
        }
    }
}
